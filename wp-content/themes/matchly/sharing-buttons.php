									<div class="share-box-wrapper">
<div class="col-sm-3">
										<a class="share-button" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url(); ?>/?ref=<?php echo get_current_user_id(); ?>"><i class="fa fa-facebook-square"></i> Share on Facebook</a>
</div>	
<div class="col-sm-3">									
										<a class="share-button" href="https://twitter.com/home?status=Have%20you%20heard%20about%20OpenGo%20yet?%20You%20should%20check%20it%20out:%20<?php echo get_home_url(); ?>/?ref=<?php echo get_current_user_id(); ?>"><i class="fa fa-twitter-square"></i> Share on Twitter</a>
</div>	
<div class="col-sm-3">	
										<a class="share-button" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_home_url(); ?>/?ref=<?php echo get_current_user_id(); ?>&amp;title=OpenGo&amp;summary=Have%20you%20heard%20about%20OpenGo%20yet?%20You%20should%20check%20it%20out:%20<?php echo get_home_url(); ?>/?ref=<?php echo get_current_user_id(); ?>&amp;source=<?php echo get_home_url(); ?>/?ref=<?php echo get_current_user_id(); ?>"><i class="fa fa-linkedin-square"></i> Share on LinkedIn</a>
</div>	
<div class="col-sm-3">	
										<a  class="share-button" style="cursor:pointer" data-toggle="modal" data-target="#share-via-email" title="Share by Email"><i class="fa fa-envelope"></i> Share via Email</a>
</div>
									<!-- Modal - Share Via Email -->
									<div class="modal fade mail-function" id="share-via-email" tabindex="-1" role="dialog" aria-labelledby="ShareViaEmail" aria-hidden="true">
									  <div class="modal-dialog">
									    <div class="modal-content">
									    	<form class="form-horizontal" role="form" method="post" id="mailform" action="<?php echo get_stylesheet_directory_uri(); ?>/email-share-controller.php">
									    		<input type="hidden" name="redirecturl" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											        <h4 class="modal-title" id="myModalLabel">Share Via Email</h4>
											      </div>
											      <div class="modal-body">
											      	<div class="form-group">
														<label for="fromemail" class="col-sm-12 control-label">From:</label>
															<div class="col-sm-12">
															 <input type="email" value="<?php $current_user = wp_get_current_user(); echo $current_user->user_email; ?>" class="form-control" id="fromemail" name="fromemail" placeholder="e.g. john@example.com" maxlength="100" required/>
															</div>
													</div>

													<div class="form-group">
														<label for="toemail" class="col-sm-12 control-label">To:</label>
															<div class="col-sm-12">
															 <input type="email" value="" class="form-control" id="toemail" name="toemail" placeholder="e.g. j.doe@example.com" maxlength="100" required />
															</div>
													</div>
													
													<label for="message" class="col-sm-12 control-label" style="padding:10px 0;">Message:</label>
<textarea name="message" placeholder="Please enter your message.." required maxLength="1000">Hi,
Have you heard about OpenGo yet? You should check it out: 
<?php echo get_home_url(); ?>/?ref=<?php echo get_current_user_id(); ?>
</textarea>											        
											      </div>
											      <div class="modal-footer">
											        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											        <button type="submit" class="btn btn-default">Share</button>
											      </div>
									  		</form>
									    </div>
									  </div>
									</div>
									<!-- End of modal -->
</div>