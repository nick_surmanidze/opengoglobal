<?php
/**
 * Matchly functions and definitions
 *
 * @package Matchly
 */


if ( ! function_exists( 'matchly_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function matchly_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Matchly, use a find and replace
	 * to change 'matchly' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'matchly', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'matchly' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );


}
endif; // matchly_setup
add_action( 'after_setup_theme', 'matchly_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function matchly_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'matchly' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'matchly_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function matchly_scripts() {
	wp_enqueue_style( 'matchly-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'matchly_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Messaging functions
 */
require get_template_directory() . '/functions-messaging.php';

/**
 * Load aggregation functions
 */
require get_template_directory() . '/functions-aggregation.php';

/**
 * Load aggregation functions
 */
require get_template_directory() . '/functions-background-processes.php';

/**
 * Load notifications functions
 */
//require get_template_directory() . '/bulk-notifications/send-notifications.php';

// Hide admin bar
show_admin_bar( false );

// Limiting access to wo-admin to non admins
add_action( 'init', 'blockusers_init' );
function blockusers_init() {
if ( is_admin() && ! current_user_can( 'administrator' ) &&
! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
wp_redirect( home_url() );
exit;
}
}



register_nav_menu( 'home-footer', 'Home Footer' );

register_nav_menu( 'header', 'Header' );

register_nav_menu( 'footer-left', 'Footer Left' );

register_nav_menu( 'footer-right', 'Footer Right' );

register_nav_menu( 'mobile', 'Mobile' );



// Options Pages
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Matchly General Settings',
		'menu_title'	=> 'Manage Matchly',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Pre-defined Field Settings',
		'menu_title'	=> 'Edit Fields',
		'parent_slug'	=> 'theme-general-settings',
	));

		acf_add_options_sub_page(array(
		'page_title' 	=> 'Step Instructions',
		'menu_title'	=> 'Instructions',
		'parent_slug'	=> 'theme-general-settings',
	));

		acf_add_options_sub_page(array(
		'page_title' 	=> 'Search Form Fields',
		'menu_title'	=> 'Search Form',
		'parent_slug'	=> 'theme-general-settings',
	));

		acf_add_options_sub_page(array(
		'page_title' 	=> 'Aggregaton Options',
		'menu_title'	=> 'Job Aggregation',
		'parent_slug'	=> 'theme-general-settings',
	));

	
}


/////////////////////////////////////////////////////////////////////////////////
//registering custom post type - Profiles
/////////////////////////////////////////////////////////////////////////////////
 add_action( 'init', 'codex_profile_init' );
function codex_profile_init() {
	$labels = array(
		'name'               => _x( 'Profiles', 'post type general name', 'matchly' ),
		'singular_name'      => _x( 'Profile', 'post type singular name', 'matchly' ),
		'menu_name'          => _x( 'Profiles', 'admin menu', 'matchly' ),
		'name_admin_bar'     => _x( 'Profile', 'add new on admin bar', 'matchly' ),
		'add_new'            => _x( 'Add New', 'Profile', 'matchly' ),
		'add_new_item'       => __( 'Add a New Profile', 'matchly' ),
		'new_item'           => __( 'New Profile', 'matchly' ),
		'edit_item'          => __( 'Edit Profile', 'matchly' ),
		'view_item'          => __( 'View Profile', 'matchly' ),
		'all_items'          => __( 'All Profiles', 'matchly' ),
		'search_items'       => __( 'Search Profiles', 'matchly' ),
		'parent_item_colon'  => __( 'Parent Profiles:', 'matchly' ),
		'not_found'          => __( 'No Profiles found.', 'matchly' ),
		'not_found_in_trash' => __( 'No Profiles found in Trash.', 'matchly' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'profiles' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'author', 'custom-fields' )
	);

	register_post_type( 'profile', $args );
flush_rewrite_rules();


		$job_labels = array(
		'name'               => _x( 'Jobs', 'post type general name', 'matchly' ),
		'singular_name'      => _x( 'Job', 'post type singular name', 'matchly' ),
		'menu_name'          => _x( 'Jobs', 'admin menu', 'matchly' ),
		'name_admin_bar'     => _x( 'Job', 'add new on admin bar', 'matchly' ),
		'add_new'            => _x( 'Add New', 'Job', 'matchly' ),
		'add_new_item'       => __( 'Add a New Job', 'matchly' ),
		'new_item'           => __( 'New Job', 'matchly' ),
		'edit_item'          => __( 'Edit Job', 'matchly' ),
		'view_item'          => __( 'View Job', 'matchly' ),
		'all_items'          => __( 'All Jobs', 'matchly' ),
		'search_items'       => __( 'Search Jobs', 'matchly' ),
		'parent_item_colon'  => __( 'Parent Jobs:', 'matchly' ),
		'not_found'          => __( 'No Jobs found.', 'matchly' ),
		'not_found_in_trash' => __( 'No Jobs found in Trash.', 'matchly' )
	);

	$job_args = array(
		'labels'             => $job_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'jobs' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'author', 'custom-fields' )
	);

	register_post_type( 'job', $job_args );
	flush_rewrite_rules();


	$pool_labels = array(
		'name'               => _x( 'Pools', 'post type general name', 'matchly' ),
		'singular_name'      => _x( 'Pool', 'post type singular name', 'matchly' ),
		'menu_name'          => _x( 'Pools', 'admin menu', 'matchly' ),
		'name_admin_bar'     => _x( 'Pool', 'add new on admin bar', 'matchly' ),
		'add_new'            => _x( 'Add New', 'Pool', 'matchly' ),
		'add_new_item'       => __( 'Add a New Pool', 'matchly' ),
		'new_item'           => __( 'New Pool', 'matchly' ),
		'edit_item'          => __( 'Edit Pool', 'matchly' ),
		'view_item'          => __( 'View Pool', 'matchly' ),
		'all_items'          => __( 'All Pools', 'matchly' ),
		'search_items'       => __( 'Search Pool', 'matchly' ),
		'parent_item_colon'  => __( 'Parent Pool:', 'matchly' ),
		'not_found'          => __( 'No Pool found.', 'matchly' ),
		'not_found_in_trash' => __( 'No Pool found in Trash.', 'matchly' )
	);

	$pool_args = array(
		'labels'             => $pool_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'pools' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'author', 'custom-fields' )
	);

	register_post_type( 'pool', $pool_args );
	flush_rewrite_rules();

// messages
	$message_labels = array(
		'name'               => _x( 'Messages', 'post type general name', 'matchly' ),
		'singular_name'      => _x( 'Message', 'post type singular name', 'matchly' ),
		'menu_name'          => _x( 'Messages', 'admin menu', 'matchly' ),
		'name_admin_bar'     => _x( 'Message', 'add new on admin bar', 'matchly' ),
		'add_new'            => _x( 'Add New', 'Message', 'matchly' ),
		'add_new_item'       => __( 'Add a New Message', 'matchly' ),
		'new_item'           => __( 'New Message', 'matchly' ),
		'edit_item'          => __( 'Edit Message', 'matchly' ),
		'view_item'          => __( 'View Message', 'matchly' ),
		'all_items'          => __( 'All Messages', 'matchly' ),
		'search_items'       => __( 'Search Message', 'matchly' ),
		'parent_item_colon'  => __( 'Parent Message:', 'matchly' ),
		'not_found'          => __( 'No Message found.', 'matchly' ),
		'not_found_in_trash' => __( 'No Message found in Trash.', 'matchly' )
	);

	$message_args = array(
		'labels'             => $message_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'messages' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'custom-fields' )
	);

	register_post_type( 'message', $message_args );
	flush_rewrite_rules();
}


// function for finding out if any of the profiles has such meta

function isMetaUsed($whattosearch = "", $wheretosearch1 = "", $wheretosearch2 = '', $wheretosearch3 = '') {

	global $wpdb;

	if ((isset($whattosearch)) && (isset($wheretosearch1)) && ($wheretosearch2 !== '') && ($wheretosearch3 !== '')) {

		$param = strip_tags($whattosearch);
		$param = substr($param, 0, -2);		
		$qstring = "(metaalias1.meta_key = '".$wheretosearch1."' AND metaalias1.meta_value LIKE '%".$param."%') OR 
		(metaalias2.meta_key = '".$wheretosearch2."' AND metaalias2.meta_value LIKE '%".$param."%') OR 
		(metaalias3.meta_key = '".$wheretosearch3."' AND metaalias3.meta_value LIKE '%".$param."%') OR ";		
		$qstring = substr($qstring, 0, -3);
		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

		$queryfrom = ", $wpdb->postmeta AS metaalias1";
		$querywhere = "AND $wpdb->posts.ID = metaalias1.post_id ";

		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias2";
		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias2.post_id ";
		
		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias3";
		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias3.post_id ";

							$querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        ".$queryfrom."
    WHERE       
        $wpdb->posts.post_type = 'profile' 
        AND $wpdb->posts.post_status = 'publish'
        ".$querywhere."
        ".$queryparameters;

$pageposts = $wpdb->get_col($querystr, 1);
$profileidarray = array();
$profileidarray = $pageposts;

if (!empty($profileidarray)) {

	return 1;

} else {

	return 0;
}
		
} elseif ((isset($whattosearch)) && (isset($wheretosearch1)) && ($wheretosearch2 !== '') && ($wheretosearch3 == '')) {

		$param = strip_tags($whattosearch);
		$param = substr($param, 0, -2);	
		$qstring = "(metaalias1.meta_key = '".$wheretosearch1."' AND metaalias1.meta_value LIKE '%".$param."%') OR 
		(metaalias2.meta_key = '".$wheretosearch2."' AND metaalias2.meta_value LIKE '%".$param."%') OR ";		
		$qstring = substr($qstring, 0, -3);
		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

		$queryfrom = ", $wpdb->postmeta AS metaalias1";
		$querywhere = "AND $wpdb->posts.ID = metaalias1.post_id ";

		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias2";
		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias2.post_id ";

							$querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        ".$queryfrom."
    WHERE       
        $wpdb->posts.post_type = 'profile' 
        AND $wpdb->posts.post_status = 'publish'
        ".$querywhere."
        ".$queryparameters;

$pageposts = $wpdb->get_col($querystr, 1);
$profileidarray = array();
$profileidarray = $pageposts;

if (!empty($profileidarray)) {

	return 1;

} else {

	return 0;
}
		
} elseif ((isset($whattosearch)) && (isset($wheretosearch1))  && ($wheretosearch2 == '') && ($wheretosearch3 == '')) {

		$param = strip_tags($whattosearch);
		$param = substr($param, 0, -2);	
		$qstring = "(metaalias1.meta_key = '".$wheretosearch1."' AND metaalias1.meta_value LIKE '%".$param."%') OR ";		
		$qstring = substr($qstring, 0, -3);
		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

		$queryfrom = ", $wpdb->postmeta AS metaalias1";
		$querywhere = "AND $wpdb->posts.ID = metaalias1.post_id ";

					$querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        ".$queryfrom."
    WHERE       
        $wpdb->posts.post_type = 'profile' 
        AND $wpdb->posts.post_status = 'publish'
        ".$querywhere."
        ".$queryparameters;

$pageposts = $wpdb->get_col($querystr, 1);
$profileidarray = array();
$profileidarray = $pageposts;

if (!empty($profileidarray)) {

	return 1;

} else {

	return 0;
}

} else {

	return 0;
}
	
}





 function ec_paginate($numrows,$limit=10,$range=7, $baseurl='/search-results/') {
 
   $pagelinks = "<div class=\"pagelinks\">";
   $currpage = get_site_url() . $baseurl ;
   if ($numrows > $limit) {
      if(isset($_GET['pg'])){
         $mypage = $_GET['pg'];
      } else {
         $mypage = 1;
      }
      $currpage = get_site_url() . $baseurl;
      // $currpage = str_replace("&mypage=".$mypage,"",$currpage); // Use this for non-pretty permalink
      $currpage = str_replace("?pg=".$mypage,"",$currpage); // Use this for pretty permalink
      if($mypage == 1){
         $pagelinks .= "<span class=\"pageprevdead\">&laquo PREV </span>";
      }else{
         $pageprev = $mypage - 1;
         $pagelinks .= "<a class=\"pageprevlink\" href=\"" . $currpage .
               "?pg=" . $pageprev . "\">&laquo PREV </a>";
      }
      $numofpages = ceil($numrows / $limit);
      if ($range == "" or $range == 0) $range = 7;
      $lrange = max(1,$mypage-(($range-1)/2));
      $rrange = min($numofpages,$mypage+(($range-1)/2));
      if (($rrange - $lrange) < ($range - 1)) {
         if ($lrange == 1) {
            $rrange = min($lrange + ($range-1), $numofpages);
         } else {
            $lrange = max($rrange - ($range-1), 0);
         }
      }
      if ($lrange > 1) {
         $pagelinks .= "<a class=\"pagenumlink\" " .
            "href=\"" . $currpage . "?pg=" . 1 . 
            "\"> [1] </a>";
         if ($lrange > 2) $pagelinks .= "&nbsp;...&nbsp;";
      } else {
         $pagelinks .= "&nbsp;&nbsp;";
      }
      for($i = 1; $i <= $numofpages; $i++){
         if ($i == $mypage) {
            $pagelinks .= "<span class=\"pagenumon\"> $i </span>";
         } else {
            if ($lrange <= $i and $i <= $rrange) {
               $pagelinks .= "<a class=\"pagenumlink\" " .
                        "href=\"" . $currpage . "?pg=" . $i . 
                        "\"> " . $i . " </a>";
            }
         }
      }
      if ($rrange < $numofpages) {
         if ($rrange < $numofpages - 1) $pagelinks .= "&nbsp;...&nbsp;";
            $pagelinks .= "<a class=\"pagenumlink\" " .
               "href=\"" . $currpage . "?pg=" . $numofpages . 
               "\"> " . $numofpages . " </a>";
      } else {
         $pagelinks .= "&nbsp;&nbsp;";
      }
      if(($numrows - ($limit * $mypage)) > 0){
         $pagenext = $mypage + 1;
         $pagelinks .= "<a class=\"pagenextlink\" href=\"" . $currpage .
                    "?pg=" . $pagenext . "\"> NEXT &raquo;</a>";
      } else {
         $pagelinks .= "<span class=\"pagenextdead\"> NEXT &raquo;</span>";
      }
 
   }
$pagelinks .= "</div>";
return $pagelinks;
}



////////////////// Registering ajax function for reporting abuse //////////////////////////

add_action('wp_head','ajaxurl');

function ajaxurl() {
  ?>
  <script type="text/javascript">
  var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
  </script>
  <?php
}

add_action('wp_ajax_reportabuse', 'reportabuse');
add_action('wp_ajax_nopriv_reportabuse', 'reportabuse');


function reportabuse() {

	$abuseurl = $_POST['url'];
	$senderID = $_POST['sender'];
	$adminmail = strip_tags(get_field('abuse_report_email', 'option'));

	$message = "Hi Ian, You received an abuse report concerning url: " . $abuseurl . ". Abuse report was submitted by user with ID # ". $senderID;

	$headers = "From: OpenGlobal Message <".$senderemail.">" . "\r\n";
   	wp_mail($adminmail, 'Abuse Report', $message, $headers);

	print_r("Report Received from" .$senderID . "and the url is:" . $abuseurl);

	die();
}



add_action('wp_ajax_deleteprofile', 'deleteprofile');
add_action('wp_ajax_nopriv_deleteprofile', 'deleteprofile');


function deleteprofile() {
 if (isset($_POST['profileID'])){

$profileid = $_POST['profileID'];

$user_ID = get_current_user_id();

wp_delete_post($profileid);

update_field( "user_step", "", 'user_'.$user_ID );	
update_field( "user_role", "", 'user_'.$user_ID );	

print_r("Profile Deleted");

 } else {}


	die();
}


function matchly_numeric_posts_nav() {

	// if( is_singular() )
	// 	return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}





function merge_search_fields($profile_id) {

//this function will merge the fields into additonal meta fields for searching purposes
//Each of sucm meta fields will have "search_" prefix


// This equals current + previous 1 + previous 2
$search_industry_experience = '';
$search_industry_experience .= sanitize_text_field(get_post_meta($profile_id, 'current_job_industry', true)) . ", ";
$search_industry_experience .= sanitize_text_field(get_post_meta($profile_id, 'previous_experience_industry1', true)). ", ";
$search_industry_experience .= sanitize_text_field(get_post_meta($profile_id, 'previous_experience_industry2', true));

update_post_meta($profile_id, "search_industry_experience", $search_industry_experience);


// This equals current + previous 1 + previous 2
$search_company_experience = '';
$search_company_experience .= sanitize_text_field(get_post_meta($profile_id, 'current_job_organisation_name', true)) . ", ";
$search_company_experience .= sanitize_text_field(get_post_meta($profile_id, 'previous_experience_organisation_name1', true)). ", ";
$search_company_experience .= sanitize_text_field(get_post_meta($profile_id, 'previous_experience_organisation_name2', true));

update_post_meta($profile_id, "search_company_experience", $search_company_experience);


// This equals current + previous 1 + previous 2
$search_role_experience = '';
$search_role_experience .= sanitize_text_field(get_post_meta($profile_id, 'current_job_job_function', true)) . ", ";
$search_role_experience .= sanitize_text_field(get_post_meta($profile_id, 'previous_experience_job_function1', true)). ", ";
$search_role_experience .= sanitize_text_field(get_post_meta($profile_id, 'previous_experience_job_function2', true));

update_post_meta($profile_id, "search_role_experience", $search_role_experience);


// This equals Prev 1 + 2 + 3
$search_institution = '';
$search_institution .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_institution1', true)) . ", ";
$search_institution .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_institution2', true)). ", ";
$search_institution .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_institution3', true));

update_post_meta($profile_id, "search_institution", $search_institution);

// This equals Prev 1 + 2 + 3
$search_qualification_type = '';
$search_qualification_type .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_qualification_type1', true)) . ", ";
$search_qualification_type .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_qualification_type2', true)). ", ";
$search_qualification_type .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_qualification_type3', true));

update_post_meta($profile_id, "search_qualification_type", $search_qualification_type);


// This equals Prev 1 + 2 + 3
$search_course_name = '';
$search_course_name .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_course_name1', true)) . ", ";
$search_course_name .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_course_name2', true)). ", ";
$search_course_name .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_course_name3', true));

update_post_meta($profile_id, "search_course_name", $search_course_name);


// This equals Prev 1 + 2 + 3
$search_grade = '';
$search_grade .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_grade_attained1', true)) . ", ";
$search_grade .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_grade_attained2', true)). ", ";
$search_grade .= sanitize_text_field(get_post_meta($profile_id, 'qualifications_and_skills_grade_attained3', true));

update_post_meta($profile_id, "search_grade", $search_grade);

}


function calculate_profile_progress($profile_id) {

	$progress = 0;

	if (strlen(get_field("ideal_job_industry", $profile_id)) > 2) {

		$progress = $progress + 20;
	}

	if (strlen(get_field("current_job_industry", $profile_id)) > 2){

		$progress = $progress + 20;
	}

	if (strlen(get_field("qualifications_and_skills_institution1", $profile_id)) > 2){

		$progress = $progress + 20;
	}

	if (strlen(get_field("qualifications_and_skills_special_skills1", $profile_id)) > 2){

		$progress = $progress + 20;
	}

	if (strlen(get_field("sales_pitch_point_of_difference", $profile_id)) > 2){

		$progress = $progress + 20;
	}


	update_post_meta($profile_id, "profile_progress", $progress);

}

function get_profile_progress($profile_id) {

	$progress = '';

	$value = get_field('profile_progress', $profile_id);

	if ($value == 0) {
		$progress = 'Incomplete';
	}

	if ($value == 20) {
		$progress = 'Incomplete';
	}

	if ($value == 40) {
		$progress = 'Weak';
	}

	if ($value == 60) {
		$progress = 'Average';
	}

	if ($value == 80) {
		$progress = 'Good';
	}

	if ($value == 100) {
		$progress = 'Perfect';
	}

	return $progress;
}




////////////////////////////////////////////////////////////////////////
// Create Job via ajax
////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_create_job', 'create_job');
add_action('wp_ajax_nopriv_create_job', 'create_job');


function create_job() {

if(isset($_POST['job-title'])) {

	// getting the values from ajax request
	$job_title 			= sanitize_text_field($_POST['job-title']);
	$employer 			= sanitize_text_field($_POST['employer']);
	$job_description 	= strip_tags($_POST['job-description']);

	$job_requirements 	= $_POST['job-requirements'];
	$job_requirements_array = array();
	parse_str($job_requirements, $job_requirements_array);

	$job_requirements_array_serialized = serialize($job_requirements_array);

	// create a post (job)
	$job_args = array(
	  'post_title'    => $job_title,
	  'post_status'   => 'publish',
	  'post_type'     => 'job'
	);

	// Insert the profile into the database
	$jobID = wp_insert_post( $job_args );

	if($jobID) {

	// if job was successfully created we need to add custom meta values
	update_post_meta($jobID, "job_employer", $employer);
	update_post_meta($jobID, "job_description", $job_description);
	update_post_meta($jobID, "job_requirements", $job_requirements_array_serialized);

	$profiles = get_profiles($job_requirements_array_serialized);
	$serialized_profiles = serialize($profiles['profiles']);
	$serialized_profile_emails = serialize($profiles['emails']);
	$number_of_matches = $profiles['total'];

	update_post_meta($jobID, "job_matched_profiles", $serialized_profiles);
	update_post_meta($jobID, "job_matched_profile_emails", $serialized_profile_emails);
	update_post_meta($jobID, "job_number_of_matches", $number_of_matches);

	$today = date('d-m-Y');

	update_post_meta($jobID, "job_last_update", $today);

	update_post_meta($jobID, "job_registered_interest", serialize(array()));
	update_post_meta($jobID, "job_to_be_reviewed", serialize(array()));
	update_post_meta($jobID, "job_candidates_contacted", serialize(array()));
	update_post_meta($jobID, "job_candidates_rejected", serialize(array()));

	// Now we need to add 17 search fields starting with "job_search_"

	$if_not_set = array("NothingSelected");

	if(isset($job_requirements_array['idealindustry'])) {
		update_post_meta($jobID, "job_search_idealindustry", serialize($job_requirements_array['idealindustry']));
	} else {
		update_post_meta($jobID, "job_search_idealindustry", serialize($if_not_set));
	}

	if(isset($job_requirements_array['idealseniority'])) {
		update_post_meta($jobID, "job_search_idealseniority", serialize($job_requirements_array['idealseniority']));
	} else {
		update_post_meta($jobID, "job_search_idealseniority", serialize($if_not_set));
	}

	if(isset($job_requirements_array['idealjobtitle'])) {
		update_post_meta($jobID, "job_search_idealjobtitle", serialize($job_requirements_array['idealjobtitle']));
	} else {
		update_post_meta($jobID, "job_search_idealjobtitle", serialize($if_not_set));
	}

	if(isset($job_requirements_array['idealjoblocation'])) {
		update_post_meta($jobID, "job_search_idealjoblocation", serialize($job_requirements_array['idealjoblocation']));
	} else {
		update_post_meta($jobID, "job_search_idealjoblocation", serialize($if_not_set));
	}

	if(isset($job_requirements_array['idealjobbasicsalary'])) {
		update_post_meta($jobID, "job_search_idealjobbasicsalary", serialize($job_requirements_array['idealjobbasicsalary']));
	} else {
		update_post_meta($jobID, "job_search_idealjobbasicsalary", serialize($if_not_set));
	}

	if(isset($job_requirements_array['idealjobcompanytype'])) {
		update_post_meta($jobID, "job_search_idealjobcompanytype", serialize($job_requirements_array['idealjobcompanytype']));
	} else {
		update_post_meta($jobID, "job_search_idealjobcompanytype", serialize($if_not_set));
	}

	if(isset($job_requirements_array['idealjobcontract'])) {
		update_post_meta($jobID, "job_search_idealjobcontract", serialize($job_requirements_array['idealjobcontract']));
	} else {
		update_post_meta($jobID, "job_search_idealjobcontract", serialize($if_not_set));
	}

	if(isset($job_requirements_array['currentjobindustry'])) {
		update_post_meta($jobID, "job_search_currentjobindustry", serialize($job_requirements_array['currentjobindustry']));
	} else {
		update_post_meta($jobID, "job_search_currentjobindustry", serialize($if_not_set));
	}

	if(isset($job_requirements_array['currentemployer'])) {
		update_post_meta($jobID, "job_search_currentemployer", serialize($job_requirements_array['currentemployer']));
	} else {
		update_post_meta($jobID, "job_search_currentemployer", serialize($if_not_set));
	}

	if(isset($job_requirements_array['currentjobtitle'])) {
		update_post_meta($jobID, "job_search_currentjobtitle", serialize($job_requirements_array['currentjobtitle']));
	} else {
		update_post_meta($jobID, "job_search_currentjobtitle", serialize($if_not_set));
	}

	if(isset($job_requirements_array['currentjobseniority'])) {
		update_post_meta($jobID, "job_search_currentjobseniority", serialize($job_requirements_array['currentjobseniority']));
	} else {
		update_post_meta($jobID, "job_search_currentjobseniority", serialize($if_not_set));
	}

	if(isset($job_requirements_array['currentsalary'])) {
		update_post_meta($jobID, "job_search_currentsalary", serialize($job_requirements_array['currentsalary']));
	} else {
		update_post_meta($jobID, "job_search_currentsalary", serialize($if_not_set));
	}

	if(isset($job_requirements_array['currentcontract'])) {
		update_post_meta($jobID, "job_search_currentcontract", serialize($job_requirements_array['currentcontract']));
	} else {
		update_post_meta($jobID, "job_search_currentcontract", serialize($if_not_set));
	}

	if(isset($job_requirements_array['institution'])) {
		update_post_meta($jobID, "job_search_institution", serialize($job_requirements_array['institution']));
	} else {
		update_post_meta($jobID, "job_search_institution", serialize($if_not_set));
	}

	if(isset($job_requirements_array['qualification'])) {
		update_post_meta($jobID, "job_search_qualification", serialize($job_requirements_array['qualification']));
	} else {
		update_post_meta($jobID, "job_search_qualification", serialize($if_not_set));
	}

	if(isset($job_requirements_array['coursename'])) {
		update_post_meta($jobID, "job_search_coursename", serialize($job_requirements_array['coursename']));
	} else {
		update_post_meta($jobID, "job_search_coursename", serialize($if_not_set));
	}

	if(isset($job_requirements_array['gradeattained'])) {
		update_post_meta($jobID, "job_search_gradeattained", serialize($job_requirements_array['gradeattained']));
	} else {
		update_post_meta($jobID, "job_search_gradeattained", serialize($if_not_set));
	}
	

	}

}



$data_array = $profiles['profiles'];

$current_user = wp_get_current_user();
$csv = "JOB ID = ".$jobID.",JOB AUTHOR = " . $current_user->user_email ."," .$current_user->display_name . ", JOB URL = " . get_permalink($jobID). "\n";
$csv .= "Profile ID,Date,First Name,Last Name,User Email \n";//Column headers
foreach ($data_array as $csv_profile_id){
    $csv.= $csv_profile_id.','.$today.','.sanitize_text_field(get_post_meta($csv_profile_id, "about_you_first_name", true)).','.sanitize_text_field(get_post_meta($csv_profile_id, "about_you_surname", true)).','.sanitize_text_field(get_post_meta($csv_profile_id, "about_you_email_address", true))."\n"; //Append data to csv
    }

// Now it is time to generate the csv file
$csv_upload_path = get_home_path().'wp-content/uploads/job-csv/';
//create directory if it does not exist (otherwise no files will be created)
if (!is_dir($csv_upload_path)) {
    @mkdir($csv_upload_path);
}
// Generate path and file name which will be date + title of the job
$csv_handler = fopen ($csv_upload_path . basename( get_permalink($jobID) ) . '.csv','w');
// if file stream was opened let's write csv data inside and close file stream
if($csv_handler) {
fwrite ($csv_handler, $csv);
fclose ($csv_handler);
}

$permalink = get_permalink( $jobID);

print_r($permalink);
	die();
}


////////////////////////////////////////////////////////////////////////
// function for search
////////////////////////////////////////////////////////////////////////

function get_profiles($serialized_query) {

	$profiles = array();

	$query = array();

	$query = unserialize($serialized_query);


	global $wpdb;

$querystr = "
   SELECT DISTINCT
      $wpdb->posts.ID      
    FROM
        $wpdb->posts,
        $wpdb->postmeta AS refsort
    WHERE       
        $wpdb->posts.post_type = 'profile' 
        AND $wpdb->posts.post_status = 'publish'
        AND $wpdb->posts.ID = refsort.post_id
        AND refsort.meta_key = 'profile_progress' 
    ORDER BY 
        refsort.meta_value DESC,
        $wpdb->posts.ID DESC
		";
		 
$pageposts = $wpdb->get_col($querystr, 0);

	$profileidarray1 = $pageposts;
	$profileidarray2 = $pageposts;
	$profileidarray3 = $pageposts;
	$profileidarray4 = $pageposts;
	$profileidarray5 = $pageposts;
	$profileidarray6 = $pageposts;
	$profileidarray7 = $pageposts;
	$profileidarray8 = $pageposts;
	$profileidarray9 = $pageposts;
	$profileidarray10 = $pageposts;
	$profileidarray11 = $pageposts;
	$profileidarray12 = $pageposts;
	$profileidarray13 = $pageposts;
	$profileidarray14 = $pageposts;
	$profileidarray15 = $pageposts;
	$profileidarray16 = $pageposts;
	$profileidarray17 = $pageposts;



if(isset($query['idealindustry'])) {

$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $query['idealindustry'];

$qstring = $qstring . "(idealindustry.meta_key = 'ideal_job_industry' AND idealindustry.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealindustry.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealindustry";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealindustry.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray1 = array();
$profileidarray1 = $pageposts;

}





if(isset($query['idealseniority'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['idealseniority'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealseniority.meta_key = 'ideal_job_seniority' AND idealseniority.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealseniority";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealseniority.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray2 = array();
$profileidarray2 = $pageposts;
}



if(isset($query['idealjobtitle'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['idealjobtitle'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealjobtitle.meta_key = 'ideal_job_job_function' AND idealjobtitle.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealjobtitle";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealjobtitle.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray3 = array();
$profileidarray3 = $pageposts;
}



if(isset($query['idealjoblocation'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $query['idealjoblocation'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(ideallocation.meta_key = 'ideal_job_location' AND ideallocation.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS ideallocation";
$querywhere = $querywhere . "AND $wpdb->posts.ID = ideallocation.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray4 = array();
$profileidarray4 = $pageposts;

}



if(isset($query['idealjobbasicsalary'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $query['idealjobbasicsalary'];

foreach ($qarray as $param) {

$param = str_replace("< ", "", $param);

$param = str_replace("> ", "", $param);

$param = $wpdb->esc_like( sanitize_text_field($param));

	if ($param == "30.000") {

		$qstring = $qstring . "(idealpay.meta_key = 'ideal_job_basic_salary' AND idealpay.meta_value LIKE '%".$param."%' AND idealpay.meta_value NOT LIKE '%30.000-%') OR ";	
	} 
	else 
	{
		$qstring = $qstring . "(idealpay.meta_key = 'ideal_job_basic_salary' AND idealpay.meta_value LIKE '%".$param."%') OR ";
	}



}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealpay";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealpay.post_id "; 

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray5 = array();
$profileidarray5 = $pageposts;
}





if(isset($query['idealjobcompanytype'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['idealjobcompanytype'];
$qstring = $qstring . "(idealcompanytype.meta_key = 'ideal_job_company_type' AND idealcompanytype.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(idealcompanytype.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealcompanytype";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealcompanytype.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray6 = array();
$profileidarray6 = $pageposts;
}





if(isset($query['idealjobcontract'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['idealjobcontract'];
$qstring = $qstring . "(idealcontract.meta_key = 'ideal_job_contract_type' AND idealcontract.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(idealcontract.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealcontract";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealcontract.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray7 = array();
$profileidarray7 = $pageposts;
}



if(isset($query['currentjobindustry'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $query['currentjobindustry'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentindustry.meta_key = 'search_industry_experience' AND currentindustry.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentindustry";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentindustry.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray8 = array();
$profileidarray8 = $pageposts;
}





if(isset($query['currentemployer'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['currentemployer'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(currentemployer.meta_key = 'search_company_experience' AND currentemployer.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentemployer";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentemployer.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray9 = array();
$profileidarray9 = $pageposts;
}



if(isset($query['currentjobtitle'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['currentjobtitle'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentjobtitle.meta_key = 'search_role_experience' AND currentjobtitle.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentjobtitle";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentjobtitle.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray10 = array();
$profileidarray10 = $pageposts;
}





if(isset($query['currentjobseniority'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['currentjobseniority'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(currentseniority.meta_key = 'current_job_seniority' AND currentseniority.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentseniority";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentseniority.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray11 = array();
$profileidarray11 = $pageposts;
}



if(isset($query['currentsalary'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $query['currentsalary'];

foreach ($qarray as $param) {

$param = str_replace("< ", "", $param);
$param = str_replace("> ", "", $param);
$param = $wpdb->esc_like( sanitize_text_field($param));

	if ($param == "30.000") {
		$qstring = $qstring . "(currentpay.meta_key = 'current_job_basic_salary' AND currentpay.meta_value LIKE '%".$param."%' AND currentpay.meta_value NOT LIKE '%30.000-%') OR ";	
	} 
	else 
	{
		$qstring = $qstring . "(currentpay.meta_key = 'current_job_basic_salary' AND currentpay.meta_value LIKE '%".$param."%') OR ";
	}

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentpay";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentpay.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray12 = array();
$profileidarray12 = $pageposts;
}





if(isset($query['currentcontract'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $query['currentcontract'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentcontract.meta_key = 'current_job_type_of_contract' AND currentcontract.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentcontract";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentcontract.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray13 = array();
$profileidarray13 = $pageposts;
}



if(isset($query['institution'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['institution'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));	
$qstring = $qstring . "(institution.meta_key = 'search_institution' AND institution.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS institution";
$querywhere = $querywhere . "AND $wpdb->posts.ID = institution.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray14 = array();
$profileidarray14 = $pageposts;
}



if(isset($query['qualification'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $query['qualification'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(qualification.meta_key = 'search_qualification_type' AND qualification.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS qualification";
$querywhere = $querywhere . "AND $wpdb->posts.ID = qualification.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray15 = array();
$profileidarray15 = $pageposts;
}



//course name

if(isset($query['coursename'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $query['coursename'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(coursename.meta_key = 'search_course_name' AND coursename.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3); 
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS coursename";
$querywhere = $querywhere . "AND $wpdb->posts.ID = coursename.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray16 = array();
$profileidarray16 = $pageposts;
}

//grade

if(isset($query['gradeattained'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $query['gradeattained'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(gradeattained.meta_key = 'search_grade' AND gradeattained.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS gradeattained";
$querywhere = $querywhere . "AND $wpdb->posts.ID = gradeattained.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray17 = array();
$profileidarray17 = $pageposts;
}

$profileidarray = array();

$profileidarray = array_intersect(
	$profileidarray1,
	$profileidarray2,
	$profileidarray3,
	$profileidarray4,
	$profileidarray5,
	$profileidarray6,
	$profileidarray7,
	$profileidarray8,
	$profileidarray9,
	$profileidarray10,
	$profileidarray11,
	$profileidarray12,
	$profileidarray13,
	$profileidarray14,
	$profileidarray15,
	$profileidarray16,
	$profileidarray17
	);

$emails_array = array();

foreach($profileidarray as $profile_id) {
	$email = sanitize_text_field(get_field("about_you_email_address", $profile_id));
	array_push($emails_array, $email);
}

$number_of_results = count($profileidarray); 

 $profiles = array(
 	'total' => $number_of_results,
 	'profiles' => $profileidarray,
 	'emails' => $emails_array
 	);


	return $profiles;
}



////////////////////////////////////////////////////////////////////////////
// update all jobs 
////////////////////////////////////////////////////////////////////////////

#this function will be running too long so we will need to run it as a background process

function update_all_jobs() {

	global $wpdb;

	$querystr = "
	   SELECT DISTINCT
	      $wpdb->posts.ID      
	    FROM
	        $wpdb->posts
	    WHERE       
	        $wpdb->posts.post_type = 'job' 
	        AND $wpdb->posts.post_status = 'publish'
			";

	$pageposts = $wpdb->get_col($querystr, 0);

	foreach ($pageposts as $jobID) {

		$job_requirements_array_serialized = get_post_meta($jobID, "job_requirements", true);
		// getting the profiles (conducting search)
		$profiles = get_profiles($job_requirements_array_serialized);

		//getting current profiles
		$old_profiles_array = array();
		$old_profiles_array = unserialize(get_post_meta($jobID, "job_matched_profiles", true));

		//getting new profiles
		$new_profiles_array = array();
		$new_profiles_array = $profiles['profiles'];

		//getting the difference
		$added_difference = array();
		$added_difference = array_diff($new_profiles_array, $old_profiles_array);

		$serialized_profiles = serialize($profiles['profiles']);
		$serialized_profile_emails = serialize($profiles['emails']);
		$number_of_matches = $profiles['total'];

		update_post_meta($jobID, "job_matched_profiles", $serialized_profiles);
		update_post_meta($jobID, "job_matched_profile_emails", $serialized_profile_emails);
		update_post_meta($jobID, "job_number_of_matches", $number_of_matches);

		$today = date('d-m-Y');
		update_post_meta($jobID, "job_last_update", $today);

		//// Prepend New Lines to csv ////

			if ( !function_exists( 'get_home_path' ) )
				require_once( dirname(__FILE__) . '/../../../wp-admin/includes/file.php' );


		// Now it is time to generate the csv file
		$csv_upload_path = get_home_path().'wp-content/uploads/job-csv/';

		// Generate path and file name which will be date + title of the job
		$csv_handler = fopen ($csv_upload_path . basename( get_permalink($jobID) ) . '.csv','a');

		foreach ($added_difference as $csv_profile_id){
			$line = array();
		    
		    array_push($line, $csv_profile_id);
		    array_push($line, $today);
		    array_push($line, sanitize_text_field(get_post_meta($csv_profile_id, "about_you_first_name", true)));
		    array_push($line, sanitize_text_field(get_post_meta($csv_profile_id, "about_you_surname", true)));
		    array_push($line, sanitize_text_field(get_post_meta($csv_profile_id, "about_you_email_address", true)));

			if($csv_handler) {
			fputcsv($csv_handler, $line);
			}

		    }

		// if file stream was opened let's write csv data inside and close file stream
		if($csv_handler) {
		fclose ($csv_handler);
		}

	}

	return "all jobs updated";
}


function update_single_job_data($jobID) {

	$job_requirements_array_serialized = get_post_meta($jobID, "job_requirements", true);
	// getting the profiles (conducting search)
	$profiles = get_profiles($job_requirements_array_serialized);

	//getting current profiles
	$old_profiles_array = array();
	$old_profiles_array = unserialize(get_post_meta($jobID, "job_matched_profiles", true));

	//getting new profiles
	$new_profiles_array = array();
	$new_profiles_array = $profiles['profiles'];

	//getting the difference
	$added_difference = array();
	$added_difference = array_diff($new_profiles_array, $old_profiles_array);

	$serialized_profiles = serialize($profiles['profiles']);
	$serialized_profile_emails = serialize($profiles['emails']);
	$number_of_matches = $profiles['total'];

	update_post_meta($jobID, "job_matched_profiles", $serialized_profiles);
	update_post_meta($jobID, "job_matched_profile_emails", $serialized_profile_emails);
	update_post_meta($jobID, "job_number_of_matches", $number_of_matches);

	$today = date('d-m-Y');
	update_post_meta($jobID, "job_last_update", $today);

	//// Prepend New Lines to csv ////

		if ( !function_exists( 'get_home_path' ) )
			require_once( dirname(__FILE__) . '/../../../wp-admin/includes/file.php' );


	// Now it is time to generate the csv file
	$csv_upload_path = get_home_path().'wp-content/uploads/job-csv/';

	// Generate path and file name which will be date + title of the job
	$csv_handler = fopen ($csv_upload_path . basename( get_permalink($jobID) ) . '.csv','a');

	foreach ($added_difference as $csv_profile_id){

		$line = array();
	    
	    array_push($line, $csv_profile_id);
	    array_push($line, $today);
	    array_push($line, sanitize_text_field(get_post_meta($csv_profile_id, "about_you_first_name", true)));
	    array_push($line, sanitize_text_field(get_post_meta($csv_profile_id, "about_you_surname", true)));
	    array_push($line, sanitize_text_field(get_post_meta($csv_profile_id, "about_you_email_address", true)));

		if($csv_handler) {
		fputcsv($csv_handler, $line);
		}

	    }

	// if file stream was opened let's write csv data inside and close file stream
	if($csv_handler) {
	fclose ($csv_handler);
	}
	
}


///////////////////////////////////////////////////////////////
// Generate serialized array for talent profile to identify matches later.
///////////////////////////////////////////////////////////////

#This function should be called every time when anyone edits his profile so new meta value is generated

function generate_talent_fields_array($profile_id) {

	# Create an array (which will be ultimately serialized and saved)
	# Step1: just declare the array with proper structure
	$talent_job_search_array = array(
		'idealindustry'   		 => array(),
		'idealseniority' 		 => array(),
		'idealjobtitle'   		 => array(),
		'idealjoblocation'		 => array(),
		'idealjobbasicsalary' 	 => array(),
		'idealjobcompanytype'    => array(),
		'idealjobcontract'       => array(),

		'currentjobindustry'     => array(),
		'currentemployer'        => array(),
		'currentjobtitle'        => array(),
		'currentjobseniority'    => array(),
		'currentsalary' 		 => array(),
		'currentcontract'		 => array(),

		'institution'			 => array(),
		'qualification'			 => array(),
		'coursename' 			 => array(),
		'gradeattained' 		 => array()
		);

	# Now we need to add values from the profile to each of those sub-arrays

	$if_nothing_selected = "NothingSelected";

	#1
	$ideal_industry_array = array();

	if(get_field('ideal_job_industry', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('ideal_job_industry', $profile_id));

		foreach($field_to_array as $field):

			array_push($ideal_industry_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($ideal_industry_array, sanitize_text_field($if_nothing_selected)); }


	#2
	$ideal_seniority_array = array();

	if(get_field('ideal_job_seniority', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('ideal_job_seniority', $profile_id));

		foreach($field_to_array as $field):

			array_push($ideal_seniority_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($ideal_seniority_array, sanitize_text_field($if_nothing_selected)); }

	#3
	$ideal_job_title_array = array();

	if(get_field('ideal_job_job_function', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('ideal_job_job_function', $profile_id));

		foreach($field_to_array as $field):

			array_push($ideal_job_title_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($ideal_job_title_array, sanitize_text_field($if_nothing_selected)); }

	#4
	$ideal_job_location_array = array();

	if(get_field('ideal_job_location', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('ideal_job_location', $profile_id));

		foreach($field_to_array as $field):

			array_push($ideal_job_location_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($ideal_job_location_array, sanitize_text_field($if_nothing_selected)); }

	#5
	$ideal_job_salary_array = array();

	if(get_field('ideal_job_basic_salary', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('ideal_job_basic_salary', $profile_id));

		foreach($field_to_array as $field):

			array_push($ideal_job_salary_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($ideal_job_salary_array, sanitize_text_field($if_nothing_selected)); }

	#6
	$ideal_company_type_array = array();

	if(get_field('ideal_job_company_type', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('ideal_job_company_type', $profile_id));

		foreach($field_to_array as $field):

			array_push($ideal_company_type_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($ideal_company_type_array, sanitize_text_field($if_nothing_selected)); }


	#7
	$ideal_contract_array = array();

	if(get_field('ideal_job_contract_type', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('ideal_job_contract_type', $profile_id));

		foreach($field_to_array as $field):

			array_push($ideal_contract_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($ideal_contract_array, sanitize_text_field($if_nothing_selected)); }


	#8
	$current_job_industry_array = array();

	if(get_field('current_job_industry', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('current_job_industry', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_industry_array, sanitize_text_field($field));

		endforeach;
	}  else { array_push($current_job_industry_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('previous_experience_industry1', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('previous_experience_industry1', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_industry_array, sanitize_text_field($field));

		endforeach;
	}  else { array_push($current_job_industry_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('previous_experience_industry2', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('previous_experience_industry2', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_industry_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_job_industry_array, sanitize_text_field($if_nothing_selected)); }


	#9
	$current_employer_array = array();

	if(get_field('current_job_organisation_name', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('current_job_organisation_name', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_employer_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_employer_array, sanitize_text_field($if_nothing_selected)); }

	if(get_field('previous_experience_organisation_name1', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('previous_experience_organisation_name1', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_employer_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_employer_array, sanitize_text_field($if_nothing_selected)); }

	if(get_field('previous_experience_organisation_name2', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('previous_experience_organisation_name2', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_employer_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_employer_array, sanitize_text_field($if_nothing_selected)); }

	#10
	$current_job_title_array = array();

	if(get_field('current_job_job_function', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('current_job_job_function', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_title_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_job_title_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('previous_experience_job_function1', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('previous_experience_job_function1', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_title_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_job_title_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('previous_experience_job_function2', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('previous_experience_job_function2', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_title_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_job_title_array, sanitize_text_field($if_nothing_selected)); }


	#11
	$current_job_seniority_array = array();

	if(get_field('current_job_seniority', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('current_job_seniority', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_seniority_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_job_seniority_array, sanitize_text_field($if_nothing_selected)); }


	#12
	$current_job_salary_array = array();

	if(get_field('current_job_basic_salary', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('current_job_basic_salary', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_salary_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_job_salary_array, sanitize_text_field($if_nothing_selected)); }

	#13
	$current_job_contract_array = array();

	if(get_field('current_job_type_of_contract', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('current_job_type_of_contract', $profile_id));

		foreach($field_to_array as $field):

			array_push($current_job_contract_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($current_job_contract_array, sanitize_text_field($if_nothing_selected)); }


	#14
	$institution_array = array();

	if(get_field('qualifications_and_skills_institution1', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_institution1', $profile_id));

		foreach($field_to_array as $field):

			array_push($institution_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($institution_array, sanitize_text_field($if_nothing_selected)); }

	if(get_field('qualifications_and_skills_institution2', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_institution2', $profile_id));

		foreach($field_to_array as $field):

			array_push($institution_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($institution_array, sanitize_text_field($if_nothing_selected)); }

	if(get_field('qualifications_and_skills_institution3', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_institution3', $profile_id));

		foreach($field_to_array as $field):

			array_push($institution_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($institution_array, sanitize_text_field($if_nothing_selected)); }


	#15
	$qualification_array = array();

	if(get_field('qualifications_and_skills_qualification_type1', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_qualification_type1', $profile_id));

		foreach($field_to_array as $field):

			array_push($qualification_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($qualification_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('qualifications_and_skills_qualification_type2', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_qualification_type2', $profile_id));

		foreach($field_to_array as $field):

			array_push($qualification_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($qualification_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('qualifications_and_skills_qualification_type3', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_qualification_type3', $profile_id));

		foreach($field_to_array as $field):

			array_push($qualification_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($qualification_array, sanitize_text_field($if_nothing_selected)); }


	#16
	$coursename_array = array();

	if(get_field('qualifications_and_skills_course_name1', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_course_name1', $profile_id));

		foreach($field_to_array as $field):

			array_push($coursename_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($coursename_array, sanitize_text_field($if_nothing_selected)); }

	if(get_field('qualifications_and_skills_course_name2', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_course_name2', $profile_id));

		foreach($field_to_array as $field):

			array_push($coursename_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($coursename_array, sanitize_text_field($if_nothing_selected)); }

	if(get_field('qualifications_and_skills_course_name3', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_course_name3', $profile_id));

		foreach($field_to_array as $field):

			array_push($coursename_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($coursename_array, sanitize_text_field($if_nothing_selected)); }

	#17

	$grade_array = array();


	if(get_field('qualifications_and_skills_grade_attained1', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_grade_attained1', $profile_id));

		foreach($field_to_array as $field):

			array_push($grade_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($grade_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('qualifications_and_skills_grade_attained2', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_grade_attained2', $profile_id));

		foreach($field_to_array as $field):

			array_push($grade_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($grade_array, sanitize_text_field($if_nothing_selected)); }


	if(get_field('qualifications_and_skills_grade_attained3', $profile_id)) {

		$field_to_array = array();
		$field_to_array = explode(",", get_field('qualifications_and_skills_grade_attained3', $profile_id));

		foreach($field_to_array as $field):

			array_push($grade_array, sanitize_text_field($field));

		endforeach;
	} else { array_push($grade_array, sanitize_text_field($if_nothing_selected)); }




		$talent_job_search_array = array(
		'idealindustry'   		 => $ideal_industry_array,
		'idealseniority' 		 => $ideal_seniority_array,
		'idealjobtitle'   		 => $ideal_job_title_array,
		'idealjoblocation'		 => $ideal_job_location_array,
		'idealjobbasicsalary' 	 => $ideal_job_salary_array,
		'idealjobcompanytype'    => $ideal_company_type_array,
		'idealjobcontract'       => $ideal_contract_array,

		'currentjobindustry'     => $current_job_industry_array,
		'currentemployer'        => $current_employer_array,
		'currentjobtitle'        => $current_job_title_array,
		'currentjobseniority'    => $current_job_seniority_array,
		'currentsalary' 		 => $current_job_salary_array,
		'currentcontract'		 => $current_job_contract_array,

		'institution'			 => $institution_array,
		'qualification'			 => $qualification_array,
		'coursename' 			 => $coursename_array,
		'gradeattained' 		 => $grade_array
		);

	return $talent_job_search_array;
}



///////////////////////////////////////////////////////////////
// Reverse search
///////////////////////////////////////////////////////////////

#Now we will be searching for jobs that match with current profile and return an array of jobs

function reverse_search_job($profile_id) {


	global $wpdb;

// 	$querystr = "
// 	   SELECT DISTINCT
// 	      $wpdb->posts.ID      
// 	    FROM
// 	        $wpdb->posts
// 	    WHERE       
// 	        $wpdb->posts.post_type = 'job' 
// 	        AND $wpdb->posts.post_status = 'publish'
// 			";
	
// 	#First we assign every single found item to each array (and later we search for intersections)		 
// 	$job_id_array = $wpdb->get_col($querystr, 0);

// 		$job_id_array1 = $job_id_array;
// 		$job_id_array2 = $job_id_array;
// 		$job_id_array3 = $job_id_array;
// 		$job_id_array4 = $job_id_array;
// 		$job_id_array5 = $job_id_array;
// 		$job_id_array6 = $job_id_array;
// 		$job_id_array7 = $job_id_array;
// 		$job_id_array8 = $job_id_array;
// 		$job_id_array9 = $job_id_array;
// 		$job_id_array10 = $job_id_array;
// 		$job_id_array11 = $job_id_array;
// 		$job_id_array12 = $job_id_array;
// 		$job_id_array13 = $job_id_array;
// 		$job_id_array14 = $job_id_array;
// 		$job_id_array15 = $job_id_array;
// 		$job_id_array16 = $job_id_array;
// 		$job_id_array17 = $job_id_array;

// 	// Now need to search for every single parameter

// 	if(get_post_meta($profile_id, "profile_to_job_array_serialized", true)) {

// 	$search_fileds_array = unserialize(get_post_meta($profile_id, "profile_to_job_array_serialized", true));
		
// 	} else {

// 	$search_fileds_array = array(
// 		'idealindustry' => array(),
// 		'idealseniority' => array(),
// 		'idealjobtitle' => array(),
// 		'idealjoblocation' => array(),
// 		'idealjobbasicsalary' => array(),
// 		'idealjobcompanytype' => array(),
// 		'idealjobcontract' => array(),
// 		'currentjobindustry' => array(),
// 		'currentemployer' => array(),
// 		'currentjobtitle' => array(),
// 		'currentjobseniority' => array(),
// 		'currentsalary' => array(),
// 		'currentcontract' => array(),
// 		'institution' => array(),
// 		'qualification' => array(),
// 		'coursename' => array(),
// 		'gradeattained' => array()

// 		);	

// 	}
	
// #######################################################################################################

// ########## Parameter 1 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['idealindustry'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_idealindustry' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array1 = array();
// 		$job_id_array1 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 2 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['idealseniority'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();
		

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_idealseniority' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array2 = array();
// 		$job_id_array2 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 3 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['idealjobtitle'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_idealjobtitle' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array3 = array();
// 		$job_id_array3 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 4 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['idealjoblocation'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_idealjoblocation' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array4 = array();
// 		$job_id_array4 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 5 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['idealjobbasicsalary'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_idealjobbasicsalary' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {

			
// 			$param = str_replace("&lt; ", "", $param);
// 			$param = str_replace("&gt; ", "", $param);
// 			$param = $wpdb->esc_like( sanitize_text_field($param));

// 			if ($param == "30.000") {
// 				$qstring .= "(metaalias.meta_value LIKE '%".$param."%' AND metaalias.meta_value NOT LIKE '%30.000-%') OR ";
// 			} 
// 			else 
// 			{
// 				$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 			}
			

// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array5 = array();
// 		$job_id_array5 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 6 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['idealjobcompanytype'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_idealjobcompanytype' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array6 = array();
// 		$job_id_array6 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 7 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['idealjobcontract'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_idealjobcontract' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array7 = array();
// 		$job_id_array7 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 8 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['currentjobindustry'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_currentjobindustry' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array8 = array();
// 		$job_id_array8 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 9 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['currentemployer'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_currentemployer' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array9 = array();
// 		$job_id_array9 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 10 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['currentjobtitle'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_currentjobtitle' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array10 = array();
// 		$job_id_array10 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 11 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['currentjobseniority'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_currentjobseniority' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array11 = array();
// 		$job_id_array11 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 12 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['currentsalary'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_currentsalary' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			# remove > and < signs, otherwise database will reject query
// 			$param = str_replace("&lt; ", "", $param);
// 			$param = str_replace("&gt; ", "", $param);
// 			$param = $wpdb->esc_like( sanitize_text_field($param));

// 			if ($param == "30.000") {
// 				$qstring .= "(metaalias.meta_value LIKE '%".$param."%' AND metaalias.meta_value NOT LIKE '%30.000-%') OR ";
// 			} 
// 			else 
// 			{
// 				$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 			}
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array12 = array();
// 		$job_id_array12 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 13 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['currentcontract'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_currentcontract' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array13 = array();
// 		$job_id_array13 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 14 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['institution'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_institution' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array14 = array();
// 		$job_id_array14 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 15 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['qualification'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_qualification' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array15 = array();
// 		$job_id_array15 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 16 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['coursename'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_coursename' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array16 = array();
// 		$job_id_array16 = $pageposts;
// 	}

// #######################################################################################################

// ########## Parameter 17 ################################################################################

// 	#What we are searching for
// 	$qarray = array();
// 	$qarray = $search_fileds_array['gradeattained'];

// 	if(!in_array("Any", $qarray)) {

// 		$queryparameters  	= "";
// 		$queryfrom 			= "";
// 		$querywhere 		= "";
// 		$qstring 			= "";
// 		$pageposts 			= array();

// 		#Where we are searching for
// 		$qstring .= "(metaalias.meta_key = 'job_search_gradeattained' AND metaalias.meta_value LIKE '%NothingSelected%') OR ";
// 		foreach ($qarray as $param) {
// 			$param = $wpdb->esc_like( sanitize_text_field($param));
// 			$qstring .= "(metaalias.meta_value LIKE '%".$param."%') OR ";
// 		}

// 		$qstring = substr($qstring, 0, -3);
// 		$queryparameters = $queryparameters . " AND (" . $qstring . ")";

// 		$queryfrom = $queryfrom . ", $wpdb->postmeta AS metaalias";
// 		$querywhere = $querywhere . "AND $wpdb->posts.ID = metaalias.post_id ";

// 		#compose a query and run it
// 		$querystr = "
// 		   SELECT DISTINCT
// 		      $wpdb->posts.ID      
// 		    FROM
// 		        $wpdb->posts
// 		        ".$queryfrom."
// 		    WHERE       
// 		        $wpdb->posts.post_type = 'job' 
// 		        AND $wpdb->posts.post_status = 'publish'
// 		        ".$querywhere."
// 		        ".$queryparameters."";

// 		$pageposts = $wpdb->get_col($querystr, 0);

// 		$job_id_array17 = array();
// 		$job_id_array17 = $pageposts;
// 	}

// #######################################################################################################

// $job_ids_found = array();

// $job_ids_found = array_intersect(
// 	$job_id_array1,
// 	$job_id_array2,
// 	$job_id_array3,
// 	$job_id_array4,
// 	$job_id_array5,
// 	$job_id_array6,
// 	$job_id_array7,
// 	$job_id_array8,
// 	$job_id_array9,
// 	$job_id_array10,
// 	$job_id_array11,
// 	$job_id_array12,
// 	$job_id_array13,
// 	$job_id_array14,
// 	$job_id_array15,
// 	$job_id_array16,
// 	$job_id_array17
// 	);


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Now when we have more powerful server we can simply update all the jobs one by one once someone registers


// Now we should also search for the jobs which have an email of the user in their meta and merge two arrays
########## Direct Search ################################################################################

	#What we are searching for
	$user_mail = get_post_meta($profile_id, "about_you_email_address", true);

		#compose a query and run it
		$querystr = "
		   SELECT DISTINCT
		      $wpdb->posts.ID      
		    FROM
		        $wpdb->posts,
		        $wpdb->postmeta AS metaalias
		    WHERE       
		        $wpdb->posts.post_type = 'job' 
		        AND $wpdb->posts.post_status = 'publish'
		        AND $wpdb->posts.ID = metaalias.post_id 
		        AND metaalias.meta_key = 'job_matched_profile_emails' AND metaalias.meta_value LIKE '%".$user_mail."%'";

		$job_id_direct_search = array();
		$job_id_direct_search = $wpdb->get_col($querystr, 0);


	// Now we need to merge arrays and get unique values
	$final_output = array();
	// $final_output = array_unique( array_merge( $job_ids_found, $job_id_direct_search ) );

	$final_output = array_unique($job_id_direct_search);

	
	return $final_output;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////
////////// Function for getting number of unread jobs for current profile
////////// will be boud to meta key "jobs_read"
////////// Return value will integer
//////////////////////////////////////////////////////////////////////////////////////////////////////

function count_new_job_matches($profile_id) {

	$matched_jobs = array();
	$read_jobs = array();

	if(get_post_meta($profile_id, "jobs_read", true)) {
		$read_jobs = unserialize(get_post_meta($profile_id, "jobs_read", true));
	}

	// searching for matched jobs
	$matched_jobs = reverse_search_job($profile_id);

	// getting total number of matched jobs
	$number_of_matches = count($matched_jobs);

	//now run the cycle and if any of the jobs is 'read' deduct one from total number of matches

	foreach ($matched_jobs as $single_job_id) {
		if(in_array($single_job_id, $read_jobs)) {
			$number_of_matches = $number_of_matches - 1;
		}
	}

	return $number_of_matches;
}





add_filter( 'redirect_canonical', 'matchly_disable_redirect_canonical_job' );

function matchly_disable_redirect_canonical_job( $redirect_url ) {

	if ( is_singular( 'job' ) )
		$redirect_url = false;

	return $redirect_url;
}

add_filter( 'redirect_canonical', 'matchly_disable_redirect_canonical_pool' );

function matchly_disable_redirect_canonical_pool( $redirect_url ) {

	if ( is_singular( 'pool' ) )
		$redirect_url = false;

	return $redirect_url;
}


////////////////////////////////////////////////////////////////////////
// Delete Job ajax
////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_delete_job', 'delete_job');
add_action('wp_ajax_nopriv_delete_job', 'delete_job');


function delete_job() { 

	if(isset($_POST['job-id'])) {

		$job_id = $_POST['job-id'];
		wp_delete_post($job_id);

		print_r("deleted");
	}

die();
}



////////////////////////////////////////////////////////////////////////
// Reject Profile Ajax
////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_reject_profile', 'reject_profile');
add_action('wp_ajax_nopriv_reject_profile', 'reject_profile');


function reject_profile() { 

	if(isset($_POST['job-id']) && isset($_POST['profile-id'])) {

		$job_id = $_POST['job-id'];
		$profile_id = $_POST['profile-id'];

		$rejected_array = array();
		$rejected_array = unserialize(get_post_meta($job_id, "job_candidates_rejected", true));

		if(!in_array($profile_id, $rejected_array)) {
			array_push($rejected_array, $profile_id);
		}
		
		update_post_meta($job_id, "job_candidates_rejected", serialize($rejected_array));

		print_r("Rejected");
	}

die();
}


////////////////////////////////////////////////////////////////////////
// Un-Reject Profile Ajax
////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_un_reject_profile', 'un_reject_profile');
add_action('wp_ajax_nopriv_un_reject_profile', 'un_reject_profile');


function un_reject_profile() { 

	if(isset($_POST['job-id']) && isset($_POST['profile-id'])) {

		$job_id = $_POST['job-id'];
		$profile_id = $_POST['profile-id'];

		$rejected_array = array();
		$rejected_array = unserialize(get_post_meta($job_id, "job_candidates_rejected", true));


		if(in_array($profile_id, $rejected_array)) {

			$un_reject = array($profile_id);
			$output_array = array_diff($rejected_array, $un_reject);

			update_post_meta($job_id, "job_candidates_rejected", serialize($output_array));
			print_r('Un-rejected');

		} else {print_r('Not Found');}

		
		

	} else {print_r('No Parameter received.');}

	

die();
}


/////////////////////////////////////////////////////////////////////
// find out if is rejected (for title)
/////////////////////////////////////////////////////////////////////

function if_this_profile_rejected($job_id, $profile_id, $output = "is-rejected") {

		$rejected_array = array();
		$rejected_array = unserialize(get_post_meta($job_id, "job_candidates_rejected", true));

		if(!in_array($profile_id, $rejected_array)) {

			$output = false;

		}
	return $output;
}

/////////////////////////////////////////////////////////////////////
// find out if is rejected (for class generation)
/////////////////////////////////////////////////////////////////////

function if_this_profile_rejected_class($job_id, $profile_id, $output = "is-rejected") {

		$rejected_array = array();
		$rejected_array = unserialize(get_post_meta($job_id, "job_candidates_rejected", true));

		if(!in_array($profile_id, $rejected_array)) {

			$output = "not-rejected";

		}
	return $output;
}


/////////////////////////////////////////////////////////////////////
// Register Interest ajax
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_register_interest', 'register_interest');
add_action('wp_ajax_nopriv_register_interest', 'register_interest');


function register_interest() { 

if(isset($_POST['job-id'])) {

	$job_id = $_POST['job-id'];

	$interested_array = unserialize(get_post_meta($job_id, "job_registered_interest", true));

	if(is_user_logged_in()) {

		$current_user_id = get_current_user_id();


		$profile_ID = 0;
		// Get user profile page
		$query = new WP_Query( 'author='.$current_user_id.'&post_type=profile' );

		// The Loop
		if ( $query->have_posts() ) {

			while ( $query->have_posts() ) {

				$query->the_post();

				$profile_ID = get_the_ID();
			}
		}
		 wp_reset_query();
		 wp_reset_postdata();


	if(!in_array($profile_ID, $interested_array)) {
		array_push($interested_array, $profile_ID);
		update_post_meta($job_id, "job_registered_interest",serialize($interested_array));
		print_r('updated');
	}


	}

}

die();
}


function is_interest_registered($job_id) {

		if(is_user_logged_in()) {

		$interested_array = unserialize(get_post_meta($job_id, "job_registered_interest", true));

		$current_user_id = get_current_user_id();
		$profile_ID = 0;
		// Get user profile page
		$query = new WP_Query( 'author='.$current_user_id.'&post_type=profile' );

		// The Loop
		if ( $query->have_posts() ) {

			while ( $query->have_posts() ) {

				$query->the_post();

				$profile_ID = get_the_ID();
			}
		}
		 wp_reset_query();
		 wp_reset_postdata();


	if(in_array($profile_ID, $interested_array)) {

		return true;
	}


	} else {return false;}


}


/////////////////////////////////////////////////////////////////////
// Send Bulk mail ajax
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_send_bulk', 'send_bulk');
add_action('wp_ajax_nopriv_send_bulk', 'send_bulk');


function send_bulk() { 

if( isset($_POST['message']) && isset($_POST['job-id'])) {

$job_id = $_POST['job-id'];
$recipients_array = array();
$recipients_array = unserialize(get_post_meta($job_id, "marked_talents", true));

// Get Subject Line
$subject = get_the_title($job_id);

$current_user = wp_get_current_user(); 
$sender_email = $current_user->user_email;

$message = strip_tags($_POST['message']);

foreach($recipients_array as $recipient) {

	$recipient_mail = sanitize_text_field(get_field("about_you_email_address", $recipient));

	if(is_email($recipient_mail)) {

		// Send Message
		$msg = $message . "\r\n \r\n \r\n \r\n" . "Simply reply to this email to send a message to the recruiter.";
		$headers = "From: 'OpenGo' <".$sender_email.">" . "\r\n";

		// "to" is ID of recipient user and we need to get it.
		$to =  get_post_field( 'post_author', $recipient );

		//wp_mail($recipient_mail, 'A recruiter wants to speak with you', $msg, $headers);
		// Sending message within internal messaging system
		create_message($to, $subject, $message);


		// Now we need to update contacted meta field

		$contacted_array = unserialize(get_post_meta($job_id, "job_candidates_contacted", true));

		if(!in_array($recipient, $contacted_array)) {

			array_push($contacted_array, $recipient);

			update_post_meta($job_id, "job_candidates_contacted",serialize($contacted_array));

		}


	}

}

update_post_meta($job_id, "marked_talents", '');

print_r("Sent");

} else {print_r("No Data Received"); }


die();
}

//////////////////////////////////////////////////////////////////////
// Function for showing subject Dropdown
//////////////////////////////////////////////////////////////////////

function show_subject_dropdown($profile_id) {

if(is_user_logged_in()){

	#get all jobs by this user

	$current_user_id = get_current_user_id();	

	$query = new WP_Query( 'author='.$current_user_id.'&post_type=job' );

	$jobs_array = array();

	$output_array = array();

	// The Loop
	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();

			$jobid = get_the_ID();
			
			array_push($jobs_array, $jobid);

		 }
	} 

	 wp_reset_query();
	 wp_reset_postdata();
	
	#search for jobs where the usr matches

	 if($jobs_array) {

	 	$match_array = reverse_search_job($profile_id); 

	 	if($match_array) {

	 		$output_array = array_intersect($jobs_array, $match_array);

	 		if($output_array) {

	 			$output_html = '<div class="form-group">';
				$output_html .= '<label  class="col-sm-12 control-label">Please Select Job</label>';
				$output_html .= '<div class="col-sm-12">';
				$output_html .= '<select id="subject">';

					$output_html .= '<option>None selected</option>';

				foreach($output_array as $job) {

					$output_html .= '<option data-job-id="'.$job.'">'.get_the_title($job).'</option>';
				}

				$output_html .= '</select></div></div>';
						
				

	 			print_r($output_html);
	 		}

	 	}

	 }

	

} else {

return;

}


}


function get_current_user_profile() {

	$current_user_ID = get_current_user_id();
		
	// Get user profile ID
	$query = new WP_Query( 'author='.$current_user_ID.'&post_type=profile' );
	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$profileID = get_the_ID();
		}
	} else {
	 $profileID = false;
	}
	wp_reset_postdata(); // leaving the loop not to interfere other parts of this page.

	if($profileID) {
		return $profileID;
	}
}

function get_profile_by_id($user_id) {

		
	// Get user profile ID
	$query = new WP_Query( 'author='.$user_id.'&post_type=profile' );
	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$profileID = get_the_ID();
		}
	} else {
	 $profileID = false;
	}
	wp_reset_postdata(); // leaving the loop not to interfere other parts of this page.

	if($profileID) {
		return $profileID;
	}
}

function generate_unread_jobs_badge() {

	if(get_current_user_profile()) {

		$profile_id = get_current_user_profile();
		
		if(count_new_job_matches($profile_id) > 0) {

			echo '<span class="badge" title="You have '.count_new_job_matches($profile_id).' new job match(es)">'.count_new_job_matches($profile_id).'</span>';
		}

	}
	
}


function generate_unread_jobs_by_id($user_id) {

	if(get_profile_by_id($user_id)) {

		$profile_id = get_profile_by_id($user_id);
		
		if(count_new_job_matches($profile_id) > 0) {

			return count_new_job_matches($profile_id);
		} else {
			return 0;
		}

	}
	
}


function is_this_job_read($job_id, $read = "job-read", $unread = "job-unread") {

	$current_profile_id = get_current_user_profile();

    if($current_profile_id) {

			if(get_post_meta($current_profile_id, "jobs_read", true)) {

				$read_jobs = unserialize(get_post_meta($current_profile_id, "jobs_read", true));

			} else {

				$read_jobs = array();
			}

		# output data depending on result

			if(in_array($job_id, $read_jobs)) {

				echo $read;

			} else {

				echo $unread;

			}

    }


}


/////////////////////////////////////////////////////////////////////
// Send single mail ajax
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_send_single_message', 'send_single_message');
add_action('wp_ajax_nopriv_send_single_message', 'send_single_message');


function send_single_message() { 

if( isset($_POST['recipient']) && isset($_POST['message']) && isset($_POST['profileid']) && isset($_POST['sender-mail']) && isset($_POST['sender-id'])) {


	$profileid = sanitize_text_field($_POST['profileid']);

	$recipientid = sanitize_text_field( $_POST['recipient']);

	$recipientmail = sanitize_text_field(get_field("about_you_email_address", $profileid));
	$recipientmail = sanitize_text_field( $recipientmail );

	$senderid = sanitize_text_field( $_POST['sender-id']);

	$senderemail = $_POST['sender-mail'];
	$senderemail = sanitize_text_field( $senderemail );

	$message = strip_tags($_POST['message']) . "\r\n \r\n \r\n \r\n" . "Simply reply to this email to send a message to the recruiter.";


		// Now it is time to send an email

   $headers = "From: 'OpenGo' <".$senderemail.">" . "\r\n";
   //wp_mail($recipientmail, 'A recruiter wants to speak with you', $message, $headers);

   // send internal message here as well. If subject is not cosen from dropdown than we need to create "(no subject)"" or "Message From User xxx"

		   if(isset($_POST['subject']) && ($_POST['subject'] != "None selected") && ($_POST['subject'] != "undefined")) {

		   	$job_id = sanitize_text_field($_POST['subject']);
		   	$subject = get_the_title($job_id);

		   } else {

		   	$subject = "Message From User " . get_current_user_id();

		   }
		   
	create_message($recipientid, $subject, strip_tags($_POST['message']));





   print_r("Sent!");

   		// message sent, now let's take care of statistics
	
		// updating recipient field
		   $recipientfield = get_field( "received_email_from",'user_'.$recipientid );
		   $recipientfield = $recipientfield . $senderemail . ", ";
		   update_field( "received_email_from", $recipientfield, 'user_'.$recipientid ); 


		 // if subject is set than we need to update one more field

		   if(isset($_POST['subject']) && ($_POST['subject'] != "None selected") && ($_POST['subject'] != "undefined")) {

		   	$job_id = sanitize_text_field($_POST['subject']);

		   	if(get_post_meta($job_id, "job_candidates_contacted", true)) {

				$contacted_array = unserialize(get_post_meta($job_id, "job_candidates_contacted", true));

			} else {

				$contacted_array = array();
			}

			# if is not inarray than add it

			if(!in_array($profileid, $contacted_array)) {
				array_push($contacted_array, $profileid);
				update_post_meta($job_id, "job_candidates_contacted", serialize($contacted_array));
			} 	

		   } else {

		//updating sender field
		   $senderfield = get_field( "sent_email_to",'user_'.$senderid );
		   $senderfield = $senderfield . $recipientmail . ", ";
		   update_field( "sent_email_to", $senderfield, 'user_'.$senderid );

		   }

} else { print_r("No Data Received"); }


die();
}




/////////////////////////////////////////////////////////////////////
// Create Talent Pool ajax
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_create_talent_pool', 'create_talent_pool');
add_action('wp_ajax_nopriv_create_talent_pool', 'create_talent_pool');


function create_talent_pool() { 

if(isset($_POST['profile'])) {

	# Get title

	$profile_id = sanitize_text_field($_POST['profile']);
	$title = get_the_title($profile_id);

	# create

	$pool_args = array(
	  'post_title'    => $title,
	  'post_status'   => 'publish',
	  'post_type'     => 'pool'
	);

	// Insert the profile into the database
	$pool_id = wp_insert_post( $pool_args );

	# update meta

	update_post_meta($pool_id, "pool_profile_id", $profile_id);

	if(isset($_POST['tag-one'])) {
		update_post_meta($pool_id, "pool_tag_one", sanitize_text_field($_POST['tag-one']));
	}

	if(isset($_POST['tag-two'])) {
		update_post_meta($pool_id, "pool_tag_two", sanitize_text_field($_POST['tag-two']));
	}

	if(isset($_POST['tag-three'])) {
		update_post_meta($pool_id, "pool_tag_three", sanitize_text_field($_POST['tag-three']));
	}


	print_r("Talent pool updated with a new record!");
}


die();
}


// name explains itself :)

function get_array_of_my_pools() {

	$current_user_ID = get_current_user_id();
		
	// Get user profile ID
	$query = new WP_Query( 'author='.$current_user_ID.'&post_type=pool&showposts=-1' );
	// The Loop

	$pool_array = array();

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			array_push($pool_array, get_the_ID());
		}
	} 
	wp_reset_postdata(); // leaving the loop not to interfere other parts of this page.

return $pool_array;
}

// function for checking if the talent is in the pool of the current user.

function is_this_talent_in_my_pool($profile_id, $yes = 1, $no = 0) {

	$my_pool_array = get_array_of_my_pools();
	$output = $no;

	foreach($my_pool_array as $pool_id) {

		$pool_profile_id = sanitize_text_field(get_post_meta($pool_id, "pool_profile_id", true));
		if($pool_profile_id == $profile_id) {
			$output = $yes;
		}
	}

	return $output;
}


/////////////////////////////////////////////////////////////////////
// Update Talent Pool ajax
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_update_talent_pool', 'update_talent_pool');
add_action('wp_ajax_nopriv_update_talent_pool', 'update_talent_pool');


function update_talent_pool() { 

if(isset($_POST['pool-id'])) {

$pool_id = sanitize_text_field($_POST['pool-id']);


	if(isset($_POST['tag-one'])) {
		update_post_meta($pool_id, "pool_tag_one", sanitize_text_field($_POST['tag-one']));
	}

	if(isset($_POST['tag-two'])) {
		update_post_meta($pool_id, "pool_tag_two", sanitize_text_field($_POST['tag-two']));
	}

	if(isset($_POST['tag-three'])) {
		update_post_meta($pool_id, "pool_tag_three", sanitize_text_field($_POST['tag-three']));
	}


	print_r("Talent pool updated!");
} else {

	print_r("Something went wrong");
}


die();
}

/////////////////////////////////////////////////////////////////////
// Delete Pool Ajax
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_delete_pool', 'delete_pool');
add_action('wp_ajax_nopriv_delete_pool', 'delete_pool');


function delete_pool() { 

if(isset($_POST['pool-id'])) {

$pool_id = sanitize_text_field($_POST['pool-id']);


	wp_delete_post($pool_id);


	print_r("Item was deleted");
} else {
	
	print_r("Something went wrong");
}


die();
}



/////////////////////////////////////////////////////////////////////
// Sort Pool
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_sort_pool', 'sort_pool');
add_action('wp_ajax_nopriv_sort_pool', 'sort_pool');


function sort_pool() { 

		global $wpdb;

	$count_results = '';
	$results_html = '';
	$pagination_html = '';

	// we are sorting by meta $sortfactor and define sorting direction
	$sort_factor = '';
	$sort_direction = '';
	$direction_str = '';

	//now let's define query variables
	$queryfrom = '';
	$querywhere = '';
	$queryparameters = '';

	$qstring = '';

	$title = '';
	$catone = '';
	$cattwo = '';
	$catthree = '';

	// now get the data and assign to varibales

	if(isset($_POST['title'])) {

	$title = sanitize_text_field($_POST['title']);

	}

	if(isset($_POST['catone'])) {

	$catone = sanitize_text_field($_POST['catone']);

	}

	if(isset($_POST['cattwo'])) {

	$cattwo = sanitize_text_field($_POST['cattwo']);

	}


	if(isset($_POST['catthree'])) {

	$catthree = sanitize_text_field($_POST['catthree']);

	}



	if(isset($_POST['sortfactor'])) {

	$sort_factor = sanitize_text_field($_POST['sortfactor']);

	}

	if(isset($_POST['sortdirection'])) {

		if(sanitize_text_field($_POST['sortdirection']) == "A") {

			$sort_direction = "ASC";

		} else {

			$sort_direction = "DESC";
		}

	$sort_direction = sanitize_text_field($_POST['sortdirection']);

	}

	// now we need to architect the complex query

	if ($title) {

	$qstring = '';
	$param = $wpdb->esc_like( $title );
	$qstring = $qstring . "(" . $wpdb->posts . ".post_title LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	}

	if ($catone) {

		$qstring = '';
	$param = $wpdb->esc_like( $catone );
	$qstring = $qstring . "(nsdscatone.meta_key = 'pool_tag_one' AND nsdscatone.meta_value LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsdscatone";
	$querywhere = $querywhere . "AND $wpdb->posts.ID = nsdscatone.post_id ";
	}


	if ($cattwo) {
			$qstring = '';
	$param = $wpdb->esc_like( $cattwo );
	$qstring = $qstring . "(nsdscattwo.meta_key = 'pool_tag_two' AND nsdscattwo.meta_value LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsdscattwo";
	$querywhere = $querywhere . "AND $wpdb->posts.ID = nsdscattwo.post_id ";
	}


	if ($catthree) {
			$qstring = '';
	$param = $wpdb->esc_like( $catthree );
	$qstring = $qstring . "(nsdscatthree.meta_key = 'pool_tag_three' AND nsdscatthree.meta_value LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsdscatthree";
	$querywhere = $querywhere . "AND $wpdb->posts.ID = nsdscatthree.post_id ";
	}


	if ($sort_factor && $sort_direction) {
			$qstring = '';

		if ($sort_factor == "title") {

			$direction_str = " ORDER BY " . $wpdb->posts . ".post_title ".$sort_direction."";

		} else {

			$qstring = $qstring . " AND sortfactor.meta_key = '".$sort_factor."'";
			$queryparameters = $queryparameters .  $qstring;

			$direction_str = " ORDER BY sortfactor.meta_value ".$sort_direction."";

			$queryfrom = $queryfrom . ", $wpdb->postmeta AS sortfactor";
			$querywhere = $querywhere . "AND $wpdb->posts.ID = sortfactor.post_id ";

		}


	}


	$querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
       ".$queryfrom."
    WHERE       
        $wpdb->posts.post_type = 'pool' 
        AND $wpdb->posts.post_author = '" . get_current_user_id() . "'
        AND $wpdb->posts.post_status = 'publish'
        
        ".$querywhere."
        
        ".$queryparameters." ".$direction_str."";



	$pool_id_array = array();

	$pool_id_array = $wpdb->get_col($querystr, 1);

	// if the custom order is not specified than we need to reverse the order to naturally descending.

	if(!$sort_factor) {
	$pool_id_array = array_reverse ($pool_id_array);
	}


	// Not it is time to take care of pagination

	$pagination = '';

	// Change this figure to output different number per page
	$pool_per_page = 10;


	if(isset($_POST['current-page'])) {

	$current_page = sanitize_text_field($_POST['current-page']);

	} else {

	$current_page = 1;	

	}
	
	

	$offset = ($current_page - 1) * $pool_per_page;
	$full_length = count($pool_id_array);

	$number_of_pages = ceil($full_length / $pool_per_page);


	if($number_of_pages > 1) {
		$first_page = '';
		$last_page = '';
		$before_current ='';
		$current_pg = '';
		$after_current ='';

		if($current_page != 1) {
		$first_page = "<li>1</li>";	
		}
		

		if(($current_page - 1) == 3)
		{

			$before_current = " <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

		} elseif(($current_page - 1) == 2)  {

			$before_current = " <li>".($current_page-1)."</li>";

		} elseif(($current_page - 1) == 1)  {

			$before_current = "";

		} elseif(($current_page - 1) == 0)  {

			$before_current = "";

		}	elseif(($current_page - 1) > 3)  {

			$before_current = "<li>".($current_page-3)."</li> <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

		}


		$current_pg = "<li class='current-page'>".$current_page."</li>";


		if((($number_of_pages - $current_page) == 3) || (($number_of_pages - $current_page) > 3)) {

			$after_current = "<li class='aft'>".($current_page+1)."</li> <li class='aft'>".($current_page+2)."</li> ";

		} elseif(($number_of_pages - $current_page) == 2) {

			$after_current = "<li class='aft'>".($current_page+1)."</li> ";

		}  elseif(($number_of_pages - $current_page) == 1) {

			$after_current = "";

		} else {

			$after_current = '';
		}
		if(!($current_page == $number_of_pages)) {
			$last_page = "<li>".$number_of_pages."</li>";
		}
		
		$pagination_html = $first_page . $before_current . $current_pg . $after_current . $last_page;
	}


	$pool_id_array = array_slice($pool_id_array, $offset, $pool_per_page);




	$results_html = '';

	foreach($pool_id_array as $pool_id) {

	$results_html .= '<div class="pool-line p-line"><div class="pool-title">';
	$results_html .= '<div class="edit-pool" title="Edit" data-pool-id="'.$pool_id.'" data-tag-one="'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_one", true)).'" data-tag-two="'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_two", true)).'" data-tag-three="'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_three", true)).'"></div>';
	$results_html .= '<a href="'.get_permalink(sanitize_text_field(get_post_meta($pool_id, "pool_profile_id", true))).'">'.get_the_title(sanitize_text_field(get_post_meta($pool_id, "pool_profile_id", true))).'</a></div>';
	$results_html .= '<div class="pool-tag pool-tag-one">'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_one", true)).'</div>';
	$results_html .= '<div class="pool-tag pool-tag-two">'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_two", true)).'</div>';
	$results_html .= '<div class="pool-tag pool-tag-three">'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_three", true)).'</div>';
	$results_html .= '</div>';
	}


	$results_output = array(
		'number'        => $full_length,
		'results' 	    => $results_html,
		'pagination'    => $pagination_html
		);

	print_r(json_encode($results_output));
	die();
}



function get_my_jobs_array() {

	$my_jobs_array = array();

	$current_user_id = get_current_user_id();
	$query = new WP_Query( 'author='.$current_user_id.'&post_type=job' );

	if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {

		$query->the_post();
		array_push($my_jobs_array, get_the_ID());
		
	 }

	}
	wp_reset_query();
	wp_reset_postdata();
	return $my_jobs_array;
}


function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}



/////////////////////////////////////////////////////////////////////
// Sort Pool
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_sort_jobs', 'sort_jobs');
add_action('wp_ajax_nopriv_sort_jobs', 'sort_jobs');


function sort_jobs() { 

		global $wpdb;

	$count_results = '';
	$results_html = '';
	$pagination_html = '';

	// we are sorting by meta $sortfactor and define sorting direction
	$sort_factor = '';
	$sort_direction = '';
	$direction_str = '';

	//now let's define query variables
	$queryfrom = '';
	$querywhere = '';
	$queryparameters = '';

	$qstring = '';

	$title = '';
	$employer = '';
	$matched = '';
	$applied = '';
	$tbr = '';
	$contacted = '';
	$rejected = '';

	// now get the data and assign to varibales

	if(isset($_POST['title'])) {

	$title = sanitize_text_field($_POST['title']);

	}

	if(isset($_POST['employer'])) {

	$employer = sanitize_text_field($_POST['employer']);

	}

	if(isset($_POST['matched'])) {

	$matched = sanitize_text_field($_POST['matched']);

	}


	if(isset($_POST['applied'])) {

	$applied = sanitize_text_field($_POST['applied']);

	}

	if(isset($_POST['tbr'])) {

	$tbr = sanitize_text_field($_POST['tbr']);

	}

	if(isset($_POST['contacted'])) {

	$contacted = sanitize_text_field($_POST['contacted']);

	}

	if(isset($_POST['rejected'])) {

	$rejected = sanitize_text_field($_POST['rejected']);

	}


	if(isset($_POST['sortfactor'])) {

	$sort_factor = sanitize_text_field($_POST['sortfactor']);

	}

	if(isset($_POST['sortdirection'])) {

		if(sanitize_text_field($_POST['sortdirection']) == "A") {

			$sort_direction = "ASC";

		} else {

			$sort_direction = "DESC";
		}

	$sort_direction = sanitize_text_field($_POST['sortdirection']);

	}

	// now we need to architect the complex query

	if ($title) {

	$qstring = '';
	$param = $wpdb->esc_like( $title );
	$qstring = $qstring . "(" . $wpdb->posts . ".post_title LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	}

	if ($employer) {

		$qstring = '';
	$param = $wpdb->esc_like( $employer);
	$qstring = $qstring . "(nsds1.meta_key = 'job_employer' AND nsds1.meta_value LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsds1";
	$querywhere = $querywhere . "AND $wpdb->posts.ID = nsds1.post_id ";
	}


	if ($matched) {

		$qstring = '';
	$param = $wpdb->esc_like($matched);
	$qstring = $qstring . "(nsds2.meta_key = 'job_number_of_matches' AND nsds2.meta_value LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsds2";
	$querywhere = $querywhere . "AND $wpdb->posts.ID = nsds2.post_id ";
	}



	if ($sort_factor && $sort_direction) {
			$qstring = '';

		if ($sort_factor == "title") {

			$direction_str = " ORDER BY " . $wpdb->posts . ".post_title ".$sort_direction."";

		} else {

			$qstring = $qstring . " AND sortfactor.meta_key = '".$sort_factor."'";
			$queryparameters = $queryparameters .  $qstring;

			$direction_str = " ORDER BY CAST(sortfactor.meta_value as SIGNED INTEGER) ".$sort_direction."";

			$queryfrom = $queryfrom . ", $wpdb->postmeta AS sortfactor";
			$querywhere = $querywhere . "AND $wpdb->posts.ID = sortfactor.post_id ";

		}


	}


	$querystr = "
   SELECT DISTINCT
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
       ".$queryfrom."
    WHERE       
        $wpdb->posts.post_type = 'job' 
        AND $wpdb->posts.post_author = '" . get_current_user_id() . "'
        AND $wpdb->posts.post_status = 'publish'
        
        ".$querywhere."
        
        ".$queryparameters." ".$direction_str."";



	$item_id_array = array();

	$item_id_array = $wpdb->get_col($querystr, 0);

	// if the custom order is not specified than we need to reverse the order to naturally descending.

	if(!$sort_factor) {
	$item_id_array = array_reverse ($item_id_array);
	}


	// Not it is time to take care of pagination

	$pagination = '';

	// Change this figure to output different number per page
	$item_per_page = 10;


	if(isset($_POST['current-page'])) {

	$current_page = sanitize_text_field($_POST['current-page']);

	} else {

	$current_page = 1;	

	}
	
	

	$offset = ($current_page - 1) * $item_per_page;
	$full_length = count($item_id_array);

	$number_of_pages = ceil($full_length / $item_per_page);


	if($number_of_pages > 1) {
		$first_page = '';
		$last_page = '';
		$before_current ='';
		$current_pg = '';
		$after_current ='';

		if($current_page != 1) {
		$first_page = "<li>1</li>";	
		}
		

		if(($current_page - 1) == 3)
		{

			$before_current = " <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

		} elseif(($current_page - 1) == 2)  {

			$before_current = " <li>".($current_page-1)."</li>";

		} elseif(($current_page - 1) == 1)  {

			$before_current = "";

		} elseif(($current_page - 1) == 0)  {

			$before_current = "";

		}	elseif(($current_page - 1) > 3)  {

			$before_current = "<li>".($current_page-3)."</li> <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

		}


		$current_pg = "<li class='current-page'>".$current_page."</li>";


		if((($number_of_pages - $current_page) == 3) || (($number_of_pages - $current_page) > 3)) {

			$after_current = "<li class='aft'>".($current_page+1)."</li> <li class='aft'>".($current_page+2)."</li> ";

		} elseif(($number_of_pages - $current_page) == 2) {

			$after_current = "<li class='aft'>".($current_page+1)."</li> ";

		}  elseif(($number_of_pages - $current_page) == 1) {

			$after_current = "";

		} else {

			$after_current = '';
		}
		if(!($current_page == $number_of_pages)) {
			$last_page = "<li>".$number_of_pages."</li>";
		}
		
		$pagination_html = $first_page . $before_current . $current_pg . $after_current . $last_page;
	}


	$item_id_array = array_slice($item_id_array, $offset, $item_per_page);




	$results_html = '';

	foreach($item_id_array as $item_id) {

	$c_contacted = count(unserialize(sanitize_text_field(get_post_meta($item_id, "job_candidates_contacted", true))));
	$c_rejected = count(unserialize(sanitize_text_field(get_post_meta($item_id, "job_candidates_rejected", true))));
	$c_total = sanitize_text_field(get_post_meta($item_id, "job_number_of_matches", true));
	$c_to_be_reviewed = $c_total - $c_rejected - $c_contacted;

	$results_html .= '<div class="item-line">';
	$results_html .= '<div class="item-title"><span><a href="'.get_permalink($item_id).'">'.get_the_title($item_id).'</a></span></div>';
	$results_html .= '<div class="item-employer"><span>'.sanitize_text_field(get_post_meta($item_id, "job_employer", true)).'</span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?all">'.$c_total.'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?interested">'.count(unserialize(sanitize_text_field(get_post_meta($item_id, "job_registered_interest", true)))).'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?tbr">'.$c_to_be_reviewed.'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?contacted">'.$c_contacted.'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?rejected">'.$c_rejected.'</a></span></div>';
	$results_html .= '</div>';

	}


	$results_output = array(
		'number'        => $full_length,
		'results' 	    => $results_html,
		'pagination'    => $pagination_html
		);

	print_r(json_encode($results_output));
	die();
}


/////////////////////////////////////////////////////////////////////
// Add Checked talent to the list (checkbox processing)
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_add_checked', 'add_checked');
add_action('wp_ajax_nopriv_add_checked', 'add_checked');


function add_checked() {

if(isset($_POST['job-id']) && isset($_POST['tal-id'])) {

$job_id = $_POST['job-id'];
$profile_id = $_POST['tal-id'];

$new_tal_array = array($profile_id);
$old_tal_array = array();

if(strlen(get_post_meta($job_id, "marked_talents", true)) > 3 )

	$old_tal_array = unserialize(get_post_meta($job_id, "marked_talents", true));
	
	if(!in_array($profile_id, $old_tal_array)) {

		array_push($old_tal_array, $profile_id);
		update_post_meta($job_id, "marked_talents", serialize($old_tal_array));
	}

} else {

	update_post_meta($job_id, "marked_talents", serialize($new_tal_array));
}

// send data
$recipients = '';
$job_id = $_POST['job-id'];
$tal_array = unserialize(get_post_meta($job_id, "marked_talents", true));

foreach($tal_array as $profile_id) {

	$recipients .= "Talent Number " . get_post_field( 'post_author', $profile_id ) . ", ";

}

$recipients = rtrim($recipients, ", ");

print_r($recipients);
die();
}



/////////////////////////////////////////////////////////////////////
// Remove Checked talent from the list (checkbox processing)
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_remove_checked', 'remove_checked');
add_action('wp_ajax_nopriv_remove_checked', 'remove_checked');


function remove_checked() {

if(isset($_POST['job-id']) && isset($_POST['tal-id'])) {

$job_id = $_POST['job-id'];
$profile_id = $_POST['tal-id'];
$new_tal_array = array($profile_id);
$old_tal_array = array();

if(strlen(get_post_meta($job_id, "marked_talents", true)) > 3 )

	$old_tal_array = unserialize(get_post_meta($job_id, "marked_talents", true));
	
	if(in_array($profile_id, $old_tal_array)) {
		// remove it
		$old_tal_array = array_diff($old_tal_array, $new_tal_array);
		update_post_meta($job_id, "marked_talents", serialize($old_tal_array));
		
	}

}
$recipients = '';
$tal_array = unserialize(get_post_meta($job_id, "marked_talents", true));

foreach($tal_array as $profile_id) {

	$recipients .= "Talent Number " . get_post_field( 'post_author', $profile_id ) . ", ";

}

$recipients = rtrim($recipients, ", ");

print_r($recipients);
// send data
die();
}



function get_recipients($job_id) {
$recipients = '';
$tal_array = array();
if(strlen(get_post_meta($job_id, "marked_talents", true)) > 3 ) {
$tal_array = unserialize(get_post_meta($job_id, "marked_talents", true));

}


foreach($tal_array as $profile_id) {

	$recipients .= "Talent Number " . get_post_field( 'post_author', $profile_id ) . ", ";

}

$recipients = rtrim($recipients, ", ");

print_r($recipients);
}

function is_checked_recipient($job_id, $profile_id) {
$tal_array = array();
if(strlen(get_post_meta($job_id, "marked_talents", true)) > 3 ) {
$tal_array = unserialize(get_post_meta($job_id, "marked_talents", true));
}

if (in_array($profile_id, $tal_array)) {
	echo "checked";
}

}



function rs_chars($string) {
$string = str_replace("&#038;","&", $string);
$string = str_replace("&#8211;","-", $string);
$string = preg_replace("/[^A-Za-z ]/", '', sanitize_text_field($string));
return $string;
}



/////////////////////////////////////////////////////////////////////
// Update settings displayname and frequency of notifications
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_update_matchly_settings', 'update_matchly_settings');
add_action('wp_ajax_nopriv_update_matchly_settings', 'update_matchly_settings');

function update_matchly_settings() {

if(isset($_POST['displayname']) && isset($_POST['frequency']) && isset($_POST['smarternotifications']) ) {

	// change things

	$name = sanitize_text_field($_POST['displayname']);
	$freq = sanitize_text_field($_POST['frequency']);
	$smarter_notifications = sanitize_text_field($_POST['smarternotifications']);

	$user_id = get_current_user_id();

	$updated_user = wp_update_user( array( 'ID' => $user_id, 'display_name' => $name ) );

	if($updated_user) {

		print_r("Name Updated! ");
	} 

	update_user_meta($user_id, "notification_frequency", $freq);

	print_r("Notification frequency updated to " . $freq . "!");
	

	update_user_meta($user_id, "smarter_notifications", $smarter_notifications);

	print_r("Smarter Notifications subscription updated to " . $smarter_notifications . "!");


} else {

	print_r("No data received!");
}

	die();
}











