<?php
/**
 * The template for displaying all single Jobs.
 *
 * @package Matchly
 */

get_header(); 

//remove saved checkboxes

$referrer = $_SERVER["HTTP_REFERER"];
$permalink = get_permalink();
if(strpos($referrer, $permalink) !== FALSE) {
//keep values
}
else {
update_post_meta(get_the_id(), "marked_talents", '');
//clean up
}

?>

	<div id="primary" class="content-area">
	<?php $backgroundimageurl = get_field("background_image", "options"); if(isset($backgroundimageurl)) { ?>
		<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
	<?php } else { ?>
		<main id="main" class="site-main" role="main">	
		<?php } ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="container container-fluid">
				<div class="row content-wrapper">
					<div class="page-content-wrapper no-padding">
						<div class="content-inner">
							
								<?php
									// get the variables

									$current_user_id = get_current_user_id();

									$job_id = get_the_ID();

									$author_id = $post->post_author;

									if(is_user_logged_in()) {

										$profile_ID = 0;
										// Get user profile page
										$query = new WP_Query( 'author='.$current_user_id.'&post_type=profile' );

										// The Loop
										if ( $query->have_posts() ) {

											while ( $query->have_posts() ) {

												$query->the_post();

												$profile_ID = get_the_ID();
											}
										}
										 wp_reset_query();
										 wp_reset_postdata();
									}
								 ?>
							 
							 	<?php 
							 	if($current_user_id == $author_id) {
							 		# I am the author

							 		# Get necessary variables

							 		if(get_post_meta($job_id, "job_matched_profiles", true)) {
								 		$matched_array = unserialize(get_post_meta($job_id, "job_matched_profiles", true));
								 	} else {
								 		$matched_array = array();
								 	}

							
								 	


								 	if(get_post_meta($job_id, "job_registered_interest", true)) {
								 		$reg_interest_array = unserialize(get_post_meta($job_id, "job_registered_interest", true));
								 	} else {
								 		$reg_interest_array = array();
								 	}

								 	if(get_post_meta($job_id, "job_candidates_rejected", true)) {
								 		$rejected_array = unserialize(get_post_meta($job_id, "job_candidates_rejected", true));
								 	} else {
								 		$rejected_array = array();
								 	}

								 	if(get_post_meta($job_id, "job_candidates_contacted", true)) {
								 		$contacted_array = unserialize(get_post_meta($job_id, "job_candidates_contacted", true));
								 	} else {
								 		$contacted_array = array();
								 	}


									$tbr_array = array_diff($matched_array, array_unique(array_merge($rejected_array, $contacted_array)));



								 	$show_array = array();

								 	if(isset($_GET['all'])) {

								 		$show_array = $matched_array;

								 	} elseif (isset($_GET['tbr'])) {

										$show_array = $tbr_array;

								 	} elseif (isset($_GET['interested'])) {
								 		
										$show_array = $reg_interest_array;

								 	} elseif (isset($_GET['rejected'])) {
								 		
										$show_array = $rejected_array;

								 	} elseif (isset($_GET['contacted'])) {
								 		
										$show_array = $contacted_array;

								 	} else {

										$show_array = $matched_array;

								 	}



							 		?>
									<div class="general-page-header">
										Manage Job
									</div>

									<div class="info-fields-wrapper">

										<div class="col-sm-6">
											<div class="title-label">Job Title:</div>
											<?php the_title(); ?>
										</div>

										<div class="col-sm-6">
											<div class="title-label">Employer:</div>
											<?php if(get_post_meta($job_id, "job_employer", true)) {
								 					echo get_post_meta($job_id, "job_employer", true);
								 				} else {
								 					echo "No Employer Entered";
								 				} ?>
										</div>

								 		<div class="description-wrapper col-sm-12">
								 			<div class="description-label">
								 				Job Description:
								 			</div>
								 			<?php
								 				if(get_post_meta($job_id, "job_description", true)) {
								 					echo nl2br(get_post_meta($job_id, "job_description", true));
								 				} else {
								 					echo "No Description Given";
								 				}
								 			  ?>
								 		</div>

							 		</div>

							 		<div class="sorting-buttons-wrapper">
							 			<a href="<?php the_permalink(); ?>?all">Show All (<?php echo count($matched_array); ?>)</a>
							 			<a href="<?php the_permalink(); ?>?tbr">To Be Reviewed (<?php echo count($tbr_array); ?>)</a>
							 			<a href="<?php the_permalink(); ?>?interested">Registered Interest (<?php echo count($reg_interest_array); ?>)</a>
							 			<a href="<?php the_permalink(); ?>?rejected">Rejected (<?php echo count($rejected_array); ?>)</a>
							 			<a href="<?php the_permalink(); ?>?contacted">Contacted (<?php echo count($contacted_array); ?>)</a>
							 		</div>


									<div class="profiles-wrapper" data-recipients-title="<?php get_recipients($job_id); ?>">
										
										<?php 

										if($show_array):

											global $paged;
											global $wp_query;

											$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;


											$profiles_per_page = 10;

											$args = array(	
												'post_type' => 'profile',
												'meta_key' => 'profile_progress',
												'orderby'   => 'meta_value_num date',
												'order'     => 'DESC',
												'posts_per_page' => $profiles_per_page,
												'post__in' => $show_array,
											    'paged' => $paged
												);

											// The Query
											query_posts( $args );
											//initiate variable for counting positions
											$i = 0;
											// The Loop
											 if ( have_posts() ) : while ( have_posts() ) : the_post();
													
													$profileid = get_the_ID();

													//autoincrement $i
													$i++;

												$current_position = 'N/A';
												$talent_education = 'N/A';
												$talent_location = 'N/A';

												// current job
												if(get_field("current_job_seniority", $profileid)) {
													$current_position = sanitize_text_field(get_field("current_job_seniority", $profileid));
												} 

												if(get_field("current_job_job_function", $profileid)) {
													$current_position .= ", " . sanitize_text_field(get_field("current_job_job_function", $profileid));
												}

												if(get_field("current_job_organisation_name", $profileid)) {
													$current_position .= ", " . sanitize_text_field(get_field("current_job_organisation_name", $profileid));
												}

												if(get_field("current_job_basic_salary", $profileid)) {
													$current_position .= ", " . sanitize_text_field(get_field("current_job_basic_salary", $profileid));
												}

												if(get_field("current_job_basic_salary_currency", $profileid)) {
													$current_position .= ", " . sanitize_text_field(get_field("current_job_basic_salary_currency", $profileid));
												}

												//education
												if(get_field("qualifications_and_skills_institution1", $profileid)) {
													$talent_education = sanitize_text_field(get_field("qualifications_and_skills_institution1", $profileid));
												}

												if(get_field("qualifications_and_skills_course_name1", $profileid)) {
													$talent_education .= ", " . sanitize_text_field(get_field("qualifications_and_skills_course_name1", $profileid));
												}

												if(get_field("qualifications_and_skills_grade_attained1", $profileid)) {
													$talent_education .= ", " . sanitize_text_field(get_field("qualifications_and_skills_grade_attained1", $profileid));
												}

												//location 
												if(get_field("ideal_job_location", $profileid)) {
												$talent_location = sanitize_text_field(get_field("ideal_job_location", $profileid));
												}
												?>



												<div class="talent-line">
													<div class="talent-info">
														<div class="talent-number">
															Talent Number <?php $post = get_post( $profileid ); $author = $post->post_author; echo $author; ?>
														</div>
														<div class="current-position">
															<span class="talent-line-span">Current Position:</span> <?php echo $current_position; ?>
														</div>
														<div class="talent-university">
															<span class="talent-line-span">University:</span> <?php echo $talent_education; ?>
														</div>
														<div class="talent-location">
															<span class="talent-line-span">Ideal Location:</span> <?php echo $talent_location; ?>

														</div>
														
													</div>

													<div class="talent-buttons">
														<div class="btn-container job-talent-buttons">

															<a target="_blank" class="btn btn-default" href="<?php $permalink = get_permalink($profileid); echo $permalink; ?>"><i class="fa fa-search"></i> View Full Profile</a>
															
															<?php 
															$rejected_text = "Reject Profile";
															if(if_this_profile_rejected($job_id, $profileid)) {
															$rejected_text = "Un-reject Profile";	
															}
															?>

															<button  class="btn btn-danger btn-reject-profile <?php echo if_this_profile_rejected_class($job_id, $profileid); ?>" data-profile-id="<?php echo $profileid; ?>"><i class="fa fa-times"></i> <span class='rejected-text'><?php echo $rejected_text; ?></span></button>

														</div>
														<div class="btn-container checkbox-container">
															<input type="checkbox" name="talent-checkbox" class="talent-checkbox" data-profile-id="<?php echo $profileid; ?>" data-profile-title="Talent Number <?php $post = get_post( $profileid ); $author = $post->post_author; echo $author; ?>" <?php is_checked_recipient($job_id, $profileid); ?>> Check To Contact
														</div>
													</div>
												</div>

												     <?php endwhile; ?>

													<?php if( (count($show_array)/$profiles_per_page) > 1 )  { ?>

														<div class="pagi-line">
														   <?php matchly_numeric_posts_nav(); ?>
														</div>

													<?php } ?>
	
													    <?php
													   wp_reset_postdata(); 
													   wp_reset_query(); 	
													     else :
													endif;
												else: 
													echo "<div class='no-matches'>Sorry, there are no matches!</div>";
												endif;
												      ?>
									</div>


							 		<div class="manage-bottom">
							 			<button class="btn btn-danger delete-job"><i class="fa fa-trash-o"></i> Delete Job</button>
										<button class="btn btn-default send-bulk-message"><i class="fa fa-envelope-o"></i> Send Message</button>
									</div>



<script>

	$(document).ready(function() {


		// When we check the checkbox add it to serialized array 

		$(".talent-checkbox").change(function() {

			
			var talID = $(this).attr("data-profile-id");

	    if(this.checked) {

	    	// removing the gap (adding data to data attribute right away (to get rid of delay while it is being processesd on the server side))
	      	var recipientsTitle = $('.profiles-wrapper').attr("data-recipients-title");
	      	var newRecipient = $(this).attr("data-profile-title");

	      	if (!recipientsTitle.indexOf(newRecipient) >= 0) {
		      	if(recipientsTitle.length > 1) {
			      		recipientsTitle = recipientsTitle + ', ' + newRecipient;
			      		$('.profiles-wrapper').attr("data-recipients-title", recipientsTitle);	
		      	} else {
			      		recipientsTitle = newRecipient;
			      		$('.profiles-wrapper').attr("data-recipients-title", recipientsTitle);	
		      	}
	      	}

	       $.ajax({
	        data: ({action : 'add_checked', 'job-id': <?php echo $job_id ?>, 'tal-id': talID }),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) { 
	      	console.log(msg); 

	      	// $('.profiles-wrapper').attr("data-recipients-title", msg); 
	      });


	    } else {

	    	// removing the gap (adding data to data attribute right away (to get rid of delay while it is being processesd on the server side))
			var recipientsTitle = $('.profiles-wrapper').attr("data-recipients-title");
	      	var newRecipient = $(this).attr("data-profile-title");
			recipientsTitle = recipientsTitle.replace(newRecipient, '');
	    	recipientsTitle = recipientsTitle.replace(', , ', ', ');
	    	if (recipientsTitle.indexOf(", ") == 0) {
	    		recipientsTitle = recipientsTitle.substr(2);
	    	}
	    	if(recipientsTitle.length < 30) {
	    		recipientsTitle = recipientsTitle.replace(', ', '');
	    	 }
	    	$('.profiles-wrapper').attr("data-recipients-title", recipientsTitle);	

	    	$.ajax({
	        data: ({action : 'remove_checked', 'job-id': <?php echo $job_id ?>, 'tal-id': talID }),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) { 
	      	console.log(msg); 
	      	// $('.profiles-wrapper').attr("data-recipients-title", msg); 
	      });

	    }
		});



//////////// Delete Job ////////////////////

	$('.delete-job').click(function(){

	swal({   title: "Are you sure?",
	 text: "You will not be able to recover it!",   
	 type: "warning",   
	 showCancelButton: true,   
	 confirmButtonColor: "#DD6B55",   
	 confirmButtonText: "Yes, delete it!",   
	 cancelButtonText: "No, cancel!",   
	 closeOnConfirm: true,   
	 closeOnCancel: false }, 
	 	function(isConfirm){   if (isConfirm) {     
	 		
	 		 $.ajax({
	        data: ({action : 'delete_job', 'job-id': <?php echo $job_id ?> }),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {
		        console.log(msg);
		        swal("Deleted!", msg + "  You will be redirected in 2 seconds.", "success");

		        setTimeout(function() {
		        var redirectUrl = "<?php echo site_url().'/dashboard/'; ?>";
		        
					 window.location.href = redirectUrl;
				}, 1000);
		      });

	 	} else {     
	 		swal("Cancelled", "Job removal has been cancelled.", "error");   
	 	} });
	});

$(document).on('click', '.btn-reject-profile.not-rejected', function(){

	var thisProfileId = $(this).data("profile-id");

	var button1 = $(this);
	button1.removeClass('not-rejected').addClass('is-rejected');
	button1.find('.rejected-text').html('Un-reject Profile');

		 $.ajax({
	        data: ({action : 'reject_profile', 'profile-id': thisProfileId, 'job-id' : <?php echo $job_id ?> }),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {
		        console.log(msg);



	 		 });

});
/////////////// Reject Profile/////////////////
$(document).on('click', '.btn-reject-profile.is-rejected', function(){

	var thisProfileId = $(this).data("profile-id");
	var button2 = $(this);

	button2.removeClass('is-rejected').addClass('not-rejected');
	button2.find('.rejected-text').html('Reject Profile');

		 $.ajax({
	        data: ({action : 'un_reject_profile', 'profile-id': thisProfileId, 'job-id' : <?php echo $job_id ?> }),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {
		        console.log(msg);


	 		 });

});

/////////// Send Mail//////////////////////////////////

$(document).on('click', '.send-bulk-message', function(){



var selected = [];
var talentIDs = [];
var talentProfileTitles = [];
var recipients = '';


recipients = $('.profiles-wrapper').attr("data-recipients-title");

	if(recipients.length > 0) {

		$('#bulk-email-modal').modal('show');
		$('.recipients-container').html(recipients);
	}


});

    $('#bulk-email-modal').on('hide.bs.modal', function () {    
    	$("#modal-message").val('');
		$('.recipients-container').html('');

    });


    $(document).on('click', '#modal-send-message', function(){

    	if( $("#modal-message").val().length > 1 ) {

	var messageText = $("#modal-message").val();
	var talentIDs = [];

	$('.talent-checkbox:checked').each(function() {
    talentIDs.push($(this).attr('data-profile-id'));
	});

		$('#bulk-email-modal').modal('hide');
		

		 $.ajax({
	        data: ({
	        	action : 'send_bulk',
	        	'recipients': talentIDs,
	        	'message' : messageText,
	        	'job-id' : <?php echo $job_id ?>}),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {
		        console.log(msg);
		        swal("Sent!", "Your Message Was Sent!.", "success");

	$('.talent-checkbox:checked').each(function() {
    $(this).prop('checked', false);
	});
	$('.profiles-wrapper').attr("data-recipients-title", '');

	 		 });
}
});



});
</script>

<!-- Modal -->
<div class="modal fade mail-function" id="bulk-email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="form-horizontal">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Send Message</h4>
		      </div>
		      <div class="modal-body">

		      	<div class="form-group">
					<label for="emailaddress" class="col-sm-12 control-label">Your Email</label>
						<div class="col-sm-12">
						 <input type="email" value="<?php $current_user = wp_get_current_user(); echo $current_user->user_email; ?>" class="form-control" id="emailaddress" name="emailaddress" placeholder="e.g. j.doe@example.com" maxlength="100" required />
						</div>
				</div>
		      	<div class="form-group">
					<label for="emailaddress" class="col-sm-12 control-label">Recipients</label>
						<div class="col-sm-12">
						 <div class="recipients-container">
						 	
						 </div>
						</div>
				</div>
		        <textarea id="modal-message" name="message" placeholder="Please enter your message.." required maxLength="1000"></textarea>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-default" id="modal-send-message">Send</button>
		      </div>
  		</div>
    </div>
  </div>
</div>


							 	<?php								
							 	} elseif ($profile_ID && in_array($job_id, reverse_search_job($profile_ID))) {
							 		# Match Detected ?>

									<div class="general-page-header">
										Congratulations! Matched job detected!
									</div>

									<div class="general-page-content">
										<div class="job-title-wrapper">
											<div class="title-label">Job Title:</div>
											<?php the_title(); ?>
										</div>

								 		<div class="description-wrapper">
								 			<div class="description-label">
								 				Job Description:
								 			</div>
								 			<?php
								 				if(get_post_meta($job_id, "job_description", true)) {
								 					echo get_post_meta($job_id, "job_description", true);
								 				} else {
								 					echo "No Description Given";
								 				}
								 			  ?>
								 		</div>

								 		<div class="requirements-wrapper">
								 			<div class="requirements-label">
								 				Job Requirements:
								 			</div>
												<?php 

												$requirements_array = unserialize(get_post_meta($job_id, "job_requirements", true));

												$in_between = " OR ";

												$ideal_industry			 = '';
												$ideal_seniority 		 = '';
												$ideal_function			 = '';
												$ideal_location			 = '';
												$ideal_pay 		  		 = '';
												$ideal_compnay_type 	 = '';
												$ideal_contract 		 = '';

												$industry_experience 	 = '';
												$company_experience 	 = '';
												$role_experience 		 = '';
												$seniority 				 = '';
												$current_pay		  	 = '';
												$contract 				 = '';

												$institution 		 	 = '';
												$qualification_type   	 = '';
												$course_name 			 = '';
												$grade_attained 		 = '';

												if(isset($requirements_array['idealindustry'])) {
													$tmp_array = $requirements_array['idealindustry'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$ideal_industry = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['idealseniority'])) {
													$tmp_array = $requirements_array['idealseniority'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$ideal_seniority = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['idealjobtitle'])) {
													$tmp_array = $requirements_array['idealjobtitle'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$ideal_function = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['idealjoblocation'])) {
													$tmp_array = $requirements_array['idealjoblocation'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$ideal_location = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['idealjobbasicsalary'])) {
													$tmp_array = $requirements_array['idealjobbasicsalary'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$ideal_pay = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['idealjobcompanytype'])) {
													$tmp_array = $requirements_array['idealjobcompanytype'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$ideal_compnay_type = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['idealjobcontract'])) {
													$tmp_array = $requirements_array['idealjobcontract'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$ideal_contract = substr($parameters, 0, -strlen($in_between));
												}


												if(isset($requirements_array['currentjobindustry'])) {
													$tmp_array = $requirements_array['currentjobindustry'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$industry_experience = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['currentemployer'])) {
													$tmp_array = $requirements_array['currentemployer'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$company_experience = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['currentjobtitle'])) {
													$tmp_array = $requirements_array['currentjobtitle'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$role_experience = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['currentjobseniority'])) {
													$tmp_array = $requirements_array['currentjobseniority'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$seniority = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['currentsalary'])) {
													$tmp_array = $requirements_array['currentsalary'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$current_pay = substr($parameters, 0, -strlen($in_between));
												}


												if(isset($requirements_array['currentcontract'])) {
													$tmp_array = $requirements_array['currentcontract'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$contract = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['institution'])) {
													$tmp_array = $requirements_array['institution'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$institution = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['qualification'])) {
													$tmp_array = $requirements_array['qualification'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$qualification_type = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['coursename'])) {
													$tmp_array = $requirements_array['coursename'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$course_name = substr($parameters, 0, -strlen($in_between));
												}

												if(isset($requirements_array['gradeattained'])) {
													$tmp_array = $requirements_array['gradeattained'];
													$parameters = '';
													foreach($tmp_array as $parameter) {
														$parameters .= $parameter . $in_between;
													}
													$grade_attained = substr($parameters, 0, -strlen($in_between));
												} ?>

												<?php if($ideal_industry) { ?>
												<span class="req-title">Industry:</span><span class="req-items"><?php echo $ideal_industry; ?></span><br>	
												<?php } ?>

												<?php if($ideal_seniority) { ?>
												<span class="req-title">Seniority:</span><span class="req-items"><?php echo $ideal_seniority; ?></span><br>		
												<?php } ?>

												<?php if($ideal_function) { ?>
												<span class="req-title">Job Function:</span><span class="req-items"><?php echo $ideal_function; ?></span><br>		
												<?php } ?>

												<?php if($ideal_location) { ?>
												<span class="req-title">Job Location:</span><span class="req-items"><?php echo $ideal_location; ?></span><br>		
												<?php } ?>

												<?php if($ideal_pay) { ?>
												<span class="req-title">Salary range:</span><span class="req-items"><?php echo $ideal_pay; ?></span><br>		
												<?php } ?>

												<?php if($ideal_compnay_type) { ?>
												<span class="req-title">Company Type:</span><span class="req-items"><?php echo $ideal_compnay_type; ?></span><br>		
												<?php } ?>

												<?php if($ideal_contract) { ?>
												<span class="req-title">Contract:</span><span class="req-items"><?php echo $ideal_contract; ?></span><br>		
												<?php } ?>

												<?php if($industry_experience) { ?>
												<span class="req-title">Industry Experience:</span><span class="req-items"><?php echo $industry_experience ?></span><br>		
												<?php } ?>

												<?php if($company_experience) { ?>
												<span class="req-title">Company Experience:</span><span class="req-items"><?php echo $company_experience; ?></span><br>		
												<?php } ?>

												<?php if($role_experience) { ?>
												<span class="req-title">Role Experience:</span><span class="req-items"><?php echo $role_experience; ?></span><br>		
												<?php } ?>

												<?php if($seniority) { ?>
												<span class="req-title">Current Seniority:</span><span class="req-items"><?php echo $seniority; ?></span><br>		
												<?php } ?>

												<?php if($current_pay) { ?>
												<span class="req-title">Current Salary:</span><span class="req-items"><?php echo $current_pay; ?></span><br>		
												<?php } ?>

												<?php if($contract) { ?>
												<span class="req-title">Current Contract:</span><span class="req-items"><?php echo $contract; ?></span><br>		
												<?php } ?>

												<?php if($institution) { ?>
												<span class="req-title">University:</span><span class="req-items"><?php echo $institution; ?></span><br>		
												<?php } ?>

												<?php if($qualification_type) { ?>
												<span class="req-title">Qualification Type:</span><span class="req-items"><?php echo $qualification_type; ?></span><br>		
												<?php } ?>

												<?php if($course_name) { ?>
												<span class="req-title">Course Name:</span><span class="req-items"><?php echo $course_name; ?></span><br>		
												<?php } ?>

												<?php if($grade_attained ) { ?>
												<span class="req-title">Grade Attained:</span><span class="req-items"><?php echo $grade_attained ; ?></span><br>		
												<?php } ?>

												
								 		</div>

								 		<div class="job-buttons-wrapper interest-btn-wrapper">
								 			<?php if(is_interest_registered($job_id)) {

								 				echo '<div class="registered-interest"> You have registered interest </div>';

								 			} else {

								 				echo '<button  class="btn btn-default accept-job-btn">Register Interest</button>';
								 			}
								 			?>
								 				
								 		</div>

								 		<script>

								 			$(document).ready(function() {

											$(document).on('click', '.accept-job-btn', function(){

												$('.interest-btn-wrapper').html('<div class="registered-interest"> You have registered interest </div>');

													 $.ajax({
												        data: ({action : 'register_interest', 'job-id' : <?php echo $job_id ?> }),
												        type: 'POST',
												        async: true,     
												        url: ajaxurl
												      }).done(function( msg ) {
													        console.log(msg);

												 		 });

											});


											});


								 		</script>
							 		
							 		</div>

								<?php
							 	} else {
							 		# Some other guy

							 		 ?>
									<div class="general-page-header">
										<?php the_title(); ?>
									</div>
									<div class="general-page-content">
								 		<div class="description-wrapper">
								 			<div class="description-label">
								 				Job Description:
								 			</div>
								 			<?php
								 				if(get_post_meta($job_id, "job_description", true)) {
								 					echo get_post_meta($job_id, "job_description", true);
								 				} else {
								 					echo "No Description Given";
								 				}
								 			  ?>
								 		</div>
							 		</div>
							 	<?php
							 	}
								?>

							 </div>

						</div>
					</div>
				</div>
			</div>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_sidebar(); ?>
<?php get_footer(); ?>
