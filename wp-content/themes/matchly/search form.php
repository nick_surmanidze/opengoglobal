<?php
/*
Template Name: Search form
/**
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Matchly
 */

get_header(); ?>

<div id="primary" class="content-area">
			<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<div class="container container-fluid">
			<div class="row content-wrapper">

				<div class="search-form-wrapper">

					<form method="post" action="/search-results/" id="search-form">

						<div class="search-header">
							Search For Your Ideal Candidate
						</div>
						<div class="s-cols-wrapper">
						<div class="col-30">
							<div class="s-col-header">What Type Of Role Are You Recruiting For?</div>
							


									<div class="form-group">
									 <select class="selectpicker" multiple title='Industry (Any)' name="idealindustry[]" >

												<?php $variable = get_field('search_industry', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {

													if(strpos($item,'Any') === false) {

														 
															$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));
															echo "<option value='".$item."'>".$item."</option>";
																									
												} 

												}
												?>	
									  </select>
									</div>  


									<div class="form-group">
									 <select class="selectpicker" multiple title='Seniority (Any)' name="idealseniority[]" >

												<?php $variable = get_field('search_seniority', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
													
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Job Function (Any)' name="idealjobtitle[]" >

												<?php $variable = get_field('search_job_function', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Location (Any)' name="idealjoblocation[]" >

												<?php $variable = get_field('search_location', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Pay (Any)' name="idealjobbasicsalary[]" >

												<?php $variable = get_field('search_pay', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Company Type (Any)' name="idealjobcompanytype[]" >

												<?php $variable = get_field('search_company_type', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Contract (Any)' name="idealjobcontract[]" >

												<?php $variable = get_field('search_contract', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

						</div>

						<div class="col-30">
							<div class="s-col-header">What Work Experience Does The Candidate Need?</div>


									<div class="form-group">
									 <select class="selectpicker" multiple title='Industry Experience (Any)' name="currentjobindustry[]">

												<?php $variable = get_field('search_current_industry', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div>  


									<div class="form-group">
									 <select class="selectpicker" multiple title='Company Experience (Any)'  name="currentemployer[]">
												<?php $variable = get_field('search_current_employer', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Role Experience (Any)' name="currentjobtitle[]">

												<?php $variable = get_field('search_current_job_function', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Seniority (Any)' name="currentjobseniority[]">

												<?php $variable = get_field('search_current_seniority', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Current Pay (Any)' name="currentsalary[]">

												<?php $variable = get_field('search_current_pay', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Contract (Any)' name="currentcontract[]" >

												<?php $variable = get_field('search_current_contract', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div>   


						</div>

						<div class="col-30">
							<div class="s-col-header">What Qualifications Does The Candidate Need?</div>

									<div class="form-group">

							<select class="selectpicker"  multiple title='Institution (Any)'  name="institution[]" >


												<?php $variable = get_field('search_institution', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?> 
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Qualification Type (Any)' name="qualification[]" >

												<?php $variable = get_field('search_qualification_type', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?> 

									  </select>
									</div> 



									<div class="form-group">
									 <select class="selectpicker" multiple title='Course Name (Any)' name="coursename[]" >

												<?php $variable = get_field('search_course_name', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?> 
									  </select> 

									  		
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Grade Attained (Any)' name="gradeattained[]" >

										<?php $variable = get_field('search_grade_attained', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));
													echo "<option value='".$item."'>".$item."</option>";
												
												} 
												}
												?> 
									  </select>
									</div> 

									<div class="number-of-results">
										<div class="number-of-results-title">Number of Matches:</div>
										<div class="number-of-results-value">0</div>
										<img src="<?php echo get_template_directory_uri(); ?>/images/spinner.gif" alt="loading.."?>
									</div>

						</div>	
						</div>					
						<div class="search-bottom">
							<input type="hidden" name="search" value="1">
							<button type="submit" class="btn btn-info search-btn">Search Profiles</button>
						</div>
					</form>
										

				</div>

			</div>
		</div>

	<?php endwhile; // end of the loop. ?>

	<script>
$(window).load(function() {
    setTimeout(function() {
        $(".s-cols-wrapper").css('visibility', 'visible')
    }, 1);


});

$(document).ready(function() {

			$('.number-of-results > img').css('display', 'inline-block');
			$('.number-of-results-value').text('');

			var datastring = $('#search-form').serialize();
	        $.ajax({
	         	data: datastring,
	            type: 'POST',
	            async: true,
	            url: "/wp-content/themes/matchly/search-results-ajax.php"
	            }).done(function( msg ) {

	            	$('.number-of-results > img').css('display', 'none');
	            	$('.number-of-results-value').text(msg);
	        	
	  		});


	// Creating a timer
	var timer;
	$('.selectpicker').on('change', function(){

		// clearing timeout
		
		clearTimeout(timer);
		var ms = 1000; // milliseconds

		timer = setTimeout(function() {


			$('.number-of-results > img').css('display', 'inline-block');
			$('.number-of-results-value').text('');

	

			var datastring = $('#search-form').serialize();
	        $.ajax({
	         	data: datastring,
	            type: 'POST',
	            async: true,
	            url: "/wp-content/themes/matchly/search-results-ajax.php"
	            }).done(function( msg ) {

	            	$('.number-of-results > img').css('display', 'none');
	            	$('.number-of-results-value').text(msg);
	        	
	  		});

		}, ms);

	  });

});

	</script>

</main><!-- #main -->
</div><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
