<?php
/*
Template Name: Publish Preview
/**
 * 
 *
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>
		

			<?php while ( have_posts() ) : the_post(); ?>
					<div class="container container-fluid">
						<div class="row content-wrapper">
							<?php

				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$user_ID = get_current_user_id();
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profid = get_the_ID();
					}

				} else {
					
				}
				wp_reset_postdata();
				////////////////// Get Profile ID - Start ////////////////////////////////////////
				?>






<div class="public-profile-wrapper">


	<div class="profile-notification-wrapper">
		<p class="bg-success"><i class="fa fa-info-circle"></i> This is your public profile. Adding additional important information will ensure it is shown in more searches by recruiters</p>
		<a href="/congratulations-talent" class="white-btn"><i class="fa fa-check-circle"></i>   Complete Profile</a>
	</div>

	<h1>Talent Number <?php  echo get_current_user_id(); ?></h1>


<div class="separator"></div>
						<div class="profile-section-title">Ideal Job</div>
						<div class="profile-content">

									<div class="profile-inner-section  row">

										<div class="col-md-6">


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Job Function:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_job_function", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Seniority:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_seniority", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Location:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_location", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Industry:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_industry", $profid)); ?></div>
										</div>

										</div>

										<div class="col-md-6">



										<div class="profile-item-wrapper">
											<div class="profile-item-title">Company Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_company_type", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Contract Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_contract_type", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Basic Salary:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_basic_salary", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Currency:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_basic_salary_currency", $profid)); ?></div>
										</div>

										</div>

									</div>


						</div>
						<div class="separator"></div>




	<div class="profile-section-title">Current Job</div>
	<div class="profile-content">
		<div class="profile-inner-section  row">

			<div class="col-md-6">

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Organisation Name:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_organisation_name", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Industry:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_industry", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Company Type:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_company_type", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Job Function:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_job_function", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Seniority:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_seniority", $profid)); ?></div>
			</div>

			</div>

			<div class="col-md-6">

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Year Started:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_year_started", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Type of Contract:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_type_of_contract", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Notice Period:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_current_notice_period", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Basic Salary:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_basic_salary", $profid)); ?></div>
			</div>

			<div class="profile-item-wrapper">
				<div class="profile-item-title">Basic Salary Currency:</div>
				<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_basic_salary_currency", $profid)); ?></div>
			</div>

			</div>

		</div>
	</div>
	
<div class="separator"></div> 




					<?php

			$val1 = get_field("previous_experience_organisation_name1", $profid);
			$val2 = get_field("previous_experience_industry1", $profid);

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>

								<div class="profile-section-title">Previous Experience</div>
								<div class="profile-content">
									<div class="profile-inner-section  row">

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Organisation Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_organisation_name1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Industry:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_industry1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Company Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_company_type1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Job Function:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_job_function1", $profid)); ?></div>
										</div>

										</div>

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Seniority:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_seniority1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Started:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_started1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Ended:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_ended1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type of Contract:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_type_of_contract1", $profid)); ?></div>
										</div>

										</div>

									</div>

<div class="separator"></div> 
						<?php

			$val1 = get_field("previous_experience_organisation_name2", $profid);
			$val2 = get_field("previous_experience_industry2", $profid);

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>


									<div class="profile-inner-section  row">

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Organisation Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_organisation_name2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Industry:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_industry2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Company Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_company_type2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Job Function:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_job_function2", $profid)); ?></div>
										</div>

										</div>

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Seniority:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_seniority2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Started:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_started2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Ended:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_ended2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type of Contract:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_type_of_contract2", $profid)); ?></div>
										</div>

										</div>

									</div>

									<div class="separator"></div> 

					<?php } ?>



								</div>
				<?php } else { ?>


	<div class="profile-add-fields-wrapper bg-success">
		<a class="btn btn-default" href="/wp-content/themes/matchly/additional-information-controller.php?step=add-previous-job"><i class="fa fa-plus-circle"></i> Add A Previous Job</a>
	</div>

				<?php } ?>


<?php if (strip_tags(get_field("qualifications_and_skills_special_skills1", $profid))) { ?> 

								<div class="profile-section-title">Specialist Skills</div>
								<div class="profile-content">
									<div class="profile-inner-section  row">

										<div class="profile-item-wrapper">
											
											<div class="specialist-skill profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_special_skills1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											
											<div class="specialist-skill profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_special_skills2", $profid)); ?></div>
										</div>	
																			
										<div class="profile-item-wrapper">
											
											<div class="specialist-skill profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_special_skills3", $profid)); ?></div>
										</div>

									</div>
								</div>

<div class="separator"></div> 

								<?php } else { ?> 

	<div class="profile-add-fields-wrapper bg-success">
		<a class="btn btn-default" href="/wp-content/themes/matchly/additional-information-controller.php?step=specialist-skills"><i class="fa fa-plus-circle"></i> Add Specialist Skills</a>
	</div>

								<?php } ?>


<?php if (strip_tags(get_field("qualifications_and_skills_institution1", $profid))) { ?> 

								<div class="profile-section-title">Qualifications</div>
								<div class="profile-content">

									<div class="profile-inner-section  row">
									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">University:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_institution1", $profid)); ?></div>
										</div>
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Course Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_course_name1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Qualification Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_qualification_type1", $profid)); ?></div>
										</div>
									</div>

									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Grade Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_grade_attained1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_year_attained1", $profid)); ?></div>
										</div>
									
									</div>
									</div>

			<div class="separator"></div> 
								
			<?php

			$val1 = get_field("qualifications_and_skills_institution2", $profid);
			$val2 = get_field("qualifications_and_skills_course_name2", $profid);

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>

									<div class="profile-inner-section  row">
									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">University:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_institution2", $profid)); ?></div>
										</div>
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Course Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_course_name2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Qualification Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_qualification_type2", $profid)); ?></div>
										</div>
									</div>

									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Grade Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_grade_attained2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_year_attained2", $profid)); ?></div>
										</div>
									
									</div>
									</div>

			<div class="separator"></div> 
									
									<?php } ?>

						<?php

			$val1 = strip_tags(get_field("qualifications_and_skills_institution3", $profid));
			$val2 = strip_tags(get_field("qualifications_and_skills_course_name3", $profid));

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>

									<div class="profile-inner-section  row">
									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">University:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_institution3", $profid)); ?></div>
										</div>
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Course Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_course_name3", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Qualification Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_qualification_type3", $profid)); ?></div>
										</div>
									</div>

									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Grade Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_grade_attained3", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_year_attained3", $profid)); ?></div>
										</div>
									
									</div>
									</div>
				<div class="separator"></div> 
				<?php } ?>
				</div>
			<?php } else {?>

	<div class="profile-add-fields-wrapper bg-success">
		<a class="btn btn-default" href="/wp-content/themes/matchly/additional-information-controller.php?step=add-qualifications"><i class="fa fa-plus-circle"></i> Add Qualifications</a>
	</div>
			 <?php } ?>

				






<?php if (strip_tags(get_field("sales_pitch_point_of_difference", $profid))) {?>
							<div class="profile-section-title">Personal</div>
								<div class="profile-content">
									<div class="profile-inner-section  row sales-pitch">

										<div class="profile-item-wrapper">
											<div class="profile-item-title-full-width">The unique or special thing about me that sets me apart from others:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("sales_pitch_point_of_difference", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title-full-width">Words that best describe my personality:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("sales_pitch_personality", $profid)); ?></div>
										</div>	
																			
										<div class="profile-item-wrapper">
											<div class="profile-item-title-full-width">What I am trying to achieve with my career:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("sales_pitch_career_aspiration", $profid)); ?></div>
										</div>

									</div>
								</div>

								<?php } else {?> 
	<div class="profile-add-fields-wrapper bg-success">
		<a class="btn btn-default" href="/wp-content/themes/matchly/additional-information-controller.php?step=add-personal-statement"><i class="fa fa-plus-circle"></i> Add Personal Statement</a>
	</div>
								<?php } ?>
							<div class="separator"></div> 
							<div class="report-abuse">

							</div>


							</div>






						
						</div>
					</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
