<?php
/*
Template Name: Create Job
/**
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
						<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>

			<?php while ( have_posts() ) : the_post(); ?>
					<div class="container container-fluid">
						<div class="row content-wrapper">
							<div class="page-content-wrapper no-padding">
								<div class="content-inner">
<div class="general-page-header">
	Advertise Job
</div>

<div class="job-labels">

<div class="job-labels-line">

	<div class="col-sm-6">
		<div class="form-group create-job-form-item">
			<label  class="col-sm-12 control-label">Job Title</label>
			<div class="col-sm-12">
				<input type="text" class="form-control" id="job-title" name="jobtitle" placeholder="Enter Job Title" maxlength="100"/>
			</div>
			<div class="val-message job-title-validation">Please Enter Job Title</div>
		</div>
	</div>


	<div class="col-sm-6">
		<div class="form-group create-job-form-item">
			<label  class="col-sm-12 control-label">Employer</label>
			<div class="col-sm-12">
				<input type="text" class="form-control" id="job-employer" name="employer" placeholder="Enter Employer" maxlength="100"/>
			</div>
		</div>
	</div>
</div>
	<div class="col-sm-12">
		<div class="form-group create-job-form-item">
			<label  class="col-sm-12 control-label">Job Description</label>
			<div class="col-sm-12">
				<textarea   class="form-control" id="job-description" name="jobdescription" placeholder="Enter Job Description" maxlength="1000"></textarea>	
			</div>
		</div>	

	</div>
</div>

<?php  

if ($_SERVER['HTTP_REFERER'] == "".site_url()."/dashboard/") {
	$query_arr = array();
} else {

	if(isset($_SESSION["search_query"])) {
		$query_arr = array();
		$query_arr = unserialize($_SESSION["search_query"]); 
	} else {
		$query_arr = array();	
	}	

}
?>




					

<form method="post" id="create-job-form">



						<div class="s-cols-wrapper">
								<div class="job-requriements-label">Job Requirements</div>
						<div class="col-30">
							<div class="s-col-header">What Type Of Role Are You Recruiting For?</div>
							


									<div class="form-group">
									 <select class="selectpicker" multiple title='Industry (Any)' name="idealindustry[]" >

												<?php $variable = get_field('search_industry', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'idealindustry';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {

													if(strpos($item,'Any') === false) {

														 
															$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));

															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
																									
												} 

												}
												?>	
									  </select>
									</div>  


									<div class="form-group">
									 <select class="selectpicker" multiple title='Seniority (Any)' name="idealseniority[]" >

												<?php $variable = get_field('search_seniority', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'idealseniority';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }
												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
													
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Job Function (Any)' name="idealjobtitle[]" >

												<?php $variable = get_field('search_job_function', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'idealjobtitle';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Location (Any)' name="idealjoblocation[]" >

												<?php $variable = get_field('search_location', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'idealjoblocation';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Pay (Any)' name="idealjobbasicsalary[]" >

												<?php $variable = get_field('search_pay', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'idealjobbasicsalary';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Company Type (Any)' name="idealjobcompanytype[]" >

												<?php $variable = get_field('search_company_type', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'idealjobcompanytype';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Contract (Any)' name="idealjobcontract[]" >

												<?php $variable = get_field('search_contract', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'idealjobcontract';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

						</div>

						<div class="col-30">
							<div class="s-col-header">What Work Experience Does The Candidate Need?</div>


									<div class="form-group">
									 <select class="selectpicker" multiple title='Industry Experience (Any)' name="currentjobindustry[]">

												<?php $variable = get_field('search_current_industry', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'currentjobindustry';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div>  


									<div class="form-group">
									 <select class="selectpicker" multiple title='Company Experience (Any)'  name="currentemployer[]">
												<?php $variable = get_field('search_current_employer', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'currentemployer';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Role Experience (Any)' name="currentjobtitle[]">

												<?php $variable = get_field('search_current_job_function', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'currentjobtitle';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Seniority (Any)' name="currentjobseniority[]">

												<?php $variable = get_field('search_current_seniority', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'currentjobseniority';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Current Pay (Any)' name="currentsalary[]">

												<?php $variable = get_field('search_current_pay', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'currentsalary';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Contract (Any)' name="currentcontract[]" >

												<?php $variable = get_field('search_current_contract', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'currentcontract';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?>	
									  </select>
									</div>   


						</div>

						<div class="col-30">
							<div class="s-col-header">What Qualifications Does The Candidate Need?</div>

									<div class="form-group">

							<select class="selectpicker"  multiple title='Institution (Any)'  name="institution[]" >


												<?php $variable = get_field('search_institution', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'institution';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array();
												}

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?> 
									  </select>
									</div> 

									<?php  ?>

									<div class="form-group">
									 <select class="selectpicker" multiple title='Qualification Type (Any)' name="qualification[]" >

												<?php $variable = get_field('search_qualification_type', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'qualification';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array();
												}

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
														$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?> 

									  </select>
									</div> 



									<div class="form-group">
									 <select class="selectpicker" multiple title='Course Name (Any)' name="coursename[]" >

												<?php $variable = get_field('search_course_name', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'coursename';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
													
												} else { $values = array();
												}

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
														$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));	
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?> 
									  </select> 

									  		
									</div> 

									<div class="form-group">
									 <select class="selectpicker" multiple title='Grade Attained (Any)' name="gradeattained[]" >

										<?php $variable = get_field('search_grade_attained', 'option'); 
												$temp_array = explode("\n", $variable);

												// inject this code to get the sub array
												$search_term = 'gradeattained';
												$values = array();
												if (array_key_exists ( $search_term , $query_arr )) {												
													$values = $query_arr[$search_term];
												} else { $values = array(); }

												foreach ($temp_array as $item) {
													if(strpos($item,'Any') === false) {
													$item = preg_replace( "/\r|\n/", "", strip_tags( $item ));
															// inject this code to pre-select values
															$selected = '';
															if(in_array($item, $values)) {
																$selected = 'selected';	
															}

															echo "<option value='".$item."' ".$selected.">".$item."</option>";
												
												} 
												}
												?> 
									  </select>
									</div> 

									<div class="number-of-results">
										<div class="number-of-results-title">Number of Matches:</div>
										<div class="number-of-results-value">0</div>
										<img  src="<?php echo get_template_directory_uri(); ?>/images/spinner.gif" alt="loading.."?>
									</div>

						</div>	
						</div>	
						</form>				
						<div class="search-bottom">
							<input type="hidden" name="search" value="1">

							<div class="preloader">
								<img src="<?php echo get_template_directory_uri(); ?>/images/spinner-white.GIF" alt="loading.."?>
							</div>
							
							<button  class="btn btn-default return-btn" >Cancel</button>
							<button  class="btn btn-default create-job-btn" >Advertise Job</button>
						</div>
					
										

				


								</div>
							</div>
						</div>
					</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


	<script>
$(window).load(function() {
    setTimeout(function() {
        $(".s-cols-wrapper").css('visibility', 'visible')
    }, 1);
});

$(document).ready(function() {

			$('.number-of-results > img').css('display', 'inline-block');
			$('.number-of-results-value').text('');

			var datastring = $('#create-job-form').serialize();
	        $.ajax({
	         	data: datastring,
	            type: 'POST',
	            async: true,
	            url: "/wp-content/themes/matchly/search-results-ajax.php"
	            }).done(function( msg ) {

	            	$('.number-of-results > img').css('display', 'none');
	            	$('.number-of-results-value').text(msg);
	        	
	  		});


	// Creating a timer
	var timer;
	$('.selectpicker').on('change', function(){

		// clearing timeout
		
		clearTimeout(timer);
		var ms = 1000; // milliseconds

		timer = setTimeout(function() {


			$('.number-of-results > img').css('display', 'inline-block');
			$('.number-of-results-value').text('');

	

			var datastring = $('#create-job-form').serialize();
	        $.ajax({
	         	data: datastring,
	            type: 'POST',
	            async: true,
	            url: "/wp-content/themes/matchly/search-results-ajax.php"
	            }).done(function( msg ) {

	            	$('.number-of-results > img').css('display', 'none');
	            	$('.number-of-results-value').text(msg);
	        	
	  		});

		}, ms);

	  });
    
	$('.return-btn').click(function(){
	window.location.href = "<?php echo site_url(); ?>/search";
	});

	$('.create-job-btn').click(function(){

		var formData = '';
		var jobTitle = '';
		var jobEmployer = '';
		var jobDescription = '';

	$(".val-message.job-title-validation").css('display', 'none');

		if(!$("#job-title").val()) {
			$(".val-message.job-title-validation").css('display', 'inline-block');
		} else {
			$(".val-message.job-title-validation").css('display', 'none');
			$(".search-bottom > .preloader").css('display', 'inline-block');
			jobTitle = $("#job-title").val();
			jobEmployer = $("#job-employer").val();
			jobDescription = $("#job-description").val();
			formData = $( "#create-job-form" ).serialize();

			console.log(jobTitle+jobEmployer+jobDescription+formData);


$.ajax({
    data: ({
    	action : 'create_job',
    	 'job-title': jobTitle,
    	 'employer': jobEmployer,
    	 'job-description': jobDescription,
    	 'job-requirements': formData
    	}),
    type: 'POST',
    async: true,     
    url: ajaxurl
  }).done(function( msg ) {
  	$(".search-bottom > .preloader").css('display', 'none');
  console.log(msg);
  window.location.href = msg;

  });



		}

	});

});

	</script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
