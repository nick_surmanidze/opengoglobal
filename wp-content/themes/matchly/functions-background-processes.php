<?php
// Here we will have background functions

/////////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_bg_update_my_jobs', 'bg_update_my_jobs');
add_action('wp_ajax_nopriv_bg_update_my_jobs', 'bg_update_my_jobs');

function bg_update_my_jobs() {

	# Get All Jobs By Current User

	$current_user_id = get_current_user_id();	
	$current_user_jobs_array = array();								
	$query = new WP_Query( 'author='.$current_user_id.'&post_type=job' );

	// The Loop
	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();

			array_push($current_user_jobs_array, get_the_ID());

 			}
		}
	 wp_reset_query();
	 wp_reset_postdata();

# Update those Jobs

	 if(count($current_user_jobs_array) > 0) {
	 	foreach ($current_user_jobs_array as $job_id) {

	 		update_single_job_data($job_id);

	 	}
	 }

$job_id = '';

die();

}

/////////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_bg_update_all_jobs', 'bg_update_all_jobs');
add_action('wp_ajax_nopriv_bg_update_all_jobs', 'bg_update_all_jobs');

function bg_update_all_jobs() {

# This is the point when we need to update all the jobs
							
	$query = new WP_Query( 'post_type=job' );

	$all_jobs_array = array();

	// The Loop
	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();

			array_push($all_jobs_array, get_the_ID());

 			}
		}
	 wp_reset_query();
	 wp_reset_postdata();

# Update those Jobs

	 if(count($all_jobs_array) > 0) {
	 	foreach ($all_jobs_array as $job_id) {

	 		update_single_job_data($job_id);

	 	}
	 }

$job_id = '';

die();

}

?>