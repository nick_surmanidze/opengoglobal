<?php
// I will put all job aggregator functions here



function get_keyword_by_function($ideal_function) {

	$keyword_array = array();

    if($ideal_function == "Administration") { $keyword_array = explode("\n", get_field("kwt_administration", "option")); } 
elseif($ideal_function == "Advertising") { $keyword_array = explode("\n", get_field("kwt_advertising", "option")); } 
elseif($ideal_function == "Business Development") { $keyword_array = explode("\n", get_field("kwt_business_development", "option")); } 
elseif($ideal_function == "Content Management") { $keyword_array = explode("\n", get_field("kwt_content_management", "option")); } 
elseif($ideal_function == "Content Creation") { $keyword_array = explode("\n", get_field("kwt_content_creation", "option")); } 
elseif($ideal_function == "Strategy Consultant") { $keyword_array = explode("\n", get_field("kwt_strategy_consultant", "option")); } 
elseif($ideal_function == "Management Consultant") { $keyword_array = explode("\n", get_field("kwt_management_consultant", "option")); } 
elseif($ideal_function == "Change Consultant") { $keyword_array = explode("\n", get_field("kwt_change_consulting", "option")); } 
elseif($ideal_function == "HR Consultant") { $keyword_array = explode("\n", get_field("kwt_human_resource_consulting", "option")); } 
elseif($ideal_function == "Technology Consultant") { $keyword_array = explode("\n", get_field("kwt_technology_consulting", "option")); } 
elseif($ideal_function == "Risk Consultant") { $keyword_array = explode("\n", get_field("kwt_risk_consultant", "option")); } 
elseif($ideal_function == "Corporate Affairs and PR") { $keyword_array = explode("\n", get_field("kwt_corporate_affairs", "option")); } 
elseif($ideal_function == "Customer Service") { $keyword_array = explode("\n", get_field("kwt_customer_service", "option")); } 
elseif($ideal_function == "Finance") { $keyword_array = explode("\n", get_field("kwt_finance", "option")); } 
elseif($ideal_function == "Accounting") { $keyword_array = explode("\n", get_field("kwt_accounting", "option")); } 
elseif($ideal_function == "General Manager") { $keyword_array = explode("\n", get_field("kwt_general_management", "option")); } 
elseif($ideal_function == "Human Resources") { $keyword_array = explode("\n", get_field("kwt_human_resources", "option")); } 
elseif($ideal_function == "Technology Infrastructure") { $keyword_array = explode("\n", get_field("kwt_technology_infrastructure", "option")); } 
elseif($ideal_function == "Design") { $keyword_array = explode("\n", get_field("kwt_design", "option")); } 
elseif($ideal_function == "Engineering") { $keyword_array = explode("\n", get_field("kwt_engineering", "option")); } 
elseif($ideal_function == "Software") { $keyword_array = explode("\n", get_field("kwt_software", "option")); } 
elseif($ideal_function == "User Experience") { $keyword_array = explode("\n", get_field("kwt_user_experience", "option")); } 
elseif($ideal_function == "Law") { $keyword_array = explode("\n", get_field("kwt_legal", "option")); } 
elseif($ideal_function == "Operations") { $keyword_array = explode("\n", get_field("kwt_operations", "option")); } 
elseif($ideal_function == "Product Management") { $keyword_array = explode("\n", get_field("kwt_product_management", "option")); } 
elseif($ideal_function == "Commercial Management") { $keyword_array = explode("\n", get_field("kwt_commercial_management", "option")); } 
elseif($ideal_function == "Project Management") { $keyword_array = explode("\n", get_field("kwt_project_management", "option")); } 
elseif($ideal_function == "Programme Management") { $keyword_array = explode("\n", get_field("kwt_programme_management", "option")); } 
elseif($ideal_function == "Quality Assurance") { $keyword_array = explode("\n", get_field("kwt_quality_assurance", "option")); } 
elseif($ideal_function == "Regulatory and Compliance") { $keyword_array = explode("\n", get_field("kwt_regulatory_compliance", "option")); } 
elseif($ideal_function == "Research and Development") { $keyword_array = explode("\n", get_field("kwt_research_development", "option")); } 
elseif($ideal_function == "Sales") { $keyword_array = explode("\n", get_field("kwt_sales", "option")); } 
elseif($ideal_function == "Marketing") { $keyword_array = explode("\n", get_field("kwt_marketing", "option")); } 
elseif($ideal_function == "Strategy and Planning") { $keyword_array = explode("\n", get_field("kwt_strategy_planning", "option")); } 
elseif($ideal_function == "Supply Chain") { $keyword_array = explode("\n", get_field("kwt_supply_chain", "option")); } 
elseif($ideal_function == "Logistics") { $keyword_array = explode("\n", get_field("kwt_logistics", "option")); } 
elseif($ideal_function == "Procurement") { $keyword_array = explode("\n", get_field("kwt_procurement", "option")); } 
elseif($ideal_function == "Teaching and Education") { $keyword_array = explode("\n", get_field("kwt_teaching_education", "option")); } 
elseif($ideal_function == "Other") { $keyword_array = explode("\n", get_field("kwt_other", "option")); }

	$sanitized_kwd_array = array();
	foreach($keyword_array as $kwd) {
		array_push($sanitized_kwd_array, sanitize_text_field($kwd));
	}
	return $sanitized_kwd_array;
}


# output is array of ideal functions or false
function get_aggregated_keywords($user_id = 0) {

	if($user_id == 0) { $user_id = get_current_user_id(); }

	if (get_profile_by_id($user_id)) {

		$current_profile_id = get_profile_by_id($user_id);

		$ideal_functions = sanitize_text_field(get_field("ideal_job_job_function", $current_profile_id));

		$ideal_function_array = explode(",", $ideal_functions);

		//OK
		//print_r($ideal_function_array);
		$kwd_array = array();

		foreach($ideal_function_array as $ideal_function) {
			// fire the matching function
			$matched_array = get_keyword_by_function(sanitize_text_field($ideal_function));
			$kwd_array = array_merge($kwd_array, $matched_array);
		}

		// once foreach is finished let's get rid of duplicate value

		$kwd_array = array_unique($kwd_array);

		// Final output >>>>
		return $kwd_array;

	} else {

		return false;
	}

}



function get_ideal_functions_array($user_id = 0) {


if($user_id == 0) { $user_id = get_current_user_id(); }


$current_profile_id = get_profile_by_id($user_id);

$ideal_functions = sanitize_text_field(get_field("ideal_job_job_function", $current_profile_id));

$ideal_function_array = explode(",", $ideal_functions);	

$sanitized_ideal_function = array_map("sanitize_text_field", $ideal_function_array);

return $sanitized_ideal_function;
}


function get_ideal_industries_array($user_id = 0) {

if($user_id == 0) { $user_id = get_current_user_id(); }

$current_profile_id = get_profile_by_id($user_id);


$ideal_ind = sanitize_text_field(get_field("ideal_job_industry", $current_profile_id));

$ideal_ind_array = explode(",", $ideal_ind);	

$sanitized_ideal_ind = array_map("sanitize_text_field", $ideal_ind_array);

if(in_array("Any", $sanitized_ideal_ind)) {
	$sanitized_ideal_ind = array();
}

return $sanitized_ideal_ind;

}


function salary_parser($user_id = 0) {

//1. get salary
if($user_id == 0) { $user_id = get_current_user_id(); }
	$current_profile_id = get_profile_by_id($user_id);

	$basic_salary = sanitize_text_field(get_field("ideal_job_basic_salary", $current_profile_id));
	$currency = sanitize_text_field(get_field("ideal_job_basic_salary_currency", $current_profile_id));

	$val1 = 0; // 0
	$val2 = 1000000; // 1m

	if (!strpos($basic_salary, "-")) {

		if(strpos($basic_salary, "30.000") !== false ) {
			$val1 = 0;
			$val2 = 30000;
		}

		if(strpos($basic_salary, "500.000") !== false ) {
			$val1 = 500000;
			$val2 = 1000000;
		}
	} else {

		$sal_array = explode("-", $basic_salary);
		$val1 =  sanitize_text_field(str_replace(".", "", $sal_array[0]));
		$val2 =  sanitize_text_field(str_replace(".", "", $sal_array[1]));
	}

	// print_r($currency); 
	// print_r($basic_salary); 

	$exchange_rate = 1;

	if ($currency != 'USD') {

		// if currency is not usd we should get exchange rate and use it

		if ($currency == 'EUR') {

			$from = 'EUR';
			$to = 'USD';
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {

			    $result = fgetcsv($handle);
			    fclose($handle);
			}	 
			$exchange_rate = $result[0];
		}

		if ($currency == 'GBP') {

			$from = 'GBP';
			$to = 'USD';
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
			$handle = fopen($url, 'r');	

			if ($handle) {

			    $result = fgetcsv($handle);
			    fclose($handle);

			}

			$exchange_rate = $result[0];
		}
		



	}

$output = "$". floor($val1*$exchange_rate)."-"."$". floor($val2*$exchange_rate);
return $output;

}

#Now we need to run the search.. Probably best way would be to make a background process as it might take quite a while...

function aggregate_jobs($offset=0, $per_page=10, $user_id = 0) {

	if($user_id == 0) { $user_id = get_current_user_id(); }

	$current_profile_id = get_profile_by_id($user_id);

	$use_seniority = true;


	#defined array structure and initialized it
	$output = array();
		
	$output['query'] = '';
	$output['total_jobs'] = '';
	$output['current_page'] = '';
	$output['current_raw'] = array();
	$output['current_formatted'] = '';


	// Define Query Vars

	# query consists of  "INDUSTRY"+or+"INDUSTRY2"+title:("function1"+or+"Function2"+"Seniority")

	$base_salary = salary_parser($user_id);
	$location = "London, gb";

	//$functions_array = get_ideal_functions_array();
	$functions_array = get_aggregated_keywords($user_id);
	$industries_array = get_ideal_industries_array($user_id);
	$ideal_seniority  = sanitize_text_field(get_field("ideal_job_seniority", $current_profile_id));


	//$keywords_array = get_aggregated_keywords();

	
	
	$q_functions_with_seniority = '';

	

// Prepare list of functions


	$q_functions_list = '';

	    foreach($functions_array as $func) {

	    	$q_functions_list .= '"'.urlencode($func) . '"+or+';

	    	if ( @strpos(get_field("skip_seniority_list", "option"), sanitize_text_field($func)) !== false) {
				$use_seniority = false;
			}
	    }

	    if(strlen($q_functions_list)>5) {
	    	$q_functions_list = substr($q_functions_list, 0, -4);
	    }	

	// Add seniority (Or skip seniority, really depends....)

	 //    $skip_seniority_word_array  = explode("\n", get_field("skip_seniority_list", "option"));

		// foreach ($skip_seniority_word_array as $skip_word) {


		//     if (@strpos($q_functions_list, sanitize_text_field($skip_word)) !== false) {

		//     	$use_seniority = false;

		//     }

	 //    }



//prepared list of indusctries

	$q_industry = '';

	if(count($industries_array) == 1) {
	    foreach($industries_array as $ind) {

	    	$q_industry .= ''.urlencode($ind) . '+or+';
	    }

	} elseif(count($industries_array) == 0) {

		$q_industry .= '';
	}

	else

	{

		foreach($industries_array as $ind) {

	    	$q_industry .= '"'.urlencode($ind) . '"+or+';
	    }
	}


	    if($use_seniority == true) {

 			$q_industry .= '"'.urlencode($ideal_seniority).'"+or+';

	    } 

	    if(strlen($q_industry) > 5) {
	    	$q_industry = substr($q_industry, 0, -3);
	    }






	   //  if($use_seniority == true) {

 			// $q_functions_with_seniority = 'title:(' . $q_functions_list . '+or+"'.urlencode($ideal_seniority).'")';

	   //  } else {

	    	 $q_functions_with_seniority = 'title:(' . $q_functions_list . ')';
	    //}
	   


$query = $q_industry . $q_functions_with_seniority;
# query consists of  "INDUSTRY"+or+"INDUSTRY2"+title:("function1"+or+"Function2"+"Seniority")




	// Getting User IP for passing it to Indeed

	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {

	    $ip = $_SERVER['HTTP_CLIENT_IP'];

	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

	} else {

	    $ip = $_SERVER['REMOTE_ADDR'];
	}

	// Now compose the query

//$request = 'http://api.indeed.com/ads/apisearch?publisher=2673410223742756&q='.$query.'&l='.htmlentities($location).'&salary='.htmlentities($base_salary).'&sort=&radius=&st=&jt=&start='.$offset.'&limit='.$per_page.'&fromage=15&filter=&latlong=1&co=us&chnl=&userip='.$ip.'&useragent='.urlencode($_SERVER['HTTP_USER_AGENT']).'&v=2';

$request = 'http://api.indeed.com/ads/apisearch?publisher=2673410223742756&q='.$query.'&l='.urlencode($location).'&salary='.urlencode($base_salary).'&sort=date&radius=&st=&jt=&start='.$offset.'&limit='.$per_page.'&fromage=15&filter=1&latlong=0&co=gb&chnl=&userip='.$ip.'&useragent='.urlencode($_SERVER['HTTP_USER_AGENT']).'&v=2';

$output['query'] = $request;

$curl_handle=curl_init();

curl_setopt($curl_handle, CURLOPT_URL, $request);

curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);

curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($curl_handle, CURLOPT_USERAGENT, 'OpenGo');

$response = curl_exec($curl_handle);

curl_close($curl_handle);


// Request sent and response received

$vals = simplexml_load_string($response);

$results = $vals->results;
$page_results_array = array();

//print_r($vals);

$output['total_jobs'] = $vals->totalresults;
$output['current_page'] = $vals->pageNumber;

$resstring = '';

if (count($results->result) > 0) {

	foreach($results->result as $job) {

		$result_line_array = array($job->jobtitle, $job->company, $job->source, $job->snippet, $job->url);
		array_push($output['current_raw'], $result_line_array);
		

		$line = '<div class="item-line"><div class="item-title">';
		$line .= '<span  title="'.$job->snippet.'"><a href="'.$job->url.'" target="_blank">'.$job->jobtitle.'</a></span></div><div class="item-employer">';
		$line .= '<span title="Employer">'. $job->company .'</span></div></div>';								
								
											
										
		$resstring .= $line;									
										
	}


} 

	$output['current_formatted'] = $resstring;
	
	return $output;
}




/////////////////////////////////////////////////////////////////////
// Find Random Job
/////////////////////////////////////////////////////////////////////

add_action('wp_ajax_find_random_job', 'find_random_job');
add_action('wp_ajax_nopriv_find_random_job', 'find_random_job');


function find_random_job() { 


	$output = aggregate_jobs(0, 30);
	$raw_jobs = array();
	$raw_jobs = $output['current_raw'];

	$random = array_rand($raw_jobs);
	$job = $raw_jobs[$random];

	$output = "<div class='list-line'><span class='lbl'><a href='".$job[4]."' target='_blank'>".$job[0]."</a></span></div>";
	print_r($output);
	die();
}