<?php
/*
Template Name: Congratulations
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
				<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>

			<?php while ( have_posts() ) : the_post(); ?>
					<div class="container container-fluid">
						<div class="row content-wrapper">
							<div class="congratulations-button-area">
								<?php 
								$user_ID = get_current_user_id(); 

								$userRole = get_field( "user_role", 'user_'.$user_ID );
								$userRole = preg_replace( "/\r|\n/", "", strip_tags($userRole));

								if ($userRole == "talent") { ?>

								<a class="btn btn-default" href="/dashboard">View Your Progress</a>

								<?php } elseif ($userRole == "recruiter") {?>

								<a class="btn btn-default" href="/search">Find Talent Now</a>

								<?php } else { ?>

								<a class="btn btn-default" href="/dashboard">View Your Progress</a>

								<?php } ?>

							</div>
								<div class="page-content-wrapper congratulations-page-wrapper">

								
 
								<div class="content-inner">
<?php if ( is_user_logged_in() ) { ?>
									<?php 
									$userole = '';
								$user_ID = get_current_user_id();
								$userole = preg_replace( "/\r|\n/", "", strip_tags(get_field( "user_role", 'user_'.$user_ID )));
								if($userole == "talent") {

									the_field("congratulations_talent", get_the_ID());

								} elseif ($userole == "recruiter") {

									the_field("congratulations_recruiter", get_the_ID());

								} else {
									
									the_field("congratulations_talent", get_the_ID());
								}


								?>

									<div class="dashboard-info">
 
 This is your unique link for you to share with others:<br><strong> <?php echo get_home_url() . "?ref=" . get_current_user_id(); ?></strong> <br> (or you can use one of the buttons below)
									</div>
										
<?php get_template_part( 'sharing-buttons'); ?>
<?php } else { echo "You need to be logged in to access this page."; }?>

								</div>
							</div>
						</div>
					</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
