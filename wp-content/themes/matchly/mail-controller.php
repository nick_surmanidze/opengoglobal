<?php 
// Include WP files so our controller can actually work with WP
 
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

if((isset($_POST['recipientid'])) && (isset($_POST['emailaddress'])) && (isset($_POST['message'])) && (isset($_POST['profileid']))) {



	$profileid = $_POST['profileid'];
	$recipientid = $_POST['recipientid'];
	$recipientmail = strip_tags(get_field("about_you_email_address", $profileid));
	$recipientmail = preg_replace( "/\r|\n/", "", $recipientmail );
	$senderid = $_POST['senderid'];
	$senderemail = $_POST['emailaddress'];
	$senderemail=preg_replace( "/\r|\n/", "", $senderemail );
	$message = strip_tags($_POST['message']) . "\r\n \r\n \r\n \r\n" . "Simply reply to this email to send a message to the recruiter.";

			if(isset($_POST['redirecturl'])) {

				$redirecturl = $_POST['redirecturl'];
			} else {
				$redirecturl = '/';
			}

		// Now it is time to send an email

   $headers = "From: 'OpenGo' <".$senderemail.">" . "\r\n";
   wp_mail($recipientmail, 'A recruiter wants to speak with you', $message, $headers);

   		// message sent, now let's take care of statistics
		//updating sender field
		   $senderfield = get_field( "sent_email_to",'user_'.$senderid );
		   $senderfield = $senderfield . $recipientmail . ", ";
		   update_field( "sent_email_to", $senderfield, 'user_'.$senderid );	



		// updating recipient field
		   $recipientfield = get_field( "received_email_from",'user_'.$recipientid );
		   $recipientfield = $recipientfield . $senderemail . ", ";
		   update_field( "received_email_from", $recipientfield, 'user_'.$recipientid ); 

}
wp_redirect( home_url().$redirecturl ); 				
exit;