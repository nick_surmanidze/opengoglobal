<?php
////////////////// Get Profile ID - Start ////////////////////////////////////////
$user_ID = get_current_user_id();
$profileID = 0;
// Get user profile page
$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );
// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();
		$profileID = get_the_ID();
	}
} else {					
}
wp_reset_postdata();
////////////////// Get Profile ID - Finish ///////////////////////////////////////
?>


<div class="dashboard-panel-wrapper">
	<div class="dashboard-panel-title">Your Recruitment Activity:</div>
	<div class="dashboard-panel-inner">

		<div class="col-sm-6">
			<div class="stat-wrapper">
			Number of Searches 
			<span>
			<?php if(strip_tags( get_field( "search_number", 'user_'.$user_ID )))
			{ echo strip_tags( get_field( "search_number", 'user_'.$user_ID ));} else { echo "0";}; ?>
			</span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="stat-wrapper">
			Number of Candidates Contacted 
				<span>
				<?php if(strip_tags( get_field( "sent_email_to", 'user_'.$user_ID )))
				{ 

					$emailsreceived = preg_replace( "/\r|\n/", "", strip_tags( get_field( "sent_email_to", 'user_'.$user_ID )));
					$emailreceivedarray = explode(",", $emailsreceived);
					$emailsnumber = count($emailreceivedarray);
					echo $emailsnumber-1;

			} else { echo "0";}; ?></span>
			</div>
		</div>
		<div class="btn-wrapper">
			<a class="btn btn-default" href="/search">Find Talent Now</a>
		</div>
	</div>
</div>


<div class="dashboard-panel-wrapper">
	<div class="dashboard-panel-title">Recruitment Management</div>
	<div class="dashboard-panel-inner">

	<?php	
	$current_user_id = get_current_user_id();									
	$query = new WP_Query( 'author='.$current_user_id.'&post_type=job' );

	// The Loop
	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();

			$job_ID = get_the_ID();
			?>
			<div class='job-line'><a target="_blank" href="<?php echo get_permalink($job_ID); ?>"><?php the_title(); ?></a></div>
		<?php }
	}
	 wp_reset_query();
	 wp_reset_postdata();
	?>

		<div class="btn-wrapper">
			<a class="btn btn-default" href="<?php echo site_url(); ?>/create-job/">Create A New Job</a>
		</div>	
	</div>
</div>


<div class="dashboard-panel-wrapper">
	<div class="dashboard-panel-title">Talent Pool</div>
	<div class="dashboard-panel-inner">

			<div class="stat-wrapper">
				Current Number of profiles in my Talent Pool
				<span>
					<?php echo count(get_array_of_my_pools()); ?>
				</span>
			</div>

		<div class="btn-wrapper">
			<a class="btn btn-default" href="<?php echo site_url(); ?>/talent-pool/">My Talent Pool</a>
		</div>	
	</div>
</div>


<div class="dashboard-panel-wrapper">
	<div class="dashboard-panel-title">Use OpenGo Premium For Free:</div>
	<div class="dashboard-panel-inner">

		<div class="dashboard-text" style="text-align:center">
OpenGo will shortly be launching a premium service for recruiters. For every person that signs up to OpenGo using your unique link we will reward you with one week of free access to the premium service.<br>
This is your unique link for you to share with others:<br><strong> <?php echo get_home_url() . "/?ref=" . get_current_user_id(); ?></strong> <br> (or you can use one of the buttons below)
		</div>

		<div class="col-sm-6">
			<div class="stat-wrapper">
			Number of friends that have signed up
			<span>
			<?php if(strip_tags( get_field( "user_invited", 'user_'.$user_ID )))
			{ echo strip_tags( get_field( "user_invited", 'user_'.$user_ID ));} else { echo "0";}; ?></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="stat-wrapper">
			Number of free OpenGo Premium weeks earned
				<span>
					<?php if(strip_tags( get_field( "user_invited", 'user_'.$user_ID )))
					{ echo strip_tags( get_field( "user_invited", 'user_'.$user_ID ));} else { echo "0";}; ?>
				</span>
			</div>
		</div>

		<?php get_template_part( 'sharing-buttons'); ?>
		
	</div>
</div>

<div class="dashboard-panel-wrapper">
	<div class="dashboard-panel-title">Your Talent Profile Statistics:</div>
	<div class="dashboard-panel-inner">
		<div class="col-sm-4">
			<div class="stat-wrapper">
			Number of times your profile has been shortlisted
			<span>
			<?php if(strip_tags(get_field("appeared_on_search", $profileID)))
			{ echo strip_tags(get_field("appeared_on_search", $profileID));} else { echo "0";}; ?></span>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="stat-wrapper">
			Number of times your profile has been viewed
			<span>
			<?php if(strip_tags(get_field("was_opened", $profileID)))
			{ echo strip_tags(get_field("was_opened", $profileID));} else { echo "0";}; ?></span>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="stat-wrapper">
			Number of job opportunity emails you have been sent
				<span>
				<?php if(strip_tags( get_field( "received_email_from", 'user_'.$user_ID )))
			{ 
					$emailsreceived = preg_replace( "/\r|\n/", "", strip_tags( get_field( "received_email_from", 'user_'.$user_ID )));
					$emailreceivedarray = explode(",", $emailsreceived);
					$emailsnumber = count($emailreceivedarray);
					echo $emailsnumber-1;
			} else { echo "0";}; ?></span>
			</div>
		</div>
	</div>
</div>



<div class="dashboard-panel-wrapper">
	<div class="dashboard-panel-title">Matched Jobs</div>
	<div class="dashboard-panel-inner">

	<?php 

	$match_array = reverse_search_job($profileID); 
	foreach($match_array as $match_id) { ?>

<div class='job-line <?php is_this_job_read($match_id); ?>'><a target="_blank" href="<?php echo get_permalink($match_id); ?>"><?php echo get_the_title($match_id); ?></a><?php is_this_job_read($match_id, '' , '<span class="badge">New Job Match</span>'); ?></div>

	<?php }


	?>
	</div>
</div>



<div class="dashboard-panel-wrapper">
	<div class="dashboard-panel-title">Get More Job Opportunities:</div>
	<div class="dashboard-panel-inner">

		<div class="dashboard-text" style="text-align:center">
			Shortlisted profiles are ranked according to how many people have signed-up to OpenGo using your unique link. Profiles with a higher shortlist position get more views and more emails from recruiters about job opportunities.

Share your unique link to improve your shortlist position by asking friends and colleagues to sign-up with OpenGo.

			 This is your unique link for you to share with others:<br><strong> <?php echo get_home_url() . "/?ref=" . get_current_user_id(); ?></strong> <br> (or you can use one of the buttons below)
		</div>

		<div class="col-sm-6">
			<div class="stat-wrapper">
			Number of friends that have signed up
			<span>
			<?php if(strip_tags( get_field( "user_invited", 'user_'.$user_ID )))
			{ echo strip_tags( get_field( "user_invited", 'user_'.$user_ID ));} else { echo "0";}; ?></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="stat-wrapper">
			Your profile average shortlist position
				<span>

				<?php if(strip_tags( get_field( "search_positions", $profileID )))
			{ 
				//prepare the string
				$positions_string = strip_tags( get_field( "search_positions", $profileID ));
				$positions_string = preg_replace('/\s+/', '', $positions_string);
				$positions_string = substr($positions_string, 0, -1);
				$positions_array = array();
				$positions_array = explode(",", $positions_string);
				//we have an array. Now we need the number of items in the array and then we need to sum up all the number in the array.
				$total_number = 0;
				foreach ($positions_array as $position) {
					$total_number = $total_number + preg_replace( "/\r|\n/", "", $position);
				}

				$average_position = $total_number / count($positions_array);
				echo floor($average_position);


			} else { echo "0";}; ?></span>
			</div>
		</div>

		<?php get_template_part( 'sharing-buttons'); ?>
		
	</div>
</div>

