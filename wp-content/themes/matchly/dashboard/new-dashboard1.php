<?php

$user_ID = get_current_user_id();
$profileID = 0;
if(get_current_user_profile()) {
	$profileID = get_current_user_profile();
}
?>


<div class="inner-wrapper">

<div class="inner-title">Account Number: OpenGo <?php echo $user_ID; ?>


</div>

<div class="inner-content-wrap">

	<div class="line-one-wrapper row line-wrapper">

		<div class="col-sm-4 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-comments-o"></i> Messages <?php echo get_number_of_new_messages(); ?></div>
					<div class="pane-content">
					<?php 
					global $wpdb;

						$querystr = "
				   SELECT DISTINCT
				      $wpdb->posts.ID      
				    FROM
				        $wpdb->posts,
				        $wpdb->postmeta AS messagefrom,
				        $wpdb->postmeta AS messageto

				    WHERE       
				        $wpdb->posts.post_type = 'message' 
				        AND $wpdb->posts.post_status = 'publish'

				        AND $wpdb->posts.ID = messageto.post_id 

				        AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".get_current_user_id()."')";

			        $message_id_array = array(); 
					$message_id_array = $wpdb->get_col($querystr, 0); 

					if( count($message_id_array) > 0) {

						if(count($message_id_array) > 5) {

							$message_id_array = array_slice($message_id_array, -5);
						}

						$message_id_array =  array_reverse($message_id_array);

						foreach ($message_id_array as $message_id) { ?>

							<div class="list-line">
								<span class="lbl"><a href="<?php echo site_url();?>/conversation/?u=<?php echo get_second_user_id($message_id); ?>" target="_blank">From: OpenGo <?php echo get_post_meta($message_id, "message_from", true); ?></a></span>
								<?php if(message_read_or_not($message_id) == 'read') { echo '<span class="num">Read</span>'; } else { echo '<span class="num num-red">New</span>'; } ?>
							</div>

						<?php }


					} else { 

						echo "<span class='notice-text' style='text-align:center'>Your inbox is empty.</span>";
					}

					?>

					</div>
				<div class="pane-footer">
					<a href="<?php echo site_url(); ?>/inbox/" class="btn btn-default btn-xs">Inbox <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>
		<div class="col-sm-4 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-user"></i> My Talent Profile</div>
				<div class="pane-content">

				<?php if(get_current_user_profile()) { ?>

					<div class="list-line">
						<span class="lbl">Profile rating</span>
						<span class="num"><?php echo get_profile_progress($profileID); ?></span>
					</div>

					<div class="list-line">
						<span class="lbl">Times appeared in search</span>
						<span class="num"><?php if(sanitize_text_field(get_field("appeared_on_search", $profileID)))
			{ echo sanitize_text_field(get_field("appeared_on_search", $profileID));} else { echo "0";}; ?></span>
					</div>

					<div class="list-line">
						<span class="lbl">Average search position</span>
						<span class="num"><?php if(sanitize_text_field(get_field( "search_positions", $profileID )))
			{ 
				//prepare the string
				$positions_string = strip_tags( get_field( "search_positions", $profileID));
				$positions_string = preg_replace('/\s+/', '', $positions_string);
				$positions_string = substr($positions_string, 0, -1);
				$positions_array = array();
				$positions_array = explode(",", $positions_string);
				
				//we have an array. Now we need the number of items in the array and then we need to sum up all the number in the array.
				$total_number = 0;
				foreach ($positions_array as $position) {
					$total_number = $total_number + sanitize_text_field($position);
				}

				$average_position = $total_number / count($positions_array);
				echo floor($average_position);


			} else { echo "0";}; ?></span>
					</div>

					<div class="list-line">
						<span class="lbl">Profile views</span>
						<span class="num"><?php if(strip_tags(get_field("was_opened", $profileID)))
			{ echo strip_tags(get_field("was_opened", $profileID));} else { echo "0";}; ?></span>
					</div>

					<div class="list-line">
						<span class="lbl">Recruiter contacts</span>
						<span class="num"><?php echo count_inbox(); ?></span>
					</div>
				
				<?php } else { ?>
					 You do not have a talent profile. Please click below to add one.
				<?php } ?>

				</div>

				<div class="pane-footer">
				<?php if(get_current_user_profile()) { ?>
					<a href="<?php echo site_url(); ?>/edit-my-profile/" class="btn btn-default btn-xs">Edit Profile <i class="fa fa-arrow-right"></i></a>
				<?php } else { ?>
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/add-recruiter-profile.php" class="btn btn-default btn-xs">Create Profile <i class="fa fa-arrow-right"></i></a>
				<?php } ?>
				</div>
			
			</div>
		</div>
		<div class="col-sm-4 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-bell-o"></i>  My Job Alerts</div>
				<div class="pane-content">
				<?php if(get_current_user_profile()) { ?>
					<?php if(count(reverse_search_job($profileID)) > 0) { ?>

					<span class="notice-text">You have been shortlisted for the following jobs. Review and register your interest.</span>
					<?php 

					$match_array = array_reverse(reverse_search_job($profileID)); 
					$total_match = count($match_array);

					if(count($match_array) > 3) {
						$match_array = array_slice($match_array, 0, 3);
					}

					foreach($match_array as $match_id) { ?>
					<div class="list-line">
						<span class="lbl"><a target="_blank" href="<?php echo get_permalink($match_id); ?>"><?php echo get_the_title($match_id); ?></a></span>
						<?php is_this_job_read($match_id, '<span class="num">Viewed</span>' , '<span class="num num-red">New</span>'); ?>
					</div>
					<?php }?>

					<?php } else { ?>
					<span class="notice-text">No matched jobs found.</span>
					<?php }?>
				<?php } else { ?>
				<span class="notice-text">No matched jobs found.</span>
				<?php }?>

				</div>
				<div class="pane-footer">
					<span class="total-num">Total: <?php echo $total_match; ?></span>
					<a href="<?php echo site_url(); ?>/job-matches/" class="btn btn-default btn-xs">All Matches <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>


	</div>

	<div class="line-two-wrapper row line-wrapper">

		<div class="col-sm-4 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-line-chart"></i> Recruitment Activity</div>
				<div class="pane-content">

					<div class="list-line">
						<span class="lbl">Talent searches</span>
						<span class="num"><?php if(strip_tags( get_field( "search_number", 'user_'.$user_ID )))
			{ echo strip_tags( get_field( "search_number", 'user_'.$user_ID ));} else { echo "0";}; ?></span>
					</div>

					<div class="list-line">
						<span class="lbl">Candidates contacted</span>
						<span class="num"><?php echo count_sent(); ?></span>
					</div>

				</div>
				<div class="pane-footer">
					<a href="<?php echo site_url(); ?>/search" class="btn btn-default btn-xs">Find Talent Now <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>


		<div class="col-sm-6 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-cog"></i> Recruitment Management</div>
				<div class="pane-content">

					<?php if(count(get_my_jobs_array()) > 0) { ?>

						<?php 

						$jobs_array = get_my_jobs_array(); 
						$total_job_num = count($jobs_array);

						if(count($jobs_array) > 3) {
							$jobs_array = array_slice($jobs_array, 0, 3);
						} ?>
						<div class="list-line">
							<span class="lbl"><strong>Job Title</strong></span>
							<span class="lbl"><strong>(Employer)</strong></span>
							<span class="lbl" style="float:right"><strong>Matches</strong></span>	
						</div>
						<?php
						foreach($jobs_array as $job_id) { 

							$employer = '';

							if(get_post_meta($job_id, "job_employer", true)) {

								$employer = " (" . sanitize_text_field(get_post_meta($job_id, "job_employer", true)) . ") ";
							}
							?>

						

						<div class="list-line">
							<span class="lbl"><a target="_blank" href="<?php echo get_permalink($job_id); ?>"><?php echo get_the_title($job_id); echo $employer; ?></a></span>
							<span class="num"><?php echo sanitize_text_field(get_post_meta($job_id, "job_number_of_matches", true)); ?></span>
						</div>
						<?php } ?>

					<?php } else { ?>
					<span class="notice-text">No recruitment activity yet.</span>
					<?php }?>

				</div>
				<div class="pane-footer">
					<span class="total-num">Total: <?php echo $total_job_num; ?></span>
					<a href="<?php echo site_url(); ?>/recruitment-management/" class="btn btn-default btn-xs">View All <i class="fa fa-arrow-right"></i></a>
					<a href="<?php echo site_url(); ?>/advertise-job/" class="btn btn-default btn-xs create-job-btn">Advertise Job <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>


		<div class="col-sm-2 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-star-o"></i> Talent Pool</div>
				<div class="pane-content text-align-center">
					Number of people in your pool</br><span class="num pool-num"><?php echo count(get_array_of_my_pools()); ?></span>
				</div>
				<div class="pane-footer">
					<a href="<?php echo site_url(); ?>/talent-pool/" class="btn btn-default btn-xs">View Pool <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>


	</div>

	<div class="line-two-wrapper row line-wrapper">

		<div class="col-sm-12 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-flask"></i> OpenGo Lab</div>
				<div class="pane-content">


<?php print_r(aggregate_jobs()); ?>



					<?php echo do_shortcode(get_post_field('post_content', get_id_by_slug("opengo-lab"))); ?></div>
			</div>
		</div>

	</div>


</div>

</div>

<script>

function equalizeLines() {

	$(".line-wrapper").each(function(index, value) {	
	  var maxHeight = -1;

	  $(this).find(".pane-content").each(function() {
	    $(this).css('height', "auto");
	   });

	   $(this).find(".pane-content").each(function() {
	     maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
	   });

	   $(this).find(".pane-content").each(function() {
	     $(this).height(maxHeight);
	   });		
	});

}


$(document).ready(function() {

equalizeLines();

// run the function on window resize
$(window).bind('resize', function () { 
	$(".pool-line > div").css("height", "auto");
	equalizeLines();
});

});

</script>
