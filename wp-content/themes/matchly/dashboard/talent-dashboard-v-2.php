<?php

$user_ID = get_current_user_id();
$profileID = 0;
if(get_current_user_profile()) {
	$profileID = get_current_user_profile();
}
?>


<div class="inner-wrapper">

<div class="inner-title"><span class="inner-title-text">Account Number: OpenGo <?php echo $user_ID; ?></span>

<a href="<?php echo site_url(); ?>/inbox/" class="btn btn-default btn-xs go-to-inbox"><i class="fa fa-envelope-o"></i>  Inbox <i class="fa fa-arrow-right"></i><span class="badge new-msg-badge"><?php  if(get_number_of_new_messages()){echo get_number_of_new_messages();} else { echo "0"; }; ?></span></a>

</div>

<div class="inner-content-wrap">

	<div class="line-one-wrapper row line-wrapper">



		<div class="col-sm-6 dashboard-pane no-padding-children">

			<div class="col-sm-4 dashboard-pane red-pane">
				
				<div class="red-pane-title">Smarter Job Search</div>
				<div class="red-pane-content pane-content">We help you find your target job by searching thousands of job boards globally.</div>
				<div class="red-pane-footer"></div>
			</div>

			<div class="col-sm-8 dashboard-pane">

				<div class="pane-inner">
					<div class="pane-header"><i class="fa fa-crosshairs"></i>  My Target Job</div>
					<div class="pane-content">
					<?php if(get_current_user_profile()) { 

						$profid = get_current_user_profile(); ?>
						
						
						<strong>Function:</strong> <?php echo strip_tags(get_field("ideal_job_job_function", $profid)); ?><br>
						<strong>Industry:</strong> <?php echo strip_tags(get_field("ideal_job_industry", $profid)); ?><br>
						<strong>Seniority:</strong> <?php echo strip_tags(get_field("ideal_job_seniority", $profid)); ?><br>
						<strong>Pay:</strong> <?php echo strip_tags(get_field("ideal_job_basic_salary_currency", $profid)); ?> <?php echo strip_tags(get_field("ideal_job_basic_salary", $profid)); ?><br>
						<strong>Location:</strong> <?php echo strip_tags(get_field("ideal_job_location", $profid)); ?><br>


					<?php } else { ?>
					<span class="notice-text">You do not have a profile yet.</span>
					<?php }?>

					</div>
					<div class="pane-footer">
						<a href="<?php echo site_url(); ?>/edit-my-profile/" class="btn btn-default btn-xs">Edit Profile <i class="fa fa-arrow-right"></i></a>
					</div>
				</div>

			</div>

		</div>



		<div class="col-sm-6 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-globe"></i> Global Job Search</div>
				<div class="pane-content">
					<span class="notice-text">Find your target job across thousands of job boards globally. Over 8 million jobs analysed in 5 seconds.</span>
					<strong>Today's Top Pick:</strong><br>
					<img class="search-random-spinner" src="<?php echo get_template_directory_uri(); ?>/images/spinner.gif" alt="loading.."?>


				</div>
				<div class="pane-footer">
					<a href="<?php echo site_url(); ?>/smart-search/" class="btn btn-default btn-xs create-job-btn" target="_blank">Launch Global Search <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>



	</div>

	<div class="line-two-wrapper row line-wrapper">


		<div class="col-sm-6 dashboard-pane no-padding-children">

			<div class="col-sm-4 dashboard-pane red-pane">
				
				<div class="red-pane-title">Recruiter Connections</div>
				<div class="red-pane-content pane-content">We connect you with recruiters that are seeking people just like you.</div>
				<div class="red-pane-footer"></div>
			</div>

			<div class="col-sm-8 dashboard-pane">

				<div class="pane-inner">
					<div class="pane-header"><i class="fa fa-user"></i> My Talent Profile</div>
					<div class="pane-content">

					<?php if(get_current_user_profile()) { ?>

						<div class="list-line">
							<span class="lbl">Profile rating</span>
							<span class="num"><?php echo get_profile_progress($profileID); ?></span>
						</div>

						<div class="list-line">
							<span class="lbl">Times appeared in search</span>
							<span class="num"><?php if(sanitize_text_field(get_field("appeared_on_search", $profileID)))
				{ echo sanitize_text_field(get_field("appeared_on_search", $profileID));} else { echo "0";}; ?></span>
						</div>

						<div class="list-line">
							<span class="lbl">Average search position</span>
							<span class="num"><?php if(sanitize_text_field(get_field( "search_positions", $profileID )))
				{ 
					//prepare the string
					$positions_string = strip_tags( get_field( "search_positions", $profileID));
					$positions_string = preg_replace('/\s+/', '', $positions_string);
					$positions_string = substr($positions_string, 0, -1);
					$positions_array = array();
					$positions_array = explode(",", $positions_string);
					
					//we have an array. Now we need the number of items in the array and then we need to sum up all the number in the array.
					$total_number = 0;
					foreach ($positions_array as $position) {
						$total_number = $total_number + sanitize_text_field($position);
					}

					$average_position = $total_number / count($positions_array);
					echo floor($average_position);


				} else { echo "0";}; ?></span>
						</div>

						<div class="list-line">
							<span class="lbl">Profile views</span>
							<span class="num"><?php if(strip_tags(get_field("was_opened", $profileID)))
				{ echo strip_tags(get_field("was_opened", $profileID));} else { echo "0";}; ?></span>
						</div>

						<div class="list-line">
							<span class="lbl">Recruiter contacts</span>
							<span class="num"><?php echo count_inbox(); ?></span>
						</div>
					
					<?php } else { ?>
						 You do not have a talent profile. Please click below to add one.
					<?php } ?>

					</div>

					<div class="pane-footer">
					<?php if(get_current_user_profile()) { ?>
						<a href="<?php echo site_url(); ?>/edit-my-profile/" class="btn btn-default btn-xs">Edit Profile <i class="fa fa-arrow-right"></i></a>
					<?php } else { ?>
						<a href="<?php echo get_stylesheet_directory_uri(); ?>/add-recruiter-profile.php" class="btn btn-default btn-xs">Create Profile <i class="fa fa-arrow-right"></i></a>
					<?php } ?>
					</div>

				</div>
			
			</div>
		</div>


		<div class="col-sm-6 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-bell-o"></i>  Opportunities For You</div>
				<div class="pane-content">
				<?php if(get_current_user_profile()) { ?>
					<?php if(count(reverse_search_job($profileID)) > 0) { ?>

					<span class="notice-text">You’re in demand. Recruiters want to connect with you about a job they’re hiring. Review and register your interest.</span>
					<?php 

					$match_array = array_reverse(reverse_search_job($profileID)); 
					$total_match = count($match_array);

					if(count($match_array) > 3) {
						$match_array = array_slice($match_array, 0, 3);
					}

					foreach($match_array as $match_id) { ?>
					<div class="list-line">
						<span class="lbl"><a target="_blank" href="<?php echo get_permalink($match_id); ?>"><?php echo get_the_title($match_id); ?></a></span>
						<?php is_this_job_read($match_id, '<span class="num">Viewed</span>' , '<span class="num num-red">New</span>'); ?>
					</div>
					<?php }?>

					<?php } else { ?>
					<span class="notice-text">No matched jobs found.</span>
					<?php }?>
				<?php } else { ?>
				<span class="notice-text">No matched jobs found.</span>
				<?php }?>

				</div>
				<div class="pane-footer">
					<span class="total-num">Total: <?php echo $total_match; ?></span>
					<a href="<?php echo site_url(); ?>/job-matches/" class="btn btn-default btn-xs" target="_blank">All Matches <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>



	</div>

	<div class="line-two-wrapper row line-wrapper">

		<div class="col-sm-12 dashboard-pane">
			<div class="pane-inner">
				<div class="pane-header"><i class="fa fa-flask"></i> OpenGo Lab</div>
				<div class="pane-content">

					<?php echo do_shortcode(get_post_field('post_content', get_id_by_slug("opengo-lab"))); ?></div>
			</div>
		</div>

	</div>


</div>

</div>

<script>

function equalizeLines() {

	if($(window).width() > 765) {


	$(".line-wrapper").each(function(index, value) {	
	  var maxHeight = -1;

	  $(this).find(".pane-content").each(function() {
	    $(this).css('height', "auto");
	   });

	   $(this).find(".pane-content").each(function() {
	     maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
	   });

	   $(this).find(".pane-content").each(function() {
	     $(this).height(maxHeight);
	   });		
	});


	} else {


	$(".line-wrapper").each(function(index, value) {	
	  var maxHeight = -1;

	  $(this).find(".pane-content").each(function() {
	    $(this).css('height', "auto");
	   });	
	});

	}



}


$(document).ready(function() {

equalizeLines();

// run the function on window resize
$(window).bind('resize', function () { 
	$(".pool-line > div").css("height", "auto");
	equalizeLines();
});




$.ajax({
data: ({action : 'find_random_job'}),
type: 'POST',
async: true,     
url: ajaxurl
}).done(function( msg ) {
	$('.search-random-spinner').after(msg).hide();
    console.log(msg);
	 });



});

</script>
