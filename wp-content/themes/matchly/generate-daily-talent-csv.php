<?php 
// Include WP files so our controller can actually work with WP
 
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

global $wpdb;

function generatecsv() {
  global $wpdb;

     header("Content-type: text/csv");
      header("Content-Disposition: attachment; filename=Daily-Talent-CSV.csv");
      header("Pragma: no-cache");
      header("Expires: 0");

      //Functions for gethering data


function how_many_talents_do_i_have() {
  global  $wp_query;

  $talents = count( get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'talent' ) ) );

  $both = count( get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) );

  return $talents + $both;
}

function how_many_talents_joined_yesterday($date='') {

    global $wpdb;

    if( empty($date) )

    $date = date('Y-m-d');

    $morning = new DateTime($date . ' 00:00:00');

    $night = new DateTime($date . ' 23:59:59'); 


    $m = $morning->format('Y-m-d H:i:s');

    $n = $night->format('Y-m-d H:i:s');


    $sql = $wpdb->prepare("SELECT og_users.* FROM og_users WHERE 1=1 AND CAST(user_registered AS DATE) BETWEEN %s AND %s ORDER BY user_login ASC",$m,$n);

    $users = $wpdb->get_results($sql);

    $talents_counter = 0;

    foreach ($users as $user) {

      if ( (sanitize_text_field(get_user_meta($user->ID, 'user_role', true)) == 'talent') || (sanitize_text_field(get_user_meta($user->ID, 'user_role', true)) == 'both') ) {

      $talents_counter++;
    }

    }
return $talents_counter;
 
}

function how_many_public_profiles() {

      global $wpdb;

      $querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        
    WHERE       
        $wpdb->posts.post_type = 'profile'
        AND $wpdb->posts.post_status = 'publish' 
        ";

        $pageposts = $wpdb->get_col($querystr, 1);

        $profileidarray = array();
        $profileidarray = $pageposts; // now we have profile ID array
        $total_public_profiles = count($profileidarray);
        return $total_public_profiles;
      }


function how_many_private_profiles() {

      global $wpdb;

      $querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        
    WHERE       
        $wpdb->posts.post_type = 'profile'
        AND $wpdb->posts.post_status = 'private' 
        ";

        $pageposts = $wpdb->get_col($querystr, 1);

        $profileidarray = array();
        $profileidarray = $pageposts; // now we have profile ID array
        $total_private_profiles = count($profileidarray);
        return $total_private_profiles;
      }


function how_many_times_each_talent_appear_on_search() {

      global $wpdb;

      $querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        
    WHERE       
        $wpdb->posts.post_type = 'profile'
        AND $wpdb->posts.post_status = 'publish' 
        ";

        $pageposts = $wpdb->get_col($querystr, 1);

        $profileidarray = array();
        $profileidarray = $pageposts; // now we have profile ID array

        $search_total_counter = 0;

        foreach ( $profileidarray as $ProfileID ) {

          $search_total_counter = $search_total_counter + get_post_meta($ProfileID, 'appeared_on_search', true);

        }


  return $search_total_counter;
}


function how_many_times_talent_was_contacted_by_recruiter() {

  global  $wp_query;

  $talents = array();

  $both =array();

  $talents = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'talent' ) ) ;

  $both = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) ;

  $merged_talents_array = array_merge($talents, $both);

  $mail_counter = 0;

  foreach ($merged_talents_array as $talent_object) {

    if(strip_tags( get_field( "received_email_from", 'user_'.$talent_object->ID )))
          { 
              $emailsreceived = preg_replace( "/\r|\n/", "", strip_tags( get_field( "received_email_from", 'user_'.$talent_object->ID )));
              $emailreceivedarray = explode(",", $emailsreceived);
              $emailsnumber = count($emailreceivedarray);
              $mail_counter = $mail_counter + ($emailsnumber-1);
          }
    }

    return $mail_counter;
  }


  function how_many_people_have_signed_up_via_referral() {

  $all_users = array();

  $all_users = get_users() ;

  $ref_counter = 0;

  foreach ($all_users as $single_user) {

    $ref_counter = $ref_counter + get_user_meta($single_user->ID, "user_invited", true);

  }

  return $ref_counter;

  }


  function how_many_people_have_signed_up_via_referral_today($date='') {

  $all_users = array();

  $all_users = get_users();

  $ref_counter = 0;

  foreach ($all_users as $single_user) {

   if (get_user_meta($single_user->ID, "was_invited", true) && get_user_meta($single_user->ID, "was_invited", true) == $date) {

    $ref_counter++;

   }

  }

  return $ref_counter;

  }

function how_many_talents_logged_in_this_day($date='') {


  global  $wp_query;

  $talents = array();

  $both = array();

  $talents = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'talent' ) ) ;

  $both = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) ;

  $merged_talents_array = array_merge($talents, $both);

  $counter = 0;

  foreach ($merged_talents_array as $user_object) {

    if(get_user_meta($user_object->ID, "user_log", true)) {

      if ( strpos(get_user_meta($user_object->ID, "user_log", true), $date)  !== false) {
        $counter++;
      }
    }
  }

return $counter;
}


      //open stream  
      $file = fopen('php://output', 'w');  

      // Add Headers                            
      fputcsv($file, array(
        'Date',
        'How many talent I have',
        'How many new talent joined this day',
        'How many public profiles',
        'How many private profiles',
        'The number of times each talent appears in search',
        'How many times talent have been contacted by recruiters',
        'How many people have signed up through a referral',
        'Referral registrations this day',
        'How many talent logged into the system this day',

        )); 

$data = array();

$offset = 30;

$counter = 0;
      while ($counter < $offset ) {
        $counter++;

        $profileRow = array(
            date('d.m.Y',strtotime("-".$counter." days")),
            how_many_talents_do_i_have(),
            how_many_talents_joined_yesterday( date('Y-m-d', strtotime("-".$counter." days"))),
            how_many_public_profiles(),
            how_many_private_profiles(),
            how_many_times_each_talent_appear_on_search(),
            how_many_times_talent_was_contacted_by_recruiter(),
            how_many_people_have_signed_up_via_referral(),
            how_many_people_have_signed_up_via_referral_today(date('d.m.Y',strtotime("-".$counter." days"))),
            how_many_talents_logged_in_this_day(date('d.m.Y',strtotime("-".$counter." days")))


          );
        array_push($data, $profileRow);
      }


     foreach ($data as $row) {

            fputcsv($file, $row);   

     }

    
      exit(); 
}

generatecsv();

?>