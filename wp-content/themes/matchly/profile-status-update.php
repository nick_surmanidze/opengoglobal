<?php

// Include WP files so our controller can actually work with WP
 
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');


////////////////// Get Profile ID - Start ////////////////////////////////////////
$user_ID = get_current_user_id();
$profileID = 0;
// Get user profile page
$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

// The Loop
if ( $query->have_posts() ) {

	while ( $query->have_posts() ) {

		$query->the_post();

		$profileID = get_the_ID();
	}

} else {
	
}
wp_reset_postdata();



// Update profile arguments
$my_profile = array(
  'ID'           => $profileID,
  'post_status' => 'publish'
);



	//re-concatenate search meta every time profile is opened and re-calculate progress

	merge_search_fields($profileID);
	calculate_profile_progress($profileID);


	// update profile to job serialized field
	$profile_to_job = generate_talent_fields_array($profileID);
	update_post_meta($profileID, "profile_to_job_array_serialized", serialize($profile_to_job));



// Now we also need to update the referral field of the profile. If we leave default value set by ACF than it won't be recognized by WPDB.
$refnum = get_field( "user_invited", 'user_'.$user_ID );
if ($refnum > 0) {
update_field( "refferal_sort", $refnum, $profileID );	
} else {
update_field( "refferal_sort", 0, $profileID );	
}


// Update the post into the database
wp_update_post( $my_profile );

//redirect user to different congratulations pages depending on user role
$user_role = get_field( "user_role", 'user_'.$user_ID );
$user_role = preg_replace( "/\r|\n/", "",  strip_tags($user_role));
if ($user_role == "recruiter") {
wp_redirect( home_url().'/congratulation-recruiter/' ); 	
} else {
wp_redirect( home_url().'/congratulation-talent/' ); 	
}



exit;