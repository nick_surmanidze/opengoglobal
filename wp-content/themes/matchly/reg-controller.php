<?php

// Include WP files so our controller can actually work with WP
 
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');


// checking if we are being redirected here from role picker page

if (isset($_GET['role'])) {

					// Refferal Network - If session variable is set, than read the value and update user field.
					// Update: for sorting purposes we also need to have similar value in profile field.

				if(isset($_SESSION['ref'])) {
					$refuser = $_SESSION['ref'];

					$curnum = get_field( "user_invited", 'user_'.$refuser );
					if($curnum)	{$curnum = $curnum + 1;} else {$curnum = 1;}		
					update_field( "user_invited", $curnum, 'user_'.$refuser );

					//////////////////////////////////////////////////////////////////////////
					//For current user add custom field "invite-registration" with date value
					// so we can see how many users registered on particular date
					//////////////////////////////////////////////////////////////////////////

					$current_user_ID = get_current_user_id();

					update_field( "was_invited", date('d.m.Y'), 'user_'.$current_user_ID );

					$_SESSION['ref'] = '';

					// check if refuser has a profile.

					// if "yes", then assign user_invited field value to the refferal_sort custom field. Then we can use it for sorting outputted profiles.		
					// Get user profile ID
					$query = new WP_Query( 'author='.$refuser.'&post_type=profile' );
					// The Loop
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							$profileID = get_the_ID();
							// Great! Profile exists and we have its ID.
							update_field( "refferal_sort", $curnum, $profileID );
							//just updated the field.
						}
					} else {
					// If profile does not exist simply do nothing.
					}
					wp_reset_postdata(); // leaving the loop not to interfere other parts of this page.





				}

// Now Starting actual controller
// 1. Let's get done with recruiter as this is the easiest role.

 if($_GET['role'] == "recruiter" ) {


		// checking if the user clicked recruiter on role picker

		$user_ID = get_current_user_id(); 
		update_field( "user_role", "recruiter", 'user_'.$user_ID );
		update_field( "user_step", "completed", 'user_'.$user_ID );
		// updated user meta fields and now it is time to redirect to congratulations page.

						// Send Data To MailChimp (recruiter)

				if (get_field('mailchimp_enabled', 'option') == true) {

				 $api_k = preg_replace( "/\r|\n/", "", strip_tags(get_field('mailchimp_api_key_recruiter', 'option'))); // Import API Key 
       			 $list_id = preg_replace( "/\r|\n/", "", strip_tags(get_field('mailchimp_list_id_recruiter', 'option'))); // Import List ID
			        if($api_k != "" && $list_id != "") {

 					$recruiter_user = wp_get_current_user();
 					$recruiter_first_name = $recruiter_user->user_firstname; 
 					$recruiter_last_name = $recruiter_user->user_lastname; 
 					$recruiter_email = $recruiter_user->user_email; 

 					if($recruiter_email) {
			            
			            require_once('MCAPI.class.php');  // same directory as reg-controller

			            // grab an API Key from http://admin.mailchimp.com/account/api/
			            $api = new MCAPI($api_k);
			            	$fname = preg_replace( "/\r|\n/", "", strip_tags($recruiter_first_name)) . " " . preg_replace( "/\r|\n/", "", strip_tags($recruiter_last_name));
			            	$mcemail = preg_replace( "/\r|\n/", "", strip_tags($recruiter_email));
			            $merge_vars = Array(
			                'EMAIL' => $mcemail,
			                'FNAME' => $fname
			            );

			            $api->listSubscribe($list_id, $mcemail, $merge_vars, 'html', false);
			        }
			        }
			    }

		wp_redirect( home_url().'/congratulations-recruiter/' ); 
		// redirected to congratulations page and recruiter controller is done.
		exit;
		 } 

// Now let's get to 'Talent' and 'Both' controller. (I do not want to turn this comments into the poem but if someone else will be supporting this web app
// then everything should be easy to understand)
 elseif (($_GET['role'] == "talent") || ($_GET['role'] == "both")) {

		// let's add role indicator first to make sure role is written into the database
		if ($_GET['role'] == "talent") {$user_ID = get_current_user_id(); update_field( "user_role", "talent", 'user_'.$user_ID );}
		if ($_GET['role'] == "both") {$user_ID = get_current_user_id(); update_field( "user_role", "both", 'user_'.$user_ID );}
		// if the user step value is not set, than set it to 0
		if(!get_field( "user_step", 'user_'.$user_ID )) { 
		update_field( "user_step", "0", 'user_'.$user_ID );	
		} 
		wp_redirect( home_url().'/steps/' ); 
		// redirected to steps page and this page will take care of itself with built in controller.
		exit;

 } else {
// if something else is chosen, which is impossible without hacking the html, than simply redirect to home page.
	 wp_redirect( home_url() ); 
	 exit;	
 }

} else {

	// Now we are dealing with $_post Request. Need to make sure that we were redirected here from any steps page.
	// For finding that out let's try checking if step variable is set. If not than send the user far away.... :)


	if(isset($_POST['step'])) {
// if step is set this mean that we are there.. Now we need to start routing and database update

		if ($_POST['step'] == '1') {
				
				//1. Create profile and get it's ID, but prior to creating check if it already exists.

				////////////////// Get Profile ID - Start ////////////////////////////////////////
				
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

				// Create profile object
				$profile = array(
				  'post_title'    => 'profile'.$user_ID,
				  'post_status'   => 'private',
				  'post_type'     => 'profile'
				);

				// Insert the profile into the database
				$profileID = wp_insert_post( $profile );

				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////



				//update fields
				update_field( "about_you_first_name", $_POST['firstname'], $profileID );

				update_field( "about_you_surname", $_POST['surname'], $profileID );

				update_field( "about_you_year_of_birth", $_POST['yearofbirth'], $profileID );

				update_field( "about_you_home_town", $_POST['hometown'], $profileID );

				update_field( "about_you_home_country", $_POST['homecountry'], $profileID );

				update_field( "about_you_email_address", $_POST['emailaddress'], $profileID );

				update_field( "about_you_phone_number", $_POST['phonenumber'], $profileID );



				//change user email if it does not exist

				$user_info = get_userdata( $user_ID);
				$mail = $user_info->user_email;
				if(!$mail) {
				wp_update_user( array( 'ID' => $user_ID, 'user_email' => $_POST['emailaddress'] ) );	
				}

				// Send Data To MailChimp

				if (get_field('mailchimp_enabled', 'option') == true) {

				 $api_k = preg_replace( "/\r|\n/", "", strip_tags(get_field('mailchimp_api_key_talent', 'option'))); // Import API Key 
       			 $list_id = preg_replace( "/\r|\n/", "", strip_tags(get_field('mailchimp_list_id_talent', 'option'))); // Import List ID
			        if($api_k != "" && $list_id != "") {

			            
			            require_once('MCAPI.class.php');  // same directory as reg-controller

			            // grab an API Key from http://admin.mailchimp.com/account/api/
			            $api = new MCAPI($api_k);
			            	$fname = preg_replace( "/\r|\n/", "", strip_tags(get_field( "about_you_first_name", $profileID ))) . " " . preg_replace( "/\r|\n/", "", strip_tags(get_field( "about_you_surname", $profileID )));
			            	$mcemail = preg_replace( "/\r|\n/", "", strip_tags(get_field( "about_you_email_address", $profileID )));
			            $merge_vars = Array(
			                'EMAIL' => $mcemail,
			                'FNAME' => $fname
			            );

			            $api->listSubscribe($list_id, $mcemail, $merge_vars, 'html', false);
			        }
			    }

				update_field( "user_step", "52", 'user_'.$user_ID );	
				wp_redirect( home_url().'/steps/' ); 				
				exit;


		} elseif ($_POST['step'] == '21') {

				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				update_field( "qualifications_and_skills_institution1", $_POST['institution1'], $profileID );

				update_field( "qualifications_and_skills_course_name1", $_POST['coursename1'], $profileID );

				update_field( "qualifications_and_skills_qualification_type1", $_POST['qualificationtype1'], $profileID );

				update_field( "qualifications_and_skills_grade_attained1", $_POST['gradeattained1'], $profileID );

				update_field( "qualifications_and_skills_year_attained1", $_POST['yearattained1'], $profileID );

				//activating skip link
				$skiplink = $_POST['skipto'];
				if($skiplink == 1) {

				update_field( "user_step", "7", 'user_'.$user_ID );
				wp_redirect( home_url().'/public-profile-preview/' );				
				exit;

				} else {
				update_field( "user_step", "21", 'user_'.$user_ID );	
				wp_redirect( home_url().'/steps/' ); 				
				exit;
				}


				

		} elseif ($_POST['step'] == '22') {

				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				update_field( "qualifications_and_skills_institution2", $_POST['institution2'], $profileID );

				update_field( "qualifications_and_skills_course_name2", $_POST['coursename2'], $profileID );

				update_field( "qualifications_and_skills_qualification_type2", $_POST['qualificationtype2'], $profileID );

				update_field( "qualifications_and_skills_grade_attained2", $_POST['gradeattained2'], $profileID );

				update_field( "qualifications_and_skills_year_attained2", $_POST['yearattained2'], $profileID );


				//activating skip link
				$skiplink = $_POST['skipto'];

				if($skiplink == 1) {
				update_field( "user_step", "7", 'user_'.$user_ID );
				wp_redirect( home_url().'/public-profile-preview/' );				
				exit;
				} else {
				update_field( "user_step", "22", 'user_'.$user_ID );
				wp_redirect( home_url().'/steps/' ); 				
				exit;	
				}	



		} elseif ($_POST['step'] == '23') {

				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				update_field( "qualifications_and_skills_institution3", $_POST['institution3'], $profileID );

				update_field( "qualifications_and_skills_course_name3", $_POST['coursename3'], $profileID );

				update_field( "qualifications_and_skills_qualification_type3", $_POST['qualificationtype3'], $profileID );

				update_field( "qualifications_and_skills_grade_attained3", $_POST['gradeattained3'], $profileID );

				update_field( "qualifications_and_skills_year_attained3", $_POST['yearattained3'], $profileID );



				update_field( "user_step", "7", 'user_'.$user_ID );
				wp_redirect( home_url().'/public-profile-preview/' );				
				exit;


		} elseif ($_POST['step'] == '3') {

				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				update_field( "qualifications_and_skills_special_skills1", $_POST['specialskill1'], $profileID );

				update_field( "qualifications_and_skills_special_skills2", $_POST['specialskill2'], $profileID );

				update_field( "qualifications_and_skills_special_skills3", $_POST['specialskill3'], $profileID );



				update_field( "user_step", "7", 'user_'.$user_ID );	
				wp_redirect( home_url().'/public-profile-preview/' );
				
				exit;


		} elseif ($_POST['step'] == '4') {


				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				update_field( "current_job_organisation_name", $_POST['organisationname'], $profileID );

				update_field( "current_job_industry", $_POST['industry'], $profileID );

				update_field( "current_job_company_type", $_POST['companytype'], $profileID );

				update_field( "current_job_job_function", $_POST['jobfunction'], $profileID );

				update_field( "current_job_seniority", $_POST['currentseniority'], $profileID );

				update_field( "current_job_year_started", $_POST['currentjobyearstarted'], $profileID );

				update_field( "current_job_type_of_contract", $_POST['currentjobtypeofcontract'], $profileID );

				update_field( "current_job_current_notice_period", $_POST['currentnoticeperiod'], $profileID );

				update_field( "current_job_basic_salary", $_POST['currentjobbasicsalary'], $profileID );

				update_field( "current_job_basic_salary_currency", $_POST['currentjobcurrency'], $profileID );



				update_field( "user_step", "7", 'user_'.$user_ID );	

				// Publish Basic Profile

				// Update profile arguments
				$my_profile = array(
				  'ID'           => $profileID,
				  'post_status' => 'publish'
				);

				// Update the post into the database
				wp_update_post( $my_profile );

				//sent data to mailchimp previouslu here(talent)

				// Now we also need to update the referral field of the profile. If we leave default value set by ACF than it won't be recognized by WPDB.
				$refnum = get_field( "user_invited", 'user_'.$user_ID );
				if ($refnum > 0) {
				update_field( "refferal_sort", $refnum, $profileID );	
				} else {
				update_field( "refferal_sort", 0, $profileID );	
				}

				wp_redirect( home_url().'/public-profile-preview/' ); 
				
				exit;


		} elseif ($_POST['step'] == '51') {



				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				
					update_field( "previous_experience_organisation_name1", $_POST['previousorganisationname1'], $profileID );

					update_field( "previous_experience_industry1", $_POST['previousindustry1'], $profileID );

					update_field( "previous_experience_company_type1", $_POST['previouscompanytype1'], $profileID );

					update_field( "previous_experience_job_function1", $_POST['previousjobfunction1'], $profileID );

					update_field( "previous_experience_seniority1", $_POST['previousseniority1'], $profileID );

					update_field( "previous_experience_year_started1", $_POST['previousyearstarted1'], $profileID );

					update_field( "previous_experience_year_ended1", $_POST['previousyearended1'], $profileID );

					update_field( "previous_experience_type_of_contract1", $_POST['previoustypeofcontract1'], $profileID );


					//activating skip link
					$skiplink = $_POST['skipto'];
					if($skiplink == 1) {
					update_field( "user_step", "7", 'user_'.$user_ID );
					wp_redirect( home_url().'/public-profile-preview/' ); 
					exit;	
					} else {
					update_field( "user_step", "51", 'user_'.$user_ID );
					wp_redirect( home_url().'/steps/' ); 
					exit;	
					}





		} elseif ($_POST['step'] == '52') {


				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////
				
					update_field( "previous_experience_organisation_name2", $_POST['previousorganisationname2'], $profileID );

					update_field( "previous_experience_industry2", $_POST['previousindustry2'], $profileID );

					update_field( "previous_experience_company_type2", $_POST['previouscompanytype2'], $profileID );

					update_field( "previous_experience_job_function2", $_POST['previousjobfunction2'], $profileID );

					update_field( "previous_experience_seniority2", $_POST['previousseniority2'], $profileID );

					update_field( "previous_experience_year_started2", $_POST['previousyearstarted2'], $profileID );

					update_field( "previous_experience_year_ended2", $_POST['previousyearended2'], $profileID );

					update_field( "previous_experience_type_of_contract2", $_POST['previoustypeofcontract2'], $profileID );


					update_field( "user_step", "7", 'user_'.$user_ID );	

				
				wp_redirect( home_url().'/public-profile-preview/' ); 
				exit;	


		} elseif ($_POST['step'] == '6') {


				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				// for multiselect fields we need different approach. It should be fatched as an array and then broken down.
				$tempstr = '';
				$temparr = array();
				$temparr = $_POST['idealjobfunction'];
				foreach ($temparr as $chosenone){
					$tempstr = $tempstr . $chosenone . ", ";
				}
				$tempstr = substr($tempstr, 0, -2);

				update_field( "ideal_job_job_function", $tempstr, $profileID );


				// now the regular select field
				update_field( "ideal_job_seniority", $_POST['idealseniority'], $profileID );

				// for multiselect fields we need different approach. It should be fatched as an array and then broken down.
				$tempstr = '';
				$temparr = array();
				$temparr = $_POST['idealjoblocation'];
				foreach ($temparr as $chosenone){
					$tempstr = $tempstr . $chosenone . ", ";
				}
				$tempstr = substr($tempstr, 0, -2);

				update_field( "ideal_job_location", $tempstr, $profileID );

				// for multiselect fields we need different approach. It should be fatched as an array and then broken down.
				$tempstr = '';
				$temparr = array();
				$temparr = $_POST['idealjobindustry'];
				foreach ($temparr as $chosenone){
					$tempstr = $tempstr . $chosenone . ", ";
				}
				$tempstr = substr($tempstr, 0, -2);

				update_field( "ideal_job_industry", $tempstr, $profileID );

				// for multiselect fields we need different approach. It should be fatched as an array and then broken down.
				$tempstr = '';
				$temparr = array();
				$temparr = $_POST['idealjobcompanytype'];
				foreach ($temparr as $chosenone){
					$tempstr = $tempstr . $chosenone . ", ";
				}
				$tempstr = substr($tempstr, 0, -2);

				update_field( "ideal_job_company_type", $tempstr, $profileID );

				//regular select fields
				update_field( "ideal_job_contract_type", $_POST['idealjobcontracttype'], $profileID );
				update_field( "ideal_job_basic_salary", $_POST['idealjobbasicsalary'], $profileID );
				update_field( "ideal_job_basic_salary_currency", $_POST['idealjobbasicsalarycurrency'], $profileID );


				update_field( "user_step", "3", 'user_'.$user_ID );	
				wp_redirect( home_url().'/steps/' ); 
				
				exit;


		} elseif ($_POST['step'] == '7') {



				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {

					echo "Error: Sorry, your profile was not found in the database.";
					update_field( "user_step", "0", 'user_'.$user_ID );	
				}
				wp_reset_postdata();

				////////////////// Get Profile ID - Finish ///////////////////////////////////////

				update_field( "sales_pitch_point_of_difference", $_POST['pointofdifference'], $profileID );
				update_field( "sales_pitch_personality", $_POST['personality'], $profileID );
				update_field( "sales_pitch_career_aspiration", $_POST['aspiration'], $profileID );


				update_field( "user_step", "7", 'user_'.$user_ID );	
				wp_redirect( home_url().'/public-profile-preview/' ); 
				
				exit;


		} else {}
		


	} else {

		echo "Cheating? Huh? :)";
	}
}





?>