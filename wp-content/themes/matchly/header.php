<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Matchly
 */





// This restricts unnecessary access to steps page

if(is_page('steps')) {



	if ( !is_user_logged_in() ) {

		wp_redirect( home_url()); 

		exit;

	  } else {


		get_current_user_id();

		$userRole = get_field( "user_role", 'user_'.$user_ID );

		$userRole = preg_replace( "/\r|\n/", "", strip_tags($userRole));

		$userStep = get_field( "user_step", 'user_'.$user_ID );

		$userStep = preg_replace( "/\r|\n/", "", strip_tags($userStep)); 

		if (($userRole == "recruiter") || ($userStep == 7)) {

		wp_redirect( home_url() . "/dashboard"); 				

		exit;	

		}

	  }

}

?>

<?php

// // when dashboard if visited update all jobs

// if(is_page('dashboard')) {

// # Get All Jobs By Current User

// 	$current_user_id = get_current_user_id();	
// 	$current_user_jobs_array = array();								
// 	$query = new WP_Query( 'author='.$current_user_id.'&post_type=job' );

// 	// The Loop
// 	if ( $query->have_posts() ) {

// 		while ( $query->have_posts() ) {

// 			$query->the_post();

// 			array_push($current_user_jobs_array, get_the_ID());

//  			}
// 		}
// 	 wp_reset_query();
// 	 wp_reset_postdata();

// # Update those Jobs

// 	 if(count($current_user_jobs_array) > 0) {
// 	 	foreach ($current_user_jobs_array as $job_id) {

// 	 		update_single_job_data($job_id);

// 	 	}
// 	 }

// $job_id = '';
// }

?>



<?php

if ( is_singular( 'job' ) ) {
    // If this job is a match than we need to add it to read jobs meta

    $current_job_id = $wp_query->post->ID;

# Get read jobs array

    $current_profile_id = get_current_user_profile();

    if($current_profile_id) {

			if(get_post_meta($current_profile_id, "jobs_read", true)) {

				$read_jobs = unserialize(get_post_meta($current_profile_id, "jobs_read", true));

			} else {

				$read_jobs = array();
			}

		# if is not inarray than add it

			if(!in_array($current_job_id, $read_jobs)) {

				array_push($read_jobs, $current_job_id);

				update_post_meta($current_profile_id, "jobs_read", serialize($read_jobs));

		}

    }
}

?>


<!-- Referral Funcitonality - setting session variable for reading it later -->

<?php if(isset($_GET['ref']))

{

 session_start();

$_SESSION['ref'] = $_GET['ref'];

} 




// updating the field user_log which records all login dates
if ( is_user_logged_in() ) { 

$user_ID = get_current_user_id();

$current_date = date('d.m.Y');

$current_log = get_user_meta($user_ID, "user_log", true);

// if current date is not yet recorder than we need to record it
if ( strpos($current_log, $current_date)  !== false) {
// if yes than nothing should happen
} else {

	$new_log = $current_log . $current_date . "#";

	update_user_meta($user_ID, "user_log", $new_log);

}



 }

?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>



 <meta charset="<?php bloginfo( 'charset' ); ?>"> 

<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11">


<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">



<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/menu-controller.js"></script>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap-select.min.js"></script>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-select.min.css" type="text/css"/>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap-combobox.js"></script>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap-multiselect.js"></script>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/chosen.jquery.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/alert/sweet-alert.min.js"></script> 

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/js/alert/sweet-alert.css">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,300,700' rel='stylesheet' type='text/css'>

<link href='<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/font-awesome-4.2.0/css/font-awesome.min.css">

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-combobox.css">

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-multiselect.css" type="text/css"/>

<meta property="og:title" content="Have you heard about OpenGo yet? You should check it out"/>

<meta property="og:url" content="<?php echo get_home_url(); ?>?ref=<?php echo get_current_user_id(); ?>"/>

<meta property="og:site_name" content="OpenGo"/>

<link rel='shortcut icon' href='<?php echo get_home_url(); ?>/favicon.ico' type='image/x-icon'/ >

<?php wp_head(); ?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/responsive.css" type="text/css"/>



<script>
var updateJobs = false;
var updateAllJobs = false;
var mergeSearchFields = false;
var calculateProgress = false;
</script>

<script>(function() {

  var _fbq = window._fbq || (window._fbq = []);

  if (!_fbq.loaded) {

    var fbds = document.createElement('script');

    fbds.async = true;

    fbds.src = '//connect.facebook.net/en_US/fbds.js';

    var s = document.getElementsByTagName('script')[0];

    s.parentNode.insertBefore(fbds, s);

    _fbq.loaded = true;

  }

  _fbq.push(['addPixelId', '945342705495492']);

})();

window._fbq = window._fbq || [];

window._fbq.push(['track', 'PixelInitialized', {}]);

</script>

<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=945342705495492&amp;ev=PixelInitialized" /></noscript>




<?php
// get id of the current logged in user
$user_ID = get_current_user_id();
// getting custom field "user role" of the current user
$userRole = get_field( "user_role", 'user_'.$user_ID );
//sanitizing the field
$userRole = preg_replace( "/\r|\n/", "", strip_tags($userRole));	

// now checking if the page template is "steps" AND is the user role is not set. If so that we are on role picker page. Your facebook code goes below line with "if" statement
 if ((is_page_template('steps.php')) && (!$userRole)) { ?>

<!-- Facebook Conversion Code for OpenGo LM Sign-Up -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6021219059441', {'value':'0.00','currency':'GBP'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6021219059441&amp;cd[value]=0.00&amp;cd[currency]=GBP&amp;noscript=1" /></noscript>
 <?php } ?>


 <?php
//adding facebook pixel for congratulations pages
  if ((is_page_template('congratulations-talent.php')) || (is_page_template('congratulations-recruiter.php'))) { 
 	if (is_page_template('congratulations-recruiter.php')) { 
 	// placing code for the recruiter 
 		?>

<!-- Facebook Conversion Code for Recruiter - Congratulations -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6021328175241', {'value':'0.00','currency':'GBP'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6021328175241&amp;cd[value]=0.00&amp;cd[currency]=GBP&amp;noscript=1" /></noscript>


 	<?php } else { 
 	// placing code for talent and both
 		?>

 <!-- Facebook Conversion Code for Talent - Congratulations -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6021328162841', {'value':'0.00','currency':'GBP'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6021328162841&amp;cd[value]=0.00&amp;cd[currency]=GBP&amp;noscript=1" /></noscript>


 <?php }} ?>


<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-61069280-1', 'auto');
ga('send', 'pageview');

</script>


</head>

<body <?php body_class(); ?>>



<div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">

		<div class="container container-fluid">

		<div class="row">

		<div class="site-branding">

			<a href="/"><img src="<?php the_field('logo', 'option'); ?>"></a>

		</div><!-- .site-branding -->

	<?php if(is_user_logged_in()) { ?>



         <div class="logged-in-menu">

         	<span>Welcome </span><?php $current_user = wp_get_current_user(); echo $current_user->display_name; ?><?php if(get_profile_by_id(get_current_user_id())) { generate_unread_jobs_badge(); } ?>  <i class="fa fa-chevron-down"></i>

		<?php $user_ID = get_current_user_id(); 



		$userRole = get_field( "user_role", 'user_'.$user_ID );

		$userRole = strip_tags($userRole);

		$userStep = get_field( "user_step", 'user_'.$user_ID );

		$userStep = strip_tags($userStep); 



				$prID = 0;

				// Get user profile page

				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop

				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$prID = get_the_ID();

						if ($prID > 0) {

							$Profile_link = get_permalink();

						}

					}



				} else {

				
				}

				wp_reset_postdata(); ?>



         	<ul class="li-menu-container">
				
				<?php if(strlen($userRole) > 2) { ?>

					<li><a href="/dashboard"><i class="fa fa-area-chart"></i>Dashboard <?php if(get_profile_by_id(get_current_user_id())) { generate_unread_jobs_badge(); } ?> </a></li>

				<?php } ?>

         		

         		<?php if(get_profile_by_id(get_current_user_id())) { ?>

         		<li><a href="<?php echo $Profile_link; ?>"><i class="fa fa-user"></i>My Profile</a></li>

         		<li><a href="/edit-my-profile/"><i class="fa fa-pencil-square-o"></i>Edit My Profile</a></li>

				<?php } ?>

         		<li><a href="/search"><i class="fa fa-search"></i>Search For Talent</a></li>

         		<li><a href="/settings"><i class="fa fa-cog"></i>Settings</a></li>

         		<li><a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-lock"></i>Log Out</a></li>

         	</ul>

         </div>



	<?php } else { ?>

		<div class="sign-in-sign-up">

			<a href="/">Sign Up</a>

			<a href="/">Sign In</a>

		</div>

		<?php } ?>

	

	</div>

	</div>

	</header><!-- #masthead -->



	<div id="content" class="site-content">

