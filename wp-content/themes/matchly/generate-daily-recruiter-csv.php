<?php 
// Include WP files so our controller can actually work with WP
 
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

global $wpdb;

function generatecsv() {
  global $wpdb;

     header("Content-type: text/csv");
      header("Content-Disposition: attachment; filename=Daily-Recruiter-CSV.csv");
      header("Pragma: no-cache");
      header("Expires: 0");

      //Functions for gethering data


function how_many_recruiters_do_i_have() {
  global  $wp_query;

  $recruiters = count( get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'recruiter' ) ) );

  $both = count( get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) );

  return $recruiters + $both;
}


function how_many_recruiters_joined_yesterday($date='') {

    global $wpdb;

    if( empty($date) )

    $date = date('Y-m-d');

    $morning = new DateTime($date . ' 00:00:00');

    $night = new DateTime($date . ' 23:59:59'); 


    $m = $morning->format('Y-m-d H:i:s');

    $n = $night->format('Y-m-d H:i:s');


    $sql = $wpdb->prepare("SELECT og_users.* FROM og_users WHERE 1=1 AND CAST(user_registered AS DATE) BETWEEN %s AND %s ORDER BY user_login ASC",$m,$n);

    $users = $wpdb->get_results($sql);

    $recruiter_counter = 0;

    foreach ($users as $user) {

      if ( (get_user_meta($user->ID, 'user_role', true) == 'recruiter') || (get_user_meta($user->ID, 'user_role', true) == 'both') ) {

      $recruiter_counter++;
    }

    }
return $recruiter_counter;
 
}

function how_many_times_did_recruiters_search() {

  global  $wp_query;

  $recruiters = array();

  $both = array();

  $recruiters = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'recruiter' ) ) ;

  $both = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) ;

  $merged_recruiters_array = array_merge($recruiters, $both);

    $search_counter = 0;

  foreach ($merged_recruiters_array as $user_object) {

    $search_counter = $search_counter + get_user_meta($user_object->ID, "search_number", true);

  }

return $search_counter;

}

function how_many_times_recruiter_sent_email() {

  global  $wp_query;

  $recruiters = array();

  $both = array();

  $recruiters = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'recruiter' ) ) ;

  $both = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) ;

  $merged_recruiters_array = array_merge($recruiters, $both);

  $mail_counter = 0;

  foreach ($merged_recruiters_array as $user_object) {

    if(strip_tags( get_field( "sent_email_to", 'user_'.$user_object->ID )))
          { 
              $emailsreceived = preg_replace( "/\r|\n/", "", strip_tags( get_field( "sent_email_to", 'user_'.$user_object->ID )));
              $emailreceivedarray = explode(",", $emailsreceived);
              $emailsnumber = count($emailreceivedarray);
              $mail_counter = $mail_counter + ($emailsnumber-1);
          }
    }

    return $mail_counter;
  }


function how_many_times_anyone_sent_email() {

  global  $wp_query;

  $recruiters = array();

  $both = array();

  $talent = array();

  $recruiters = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'recruiter' ) ) ;

  $both = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) ;

  $talent = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'talent' ) ) ;

  $merged_recruiters_array = array_merge($recruiters, $both, $talent);

  $mail_counter = 0;

  foreach ($merged_recruiters_array as $user_object) {

    if(strip_tags( get_field( "sent_email_to", 'user_'.$user_object->ID )))
          { 
              $emailsreceived = preg_replace( "/\r|\n/", "", strip_tags( get_field( "sent_email_to", 'user_'.$user_object->ID )));
              $emailreceivedarray = explode(",", $emailsreceived);
              $emailsnumber = count($emailreceivedarray);
              $mail_counter = $mail_counter + ($emailsnumber-1);
          }
    }

    return $mail_counter;
  }

function how_many_recruiters_logged_in_this_day($date='') {


  global  $wp_query;

  $recruiters = array();

  $both = array();

  $recruiters = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'recruiter' ) ) ;

  $both = get_users( array( 'meta_key' => 'user_role', 'meta_value' => 'both' ) ) ;

  $merged_recruiters_array = array_merge($recruiters, $both);

  $counter = 0;

  foreach ($merged_recruiters_array as $user_object) {

    if(get_user_meta($user_object->ID, "user_log", true)) {

      if ( strpos(get_user_meta($user_object->ID, "user_log", true), $date)  !== false) {
        $counter++;
      }
    }
  }

return $counter;
}


      //open stream  
      $file = fopen('php://output', 'w');  

      // Add Headers                            
      fputcsv($file, array(
        'Date',
        'How many recruiters I have',
        'How many new recruiters joined today',
        'How many searches the recruiters did',
        'How many times recruiters contacted talent',
        'How many anyone contacted talent',
        'How many recruiters logged into the system'

        )); 

$data = array();

$offset = 30;

$counter = 0;
      while ($counter < $offset ) {
        $counter++;

        $profileRow = array(
            date('d.m.Y',strtotime("-".$counter." days")),
            how_many_recruiters_do_i_have(),
            how_many_recruiters_joined_yesterday( date('Y-m-d',strtotime("-".$counter." days"))),
            how_many_times_did_recruiters_search(),
            how_many_times_recruiter_sent_email(),
            how_many_times_anyone_sent_email(),
            how_many_recruiters_logged_in_this_day(date('d.m.Y',strtotime("-".$counter." days")))



          );
        array_push($data, $profileRow);
      }


     foreach ($data as $row) {

            fputcsv($file, $row);   

     }

    
      exit(); 
}

generatecsv();

?>