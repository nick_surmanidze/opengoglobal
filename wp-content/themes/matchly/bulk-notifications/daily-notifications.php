<?php
$f = fopen('lock', 'w') or die ('Cannot create lock file');
if (flock($f, LOCK_EX | LOCK_NB)) {
 
// Runs forever and ever...
set_time_limit(-1);
ignore_user_abort(1);
ini_set('memory_limit', '4000M');
ini_set('post_max_size', '1024M');
ini_set('upload_max_filesize', '1024M');


// Include WP files so our controller can actually work with WP
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
///
///    Function For Sending Notifications to all users
///
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
//Solving the problem with interruptions
// Introducting offset
// Offset will be stored in options field




	$offset_option = get_option("daily_notification_offset");

	if($offset_option > 0) {
		send_daily_notifications($offset_option);	
	} else {
		send_daily_notifications(0);	
	}

}
function set_html_content_type() {
	return 'text/html';
}


function update_log($file, $message) {

	$message = $message . "\r\n";

	file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
}

function send_daily_notifications($offset) {

	$allusers = '';

	if($offset == 0) {
	update_log('daily-log.txt', "\r\n");
	update_log('daily-log.txt', "############################################");
	update_log('daily-log.txt', "##### Started At ##### ".date('m/d/Y h:i:s a', time()));
	update_log('daily-log.txt', "##### Sending daily notifications #####");	
	} else {
	update_log('daily-log.txt', "##### Resuming sending. Offset:" . $offset);		
	}


	// Array of WP_User objects.
	$allusers = get_users();

	// flush variable
	$frequency = '';
	$total_number_of_recipients = 0;
	$pre_delay_counter = 0;

		foreach ($allusers as $user) {	

			// This is kind of a red alert button for stoping script execution
			if (file_exists('stop.txt')) {
			    die();
			}



			if((get_user_meta($user->ID, "notification_frequency", true) == 'daily') || ((!get_user_meta($user->ID, "notification_frequency", true) == 'weekly') && (!get_user_meta($user->ID, "notification_frequency", true) == 'never'))) {

				// Now we need to check if this user has unread messages
				// AND check if the user has unread jobs

				// flush
				$unread_messages = 0;
				$unread_jobs     = 0;
				$message = '';
				
				$to = $user->user_email;

				if (get_profile_by_id($user->ID)) {
					$to = sanitize_text_field(get_post_meta(get_profile_by_id($user->ID), "about_you_email_address", true));
				}

				// Get Actual Figure of unread messages
				$unread_messages = get_number_of_new_messages_by_id($user->ID);
				$unread_jobs     = generate_unread_jobs_by_id($user->ID);

				// Now we need to compose the message

				if(($unread_jobs > 0) || ($unread_messages > 0)) {	

					$total_number_of_recipients ++; 

					if ($total_number_of_recipients > $offset) {

					add_filter( 'wp_mail_content_type', 'set_html_content_type' );

					$message = "Hi ". $user->display_name . ",<br><br>";
					$message = $message . "Great news! A recruiter on OpenGo wants to connect with you.";
					$message = $message . "You Have " . $unread_messages . " message(s)";
					$message = $message . " and " . $unread_jobs . " job match(es). <br> <br>";
					$message = $message . "<a href='http://opengoglobal.com/'>Click here to visit your dashboard.</a><br><br>";
					$message = $message . "Best wishes,<br>";
					$message = $message . "Ian Hallett from OpenGo<br><br><br>";
					$message = $message . "P.S. Get more job opportunities from OpenGo by increasing the quality of your profile (you will show up in more recruiter searches if you do this). You can do this by <a href='http://opengoglobal.com/dashboard'>editing your profile</a> and completing all sections in the template.<br><br>";
					$message = $message . "This is an automated email. You can unsubscribe from further email notifications <a href='http://opengoglobal.com/settings'>here</a>.";
					// Now send an email.

					$headers = 'From: "OpenGo" <ian@opengoglobal.com><br>';
					$sent = wp_mail($to, 'A Recruiter Wants To Connect With You', $message, $headers);

					// Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
					remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

					update_option( "daily_notification_offset", $total_number_of_recipients );

					if($sent) {
					#update log
					update_log('daily-log.txt', "(".$total_number_of_recipients.")Sent to: (".$user->display_name. ") " . $to . " ( j: ".$unread_jobs." m: " .$unread_messages." )" );
					
					} else {
					update_log('daily-log.txt', "(".$total_number_of_recipients.")Not Sent: (".$user->display_name. ") " . $to . " ( j: ".$unread_jobs." m: " .$unread_messages." )" );
					}
					
					

					

					### if more than 2 messages had been sent than let's make a delay of 5 seconds
					$pre_delay_counter ++;
					if ($pre_delay_counter > 1) {
						if ($total_number_of_recipients > $offset) {
						sleep ( 4 );
						}
						$pre_delay_counter = 0;
					}



					}
				
					
				} 	
			}


		} // endforeach



	update_log('daily-log.txt', "##### Finished At ##### ".date('m/d/Y h:i:s a', time()));
	update_log('daily-log.txt', "Sent to: " . $total_number_of_recipients . " recipients.");
	update_log('daily-log.txt', "############################################");
	update_log('daily-log.txt', "\r\n");

	// Once finished we need to update option and set it to 0

	$total_number_of_recipients = 0;

	update_option( "daily_notification_offset", $total_number_of_recipients );

   
} // end of function


