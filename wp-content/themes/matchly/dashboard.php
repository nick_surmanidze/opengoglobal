<?php
/*
Template Name: Dashboard
/**
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		<?php } else { ?>
			<main id="main" class="site-main" role="main">
		<?php } ?>
		

<?php while ( have_posts() ) : the_post(); ?>
	<div class="container container-fluid">
		<div class="row content-wrapper">							
	<?php
	////////////////// Get Profile ID - Start ////////////////////////////////////////
	$user_ID = get_current_user_id();
	$profileID = 0;
	// Get user profile page
	$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );
	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$profileID = get_the_ID();
		}
	} else {					
	}
	wp_reset_postdata();
	////////////////// Get Profile ID - Finish ///////////////////////////////////////

		$user_ID = get_current_user_id(); 
		$userRole = get_field( "user_role", 'user_'.$user_ID );
		$userRole = preg_replace( "/\r|\n/", "", strip_tags($userRole));


	if ($userRole == "talent") {
		get_template_part( '/dashboard/talent-dashboard-v-2'); 
	} elseif ($userRole == "recruiter") {
		get_template_part( '/dashboard/recruiter-dashboard-v-2');
	} elseif ($userRole == "both") {
		get_template_part( '/dashboard/both-dashboard-v-2');
	} else {					
		echo $userRole . "<div class='page-content-wrapper'>Oops!  You need to sign in to access this page.  Please sign to continue using OpenGo.</div>"; 
	} ?> 
				
		</div>
	</div>	
<?php endwhile; // end of the loop. ?>
</main><!-- #main -->
</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
