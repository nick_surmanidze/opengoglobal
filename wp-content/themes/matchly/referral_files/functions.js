// leadpages_input_data variables come from the template.json "variables" section
var leadpages_input_data = {};

$(function () {

    // a simple pop-up function for social sharings windows
    function popup(h, n) {
        var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            url = h,
            opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;

        window.open(h, n, opts);
    }

    // set the HREF for the Facebook share button using the "facebookurl"
    // passed from leadpages_input_data
    $('.pop-facebook').attr(
        'href',
        "https://www.facebook.com/sharer/sharer.php?u=" + leadpages_input_data["facebookurl"]
    );

    // attach "click" event handler to the social buttons
    $('.pop-facebook').click(function (event) {
        popup(this.href, this.id);
        return false;
    });
});

/**
 * The following is a hack to get the builder to dynamically update the DOM to display the background-image
 * which is being pulled in from a hidden area as a hack to display background-image
 */
$(function(){


    /**
     * Updates the DOM with the backround-image
     */
    function updatePageForBgImg(){
        //copy image background src and place into the container with background size set to cover
        //image is set to 1px width and height to be reference and not shown
        $('#my-banner').css('background-image', 'url('+$("#my-banner-img").attr("src")+')').css('background-size' , 'cover').css('background-position' , 'top center');
    }


    /* Either run the DOM update functions once for a published page or continuously for within the builder. */
    if ( typeof window.top.App === 'undefined' ) {
        // Published page
        $(window).on('load', function(){
            updatePageForBgImg();
        });
    } else {
        // within the builder
        setInterval( function(){
            updatePageForBgImg();
        }, 500);
    }
});


