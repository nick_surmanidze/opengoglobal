<?php
/*
Template Name: Landing Page
*/
/**
 * The template for displaying Landing Pages.
 *
 * @package Matchly
 */


?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,300,700' rel='stylesheet' type='text/css'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css' rel='stylesheet' type='text/css'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-theme.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/font-awesome-4.2.0/css/font-awesome.min.css">
<?php wp_head(); ?>

<!-- Referral Funcitonality - setting the cookie for reading it later -->
<?php if(isset($_GET['ref']))
{
 session_start();
$_SESSION['ref'] = $_GET['ref'];
} ?>


</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

			<?php while ( have_posts() ) : the_post(); ?>
				
				<div class="landing-wrapper" style="width:100%;overflow-x: hidden;">
					
					<?php the_content(); ?>

				</div>


			<?php endwhile; // end of the loop. ?>

</div><!-- #page -->
<?php get_sidebar(); ?>
<?php wp_footer(); ?>

</body>
</html>

