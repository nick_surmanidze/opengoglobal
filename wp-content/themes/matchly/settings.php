<?php
/*
Template Name: Settings
/**
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		<?php } else { ?>
			<main id="main" class="site-main" role="main">
		<?php } ?>
		

<?php while ( have_posts() ) : the_post(); ?>
	<div class="container container-fluid">
		<div class="row content-wrapper">							
			<div class="inner-wrapper">
				<div class="inner-title"><i class="fa fa-cog"></i>  Settings</div>

				<?php if(is_user_logged_in()) { ?>
					<div class="settings-wrapper">


						<div class="form-horizontal settings-fields">
							
								<div class="form-group">
									<label class="col-sm-6 control-label">Update display name</label>
									<div class="col-sm-6">
										<input type="text" value="<?php $current_user = wp_get_current_user(); echo $current_user->display_name; ?>" class="form-control" id="display-name"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-6 control-label">Set the frequency of email notifications</label>
		
									<div class="col-sm-6">
										<select id="notification-frequency" class="form-control">
											<option value="daily" <?php if(get_user_meta(get_current_user_id(), "notification_frequency", true) == 'daily') { echo "selected"; } ?>>Notify me daily</option>
											<option value="weekly" <?php if(get_user_meta(get_current_user_id(), "notification_frequency", true) == 'weekly') { echo "selected"; } ?>>Notify me weekly</option>
											<option value="never" <?php if(get_user_meta(get_current_user_id(), "notification_frequency", true) == 'never') { echo "selected"; } ?>>Do not send me email notifications</option>
										</select>
									</div>
								</div>


								<div class="form-group">
									<label class="col-sm-6 control-label">Subscription to Smarter Job Notifications</label>
		
									<div class="col-sm-6">
										<select id="smarter-notifications" class="form-control">
											<option value="enabled" <?php if(get_user_meta(get_current_user_id(), "smarter_notifications", true) != 'disabled') { echo "selected"; } ?>>Enabled</option>
											<option value="disabled" <?php if(get_user_meta(get_current_user_id(), "smarter_notifications", true) == 'disabled') { echo "selected"; } ?>>Disabled</option>
										</select>
									</div>
								</div>


						</div>

						<div class="footer-wrap">
							<img class="updating-settings" src="<?php echo get_template_directory_uri(); ?>/images/spinner-white.GIF" alt="loading.." >
							<button class="btn btn-default update-settings">Update Settings</button>
						</div>



					</div>

					<?php } else { echo "<span style='display:inline-block; padding:15px;'>You need to be logged in to access this page!</span>"; } ?>
				</div>
			</div>	
		</div>
	</div>	

	<script>

	$( document ).ready(function() {
    
    //if not starred than star
	$( document ).on("click", ".update-settings", function(){

		var displayName = $('#display-name').val();
		var frequency   = $('#notification-frequency').val();
		var smarterNotifications   = $('#smarter-notifications').val();

		$('.updating-settings').css("display", "inline-block");

			$.ajax({
	        data: ({
	        		action : 'update_matchly_settings',
	        	 'displayname': displayName,
	        	 'frequency': frequency,
	        	 'smarternotifications': smarterNotifications
	     			}),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) { 
	      		console.log(msg);
	      		$('.updating-settings').css("display", "none");
	      		window.location.href = "<?php echo site_url(); ?>/dashboard";
	      });

		console.log(displayName);
		console.log(frequency);
		
	});


	});
	</script>


<?php endwhile; // end of the loop. ?>
</main><!-- #main -->
</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
