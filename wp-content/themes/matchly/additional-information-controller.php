<?php

// Include WP files so our controller can actually work with WP
 
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');


if(isset($_GET['step'])) {

	$user_ID = get_current_user_id(); 


	if($_GET['step'] == "back") {

		update_field( "user_step", "7", 'user_'.$user_ID );
		wp_redirect( home_url().'/public-profile-preview/' ); 
		exit;
		// This code returns us back to preview page
	}


	if($_GET['step'] == "add-previous-job") {

		update_field( "user_step", "4", 'user_'.$user_ID );
		wp_redirect( home_url().'/steps/' ); 
		exit;
		// changed step to 4 and redirected to steps
	}

		if($_GET['step'] == "specialist-skills") {

		update_field( "user_step", "23", 'user_'.$user_ID );
		wp_redirect( home_url().'/steps/' ); 
		exit;
		// changed step to 23 and redirected to steps
	}

		if($_GET['step'] == "add-personal-statement") {

		update_field( "user_step", "6", 'user_'.$user_ID );
		wp_redirect( home_url().'/steps/' ); 
		exit;
		// changed step to 6 and redirected to steps
	}


		if($_GET['step'] == "add-qualifications") {

		update_field( "user_step", "1", 'user_'.$user_ID );
		wp_redirect( home_url().'/steps/' ); 
		exit;
		// changed step to 1 and redirected to steps
	}



} else {

wp_redirect( home_url().'/public-profile-preview/' ); 
// if step is not set redirecting back to public preview page.
exit;

}







