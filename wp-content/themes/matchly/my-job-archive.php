<?php
/*
Template Name: My Job Archive
/**
 * The template for displaying all single Jobs.
 *
 * @package Matchly
 */

get_header(); 
?>

	<div id="primary" class="content-area">
	<?php $backgroundimageurl = get_field("background_image", "options"); if(isset($backgroundimageurl)) { ?>
		<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
	<?php } else { ?>
		<main id="main" class="site-main" role="main">	
		<?php } ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="container container-fluid">
				<div class="row content-wrapper">
					<div class="page-content-wrapper no-padding">
						<div class="content-inner">

							<div class="general-page-header">Recruitment Management</div>

							<div class="items-wrapper rec-management">

								<?php 
								$jobs_array = get_my_jobs_array();
								if(count($jobs_array) > 0) { ?>


								<div class="item-line head-line">
									<div class="item-title">
										<span class="sort-item" data-factor="title" title="Job Title">Job Title</span>
										<input type="text" id="title-search" class="search-input" placeholder="Search..">
									</div>
									<div class="item-employer">
										<span class="sort-item" data-factor="job_employer" title="Employer">Employer</span>
										<input type="text" id="employer-search" class="search-input" placeholder="Search..">
									</div>
									<div class="item-tag">
										<span class="sort-item" data-factor="job_number_of_matches" title="Number Of Candidates That Match Requirements">Matched</span>
										<input type="text" id="match-search" class="search-input" placeholder="Search..">
									</div>
									<div class="item-tag center">
										<span data-factor="interest" title="Candidates That Have Registered Interest">Interested</span>
										
									</div>

									<div class="item-tag center">
										<span data-factor="tbr" title="Candidates To Be Reviewed">To Be Reviewed</span>
										
									</div>

									<div class="item-tag center">
										<span data-factor="contacted" title="Candidates Contacted">Contacted</span>
										
									</div>
									
									<div class="item-tag center">
										<span data-factor="rejected" title="Candidates Rejected">Rejected</span>
										
									</div>
								</div>

								




								<?php

									global $wpdb;

	$count_results = '';
	$results_html = '';
	$pagination_html = '';

	// we are sorting by meta $sortfactor and define sorting direction
	$sort_factor = '';
	$sort_direction = '';
	$direction_str = '';

	//now let's define query variables
	$queryfrom = '';
	$querywhere = '';
	$queryparameters = '';

	$qstring = '';

	$title = '';
	$employer = '';
	$matched = '';
	$applied = '';
	$tbr = '';
	$contacted = '';
	$rejected = '';

	// now get the data and assign to varibales

	if(isset($_POST['title'])) {

	$title = sanitize_text_field($_POST['title']);

	}

	if(isset($_POST['employer'])) {

	$employer = sanitize_text_field($_POST['employer']);

	}

	if(isset($_POST['matched'])) {

	$matched = sanitize_text_field($_POST['matched']);

	}


	if(isset($_POST['applied'])) {

	$applied = sanitize_text_field($_POST['applied']);

	}

	if(isset($_POST['tbr'])) {

	$tbr = sanitize_text_field($_POST['tbr']);

	}

	if(isset($_POST['contacted'])) {

	$contacted = sanitize_text_field($_POST['contacted']);

	}

	if(isset($_POST['rejected'])) {

	$rejected = sanitize_text_field($_POST['rejected']);

	}


	if(isset($_POST['sortfactor'])) {

	$sort_factor = sanitize_text_field($_POST['sortfactor']);

	}

	if(isset($_POST['sortdirection'])) {

		if(sanitize_text_field($_POST['sortdirection']) == "A") {

			$sort_direction = "ASC";

		} else {

			$sort_direction = "DESC";
		}

	$sort_direction = sanitize_text_field($_POST['sortdirection']);

	}

	// now we need to architect the complex query

	if ($title) {

	$qstring = '';
	$param = $wpdb->esc_like( $title );
	$qstring = $qstring . "(" . $wpdb->posts . ".post_title LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	}

	if ($employer) {

		$qstring = '';
	$param = $wpdb->esc_like( $employer);
	$qstring = $qstring . "(nsds1.meta_key = 'job_employer' AND nsds1.meta_value LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsds1";
	$querywhere = $querywhere . "AND $wpdb->posts.ID = nsds1.post_id ";
	}


	if ($matched) {

		$qstring = '';
	$param = $wpdb->esc_like($matched);
	$qstring = $qstring . "(nsds2.meta_key = 'job_number_of_matches' AND nsds2.meta_value LIKE '%".$param."%') OR ";

	$qstring = substr($qstring, 0, -3);
	$queryparameters = $queryparameters . " AND (" . $qstring . ")";

	$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsds2";
	$querywhere = $querywhere . "AND $wpdb->posts.ID = nsds2.post_id ";
	}



	if ($sort_factor && $sort_direction) {
			$qstring = '';

		if ($sort_factor == "title") {

			$direction_str = " ORDER BY " . $wpdb->posts . ".post_title ".$sort_direction."";

		} else {

			$qstring = $qstring . " AND sortfactor.meta_key = '".$sort_factor."'";
			$queryparameters = $queryparameters .  $qstring;

			$direction_str = " ORDER BY CAST(sortfactor.meta_value as SIGNED INTEGER) ".$sort_direction."";

			$queryfrom = $queryfrom . ", $wpdb->postmeta AS sortfactor";
			$querywhere = $querywhere . "AND $wpdb->posts.ID = sortfactor.post_id ";

		}


	}


	$querystr = "
   SELECT DISTINCT
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
       ".$queryfrom."
    WHERE       
        $wpdb->posts.post_type = 'job' 
        AND $wpdb->posts.post_author = '" . get_current_user_id() . "'
        AND $wpdb->posts.post_status = 'publish'
        
        ".$querywhere."
        
        ".$queryparameters." ".$direction_str."";



	$item_id_array = array();

	$item_id_array = $wpdb->get_col($querystr, 0);

	// if the custom order is not specified than we need to reverse the order to naturally descending.

	if(!$sort_factor) {
	$item_id_array = array_reverse ($item_id_array);
	}


	// Not it is time to take care of pagination

	$pagination = '';

	// Change this figure to output different number per page
	$item_per_page = 10;


	if(isset($_POST['current-page'])) {

	$current_page = sanitize_text_field($_POST['current-page']);

	} else {

	$current_page = 1;	

	}
	
	

	$offset = ($current_page - 1) * $item_per_page;
	$full_length = count($item_id_array);

	$number_of_pages = ceil($full_length / $item_per_page);


	if($number_of_pages > 1) {
		$first_page = '';
		$last_page = '';
		$before_current ='';
		$current_pg = '';
		$after_current ='';

		if($current_page != 1) {
		$first_page = "<li>1</li>";	
		}
		

		if(($current_page - 1) == 3)
		{

			$before_current = " <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

		} elseif(($current_page - 1) == 2)  {

			$before_current = " <li>".($current_page-1)."</li>";

		} elseif(($current_page - 1) == 1)  {

			$before_current = "";

		} elseif(($current_page - 1) == 0)  {

			$before_current = "";

		}	elseif(($current_page - 1) > 3)  {

			$before_current = "<li>".($current_page-3)."</li> <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

		}


		$current_pg = "<li class='current-page'>".$current_page."</li>";


		if((($number_of_pages - $current_page) == 3) || (($number_of_pages - $current_page) > 3)) {

			$after_current = "<li class='aft'>".($current_page+1)."</li> <li class='aft'>".($current_page+2)."</li> ";

		} elseif(($number_of_pages - $current_page) == 2) {

			$after_current = "<li class='aft'>".($current_page+1)."</li> ";

		}  elseif(($number_of_pages - $current_page) == 1) {

			$after_current = "";

		} else {

			$after_current = '';
		}
		if(!($current_page == $number_of_pages)) {
			$last_page = "<li>".$number_of_pages."</li>";
		}
		
		$pagination_html = $first_page . $before_current . $current_pg . $after_current . $last_page;
	}


	$item_id_array = array_slice($item_id_array, $offset, $item_per_page);




	$results_html = '';

	foreach($item_id_array as $item_id) {

	$c_contacted = count(unserialize(sanitize_text_field(get_post_meta($item_id, "job_candidates_contacted", true))));
	$c_rejected = count(unserialize(sanitize_text_field(get_post_meta($item_id, "job_candidates_rejected", true))));
	$c_total = sanitize_text_field(get_post_meta($item_id, "job_number_of_matches", true));

	$to_be_deducted_array = array_unique(array_merge(unserialize(sanitize_text_field(get_post_meta($item_id, "job_candidates_contacted", true))), unserialize(sanitize_text_field(get_post_meta($item_id, "job_candidates_rejected", true)))));

	$c_to_be_reviewed = $c_total - count($to_be_deducted_array);

	$results_html .= '<div class="item-line">';
	$results_html .= '<div class="item-title"><span><a href="'.get_permalink($item_id).'">'.get_the_title($item_id).'</a></span></div>';
	$results_html .= '<div class="item-employer"><span>'.sanitize_text_field(get_post_meta($item_id, "job_employer", true)).'</span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?all">'.$c_total.'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?interested">'.count(unserialize(sanitize_text_field(get_post_meta($item_id, "job_registered_interest", true)))).'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?tbr">'.$c_to_be_reviewed.'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?contacted">'.$c_contacted.'</a></span></div>';
	$results_html .= '<div class="item-tag"><span><a href="'.get_permalink($item_id).'?rejected">'.$c_rejected.'</a></span></div>';
	$results_html .= '</div>';

	}


echo $results_html;
								// echo the results here

								 } else {

									echo "<span class='no-talents-yet'>You have no recruitment activities yet.</span>";
								} ?>
							

							</div>

								<div class="action-button">
									<ul class="pagination">
										<?php echo $pagination_html; ?>
									</ul>
									<div class="count-results"><div class="count"><?php echo count($jobs_array) ?></div>Result(s)</div>
									<img class="ajax-spinner" src="<?php echo get_stylesheet_directory_uri(); ?>/images/spinner-white.GIF" alt="loading">
								</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<script>

function equalizeLines() {

	$(".item-line").each(function(index, value) {	


			  var maxHeight = -1;

				$(this).find("div").each(function() {
			   		$(this).css('height', "auto");
			   	});
			   $(this).find("div").each(function() {
			     maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
			   });



			   $(this).find("div").each(function() {
			     $(this).height(maxHeight);
			   });		

	});

}

$(document).ready(function() {

equalizeLines();

// run the function on window resize
$(window).bind('resize', function () { 
	equalizeLines();
});


});

//-----------------------------------
// Change Filter State


 $(document).on("click", '.item-line.head-line span.sort-item',  function(){ 


if($(this).hasClass("asc")) {

		$(".item-line.head-line span.sort-item").each(function(index, value) {	
			$(this).removeClass("asc").removeClass("desc"); 
	});

	$(this).removeClass("asc").addClass("desc");

} else if ($(this).hasClass("desc")) {

		$(".item-line.head-line span.sort-item").each(function(index, value) {	
			$(this).removeClass("asc").removeClass("desc"); 
	});

	$(this).removeClass("asc").removeClass("desc"); 

} else {

		$(".item-line.head-line span.sort-item").each(function(index, value) {	
			$(this).removeClass("asc").removeClass("desc"); 
	});

	$(this).addClass("asc");

}

 

 });

// Change Filter State
//-----------------------------------


//-----------------------------------
// Trigger for sorting



function get_filter_results(page) {

$(".action-button .ajax-spinner").show();

var title     = '';
var employer    = '';
var matched    = '';
var applied  = '';
var tbr  = '';
var contacted  = '';
var rejected  = '';


var sortfactor = '';
var direction = '';

if(page) {
	var curPage = page;
} else {
	curPage = 1;
}


title     = $('#title-search').val();
employer  = $('#employer-search').val();
matched   = $('#match-search').val();
applied   = $('#interest-search').val();
tbr       = $('#tbr-search').val();
contacted = $('#contacted-search').val();
rejected  = $('#rejected-search').val();


$(".item-line.head-line span.sort-item").each(function(index, value) {	

	if($(this).hasClass("asc")) {

	sortfactor = $(this).attr('data-factor');
	direction = "ASC";

	} else if ($(this).hasClass("desc")) {

	sortfactor = $(this).attr('data-factor');
	direction = "DESC";

	}

});


 $.ajax({
    data: 
        ({
        	action : 'sort_jobs',
        	'sortfactor': sortfactor,
        	'sortdirection': direction,
        	'current-page' : curPage,

        	'title': title,
        	'employer': employer,
        	'matched': matched,
        	'applied': applied,
        	'tbr':tbr,
        	'contacted':contacted,
        	'rejected':rejected
        }),

    type: 'POST',
    async: true,     
    url: ajaxurl,
  }).done(function( msg ) {

		console.log(msg);
		var output = JSON.parse(msg);

		$(".item-line").each(function(index, value) {	

			if(!$(this).hasClass("head-line")) {

				$(this).remove();

			} 

		});


		$(".item-line.head-line").after(output.results);

		$(".pagination").html(output.pagination);

		$(".count-results .count").html(output.number);

		equalizeLines();

		$(".action-button .ajax-spinner").hide();

	});

}


$( ".search-input" ).keyup(function() {
  get_filter_results();
});

 $(document).on("click", '.item-line.head-line span.sort-item',  function(){ 

	get_filter_results();

 });


// now the pagination
   $(document).on("click", '.pagination > li',  function(){ 

   	$(".pagination > li").each(function(index, value) {	

		$(this).removeClass("current-page");

	});

	$(this).addClass("current-page");

 	var curpage = $(this).html();
 	console.log(curpage);
	get_filter_results(curpage);

 });

// trigger for sorting
//-----------------------------------




</script>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
