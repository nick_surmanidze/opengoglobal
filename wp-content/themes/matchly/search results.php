<?php

/*

Template Name: Search results

/**

 * The template for displaying all pages.

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site will use a

 * different template.

 *

 * @package Matchly

 */



get_header(); ?>



<div id="primary" class="content-area">

			<?php $backgroundimageurl = get_field("background_image", "options");



		 if(isset($backgroundimageurl)) { ?>

		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">

		 <?php } else { ?>



		<main id="main" class="site-main" role="main">

			

		<?php } ?>


		<div class="container container-fluid">

			<div class="row content-wrapper">


				<div class="search-results-wrapper">



					<?php 

					//starting to write query string generator





if ((isset($_POST['search'])) && $_POST['search'] = 1) { // checking if we here just after the search page

global $wpdb;

$_SESSION["search_query"] = serialize($_POST);


$querystr = "
   SELECT DISTINCT
      $wpdb->posts.ID      
    FROM
        $wpdb->posts
    WHERE       
        $wpdb->posts.post_type = 'profile' 
        AND $wpdb->posts.post_status = 'publish'
        ";
         
$pageposts = $wpdb->get_col($querystr, 0);

    $profileidarray1 = $pageposts;
    $profileidarray2 = $pageposts;
    $profileidarray3 = $pageposts;
    $profileidarray4 = $pageposts;
    $profileidarray5 = $pageposts;
    $profileidarray6 = $pageposts;
    $profileidarray7 = $pageposts;
    $profileidarray8 = $pageposts;
    $profileidarray9 = $pageposts;
    $profileidarray10 = $pageposts;
    $profileidarray11 = $pageposts;
    $profileidarray12 = $pageposts;
    $profileidarray13 = $pageposts;
    $profileidarray14 = $pageposts;
    $profileidarray15 = $pageposts;
    $profileidarray16 = $pageposts;
    $profileidarray17 = $pageposts;



if(isset($_POST['idealindustry'])) {

$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $_POST['idealindustry'];

$qstring = $qstring . "(idealindustry.meta_key = 'ideal_job_industry' AND idealindustry.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealindustry.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealindustry";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealindustry.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray1 = array();
$profileidarray1 = $pageposts;

}





if(isset($_POST['idealseniority'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealseniority'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealseniority.meta_key = 'ideal_job_seniority' AND idealseniority.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealseniority";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealseniority.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray2 = array();
$profileidarray2 = $pageposts;
}



if(isset($_POST['idealjobtitle'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealjobtitle'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealjobtitle.meta_key = 'ideal_job_job_function' AND idealjobtitle.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealjobtitle";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealjobtitle.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray3 = array();
$profileidarray3 = $pageposts;
}



if(isset($_POST['idealjoblocation'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $_POST['idealjoblocation'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(ideallocation.meta_key = 'ideal_job_location' AND ideallocation.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS ideallocation";
$querywhere = $querywhere . "AND $wpdb->posts.ID = ideallocation.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray4 = array();
$profileidarray4 = $pageposts;

}



if(isset($_POST['idealjobbasicsalary'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['idealjobbasicsalary'];

foreach ($qarray as $param) {

$param = str_replace("< ", "", $param);

$param = str_replace("> ", "", $param);

$param = $wpdb->esc_like( sanitize_text_field($param));

    if ($param == "30.000") {

        $qstring = $qstring . "(idealpay.meta_key = 'ideal_job_basic_salary' AND idealpay.meta_value LIKE '%".$param."%' AND idealpay.meta_value NOT LIKE '%30.000-%') OR ";    
    } 
    else 
    {
        $qstring = $qstring . "(idealpay.meta_key = 'ideal_job_basic_salary' AND idealpay.meta_value LIKE '%".$param."%') OR ";
    }



}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealpay";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealpay.post_id "; 

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray5 = array();
$profileidarray5 = $pageposts;
}





if(isset($_POST['idealjobcompanytype'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealjobcompanytype'];
$qstring = $qstring . "(idealcompanytype.meta_key = 'ideal_job_company_type' AND idealcompanytype.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(idealcompanytype.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealcompanytype";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealcompanytype.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray6 = array();
$profileidarray6 = $pageposts;
}





if(isset($_POST['idealjobcontract'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealjobcontract'];
$qstring = $qstring . "(idealcontract.meta_key = 'ideal_job_contract_type' AND idealcontract.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(idealcontract.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealcontract";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealcontract.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray7 = array();
$profileidarray7 = $pageposts;
}



if(isset($_POST['currentjobindustry'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['currentjobindustry'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentindustry.meta_key = 'search_industry_experience' AND currentindustry.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentindustry";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentindustry.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray8 = array();
$profileidarray8 = $pageposts;
}





if(isset($_POST['currentemployer'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['currentemployer'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(currentemployer.meta_key = 'search_company_experience' AND currentemployer.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentemployer";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentemployer.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray9 = array();
$profileidarray9 = $pageposts;
}



if(isset($_POST['currentjobtitle'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['currentjobtitle'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentjobtitle.meta_key = 'search_role_experience' AND currentjobtitle.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentjobtitle";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentjobtitle.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray10 = array();
$profileidarray10 = $pageposts;
}





if(isset($_POST['currentjobseniority'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['currentjobseniority'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(currentseniority.meta_key = 'current_job_seniority' AND currentseniority.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentseniority";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentseniority.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray11 = array();
$profileidarray11 = $pageposts;
}



if(isset($_POST['currentsalary'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['currentsalary'];

foreach ($qarray as $param) {

$param = str_replace("< ", "", $param);
$param = str_replace("> ", "", $param);
$param = $wpdb->esc_like( sanitize_text_field($param));

    if ($param == "30.000") {
        $qstring = $qstring . "(currentpay.meta_key = 'current_job_basic_salary' AND currentpay.meta_value LIKE '%".$param."%' AND currentpay.meta_value NOT LIKE '%30.000-%') OR ";    
    } 
    else 
    {
        $qstring = $qstring . "(currentpay.meta_key = 'current_job_basic_salary' AND currentpay.meta_value LIKE '%".$param."%') OR ";
    }

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentpay";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentpay.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray12 = array();
$profileidarray12 = $pageposts;
}





if(isset($_POST['currentcontract'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['currentcontract'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentcontract.meta_key = 'current_job_type_of_contract' AND currentcontract.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentcontract";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentcontract.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray13 = array();
$profileidarray13 = $pageposts;
}



if(isset($_POST['institution'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['institution'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param)); 
$qstring = $qstring . "(institution.meta_key = 'search_institution' AND institution.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS institution";
$querywhere = $querywhere . "AND $wpdb->posts.ID = institution.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray14 = array();
$profileidarray14 = $pageposts;
}



if(isset($_POST['qualification'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $_POST['qualification'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(qualification.meta_key = 'search_qualification_type' AND qualification.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS qualification";
$querywhere = $querywhere . "AND $wpdb->posts.ID = qualification.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray15 = array();
$profileidarray15 = $pageposts;
}



//course name

if(isset($_POST['coursename'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['coursename'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(coursename.meta_key = 'search_course_name' AND coursename.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3); 
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS coursename";
$querywhere = $querywhere . "AND $wpdb->posts.ID = coursename.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray16 = array();
$profileidarray16 = $pageposts;
}

//grade

if(isset($_POST['gradeattained'])) {
    $queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['gradeattained'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(gradeattained.meta_key = 'search_grade' AND gradeattained.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS gradeattained";
$querywhere = $querywhere . "AND $wpdb->posts.ID = gradeattained.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray17 = array();
$profileidarray17 = $pageposts;
}






$profileidarray = array();

$profileidarray = array_intersect(
    $profileidarray1,
    $profileidarray2,
    $profileidarray3,
    $profileidarray4,
    $profileidarray5,
    $profileidarray6,
    $profileidarray7,
    $profileidarray8,
    $profileidarray9,
    $profileidarray10,
    $profileidarray11,
    $profileidarray12,
    $profileidarray13,
    $profileidarray14,
    $profileidarray15,
    $profileidarray16,
    $profileidarray17
    );


	$is_initial_search = 1;

	$_SESSION["profileidarray"] = $profileidarray;

} else {

	$profileidarray = $_SESSION["profileidarray"];

}


$profileidarray = array_unique($profileidarray);


$number_of_results = count($profileidarray); 


?>


<div class="search-results-title">
<div class="sr-title-wrapper">
  <?php 
  if ($number_of_results == 0) {
  	echo "Sorry, none of the profiles match your search criteria.";
  } elseif ($number_of_results == 1) {
  	echo number_format($number_of_results) . " profile matches your requirements.";
  } else {
  	echo number_format($number_of_results) . " people match your requirements.";
  } 

//echo  $querystr;

  ?>	
</div>

<div class="sr-button-wrapper">
	<a href="<?php echo site_url();?>/advertise-job/" class="save-search-btn btn btn-danger">Advertise Job</a>
</div>

</div>

<div class="talent-lines">

<?php 

if ($number_of_results > 0) {


global $paged;
global $wp_query;

$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

$profiles_per_page = 10;

$args = array(  
    'post_type' => 'profile',
    'meta_key' => 'profile_progress',
    'orderby'   => 'meta_value_num date',
    'order'     => 'DESC',
    'posts_per_page' => $profiles_per_page,
    'post__in' => $profileidarray,
    'paged' => $paged
    );

// The Query

query_posts( $args );

//initiate variable for counting positions
$i = 0;
// The Loop
 if ( have_posts() ) : while ( have_posts() ) : the_post();
		
		$profileid = get_the_ID();

		//autoincrement $i
		$i++;


//How many times did the profile appear on search
$curnum = strip_tags(get_field( "appeared_on_search", $profileid ));
$curnum = $curnum + 1;			
update_field( "appeared_on_search", $curnum, $profileid);
?>

<?php 

// Updating search position. We have the counter ($i) and now writing the value of that counter to the meta field. Usually array stars with 0 so we are adding 1.
// the following piece of code checks if the profile is not on the first page than we get the page number and calculate position accordingly
if (get_query_var( 'paged' )) {$profile_position = (get_query_var( 'paged' ) - 1) * $profiles_per_page + $i;} else {$profile_position = $i;}
$list_of_positions = strip_tags(get_field( "search_positions", $profileid ));
$updated_list_of_positions = $list_of_positions . $profile_position . ",";
update_field( "search_positions", $updated_list_of_positions, $profileid );
?>


<div class="talent-line">

<?php 

$current_position = 'N/A';
$talent_education = 'N/A';
$talent_location = 'N/A';

// current job
if(get_field("current_job_seniority", $profileid)) {
	$current_position = sanitize_text_field(get_field("current_job_seniority", $profileid));
} 

if(get_field("current_job_job_function", $profileid)) {
	$current_position .= ", " . sanitize_text_field(get_field("current_job_job_function", $profileid));
}

if(get_field("current_job_organisation_name", $profileid)) {
	$current_position .= ", " . sanitize_text_field(get_field("current_job_organisation_name", $profileid));
}

if(get_field("current_job_basic_salary", $profileid)) {
	$current_position .= ", " . sanitize_text_field(get_field("current_job_basic_salary", $profileid));
}

if(get_field("current_job_basic_salary_currency", $profileid)) {
	$current_position .= ", " . sanitize_text_field(get_field("current_job_basic_salary_currency", $profileid));
}

//education
if(get_field("qualifications_and_skills_institution1", $profileid)) {
	$talent_education = sanitize_text_field(get_field("qualifications_and_skills_institution1", $profileid));
}

if(get_field("qualifications_and_skills_course_name1", $profileid)) {
	$talent_education .= ", " . sanitize_text_field(get_field("qualifications_and_skills_course_name1", $profileid));
}

if(get_field("qualifications_and_skills_grade_attained1", $profileid)) {
	$talent_education .= ", " . sanitize_text_field(get_field("qualifications_and_skills_grade_attained1", $profileid));
}


//location 
if(get_field("ideal_job_location", $profileid)) {
$talent_location = sanitize_text_field(get_field("ideal_job_location", $profileid));
}



?>

	<div class="talent-info">
		<div class="talent-number">
			Talent Number <?php $post = get_post( $profileid ); $author_id = $post->post_author; echo $author_id; ?> 
		</div>
		<div class="current-position">
			<span class="talent-line-span">Current Position:</span> <?php echo $current_position; ?>
		</div>
		<div class="talent-university">
			<span class="talent-line-span">University:</span> <?php echo $talent_education; ?>
		</div>
		<div class="talent-location">
			<span class="talent-line-span">Ideal Location:</span> <?php echo $talent_location; ?>
		</div>
		
	</div>


	<div class="talent-buttons">
		<div class="btn-container">
			<a target="_blank" class="btn btn-default" href="<?php $permalink = get_permalink($profileid); echo $permalink; ?>"><i class="fa fa-search"></i> View Full Profile</a>
		</div>
		<div class="btn-container">

            <?php $check_pool = is_this_talent_in_my_pool($profileid, 1, '');

            if($check_pool != 1) { ?>

                <div class="add-to-talent-pool btn btn-default add-new" data-profile-id="<?php echo $profileid; ?>" ><i class="fa fa-plus-circle"></i> Add To Talent Pool</div>

            <?php } else { ?>

                 <div class="add-to-talent-pool btn btn-default in-my-pool" data-profile-id="<?php echo $profileid; ?>"><i class="fa fa-users"></i> In My Talent Pool</div>

            <?php } ?>
 

		</div>
	</div>


</div>

     <?php

     	endwhile;	?>
</div>
<?php
    matchly_numeric_posts_nav(); 
   wp_reset_postdata();  	
     else :
endif;

// Reset main query object
}
?>


</div>


				</div>
			</div>
		</div>


	<?php // end of the loop. ?>
	<?php // update user search number
	if ($is_initial_search) {
	$user_ID = get_current_user_id();
	$current_searched_index = strip_tags( get_field( "search_number", 'user_'.$user_ID ));
	$updated_search_index = $current_searched_index + 1;
	update_field( "search_number", $updated_search_index, 'user_'.$user_ID );
	}
	?>
</main><!-- #main -->
</div><!-- #primary -->

<!-- Modal -->
<div class="modal fade mail-function" id="talent-pool-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top: 120px;">
    <div class="modal-content">
        <div class="form-horizontal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Add To Talent Pool</h4>
                <h5 style="  margin-bottom: 0; margin-top: 3px;">Tag this talent profile with up to three descriptors</h5>
              </div>

              <div class="modal-body">

                <div id="misc-info"></div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 1</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-one" class="form-control" />
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 2</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-two" class="form-control"  />
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 3</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-three" class="form-control"  />
                        </div>
                </div>

                </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-default" id="create-talent-pool"><i class="fa fa-plus-circle"></i> Add</button>
              </div>
        </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function() {

  
    $(document).on('click', '.add-to-talent-pool.add-new', function(){

        $("#talent-pool-modal").attr('data-profile-id', $(this).data("profile-id"));
        $("#talent-pool-modal").modal('show');

    });

    // clean up on hide

    $('#talent-pool-modal').on('hide.bs.modal', function(e) {

       $("#talent-pool-modal").attr('data-profile-id', "");
       $("#tag-one").val('');
       $("#tag-two").val('');
       $("#tag-three").val('');

    });

    $(document).on('click', '#create-talent-pool', function(){

        var tagOne = $("#tag-one").val();
        var tagTwo = $("#tag-two").val();
        var tagThree = $("#tag-three").val();
        var profileID = $("#talent-pool-modal").attr('data-profile-id');

        $("#talent-pool-modal").modal('hide');

                 $.ajax({
            data: ({
                action : 'create_talent_pool',
                'profile': profileID,
                'tag-one':tagOne,
                'tag-two':tagTwo,
                'tag-three':tagThree
            }),
            type: 'POST',
            async: true,     
            url: ajaxurl
          }).done(function( msg ) {
                console.log(msg);

                $(".add-to-talent-pool.add-new[data-profile-id='" + profileID + "']").html('<i class="fa fa-users"></i> In My Talent Pool').addClass('in-my-pool').removeClass('add-new');
                swal("Done!", "Profile was added to your talent pool!", "success");
             });

    });

});

</script>

<?php get_sidebar(); ?>

<?php get_footer(); ?>



