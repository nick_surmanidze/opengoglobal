<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
				<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>

			
					<div class="container container-fluid">
						<div class="row content-wrapper">
							<div class="page-content-wrapper">
									<h1 class="error404">ERROR 404!</h1>
							</div>
						</div>
					</div>

			

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>