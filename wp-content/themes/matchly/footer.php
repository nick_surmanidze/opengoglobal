<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Matchly
 */
?>

	</div><!-- #content -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bg-process-trigger.js"></script> 
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<div class="container container-fluid">
				<div class="row footer-menu-wrapper">
					<div class="footer-left-menu-wrapper"><?php wp_nav_menu( array( 'theme_location' => 'footer-left' ) ); ?></div>
					<div class="footer-right-menu-wrapper"><?php wp_nav_menu( array( 'theme_location' => 'footer-right' ) ); ?></div>
					<div class="copyright">
						Copyright © 2015 OpenGo. All Rights Reserved.
					</div>	
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>


</body>
</html>
