<?php 
// Include WP files so our controller can actually work with WP
 
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

// updating currnt user to both and then redirecting him to steps.	
$user_ID = get_current_user_id(); 
update_field( "user_role", "both", 'user_'.$user_ID );
update_field( "user_step", "0", 'user_'.$user_ID );

wp_redirect( home_url()."/steps" ); 				
exit;