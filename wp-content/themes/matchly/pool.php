<?php
/*
Template Name: Pool
/**
 * The template for displaying all single Jobs.
 *
 * @package Matchly
 */

get_header(); 
?>

	<div id="primary" class="content-area">
	<?php $backgroundimageurl = get_field("background_image", "options"); if(isset($backgroundimageurl)) { ?>
		<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
	<?php } else { ?>
		<main id="main" class="site-main" role="main">	
		<?php } ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="container container-fluid">
				<div class="row content-wrapper">
					<div class="page-content-wrapper no-padding">
						<div class="content-inner">

							<div class="general-page-header">My Talent Pool</div>

							<div class="pool-wrapper">

								<?php 
								$pool_array = get_array_of_my_pools();
								if(count($pool_array) > 0) { ?>


								<div class="pool-line head-line">
									<div class="pool-title">
										<span data-factor="title">Talent</span>
										<input type="text" id="title-search" class="search-input" placeholder="Search..">
									</div>
									<div class="pool-tag pool-tag-one">
										<span data-factor="pool_tag_one">Tag One</span>
										<input type="text" id="cat-one-search" class="search-input" placeholder="Search..">
									</div>
									<div class="pool-tag pool-tag-two">
										<span data-factor="pool_tag_two">Tag Two</span>
										<input type="text" id="cat-two-search" class="search-input" placeholder="Search..">
									</div>
									<div class="pool-tag pool-tag-three">
										<span data-factor="pool_tag_three">Tag Three</span>
										<input type="text" id="cat-three-search" class="search-input" placeholder="Search..">
									</div>
									<!-- <div class="pool-contacted">Contacted</div> -->
								</div>

							<?php 
									global $wpdb;

									$count_results = '';
									$results_html = '';
									$pagination_html = '';

									// we are sorting by meta $sortfactor and define sorting direction
									$sort_factor = '';
									$sort_direction = '';
									$direction_str = '';

									//now let's define query variables
									$queryfrom = '';
									$querywhere = '';
									$queryparameters = '';

									$qstring = '';

									$title = '';
									$catone = '';
									$cattwo = '';
									$catthree = '';

									// now get the data and assign to varibales

									if(isset($_POST['title'])) {

									$title = sanitize_text_field($_POST['title']);

									}

									if(isset($_POST['catone'])) {

									$catone = sanitize_text_field($_POST['catone']);

									}

									if(isset($_POST['cattwo'])) {

									$cattwo = sanitize_text_field($_POST['cattwo']);

									}


									if(isset($_POST['catthree'])) {

									$catthree = sanitize_text_field($_POST['catthree']);

									}



									if(isset($_POST['sortfactor'])) {

									$sort_factor = sanitize_text_field($_POST['sortfactor']);

									}

									if(isset($_POST['sortdirection'])) {

										if(sanitize_text_field($_POST['sortdirection']) == "A") {

											$sort_direction = "ASC";

										} else {

											$sort_direction = "DESC";
										}

									$sort_direction = sanitize_text_field($_POST['sortdirection']);

									}

									// now we need to architect the complex query

									if ($title) {

									$qstring = '';
									$param = $wpdb->esc_like( $title );
									$qstring = $qstring . "(" . $wpdb->posts . ".post_title LIKE '%".$param."%') OR ";

									$qstring = substr($qstring, 0, -3);
									$queryparameters = $queryparameters . " AND (" . $qstring . ")";

									}

									if ($catone) {

										$qstring = '';
									$param = $wpdb->esc_like( $catone );
									$qstring = $qstring . "(nsdscatone.meta_key = 'pool_tag_one' AND nsdscatone.meta_value LIKE '%".$param."%') OR ";

									$qstring = substr($qstring, 0, -3);
									$queryparameters = $queryparameters . " AND (" . $qstring . ")";

									$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsdscatone";
									$querywhere = $querywhere . "AND $wpdb->posts.ID = nsdscatone.post_id ";
									}


									if ($cattwo) {
											$qstring = '';
									$param = $wpdb->esc_like( $cattwo );
									$qstring = $qstring . "(nsdscattwo.meta_key = 'pool_tag_two' AND nsdscattwo.meta_value LIKE '%".$param."%') OR ";

									$qstring = substr($qstring, 0, -3);
									$queryparameters = $queryparameters . " AND (" . $qstring . ")";

									$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsdscattwo";
									$querywhere = $querywhere . "AND $wpdb->posts.ID = nsdscattwo.post_id ";
									}


									if ($catthree) {
											$qstring = '';
									$param = $wpdb->esc_like( $catthree );
									$qstring = $qstring . "(nsdscatthree.meta_key = 'pool_tag_three' AND nsdscatthree.meta_value LIKE '%".$param."%') OR ";

									$qstring = substr($qstring, 0, -3);
									$queryparameters = $queryparameters . " AND (" . $qstring . ")";

									$queryfrom = $queryfrom . ", $wpdb->postmeta AS nsdscatthree";
									$querywhere = $querywhere . "AND $wpdb->posts.ID = nsdscatthree.post_id ";
									}


									if ($sort_factor && $sort_direction) {
											$qstring = '';

										if ($sort_factor == "title") {

											$direction_str = " ORDER BY " . $wpdb->posts . ".post_title ".$sort_direction."";

										} else {

											$qstring = $qstring . " AND sortfactor.meta_key = '".$sort_factor."'";
											$queryparameters = $queryparameters .  $qstring;

											$direction_str = " ORDER BY sortfactor.meta_value ".$sort_direction."";

											$queryfrom = $queryfrom . ", $wpdb->postmeta AS sortfactor";
											$querywhere = $querywhere . "AND $wpdb->posts.ID = sortfactor.post_id ";

										}


									}


									$querystr = "
								   SELECT DISTINCT
								        $wpdb->posts.post_title,
								        $wpdb->posts.ID      
								    FROM
								        $wpdb->posts
								       ".$queryfrom."
								    WHERE       
								        $wpdb->posts.post_type = 'pool' 
								        AND $wpdb->posts.post_author = '" . get_current_user_id() . "'
								        AND $wpdb->posts.post_status = 'publish'
								        
								        ".$querywhere."
								        
								        ".$queryparameters." ".$direction_str."";



									$pool_id_array = array();

									$pool_id_array = $wpdb->get_col($querystr, 1);

									// if the custom order is not specified than we need to reverse the order to naturally descending.

									if(!$sort_factor) {
									$pool_id_array = array_reverse ($pool_id_array);
									}


									// Not it is time to take care of pagination

									$pagination = '';

									// Change this figure to output different number per page
									$pool_per_page = 10;


									if(isset($_POST['current-page'])) {

									$current_page = sanitize_text_field($_POST['current-page']);

									} else {

									$current_page = 1;	

									}
									
									

									$offset = ($current_page - 1) * $pool_per_page;
									$full_length = count($pool_id_array);

									$number_of_pages = ceil($full_length / $pool_per_page);


									if($number_of_pages > 1) {
										$first_page = '';
										$last_page = '';
										$before_current ='';
										$current_pg = '';
										$after_current ='';

										if($current_page != 1) {
										$first_page = "<li>1</li>";	
										}
										

										if(($current_page - 1) == 3)
										{

											$before_current = " <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

										} elseif(($current_page - 1) == 2)  {

											$before_current = " <li>".($current_page-1)."</li>";

										} elseif(($current_page - 1) == 1)  {

											$before_current = "";

										} elseif(($current_page - 1) == 0)  {

											$before_current = "";

										}	elseif(($current_page - 1) > 3)  {

											$before_current = "<li>".($current_page-3)."</li> <li>".($current_page-2)."</li> <li>".($current_page-1)."</li>";

										}


										$current_pg = "<li class='current-page'>".$current_page."</li>";


										if((($number_of_pages - $current_page) == 3) || (($number_of_pages - $current_page) > 3)) {

											$after_current = "<li class='aft'>".($current_page+1)."</li> <li class='aft'>".($current_page+2)."</li> ";

										} elseif(($number_of_pages - $current_page) == 2) {

											$after_current = "<li class='aft'>".($current_page+1)."</li> ";

										}  elseif(($number_of_pages - $current_page) == 1) {

											$after_current = "";

										} else {

											$after_current = '';
										}
										if(!($current_page == $number_of_pages)) {
											$last_page = "<li>".$number_of_pages."</li>";
										}
										
										$pagination_html = $first_page . $before_current . $current_pg . $after_current . $last_page;
									}


									$pool_id_array = array_slice($pool_id_array, $offset, $pool_per_page);




									$results_html = '';

									foreach($pool_id_array as $pool_id) {

									$results_html .= '<div class="pool-line p-line"><div class="pool-title">';
									$results_html .= '<div class="edit-pool" title="Edit" data-pool-id="'.$pool_id.'" data-tag-one="'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_one", true)).'" data-tag-two="'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_two", true)).'" data-tag-three="'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_three", true)).'"></div>';
									$results_html .= '<a href="'.get_permalink(sanitize_text_field(get_post_meta($pool_id, "pool_profile_id", true))).'">'.get_the_title(sanitize_text_field(get_post_meta($pool_id, "pool_profile_id", true))).'</a></div>';
									$results_html .= '<div class="pool-tag pool-tag-one">'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_one", true)).'</div>';
									$results_html .= '<div class="pool-tag pool-tag-two">'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_two", true)).'</div>';
									$results_html .= '<div class="pool-tag pool-tag-three">'.sanitize_text_field(get_post_meta($pool_id, "pool_tag_three", true)).'</div>';
									$results_html .= '</div>';
									}

								echo $results_html;
								 ?>



								<?php } else {

									echo "<span class='no-talents-yet'>You have no talents in your pool yet.</span>";
								} ?>
							

							</div>

								<div class="action-button">
									<ul class="pagination">
										<?php echo $pagination_html; ?>
									</ul>
									<div class="count-results"><div class="count"><?php echo $full_length; ?></div>Result(s)</div>
									<img class="ajax-spinner" src="<?php echo get_stylesheet_directory_uri(); ?>/images/spinner-white.GIF" alt="loading">
								</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<!-- Modal -->
<div class="modal fade mail-function" id="edit-pool-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="form-horizontal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Add To Talent Pool</h4>
              </div>

              <div class="modal-body">

                <div id="misc-info"></div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 1</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-one" class="form-control" value=""/>
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 2</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-two" class="form-control"  value=""/>
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 3</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-three" class="form-control"  value=""/>
                        </div>
                </div>

                </div>

              <div class="modal-footer">
              	<button type="button" class="btn btn-danger" id="delete-pool"><i class="fa fa-trash-o"></i> Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-default" id="update-talent-pool"><i class="fa fa-plus-circle"></i> Update</button>
              </div>
        </div>
    </div>
  </div>
</div>
<script>

function equalizeLines() {

	$(".pool-line").each(function(index, value) {	


			  var maxHeight = -1;

			   $(this).find("div").each(function() {
			     maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
			   });

			   $(this).find("div").each(function() {
			     $(this).height(maxHeight);
			   });		

	});

}

$(document).ready(function() {

equalizeLines();

// run the function on window resize
$(window).bind('resize', function () { 
	$(".pool-line > div").css("height", "auto");
	equalizeLines();
});


    $(document).on('click', '.edit-pool', function(){

        $("#edit-pool-modal").attr('data-pool-id', $(this).data("pool-id"));

        $("#edit-pool-modal #tag-one").val($(this).attr("data-tag-one"));
        $("#edit-pool-modal #tag-two").val($(this).attr("data-tag-two"));
        $("#edit-pool-modal #tag-three").val($(this).attr("data-tag-three"));

        $("#edit-pool-modal").modal('show');

    });

    $(document).on('click', '#update-talent-pool', function(){

	var poolID = $("#edit-pool-modal").attr('data-pool-id');

	var tag1 = $("#edit-pool-modal #tag-one").val();
	var tag2 = $("#edit-pool-modal #tag-two").val();
	var tag3 = $("#edit-pool-modal #tag-three").val();

	console.log(poolID + tag1 + tag2 + tag3);

	$("#edit-pool-modal").modal('hide');

			 $.ajax({
	        data: ({
	        	action : 'update_talent_pool',
	        	'pool-id' : poolID,
	        	'tag-one' : tag1,
	        	'tag-two' : tag2,
	        	'tag-three' : tag3
	        }),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {
		        console.log(msg);
		        swal("Record Updated!", "Talent Pool Had Been Updated!", "success");

		        //Now let's update GUI
				console.log(poolID + tag1 + tag2 + tag3);

		        $(".edit-pool[data-pool-id='"+poolID+"']").attr('data-tag-one', tag1);
		        $(".edit-pool[data-pool-id='"+poolID+"']").attr('data-tag-two', tag2);
		        $(".edit-pool[data-pool-id='"+poolID+"']").attr('data-tag-three', tag3);

		        $(".edit-pool[data-pool-id='"+poolID+"']").parent().parent().find(".pool-tag-one").html(tag1).css("height", "auto");
		        $(".edit-pool[data-pool-id='"+poolID+"']").parent().parent().find(".pool-tag-two").html(tag2).css("height", "auto");
		        $(".edit-pool[data-pool-id='"+poolID+"']").parent().parent().find(".pool-tag-three").html(tag3).css("height", "auto");

		        equalizeLines();
	 		 });

    });


    $(document).on('click', '#delete-pool', function(){

	var poolID = $("#edit-pool-modal").attr('data-pool-id');

	$("#edit-pool-modal").modal('hide');  

	swal({   title: "Are you sure?",
	 text: "Please confirm your intention!",   
	 type: "warning",   
	 showCancelButton: true,   
	 confirmButtonColor: "#DD6B55",   
	 confirmButtonText: "Yes, delete it!",   
	 cancelButtonText: "No, cancel!",   
	 closeOnConfirm: true,   
	 closeOnCancel: false }, 
	 	function(isConfirm){   if (isConfirm) {  

	 		 $.ajax({
	        data: ({action : 'delete_pool', 'pool-id':poolID}),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {
		        console.log(msg);
		        swal("Deleted!", "Item was successfully deleted!", "success");
		        $(".edit-pool[data-pool-id='"+poolID+"']").parent().parent().remove(); 
		      });

	 	} else {     
	 		swal("Cancelled", "Item removal has been cancelled.", "error");  
	 		
	 	} });

    });

    });

//-----------------------------------
// Change Filter State


 $(document).on("click", '.pool-line.head-line span',  function(){ 


if($(this).hasClass("asc")) {

		$(".pool-line.head-line span").each(function(index, value) {	
			$(this).removeClass("asc").removeClass("desc"); 
	});

	$(this).removeClass("asc").addClass("desc");

} else if ($(this).hasClass("desc")) {

		$(".pool-line.head-line span").each(function(index, value) {	
			$(this).removeClass("asc").removeClass("desc"); 
	});

	$(this).removeClass("asc").removeClass("desc"); 

} else {

		$(".pool-line.head-line span").each(function(index, value) {	
			$(this).removeClass("asc").removeClass("desc"); 
	});

	$(this).addClass("asc");



}

 

 });

// Change Filter State
//-----------------------------------


//-----------------------------------
// Trigger for sorting



function get_filter_results(page) {

$(".action-button .ajax-spinner").show();

var title     = '';
var catOne    = '';
var catTwo    = '';
var catThree  = '';


var sortfactor = '';
var direction = '';

if(page) {
	var curPage = page;
} else {
	curPage = 1;
}

title    = $('#title-search').val();
catOne   = $('#cat-one-search').val();
catTwo   = $('#cat-two-search').val();
catThree = $('#cat-three-search').val();


$(".pool-line.head-line span").each(function(index, value) {	

	if($(this).hasClass("asc")) {

	sortfactor = $(this).attr('data-factor');
	direction = "ASC";

	} else if ($(this).hasClass("desc")) {

	sortfactor = $(this).attr('data-factor');
	direction = "DESC";

	}

});


 $.ajax({
    data: 
        ({
        	action : 'sort_pool',
        	'title': title,
        	'catone': catOne,
        	'cattwo': catTwo,
        	'catthree': catThree,
        	'sortfactor': sortfactor,
        	'sortdirection': direction,
        	'current-page' : curPage

        }),

    type: 'POST',
    async: true,     
    url: ajaxurl,
  }).done(function( msg ) {
		console.log(msg);

		var output = JSON.parse(msg);

		$(".pool-line").each(function(index, value) {	

			if(!$(this).hasClass("head-line")) {

				$(this).remove();

			} 

		});


		$(".pool-line.head-line").after(output.results);

		$(".pagination").html(output.pagination);

		$(".count-results .count").html(output.number);

		equalizeLines();

		$(".action-button .ajax-spinner").hide();

	});

}


$( ".search-input" ).keyup(function() {
  get_filter_results();
});

 $(document).on("click", '.pool-line.head-line span',  function(){ 

	get_filter_results();

 });


// now the pagination
   $(document).on("click", '.pagination > li',  function(){ 

   	$(".pagination > li").each(function(index, value) {	

		$(this).removeClass("current-page");

	});

	$(this).addClass("current-page");

 	var curpage = $(this).html();
 	console.log(curpage);
	get_filter_results(curpage);

 });

// trigger for sorting
//-----------------------------------




</script>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
