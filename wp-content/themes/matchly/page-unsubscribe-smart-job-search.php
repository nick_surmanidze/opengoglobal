<?php
/*
Template Name: Unsubscribe Smart Job Search
/**
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Matchly
 */

get_header(); 

if(isset($_GET['uid'])) {
	
	 $user_id =  sanitize_text_field($_GET['uid']);

	 update_user_meta($user_id, "smarter_notifications", "disabled");

}

?>

	<div id="primary" class="content-area">
						<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>

			<?php while ( have_posts() ) : the_post(); ?>
					<div class="container container-fluid">
						<div class="row content-wrapper">
							<div class="page-content-wrapper">
							

								<div class="content-inner">
									<?php the_content(); ?>

								</div>
							</div>
						</div>
					</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
