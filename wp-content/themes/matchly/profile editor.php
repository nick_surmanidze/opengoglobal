<?php
/*
Template Name: Profile Editor
/**
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">

		<?php } ?>
		

			<?php while ( have_posts() ) : the_post(); ?>
					<div class="container container-fluid">
						<div class="row content-wrapper">
							<?php
		if ( is_user_logged_in() ) {
				////////////////// Get Profile ID - Start ////////////////////////////////////////
				$user_ID = get_current_user_id();
				$profileID = 0;
				// Get user profile page
				$query = new WP_Query( 'author='.$user_ID.'&post_type=profile' );

				// The Loop
				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {

						$query->the_post();

						$profileID = get_the_ID();
					}

				} else {
					
				}
				wp_reset_postdata();?>

				<!--////////////////// Get Profile ID - Finish /////////////////////////////////////// -->
							 
					<?php if ($profileID > 0) { ?>
					

								<div class="content-inner">
									
									<div class="edit-profile-wrapper">
									<form class="form-horizontal" role="form" method="post" id="updateform" action="<?php echo get_stylesheet_directory_uri(); ?>/profile-update.php">	
											<input type="hidden" name="profileid" value="<?php echo $profileID; ?>" >


											<div class="e-section-title"><i class="fa fa-pencil"></i>  About Me</div>

											<div class="e-col-50">

													<div class="form-group">
														<label for="firstname" class="col-sm-3 control-label">First Name</label>
														<div class="col-sm-9">
															<input type="text" value="<?php echo preg_replace( "/\r|\n/", "", strip_tags(get_field("about_you_first_name", $profileID))); ?>" class="form-control" id="firstname" name="firstname" placeholder="e.g. John" maxlength="100" required />
														</div>
													</div>

													<div class="form-group">
														<label for="surname" class="col-sm-3 control-label">Surname</label>
														<div class="col-sm-9">
															<input type="text" value="<?php echo preg_replace( "/\r|\n/", "", strip_tags(get_field("about_you_surname", $profileID))); ?>" class="form-control" id="surname" name="surname" placeholder="e.g. Doe" maxlength="100" required />
														</div>

													</div>


											</div>

											<div class="e-col-50">



													<div class="form-group">
														<label for="emailaddress" class="col-sm-3 control-label">Email</label>
														<div class="col-sm-9">
															<input type="email" value="<?php echo preg_replace( "/\r|\n/", "", strip_tags(get_field("about_you_email_address", $profileID))); ?>" class="form-control" id="email" name="emailaddress" placeholder="e.g. j.doe@example.com" maxlength="100" required />
														</div>
													</div>


											</div>
<!--/////////////////////////////////////////////////// END OF SECTION 1 - About Me  ////////////////////////////////////////////////////////////////////////////-->

											<div class="e-section-title"><i class="fa fa-pencil"></i>  Current Job</div>

											<div class="e-col-50">

												<div class="form-group">													
													<label for="organisationname" class="col-sm-3 control-label">Organisation Name</label>
													<div class="col-sm-9">
														<input type="text" value="<?php echo preg_replace( "/\r|\n/", "", strip_tags(get_field("current_job_organisation_name", $profileID))); ?>"  class="form-control" id="organisationname" name="organisationname" placeholder="e.g. Google Inc." maxlength="100" required />
													</div>
													<div class="val-message"></div>
												</div>	
												
												<div class="form-group">													
													<label for="industry" class="col-sm-3 control-label">Industry</label>
													<div class="col-sm-9">
														<select id="industry" name="industry" class="form-control multiselect-one-value  manual-validation" required >
															
															<?php $variable = get_field('current_job_industry', 'option'); 
															$temp_array = explode("\n", $variable);

															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("current_job_industry", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>	
																										    
														</select>
													</div>
													
												</div>	

												<div class="form-group">													
													<label for="companytype" class="col-sm-3 control-label">Company Type</label>
													<div class="col-sm-9">
														<select id="companytype" name="companytype" class="form-control manual-validation" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_company_type', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);

																	$selected = '';
																	$WhereToSearch = get_field("current_job_company_type", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."'  ".$selected.">".$item."</option>";
															}
															?>	
																												    
														</select>
													</div>
												</div>  


												<div class="form-group">													
													<label for="jobfunction" class="col-sm-3 control-label">Job Function</label>
													<div class="col-sm-9">
														<select id="jobfunction" name="jobfunction" class="form-control" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_job_function', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("current_job_job_function", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);

																	$WhereToSearch = rs_chars($WhereToSearch);

																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", rs_chars($item) )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div> 


												<div class="form-group">													
													<label for="currentseniority" class="col-sm-3 control-label">Seniority</label>
													<div class="col-sm-9">
														<select id="currentseniority" name="currentseniority" class="form-control" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_seniority', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("current_job_seniority", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($item, preg_replace( "/\r|\n/", "", trim(strip_tags($WhereToSearch)))) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div> 

											</div>


											<div class="e-col-50">

												<div class="form-group">													
													<label for="currentjobyearstarted" class="col-sm-3 control-label">Year Started</label>
													<div class="col-sm-9">
														<select id="currentjobyearstarted" name="currentjobyearstarted" class="form-control" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_year_started', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("current_job_year_started", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div> 

												<div class="form-group">													
													<label for="currentjobtypeofcontract" class="col-sm-3 control-label">Type of Contract</label>
													<div class="col-sm-9">
														<select id="currentjobtypeofcontract" name="currentjobtypeofcontract" class="form-control" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_type_of_contract', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																    $selected = '';
																	$WhereToSearch = get_field("current_job_type_of_contract", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div> 

												<div class="form-group">													
													<label for="currentnoticeperiod" class="col-sm-3 control-label">Notice Period</label>
													<div class="col-sm-9">
														<select id="currentnoticeperiod" name="currentnoticeperiod" class="form-control" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_current_notice_period', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("current_job_current_notice_period", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div> 


												<div class="form-group">													
													<label for="currentjobbasicsalary" class="col-sm-3 control-label">Basic Salary</label>
													<div class="col-sm-9">
														<select id="currentjobbasicsalary" name="currentjobbasicsalary" class="form-control" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_basic_salary', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("current_job_basic_salary", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div> 

												<div class="form-group">													
													<label for="currentjobcurrency" class="col-sm-3 control-label">Basic Salary Currency</label>
													<div class="col-sm-9">
														<select id="currentjobcurrency" name="currentjobcurrency" class="form-control" required>
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('current_job_basic_salary_currency', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("current_job_basic_salary_currency", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div> 

													
											</div>
<!--/////////////////////////////////////////////////// END OF SECTION 6 - Current Job  ////////////////////////////////////////////////////////////////////////////-->





									
									<div class="e-section-title"><i class="fa fa-pencil"></i>  Previous Experience (1/2)</div>
									<div class="e-col-50">
										<div class="form-group">													
											<label for="previousorganisationname1" class="col-sm-3 control-label">Organisation Name</label>
											<div class="col-sm-9">
												<input type="text" value="<?php echo strip_tags(get_field("previous_experience_organisation_name1", $profileID)); ?>" class="form-control" id="previousorganisationname1" name="previousorganisationname1" placeholder="e.g. Google Inc." maxlength="100"  />
											</div>
											<div class="val-message"></div>
										</div>										

										<div class="form-group">													
											<label for="previousindustry1" class="col-sm-3 control-label">Industry</label>
											<div class="col-sm-9">
												<select id="previousindustry1" name="previousindustry1" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_industry', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_industry1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 



										<div class="form-group">													
											<label for="previouscompanytype1" class="col-sm-3 control-label">Company Type</label>
											<div class="col-sm-9">
												<select id="previouscompanytype1" name="previouscompanytype1" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('current_job_company_type', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																$selected = '';
																	$WhereToSearch = get_field("previous_experience_company_type1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>	
																									    
												</select>
											</div>
											<div class="val-message"></div>
										</div>  


										<div class="form-group">													
											<label for="previousjobfunction1" class="col-sm-3 control-label">Job Function</label>
											<div class="col-sm-9">
												<select id="previousjobfunction1" name="previousjobfunction1" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_job_function', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_job_function1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 
									</div>

									<div class="e-col-50">
										<div class="form-group">													
											<label for="previousseniority1" class="col-sm-3 control-label">Seniority</label>
											<div class="col-sm-9">
												<select id="previousseniority1" name="previousseniority1" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_seniority', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_seniority1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($item, preg_replace( "/\r|\n/", "", trim(strip_tags($WhereToSearch)))) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 

										<div class="form-group">													
											<label for="previousyearstarted1" class="col-sm-3 control-label">Year Started</label>
											<div class="col-sm-9">
												<select id="previousyearstarted1" name="previousyearstarted1" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_year_started', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
																	$WhereToSearch = get_field("previous_experience_year_started1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 


										<div class="form-group">													
											<label for="previousyearended1" class="col-sm-3 control-label">Year Ended</label>
											<div class="col-sm-9">
												<select id="previousyearended1" name="previousyearended1" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_year_ended', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_year_ended1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 

										<div class="form-group">													
											<label for="previoustypeofcontract1" class="col-sm-3 control-label">Type of Contract</label>
											<div class="col-sm-9">
												<select id="previoustypeofcontract1" name="previoustypeofcontract1" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_type_of_contract', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
																	$WhereToSearch = get_field("previous_experience_type_of_contract1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
													echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 

									</div>																		
<!--/////////////////////////////////////////////////// END OF SECTION 7 - Previous Experience  ////////////////////////////////////////////////////////////////////////////-->
									
									<div class="e-section-title"><i class="fa fa-pencil"></i>  Previous Experience (2/2)</div>
									<div class="e-col-50">
										<div class="form-group">													
											<label for="previousorganisationname2" class="col-sm-3 control-label">Organisation Name</label>
											<div class="col-sm-9">
												<input type="text" value="<?php echo strip_tags(get_field("previous_experience_organisation_name2", $profileID)); ?>" class="form-control" id="previousorganisationname2" name="previousorganisationname2" placeholder="e.g. Google Inc." maxlength="100" />
											</div>
											<div class="val-message"></div>
										</div>										

										<div class="form-group">													
											<label for="previousindustry2" class="col-sm-3 control-label">Industry</label>
											<div class="col-sm-9">
												<select id="previousindustry2" name="previousindustry2" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_industry', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_industry2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 



										<div class="form-group">													
											<label for="previouscompanytype2" class="col-sm-3 control-label">Company Type</label>
											<div class="col-sm-9">
												<select id="previouscompanytype2" name="previouscompanytype2" class="form-control">
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('current_job_company_type', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																$selected = '';
																	$WhereToSearch = get_field("previous_experience_company_type2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>	
																									    
												</select>
											</div>
											<div class="val-message"></div>
										</div>  


										<div class="form-group">													
											<label for="previousjobfunction2" class="col-sm-3 control-label">Job Function</label>
											<div class="col-sm-9">
												<select id="previousjobfunction2" name="previousjobfunction2" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_job_function', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_job_function2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 
									</div>

									<div class="e-col-50">
										<div class="form-group">													
											<label for="previousseniority2" class="col-sm-3 control-label">Seniority</label>
											<div class="col-sm-9">
												<select id="previousseniority2" name="previousseniority2" class="form-control" >
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_seniority', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_seniority2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($item, preg_replace( "/\r|\n/", "", trim(strip_tags($WhereToSearch)))) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 

										<div class="form-group">													
											<label for="previousyearstarted2" class="col-sm-3 control-label">Year Started</label>
											<div class="col-sm-9">
												<select id="previousyearstarted2" name="previousyearstarted2" class="form-control">
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_year_started', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
																	$WhereToSearch = get_field("previous_experience_year_started2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 


										<div class="form-group">													
											<label for="previousyearended2" class="col-sm-3 control-label">Year Ended</label>
											<div class="col-sm-9">
												<select id="previousyearended2" name="previousyearended2" class="form-control">
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_year_ended', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("previous_experience_year_ended2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 

										<div class="form-group">													
											<label for="previoustypeofcontract2" class="col-sm-3 control-label">Type of Contract</label>
											<div class="col-sm-9">
												<select id="previoustypeofcontract2" name="previoustypeofcontract2" class="form-control">
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('previous_experience_type_of_contract', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
																	$WhereToSearch = get_field("previous_experience_type_of_contract2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
													echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
										</div> 

									</div>	
<!--/////////////////////////////////////////////////// END OF SECTION 8 - Previous Experience 2 ////////////////////////////////////////////////////////////////////////////-->

											<div class="e-section-title"><i class="fa fa-pencil"></i> Specialist Skills</div>

											<div class="e-col-50">

													<div class="form-group">													
														<label for="specialskill1" class="col-sm-3 control-label">Specialist Skill</label>
														<div class="col-sm-9">
															<input type="text" value="<?php echo strip_tags(get_field("qualifications_and_skills_special_skills1", $profileID)); ?>" class="form-control" id="specialskill1" name="specialskill1" placeholder="e.g. Project Management" maxlength="100"/>
														</div>
														<div class="val-message"></div>
													</div>	
													
													<div class="form-group">													
														<label for="specialskill2" class="col-sm-3 control-label">Specialist Skill</label>
														<div class="col-sm-9">
															<input type="text" value="<?php echo strip_tags(get_field("qualifications_and_skills_special_skills2", $profileID)); ?>" class="form-control" id="specialskill2" name="specialskill2" placeholder="e.g. Project Management" maxlength="100"/>
														</div>
														<div class="val-message"></div>
													</div>	

													<div class="form-group">													
														<label for="specialskill3" class="col-sm-3 control-label">Specialist Skill</label>
														<div class="col-sm-9">
															<input type="text" value="<?php echo strip_tags(get_field("qualifications_and_skills_special_skills3", $profileID)); ?>" class="form-control" id="specialskill3" name="specialskill3" placeholder="e.g. Project Management" maxlength="100"/>
														</div>
														<div class="val-message"></div>
													</div>	

											</div>

											<div class="e-col-50">

												
											</div>

<!--/////////////////////////////////////////////////// END OF SECTION 5 - Special Skills ////////////////////////////////////////////////////////////////////////////-->

											<div class="e-section-title"><i class="fa fa-pencil"></i>  Qualifications</div>

											<div class="e-col-50">

												<div class="form-group">													
													<label for="institution1" class="col-sm-3 control-label">University</label>
													<div class="col-sm-9">
														<select id="institution1" name="institution1" class="form-control multiselect-one-value chosen-select manual-validation" data-placeholder="Choose institution">
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_institution', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);

																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_institution1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
													<div class="val-message"></div>
												</div>	

												<div class="form-group">													
													<label for="coursename1" class="col-sm-3 control-label">Course Topic</label>
													<div class="col-sm-9">
														

											<select id="coursename1" name="coursename1" class="form-control">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_course_topics', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
														$WhereToSearch = get_field("qualifications_and_skills_course_name1", $profileID);
														$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
														if (strpos($item, preg_replace( "/\r|\n/", "", strip_tags($WhereToSearch))) !== false) {
															$selected = 'selected';
														}
													echo "<option value='".$item."' ".$selected.">".$item."</option>";
												}
												?>	

											</select>

													</div>
													<div class="val-message"></div>
												</div>	
				
												<div class="form-group">													
													<label for="qualificationtype1" class="col-sm-3 control-label">Type</label>
													<div class="col-sm-9">
														<select id="qualificationtype1" name="qualificationtype1" class="form-control">
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_qualification_type', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_qualification_type1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>   				

											</div>

											<div class="e-col-50">

												<div class="form-group">													
													<label for="gradeattained1" class="col-sm-3 control-label">Grade Attained</label>
													<div class="col-sm-9">
														<select id="gradeattained1" name="gradeattained1" class="form-control">
<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_grade_attained', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_grade_attained1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>   

												<div class="form-group">													
													<label for="yearattained1" class="col-sm-3 control-label">Year Attained</label>
													<div class="col-sm-9">
														<select id="yearattained1" name="yearattained1" class="form-control">
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_year_attained', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_year_attained1", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>  				
											</div>

<!--/////////////////////////////////////////////////// END OF SECTION 2 - Qualifications  ////////////////////////////////////////////////////////////////////////////-->
									
											<div class="e-section-title"><i class="fa fa-pencil"></i> Additonal Qualification (1/2)</div>

											<div class="e-col-50">

												<div class="form-group">													
													<label for="institution2" class="col-sm-3 control-label">University</label>
													<div class="col-sm-9">
														<select id="institution2" name="institution2" class="form-control multiselect-one-value chosen-select" data-placeholder="Choose institution">
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_institution', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);

																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_institution2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
													<div class="val-message"></div>
												</div>	

												<div class="form-group">													
													<label for="coursename2" class="col-sm-3 control-label">Course Topic</label>
													<div class="col-sm-9">
												<select id="coursename2" name="coursename2" class="form-control" >
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_course_topics', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
														$WhereToSearch = get_field("qualifications_and_skills_course_name2", $profileID);
														$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
														if (strpos($item, preg_replace( "/\r|\n/", "", strip_tags($WhereToSearch))) !== false) {
															$selected = 'selected';
														}
													echo "<option value='".$item."' ".$selected.">".$item."</option>";
												}
												?>	

											</select>
													</div>
													<div class="val-message"></div>
												</div>	
				
												<div class="form-group">													
													<label for="qualificationtype2" class="col-sm-3 control-label">Type</label>
													<div class="col-sm-9">
														<select id="qualificationtype2" name="qualificationtype2" class="form-control" >
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_qualification_type', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_qualification_type2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>   				

											</div>

											<div class="e-col-50">

												<div class="form-group">													
													<label for="gradeattained2" class="col-sm-3 control-label">Grade Attained</label>
													<div class="col-sm-9">
														<select id="gradeattained2" name="gradeattained2" class="form-control" >
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_grade_attained', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_grade_attained2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>   

												<div class="form-group">													
													<label for="yearattained2" class="col-sm-3 control-label">Year Attained</label>
													<div class="col-sm-9">
														<select id="yearattained2" name="yearattained2" class="form-control">
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_year_attained', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_year_attained2", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>  				
											</div>

<!--/////////////////////////////////////////////////// END OF SECTION 3 - Additional Qualification 1 ////////////////////////////////////////////////////////////////////////////-->
									
											<div class="e-section-title"><i class="fa fa-pencil"></i> Additonal Qualification (2/2)</div>

											<div class="e-col-50">

												<div class="form-group">													
													<label for="institution3" class="col-sm-3 control-label">University</label>
													<div class="col-sm-9">
														<select id="institution3" name="institution3" class="form-control multiselect-one-value chosen-select" data-placeholder="Choose institution">
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_institution', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);

																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_institution3", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
													<div class="val-message"></div>
												</div>	

												<div class="form-group">													
													<label for="coursename3" class="col-sm-3 control-label">Course Topic</label>
													<div class="col-sm-9">
												<select id="coursename3" name="coursename3" class="form-control">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_course_topics', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
														$WhereToSearch = get_field("qualifications_and_skills_course_name3", $profileID);
														$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
														if (strpos($item, preg_replace( "/\r|\n/", "", strip_tags($WhereToSearch))) !== false) {
															$selected = 'selected';
														}
													echo "<option value='".$item."' ".$selected.">".$item."</option>";
												}
												?>	

											</select>
													</div>
													<div class="val-message"></div>
												</div>	
				
												<div class="form-group">													
													<label for="qualificationtype3" class="col-sm-3 control-label">Type</label>
													<div class="col-sm-9">
														<select id="qualificationtype2" name="qualificationtype3" class="form-control">
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_qualification_type', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_qualification_type3", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>   				

											</div>

											<div class="e-col-50">

												<div class="form-group">													
													<label for="gradeattained3" class="col-sm-3 control-label">Grade Attained</label>
													<div class="col-sm-9">
														<select id="gradeattained3" name="gradeattained3" class="form-control" >
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_grade_attained', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_grade_attained3", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>   

												<div class="form-group">													
													<label for="yearattained3" class="col-sm-3 control-label">Year Attained</label>
													<div class="col-sm-9">
														<select id="yearattained2" name="yearattained3" class="form-control" >
															<option selected disabled hidden value=''>None Selected</option>
															<?php $variable = get_field('qualifications_and_skills_year_attained', 'option'); 
															$temp_array = explode("\n", $variable);
															foreach ($temp_array as $item) {
																$item = preg_replace( "/\r|\n/", "", $item);
																	$selected = '';
																	$WhereToSearch = get_field("qualifications_and_skills_year_attained3", $profileID);
																	$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																	if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																		$selected = 'selected';
																	}
																echo "<option value='".$item."' ".$selected.">".$item."</option>";
															}
															?>													    
														</select>
													</div>
												</div>  				
											</div>

<!--/////////////////////////////////////////////////// END OF SECTION 4 - Additional Qualification 2  ////////////////////////////////////////////////////////////////////////////-->
									

									

									<div class="e-section-title"><i class="fa fa-pencil"></i>  Ideal Job</div>

									<div class="e-col-50">

										<div class="form-group">													
											<label for="idealjobfunction" class="col-sm-3 control-label">Job Function</label>
											<div class="col-sm-9">
										
											<select class="selectpicker form-control manual-validation" multiple title='Type three letters or select from list' name="idealjobfunction[]"  data-max-options="3" required>		
													<?php $variable = get_field('ideal_job_job_function', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
															$selected = '';
																		$WhereToSearch = get_field("ideal_job_job_function", $profileID);
																		$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																		if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																			$selected = 'selected';
																		}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>	
																									    
												</select>
													
											</div>
											<div class="val-message"></div>
										</div> 

										<div class="form-group">													
											<label for="idealseniority" class="col-sm-3 control-label">Seniority</label>
											<div class="col-sm-9">
												<select id="idealseniority" name="idealseniority" class="form-control" required>
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('ideal_job_seniority', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																$selected = '';
																		$WhereToSearch = get_field("ideal_job_seniority", $profileID);
																		$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																		if (strpos($item, preg_replace( "/\r|\n/", "", trim(strip_tags($WhereToSearch)))) !== false) {
																			$selected = 'selected';
																		}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
											<div class="val-message"></div>
										</div> 	

										<div class="form-group">													
											<label for="idealjoblocation" class="col-sm-3 control-label">Location</label>
											<div class="col-sm-9">
								<select  class="selectpicker form-control manual-validation" multiple title='Type three letters or select from list' name="idealjoblocation[]" data-max-options="3" required>
													
									<?php $variable = get_field('ideal_job_location', 'option'); 
									$temp_array = explode("\n", $variable);
									foreach ($temp_array as $item) {
										
												$selected = '';
													$WhereToSearch = get_field("ideal_job_location", $profileID);
													$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
													$WhereToSearch = str_replace("&#8211;","-", $WhereToSearch);
													$WhereToSearch = preg_replace("/[^A-Za-z ]/", '', strip_tags($WhereToSearch));
													
													
													$item = preg_replace( "/\r|\n/", "", $item);
													if (strpos($WhereToSearch, trim(preg_replace("/[^A-Za-z0-9 ]/", '', $item))) !== false) {
														$selected = 'selected';
													}
										echo "<option value='".$item."' ".$selected.">".$item."</option>";
									}
									?>	
																									    
							</select>
							
											</div>
											<div class="val-message"></div>
										</div> 								

										<div class="form-group">													
											<label for="idealjobindustry" class="col-sm-3 control-label">Industry</label>
											<div class="col-sm-9">
											<select  class="selectpicker form-control manual-validation" multiple title='Type three letters or select from list' name="idealjobindustry[]" data-max-options="3" required>	
													
													<?php $variable = get_field('ideal_job_industry', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
															$selected = '';
																		$WhereToSearch = get_field("ideal_job_industry", $profileID);
																		$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);

																		if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																			$selected = 'selected';
																		}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>	
																									    
												</select>
											</div>
											<div class="val-message"></div>
										</div>

										<div class="form-group">													
											<label for="idealjobcompanytype" class="col-sm-3 control-label">Company Type</label>
											<div class="col-sm-9">
												<select  class="selectpicker form-control manual-validation" multiple title='Type three letters or select from list' name="idealjobcompanytype[]"  data-max-options="3" required>
													
													<?php $variable = get_field('ideal_job_company_type', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																$selected = '';
																		$WhereToSearch = get_field("ideal_job_company_type", $profileID);
																		$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																		if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																			$selected = 'selected';
																		}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>	
																								    
												</select>
											</div>
											<div class="val-message"></div>
										</div>

									</div>

									<div class="e-col-50">


										<div class="form-group">													
											<label for="idealjobcontracttype" class="col-sm-3 control-label">Contract Type</label>
											<div class="col-sm-9">
												<select id="idealjobcontracttype" name="idealjobcontracttype" class="form-control" required>
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('ideal_job_contract_type', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																$selected = '';
																		$WhereToSearch = get_field("ideal_job_contract_type", $profileID);
																		$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																		if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																			$selected = 'selected';
																		}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
											<div class="val-message"></div>
										</div> 


										<div class="form-group">													
											<label for="idealjobbasicsalary" class="col-sm-3 control-label">Basic Salary</label>
											<div class="col-sm-9">
												<select id="idealjobbasicsalary" name="idealjobbasicsalary" class="form-control" required>
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('ideal_job_basic_salary', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
														$selected = '';
																		$WhereToSearch = get_field("ideal_job_basic_salary", $profileID);
																		$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																		if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																			$selected = 'selected';
																		}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
											<div class="val-message"></div>
										</div> 

										<div class="form-group">													
											<label for="idealjobbasicsalarycurrency" class="col-sm-3 control-label">Basic Salary Currency</label>
											<div class="col-sm-9">
												<select id="idealjobbasicsalarycurrency" name="idealjobbasicsalarycurrency" class="form-control" required>
													<option selected disabled hidden value=''>None Selected</option>
													<?php $variable = get_field('ideal_job_basic_salary_currency', 'option'); 
													$temp_array = explode("\n", $variable);
													foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
																		$selected = '';
																		$WhereToSearch = get_field("ideal_job_basic_salary_currency", $profileID);
																		$WhereToSearch = str_replace("&#038;","&", $WhereToSearch);
																		if (strpos($WhereToSearch, preg_replace( "/\r|\n/", "", $item )) !== false) {
																			$selected = 'selected';
																		}
														echo "<option value='".$item."' ".$selected.">".$item."</option>";
													}
													?>													    
												</select>
											</div>
											<div class="val-message"></div>
										</div> 

									</div>
<!--/////////////////////////////////////////////////// END OF SECTION 9 - Ideal Job ////////////////////////////////////////////////////////////////////////////-->
									

									<div class="e-section-title"><i class="fa fa-pencil"></i>  Personal</div>

									<div class="e-col-50">
										<div class="form-group">

											<label for="pointofdifference" class="col-sm-3 control-label">Point of Difference</label>
											<div class="col-sm-9">
												<textarea onkeyup="countChar1(this)" class="form-control" id="pointofdifference" name="pointofdifference" placeholder="Please decribe your point of difference" maxlength="140"><?php echo strip_tags(get_field("sales_pitch_point_of_difference", $profileID)); ?></textarea>
											<div id="charNum1" class="charsleft"></div>
											</div>
											<div class="val-message"></div>

										</div>	


										<div class="form-group">

											<label for="personality" class="col-sm-3 control-label">Personality</label>
											<div class="col-sm-9">
												<textarea onkeyup="countChar2(this)" class="form-control" id="personality" name="personality" placeholder="Please decribe your personality" maxlength="140"><?php echo strip_tags(get_field("sales_pitch_personality", $profileID)); ?></textarea>
											<div id="charNum2" class="charsleft"></div>
											</div>
											<div class="val-message"></div>

										</div>	



									</div>

									<div class="e-col-50">

										<div class="form-group">													
											<label for="aspiration" class="col-sm-3 control-label">Career aspiration</label>
											<div class="col-sm-9">
												<textarea onkeyup="countChar3(this)" class="form-control" id="aspiration" name="aspiration" placeholder="Please decribe your career aspiration" maxlength="140"><?php echo strip_tags(get_field("sales_pitch_career_aspiration", $profileID)); ?></textarea>
											<div id="charNum3" class="charsleft"></div>
											</div>
											<div class="val-message"></div>
										</div>	

									</div>
<!--/////////////////////////////////////////////////// END OF SECTION 10 - Sales Pitch ////////////////////////////////////////////////////////////////////////////-->
									
									<button type="submit" id="submitform" style="display:none">Submit</button>
									</form>

									<div class="profile-update-footer">
										<div class="had-errors">Please correct the hilighted errors above</div>
										<button class="btn btn-danger delete-profile-btn"><i class="fa fa-trash-o"></i> Delete Profile</button>
										<button class="btn btn-success profile-update-btn"><i class="fa fa-paper-plane-o"></i> Update & Publish</button>
									</div>
									</div>

									<form style="display:none">
										<input type="hidden" name="profileid" value="<?php echo $profileID; ?>">
										<input type="hidden" name="userid" value="<?php echo $user_ID; ?>">
										<input type="submit" id="dlt-profile">
									</form>
								</div>
								
						<script>



							function countChar1(val) {
						        var len = val.value.length;
						        if (len >= 140) {
						          val.value = val.value.substring(0, 140);
						        } else {
						          $('#charNum1').text(140 - len);
						        }
						      };

						      	function countChar2(val) {
						        var len = val.value.length;
						        if (len >= 140) {
						          val.value = val.value.substring(0, 140);
						        } else {
						          $('#charNum2').text(140 - len);
						        }
						      };


						      function countChar3(val) {
						        var len = val.value.length;
						        if (len >= 140) {
						          val.value = val.value.substring(0, 140);
						        } else {
						          $('#charNum3').text(140 - len);
						        }
						      };

							$(document).ready(function() {

							$('.delete-profile-btn').click(function(){

							swal({   title: "Are you sure?",
							 text: "You will not be able to recover your profile info!",   
							 type: "warning",   
							 showCancelButton: true,   
							 confirmButtonColor: "#DD6B55",   
							 confirmButtonText: "Yes, delete it!",   
							 cancelButtonText: "No, cancel!",   
							 closeOnConfirm: true,   
							 closeOnCancel: false }, 
							 	function(isConfirm){   if (isConfirm) {     
							 		
							 		 $.ajax({
							        data: ({action : 'deleteprofile', 'profileID': <?php echo $profileID ?> }),
							        type: 'POST',
							        async: true,     
							        url: ajaxurl
							      }).done(function( msg ) {
								        console.log(msg);
								        swal("Deleted!", msg + "  You will be redirected in 2 seconds.", "success");

								        setTimeout(function() {
								        var LogoutURL = "<?php echo wp_logout_url(home_url().'/whats-next/'); ?>";
								        var NewLogoutURL = LogoutURL.replace(/&amp;/g, "&");
								        
  										 window.location.href = NewLogoutURL;
										}, 2000);
								      });

							 	} else {     
							 		swal("Cancelled", "Profile removal has been cancelled.", "error");   
							 	} });
							});

								
							

							$( ".profile-update-btn" ).click(function() {
								$('.had-errors').css('display','none');
								var thisisvalid = true;

								 $('.selectpicker').selectpicker('render');	

								 $("#updateform .manual-validation").each(function(){ 
								 	$(this).closest('.form-group').find('.val-message').html('').css('display','none');
								 	

	             					var hasvalues = $(this).closest('.form-group').find('ul.selectpicker > li.selected').html();
             						  if (!hasvalues) {
             						  	var thisisvalid = false;
             						   $(this).closest('.form-group').find('.val-message').html('please pick a value').css('display','inline-block');
             						   $('.had-errors').css('display','inline-block');
             						  }

        							});


								 	if (thisisvalid !== false)
								 	{
								 		$('.had-errors').css('display','none');
								 	$("#submitform").click();	
								 	}  else {
								 		$('.had-errors').css('display','inline-block');
								 		console.log("There were errors :(");
								 	}                                      

							});

							});


					
					</script>

					<?php } else { echo "<div class='page-content-wrapper'> Sorry but you have no profile to edit!</div>"; }



					} else { echo "<div class='page-content-wrapper'>You need to log in to access this page</div>"; } ?>
						</div>
					</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
