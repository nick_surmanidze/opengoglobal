<?php
/*
Template Name: Aggregated job results
/**
 * The template for displaying all single Jobs.
 *
 * @package Matchly
 */

get_header(); 


//OK. Now we need to use $_GET request tonavigate through pages

$current_page = 1;
if(isset($_GET["pg"]) && is_numeric($_GET["pg"])) {
$current_page = $_GET["pg"];
}
$show_per_page = 10;
$show_offset = ($current_page - 1) * $show_per_page;

?>

	<div id="primary" class="content-area">
	<?php $backgroundimageurl = get_field("background_image", "options"); if(isset($backgroundimageurl)) { ?>
		<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
	<?php } else { ?>
		<main id="main" class="site-main" role="main">	
		<?php } ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="container container-fluid">
				<div class="row content-wrapper">
					<div class="page-content-wrapper no-padding">
						<div class="content-inner">

							<div class="general-page-header"><span class="page-title">My Global Job Matches</span><a class="btn btn-default btn-back-to-dashboard"  href="/dashboard">Back To Dashboard <i class="fa fa-arrow-right"></i></a></div>

							<?php $results = array();
								$results = aggregate_jobs($show_offset);
								?>
							<div class="grey-extra-section">

								<div class="col-sm-6 dashboard-pane">

									<strong class="grey-section-head">Search Criteria</strong><br>

									<?php if(get_current_user_profile()) { 

										$profid = get_current_user_profile(); ?>
										
										<strong>Function:</strong> <?php echo strip_tags(get_field("ideal_job_job_function", $profid)); ?><br>
										<strong>Industry:</strong> <?php echo strip_tags(get_field("ideal_job_industry", $profid)); ?><br>
										<strong>Seniority:</strong> <?php echo strip_tags(get_field("ideal_job_seniority", $profid)); ?><br>
										<strong>Pay:</strong> <?php echo strip_tags(get_field("ideal_job_basic_salary_currency", $profid)); ?> <?php echo strip_tags(get_field("ideal_job_basic_salary", $profid)); ?><br>
										<strong>Location:</strong> <?php echo strip_tags(get_field("ideal_job_location", $profid)); ?><br>


									<?php } else { ?>
									<span class="notice-text">You do not have a profile yet.</span>
									<?php }?>

								</div>

								<div class="col-sm-6 dashboard-pane">

									<strong class="grey-section-head">Stats</strong><br>
										
										<strong>Number of Jobs Found:</strong> <?php echo $results['total_jobs']; ?><br>
										<strong>Current Page:</strong> <?php echo $current_page; ?><br>
									

								</div>

							</div>
							<div class="items-wrapper matched-jobs jobs-aggregator">

								<div class="item-line head-line">
									<div class="item-title">
										<span   title="Job Title">Job Title</span>
									</div>
									<div class="item-employer">
										<span  title="Employer">Recruiter</span>
									</div>
								</div>


								<?php 

								//echo($results['query']);

								if(count($results['current_raw']) < 1) {
									echo "Sorry, no jobs found.";
								} else {
									print_r($results['current_formatted']);
									

								} ?>

								<script>
								console.log('<?php print_r($results["query"]) ?>');
								</script>

							</div>

							<div class="footer-wrap">
								<?php if($current_page > 1) { ?>
								<a class="btn btn-default btn-previous" style="float:left;" href="/smart-search/?pg=<?php echo ($current_page - 1); ?>"><i class="fa fa-arrow-left"></i> Back</a>
								<?php } ?>
								<?php if($results['total_jobs'] > ($show_offset+$show_per_page)) { ?>
									<a class="btn btn-default btn-next"  href="/smart-search/?pg=<?php echo ($current_page + 1); ?>">Next <i class="fa fa-arrow-right"></i></a>
								<?php } ?>

							</div>
							

							</div>

						</div>

					</div>
				</div>
			</div>
		</div>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<script>

function equalizeLines() {

	$(".item-line").each(function(index, value) {	


			  var maxHeight = -1;

				$(this).find("div").each(function() {
			   		$(this).css('height', "auto");
			   	});
			   $(this).find("div").each(function() {
			     maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
			   });



			   $(this).find("div").each(function() {
			     $(this).height(maxHeight);
			   });		

	});

}

$(document).ready(function() {

equalizeLines();

// run the function on window resize
$(window).bind('resize', function () { 
	equalizeLines();
});


    });






</script>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
