<?php
/*
Template Name: Inbox
/**
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		<?php } else { ?>
			<main id="main" class="site-main" role="main">
		<?php } ?>
<?php

function is_current_view($value) {

	$current = '';

	if(isset($_GET['show']) && $_GET['show'] == $value) {
		$current = 'current';
	} elseif (!isset($_GET['show']) && $value == "inbox"){

		$current = 'current';

	}
	print_r($current);
}


?>
		

<?php while ( have_posts() ) : the_post(); ?>
	<div class="container container-fluid">
		<div class="row content-wrapper">							
			<div class="inner-wrapper">
				<div class="inner-title"><i class="fa fa-envelope-o"></i>  Inbox
				<a href="<?php echo site_url(); ?>/dashboard" class="btn btn-default" style="float:right;">Back To Dashboard</a>
			</div>
					<div class="inner-content-wrap inbox-wrapper">

						<div class="col-sm-3 inbox-menu">
							<a href="<?php echo site_url().'/inbox/?show=inbox'; ?>" class="<?php is_current_view('inbox'); ?>"><i class="fa fa-envelope-o"></i> Inbox</a><br>
							<a href="<?php echo site_url().'/inbox/?show=sent'; ?>" class="<?php is_current_view('sent'); ?>"><i class="fa fa-paper-plane-o"></i> Sent</a><br>
							<a href="<?php echo site_url().'/inbox/?show=unread'; ?>" class="<?php is_current_view('unread'); ?>"><i class="fa fa-bell-o"></i> Unread</a><br>
							<a href="<?php echo site_url().'/inbox/?show=starred'; ?>" class="<?php is_current_view('starred'); ?>"><i class="fa fa-star-o"></i> Starred</a><br>
							<a href="<?php echo site_url().'/inbox/?show=all'; ?>" class="<?php is_current_view('all'); ?>"><i class="fa fa-archive"></i> All</a><br>
						</div>

						<div class="col-sm-9 inbox-content">


							<?php 
								global $wpdb;

	if(isset($_GET['show'])) {

		if($_GET['show'] == "inbox"){

			$querystr = "
			   SELECT DISTINCT
			      $wpdb->posts.ID      
			    FROM
			        $wpdb->posts,
			        $wpdb->postmeta AS messagefrom,
			        $wpdb->postmeta AS messageto

			    WHERE       
			        $wpdb->posts.post_type = 'message' 
			        AND $wpdb->posts.post_status = 'publish'

			        AND $wpdb->posts.ID = messageto.post_id 

			        AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".get_current_user_id()."')";
			        $message_id_array = array(); 
					$message_id_array = $wpdb->get_col($querystr, 0);

		} elseif ($_GET['show'] == "sent") {

						$querystr = "
			   SELECT DISTINCT
			      $wpdb->posts.ID      
			    FROM
			        $wpdb->posts,
			        $wpdb->postmeta AS messagefrom,
			        $wpdb->postmeta AS messageto

			    WHERE       
			        $wpdb->posts.post_type = 'message' 
			        AND $wpdb->posts.post_status = 'publish'

			        AND $wpdb->posts.ID = messagefrom.post_id 

			        AND (messagefrom.meta_key = 'message_from' AND messagefrom.meta_value = '".get_current_user_id()."')";
					$message_id_array = array(); 
					$message_id_array = $wpdb->get_col($querystr, 0);

		} elseif ($_GET['show'] == "unread") {

				$querystr = "
				SELECT DISTINCT
				  $wpdb->posts.ID      
				FROM
				    $wpdb->posts,
				    $wpdb->postmeta AS messageread,
				    $wpdb->postmeta AS messageto

				WHERE       
				    $wpdb->posts.post_type = 'message' 
				    AND $wpdb->posts.post_status = 'publish'

				    AND $wpdb->posts.ID = messageread.post_id 
				    AND $wpdb->posts.ID = messageto.post_id 
					
					AND messageto.meta_key = 'message_to' AND messageto.meta_value = '".get_current_user_id()."' 
					AND messageread.meta_key = 'message_read' AND messageread.meta_value = 'unread' ";

					$message_id_array = array(); 
					$message_id_array = $wpdb->get_col($querystr, 0);

		} elseif($_GET['show'] == "all") {

					$querystr = "
		   SELECT DISTINCT
		      $wpdb->posts.ID      
		    FROM
		        $wpdb->posts,
		        $wpdb->postmeta AS messagefrom,
		        $wpdb->postmeta AS messageto

		    WHERE       
		        $wpdb->posts.post_type = 'message' 
		        AND $wpdb->posts.post_status = 'publish'

		        AND $wpdb->posts.ID = messagefrom.post_id 
		        AND $wpdb->posts.ID = messageto.post_id 


		        AND ((messagefrom.meta_key = 'message_from' AND messagefrom.meta_value = '".get_current_user_id()."')
		        OR (messageto.meta_key = 'message_to' AND messageto.meta_value = '".get_current_user_id()."'))
				";

					$message_id_array = array(); 
					$message_id_array = $wpdb->get_col($querystr, 0);

		} elseif($_GET['show'] == "starred") {
				
			$current_user_id = get_current_user_id();

			if(strlen(get_user_meta($current_user_id, "serialized_starred_messages", true)) > 3) {

				$starred_messages_array = unserialize(get_user_meta($current_user_id, "serialized_starred_messages", true));

				if(count($starred_messages_array) > 0) {
					$message_id_array = $starred_messages_array;	
				} else {

					$message_id_array = array();

				}


			} else {

			$message_id_array = array();

			}



		}
	} else {

					$querystr = "
			   SELECT DISTINCT
			      $wpdb->posts.ID      
			    FROM
			        $wpdb->posts,
			        $wpdb->postmeta AS messagefrom,
			        $wpdb->postmeta AS messageto

			    WHERE       
			        $wpdb->posts.post_type = 'message' 
			        AND $wpdb->posts.post_status = 'publish'

			        AND $wpdb->posts.ID = messageto.post_id 

			        AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".get_current_user_id()."')
					";

					$message_id_array = array(); 
					$message_id_array = $wpdb->get_col($querystr, 0);
	}

//print_r($message_id_array);

								// now we need normal wordpress query with pagination
								if(count($message_id_array) > 0):

								if(isset($_GET['show'])) {

									$urlpart = "?show=".$_GET['show']."&amp;";
								} else {
									$urlpart = "?";
								}


									if(!(isset($_GET['show']) && ($_GET['show'] == 'starred' || $_GET['show'] == 'all' )))  {
								?> 
								
								<div class="sorting-line">Sort By: <a href="<?php echo $urlpart; ?>sortby=date">Date</a> | <a href="<?php echo $urlpart; ?>sortby=id">OpenGo ID</a></div> 

								<?php 
							}


										global $paged;
									
										global $wp_query;

										$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;


										$items_per_page = 10;

										if(isset($_GET['sortby'])) {

											if($_GET['sortby'] == 'id') {

												if(isset($_GET['show'])) {

													if($_GET['show'] == 'inbox' || $_GET['show'] == 'all' || $_GET['show'] == 'starred' || $_GET['show'] == 'unread') {
														$args = array(	
															'post_type' => 'message',
															'posts_per_page' => $items_per_page,
															'post__in' => $message_id_array,
														    'paged' => $paged,
														    'meta_key' => 'message_from',
												            'orderby' => 'meta_value_num',
												            'order' => 'ASC'
															);

													} elseif($_GET['show'] == 'sent') {

														$args = array(	
															'post_type' => 'message',
															'posts_per_page' => $items_per_page,
															'post__in' => $message_id_array,
														    'paged' => $paged,
														    'meta_key' => 'message_to',
												            'orderby' => 'meta_value_num',
												            'order' => 'ASC'
															);

													}




												} else {

													// if show is not set than we are in the inbox and should use that parameters

												$args = array(	
													'post_type' => 'message',
													'posts_per_page' => $items_per_page,
													'post__in' => $message_id_array,
												    'paged' => $paged,
												    'meta_key' => 'message_from',
										            'orderby' => 'meta_value_num',
										            'order' => 'ASC'
													);


												}



											} else {
												$args = array(	
													'post_type' => 'message',
													'posts_per_page' => $items_per_page,
													'post__in' => $message_id_array,
												    'paged' => $paged
													);
											}
										} else {
											$args = array(	
													'post_type' => 'message',
													'posts_per_page' => $items_per_page,
													'post__in' => $message_id_array,
												    'paged' => $paged
													);
										}




										// The Query
										query_posts( $args );
										// The Loop
										 if ( have_posts() ) : while ( have_posts() ) : the_post();
											$message_id = get_the_ID();
											?>

											<div class="message-line <?php echo message_read_or_not($message_id); ?>">
												<div class="message-subject"><span data-message-id="<?php echo $message_id; ?>" class="mark-star <?php check_starred($message_id); ?>"></span><a href="<?php echo site_url();?>/conversation/?u=<?php echo get_second_user_id($message_id); ?>"><?php echo get_from_or_to($message_id) ?> (<?php the_title(); ?>)</a></div>
												<div class="message-date"><?php echo message_time_by_id($message_id); ?></div>
												<div class="message-body"><?php echo truncate(get_the_content(), 100, $append=" ..."); ?></div>
											</div>

											<?php endwhile; ?>
			
											<?php matchly_numeric_posts_nav(); ?>

											<?php
											   wp_reset_postdata(); 
											   wp_reset_query(); 	
										endif;

									else: 
										echo "<div class='no-matches'>No Mesages So Far</div>";
									endif; ?>
					</div>
				</div>
			</div>	
		</div>
	</div>	

	<script>

	$( document ).ready(function() {
    
    //if not starred than star
	$( document ).on("click", ".mark-star.not-starred", function(){

		$(this).addClass("is-starred").removeClass("not-starred");

		var messageID = $(this).data("message-id");

		console.log(messageID);

		$.ajax({
	        data: ({action : 'make_message_starred', 'message-id': messageID}),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) { 
	      	console.log(msg); 
	      });

		
	});

	// If starred than un-star
	$( document ).on("click", ".mark-star.is-starred", function(){

		$(this).addClass("not-starred").removeClass("is-starred");

		var messageID = $(this).data("message-id");

		console.log(messageID);

		$.ajax({
	        data: ({action : 'make_message_unstarred', 'message-id': messageID}),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) { 
	      	console.log(msg); 
	      });


	});

	});
	</script>


<?php endwhile; // end of the loop. ?>
</main><!-- #main -->
</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
