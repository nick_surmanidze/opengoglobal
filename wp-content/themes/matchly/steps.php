<?php
/*
Template Name: Steps
/**
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Matchly
 */

get_header(); ?>

<div id="primary" class="content-area">
			<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>

		<?php while ( have_posts() ) : the_post(); ?>
		<div class="container container-fluid">
			<div class="row content-wrapper">

				<div class="q-form-wrapper">



					<?php 

					//import steps. 

					$user_ID = get_current_user_id();
					$previous_Step = get_field( "user_step", 'user_'.$user_ID );
					$previous_Step = preg_replace( "/\r|\n/", "", strip_tags($previous_Step));
					$userRole = get_field( "user_role", 'user_'.$user_ID );
					$userRole = preg_replace( "/\r|\n/", "", strip_tags($userRole));					

					if (!$userRole) {

							get_template_part( 'steps/role'); 

					} elseif ($previous_Step == 0) {

							get_template_part( 'steps/step1');
						
					} elseif ($previous_Step == 1) {

							get_template_part( 'steps/step21'); 

					} elseif ($previous_Step == 21) {

							get_template_part( 'steps/step22');  

					} elseif ($previous_Step == 22) {

							get_template_part( 'steps/step23'); 

					} elseif ($previous_Step == 23) {

							get_template_part( 'steps/step3');

					} elseif ($previous_Step == 3) { 

							get_template_part( 'steps/step4'); 

					} elseif ($previous_Step == 4) { 

							get_template_part( 'steps/step51'); 

					} elseif ($previous_Step == 51) { 

							get_template_part( 'steps/step52'); 

					} elseif ($previous_Step == 52) { 

							get_template_part( 'steps/step6'); 

					}  elseif ($previous_Step == 6) { 

							get_template_part( 'steps/step7'); 

					} else {

							get_template_part( 'steps/steperror'); 
					} ?> 



				</div>

			</div>
		</div>

	<?php endwhile; // end of the loop. ?>

</main><!-- #main -->
</div><!-- #primary -->

<script>
$(function () { $("input,select,textarea").not("[type=submit], .manual-validation").jqBootstrapValidation(); } );
</script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
