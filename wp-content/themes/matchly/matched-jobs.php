<?php
/*
Template Name: Matched Jobs
/**
 * The template for displaying all single Jobs.
 *
 * @package Matchly
 */

get_header(); 
?>

	<div id="primary" class="content-area">
	<?php $backgroundimageurl = get_field("background_image", "options"); if(isset($backgroundimageurl)) { ?>
		<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
	<?php } else { ?>
		<main id="main" class="site-main" role="main">	
		<?php } ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="container container-fluid">
				<div class="row content-wrapper">
					<div class="page-content-wrapper no-padding">
						<div class="content-inner">

							<div class="general-page-header"><span class="page-title">My Job Matches</span><a class="btn btn-default btn-back-to-dashboard"  href="/dashboard">Back To Dashboard <i class="fa fa-arrow-right"></i></a></div>

							<div class="items-wrapper matched-jobs">

								<?php 
								$jobs_array = reverse_search_job(get_current_user_profile());
								if(count($jobs_array) > 0) { ?>


								<div class="item-line head-line">
									<div class="item-title">
										<span >Job Title</span>
										
									</div>
									<div class="item-tag employer">
										<span >Employer</span>
										
									</div>

									<div class="item-tag date-posted">
										<span >Date</span>
										
									</div>
									
								</div>
								<?php							
								global $paged;
								global $wp_query;

								$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

								$profiles_per_page = 10;

								$args = array(	
									'post_type' => 'job',
									'posts_per_page' => $profiles_per_page,
									'post__in' => $jobs_array,
								    'paged' => $paged,
								    'order' => 'DESC',
									);

								// The Query
								query_posts( $args );
								//initiate variable for counting positions
								
								// The Loop
								 if ( have_posts() ) : while ( have_posts() ) : the_post();
										
										$match_id = get_the_ID();

										 
								?>
									<div class="item-line">
									<div class="item-title">
										<span ><a target="_blank" href="<?php echo get_permalink($match_id); ?>"><?php echo get_the_title($match_id); ?></a>
										<?php is_this_job_read($match_id, '<span class="badge">Viewed</span>' , '<span class="badge badge-red">New</span>'); ?>
									</span>
										
									</div>
									<div class="item-tag employer">
										<span >
											<?php 
												$employer = 'N/A';
												if(get_post_meta($match_id, "job_employer", true)) {
													$employer = sanitize_text_field(get_post_meta($match_id, "job_employer", true));
												}
												echo $employer;
												?>
											</span>
										
									</div>
									<div class="item-tag date-posted">
										<span ><?php echo get_the_date(); ?></span>
										
									</div>
									
								</div>
							     <?php

							     	endwhile;	?>
							</div>
							<?php
	
							     else :
							endif;
							// Reset main query object
							
							?>


								<?php } else {

									echo "<span class='no-talents-yet'>You have no matching jobs.</span>";
								} ?>
							

							</div>

								<div class="action-button">
									<ul class="pagination">
										<?php 
										 matchly_numeric_posts_nav(); 
										   wp_reset_postdata();  
										   wp_reset_query(); ?>
									</ul>
									<div class="count-results my-matches"><div class="count"><?php echo count($jobs_array); ?></div>Result(s)</div>
									<img class="ajax-spinner" src="<?php echo get_stylesheet_directory_uri(); ?>/images/spinner-white.GIF" alt="loading">
								</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<script>

function equalizeLines() {

	$(".item-line").each(function(index, value) {	


			  var maxHeight = -1;

				$(this).find("div").each(function() {
			   		$(this).css('height', "auto");
			   	});
			   $(this).find("div").each(function() {
			     maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
			   });



			   $(this).find("div").each(function() {
			     $(this).height(maxHeight);
			   });		

	});

}

$(document).ready(function() {

equalizeLines();

// run the function on window resize
$(window).bind('resize', function () { 
	equalizeLines();
});


    });






</script>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
