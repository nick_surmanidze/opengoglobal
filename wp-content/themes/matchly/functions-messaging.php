<?php
// I will put all messaging functions here


## function for creating a message and getting id

function create_message($to, $subject, $message) {

	$from = get_current_user_id();
	
	// create a message
	$message_args = array(
	  'post_title'    => $subject,
	  'post_status'   => 'publish',
	  'post_type'     => 'message',
	  'post_content'   => $message
	);

	// Insert the profile into the database
	$message_id = wp_insert_post( $message_args );

	if($message_id) {

		update_post_meta($message_id, "message_from", $from);
		update_post_meta($message_id, "message_to", $to);
		update_post_meta($message_id, "message_read", "unread");
	}

	return $message_id;
}


function check_starred($message_id) {

	$current_user_id = get_current_user_id();
	if(strlen(get_user_meta($current_user_id, "serialized_starred_messages", true)) > 3) {

		$starred_array = unserialize(get_user_meta($current_user_id, "serialized_starred_messages", true));

	} else {

		$starred_array = array();
	}

	if(in_array($message_id, $starred_array)) {
		print_r("is-starred");
	} else {
		print_r("not-starred");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
## Make Starred
/////////////////////////////////////////////////////////////////////////////////////////////


add_action('wp_ajax_make_message_starred', 'make_message_starred');
add_action('wp_ajax_nopriv_make_message_starred', 'make_message_starred');


// when we mark message as starred it should get written into the user meta field

function make_message_starred() { 

	if(isset($_POST['message-id'])) {

		$message_id = sanitize_text_field($_POST['message-id']);
		$current_user_id = get_current_user_id();

		// serialized array

		$starred_messages_array = array();

		if(strlen(get_user_meta($current_user_id, "serialized_starred_messages", true)) > 3) {

			$starred_messages_array = unserialize(get_user_meta($current_user_id, "serialized_starred_messages", true));

		}

		if(!in_array($message_id, $starred_messages_array)) {

			array_push($starred_messages_array, $message_id);

			update_user_meta($current_user_id, "serialized_starred_messages", serialize($starred_messages_array));

			print_r("message_starred");
		}



	} else {

		print_r("Message ID NOT SET");
		
	}
die();
}


/////////////////////////////////////////////////////////////////////////////////////////////
## Remove Star
/////////////////////////////////////////////////////////////////////////////////////////////


add_action('wp_ajax_make_message_unstarred', 'make_message_unstarred');
add_action('wp_ajax_nopriv_make_message_unstarred', 'make_message_unstarred');


// when we mark message as starred it should get written into the user meta field

function make_message_unstarred() { 

	if(isset($_POST['message-id'])) {

		$message_id = sanitize_text_field($_POST['message-id']);
		$current_user_id = get_current_user_id();

		// serialized array

		$starred_messages_array = array();

		if(strlen(get_user_meta($current_user_id, "serialized_starred_messages", true)) > 3) {

			$starred_messages_array = unserialize(get_user_meta($current_user_id, "serialized_starred_messages", true));

		}

		if(in_array($message_id, $starred_messages_array)) {

			$array_to_remove = array($message_id);

			$new_starred_array = array_diff($starred_messages_array, $array_to_remove);

			update_user_meta($current_user_id, "serialized_starred_messages", serialize($new_starred_array));

			print_r("message un-starred");

		}



	} else {

		print_r("Message ID NOT SET");
		
	}
die();
}


function message_time_by_id($message_id) {

	// if message date is not today than show date, otherwise, show time
	$value = 'N/A';

	$time = get_the_date( 'g:i A', $message_id );

    $date = get_the_date( 'm/d/Y', $message_id );

    if ($date == date('m/d/Y')) {
    	// this is today - show time
    	$value = $time;
    } else {

    	$value = $date;

    }

	return $value;
}


function get_from_or_to($message_id) {

	$current_user = get_current_user_id();

	$from = get_post_meta($message_id, "message_from", true);

	$to = get_post_meta($message_id, "message_to", true);

	if($current_user == $from) {
		// if current user is the sender than display "to:xxx"

		return("To: OpenGo " .  get_post_meta($message_id, 'message_to', true));

	} else {

		return("From: OpenGo " .  get_post_meta($message_id, 'message_from', true));

	}

}


function get_second_user_id($message_id) {

	$current_user = get_current_user_id();

	$from = get_post_meta($message_id, "message_from", true);

	$to = get_post_meta($message_id, "message_to", true);

	if($current_user == $from) {
		// if current user is the sender than display "to:xxx"

		return($to);

	} else {

		return($from);

	}
}


function truncate($string,$length=100,$append=" ...") {
  $string = trim($string);

  if(strlen($string) > $length) {
    $string = wordwrap($string, $length);
    $string = explode("\n", $string, 2);
    $string = $string[0] . $append;
  }

  return $string;
}


function conversation_in_or_out($message_id) {

	$current_user = get_current_user_id();

	$from = get_post_meta($message_id, "message_from", true);

	$to = get_post_meta($message_id, "message_to", true);

	if($current_user == $from) {
		// if current user is the sender than display "to:xxx"

		return("message-out");

	} else {

		return("message-in");

	}

}




/////////////////////////////////////////////////////////////////////////////////////////////
## Ajax send message
/////////////////////////////////////////////////////////////////////////////////////////////


add_action('wp_ajax_send_message_in_conversation', 'send_message_in_conversation');
add_action('wp_ajax_nopriv_send_message_in_conversation', 'send_message_in_conversation');

function send_message_in_conversation() {

	if(isset($_POST['message']) && isset($_POST['to'])) {

		$message = esc_textarea($_POST['message']);

		$to = $_POST['to'];

		if(isset($_POST['subject'])) {

		$subject = sanitize_text_field($_POST['subject']);
		} else {
			$subject = "No Subject";
		}

		$message_id = create_message($to, $subject, $message);

		$new_message = '<div class="message-item ' . conversation_in_or_out($message_id) . '">';
		$new_message .= '<div class="triangle"></div>';
		$new_message .= '<div class="message-subject">' . get_from_or_to($message_id) . ' (' . get_the_title($message_id) . ')</div>';
		$new_message .= '<div class="message-date">' . message_time_by_id($message_id) .' </div>';
		$new_message .= '<div class="message-body">'. apply_filters("the_content", get_post_field("post_content", $message_id)) . '</div>';
		$new_message .= '</div>';

		print_r($new_message);

	} else {

		print_r("Failed");
	}



	die();
}



/////////////////////////////////////////////////////////////////////////////////////////////
## Ajax Load More Messages
/////////////////////////////////////////////////////////////////////////////////////////////


add_action('wp_ajax_load_more_messages', 'load_more_messages');
add_action('wp_ajax_nopriv_load_more_messages', 'load_more_messages');

function load_more_messages() {

if(isset($_POST['from']) && isset($_POST['offset'])) {

	$me 	= get_current_user_id();
	$other 	= $_POST['from'];
	$offset_id = $_POST['offset'];

	$has_more_values = 0;


	// Now we need to get the array of all the messages


	global $wpdb;

	$querystr = "
				SELECT DISTINCT
				  $wpdb->posts.ID      
				FROM
				    $wpdb->posts,
				    $wpdb->postmeta AS messagefrom,
				    $wpdb->postmeta AS messageto

				WHERE       
				    $wpdb->posts.post_type = 'message' 
				    AND $wpdb->posts.post_status = 'publish'

				    AND $wpdb->posts.ID = messagefrom.post_id 
				    AND $wpdb->posts.ID = messageto.post_id 


				    AND 

				    (
				    	((messagefrom.meta_key = 'message_from' AND messagefrom.meta_value = '".$me."')
				    AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".$other."')) 
				OR 
					((messagefrom.meta_key = 'message_from' AND messagefrom.meta_value = '".$other."')
				    AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".$me."'))
				)
					";

		$message_id_array = array(); 
		$message_id_array = $wpdb->get_col($querystr, 0);

		#now let's search for the position of offset value

		$key = array_search($offset_id, $message_id_array);

		// POSITION OF THE VALUE
		$offset = array_search($key, array_keys($message_id_array));

		$diff_piece = array_slice($message_id_array, $offset);

		$sliced_to_offset_point = array_diff($message_id_array, $diff_piece);

		// NOW WE NEED TO CHECK IF THIS ARRAY HAS MORE THAN 10 ITEMS

			if(count($sliced_to_offset_point) > 10 ) {

				$has_more_values = true;
				$output_array = array_slice($sliced_to_offset_point, -10);

			} else {

				$output_array = $sliced_to_offset_point;
			}

				$message_output = '';

				$new_offset = '';
				$i = 0;
			foreach($output_array as $message_id) {

				$i++;
				if($i == 1) {
				$new_offset = $message_id;
				}


				$message_output .= '<div class="message-item ' . conversation_in_or_out($message_id) . '">';
				$message_output .= '<div class="triangle"></div>';
				$message_output .= '<div class="message-subject">' . get_from_or_to($message_id) . ' (' . get_the_title($message_id) . ')</div>';
				$message_output .= '<div class="message-date">' . message_time_by_id($message_id) .' </div>';
				$message_output .= '<div class="message-body">'. apply_filters("the_content", get_post_field("post_content", $message_id)) . '</div>';
				$message_output .= '</div>';


				mark_read($message_id);

			}


		$return_array = array( 
			'hasmore' => $has_more_values,
		 'messages' => $message_output,
		 'newoffset' => $new_offset
		 
		    );

		print_r(json_encode($return_array));

} else {

	return 0;
}

	die();
}



function message_read_or_not($message_id) {

	if(get_post_meta($message_id, "message_read", true) == "unread") {
		return "unread";
	} else {
		return "read";
	}
}


function mark_read($message_id) {

	$to = get_post_meta($message_id, "message_to", true);
	if(get_current_user_id() == $to) {

	update_post_meta($message_id, "message_read", "read");

		$from = get_post_meta($message_id, "message_from", true); 
		if(get_current_user_id() == $from) { 
			update_post_meta($message_id, "message_read", "read");
		}
		
	}
}


function get_number_of_new_messages() {

	$number = '';

	$me = get_current_user_id();

	global $wpdb;

	$querystr = "
				SELECT DISTINCT
				  $wpdb->posts.ID      
				FROM
				    $wpdb->posts,
				    $wpdb->postmeta AS messageread,
				    $wpdb->postmeta AS messageto

				WHERE       
				    $wpdb->posts.post_type = 'message' 
				    AND $wpdb->posts.post_status = 'publish'

				    AND $wpdb->posts.ID = messageread.post_id 
				    AND $wpdb->posts.ID = messageto.post_id 
					
					AND messageto.meta_key = 'message_to' AND messageto.meta_value = '".$me."' 
					AND messageread.meta_key = 'message_read' AND messageread.meta_value = 'unread' ";

		$message_id_array = array(); 
		$message_id_array = $wpdb->get_col($querystr, 0);

if(count($message_id_array) > 0) {
$number = '<span class="badge" title="You have ' . count($message_id_array). ' new message(s)">'.count($message_id_array).' New</span>';	
}
 return $number;
}



function get_number_of_new_messages_by_id($user_id) {

	$number = '';
	global $wpdb;
	$querystr = "
				SELECT DISTINCT
				  $wpdb->posts.ID      
				FROM
				    $wpdb->posts,
				    $wpdb->postmeta AS messageread,
				    $wpdb->postmeta AS messageto

				WHERE       
				    $wpdb->posts.post_type = 'message' 
				    AND $wpdb->posts.post_status = 'publish'

				    AND $wpdb->posts.ID = messageread.post_id 
				    AND $wpdb->posts.ID = messageto.post_id 
					
					AND messageto.meta_key = 'message_to' AND messageto.meta_value = '".$user_id."' 
					AND messageread.meta_key = 'message_read' AND messageread.meta_value = 'unread' ";

		$message_id_array = array(); 
		$message_id_array = $wpdb->get_col($querystr, 0);

$number = count($message_id_array);	
return $number;
}



function count_inbox() {

global $wpdb;
	$querystr = "
SELECT DISTINCT
  $wpdb->posts.ID      
FROM
    $wpdb->posts,
    $wpdb->postmeta AS messagefrom,
    $wpdb->postmeta AS messageto

WHERE       
    $wpdb->posts.post_type = 'message' 
    AND $wpdb->posts.post_status = 'publish'

    AND $wpdb->posts.ID = messageto.post_id 

    AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".get_current_user_id()."')
	";

	$message_id_array = array(); 
	$message_id_array = $wpdb->get_col($querystr, 0);	

	$user_id_array = array();

	$user_id = '';
	foreach($message_id_array as $message_id) {

		$user_id = get_post_meta($message_id, 'message_from', true );

		array_push($user_id_array, $user_id);

	}

	return count(array_unique($user_id_array));
}

function count_sent() {

global $wpdb;
	$querystr = "
SELECT DISTINCT
  $wpdb->posts.ID      
FROM
    $wpdb->posts,
    $wpdb->postmeta AS messagefrom,
    $wpdb->postmeta AS messageto

WHERE       
    $wpdb->posts.post_type = 'message' 
    AND $wpdb->posts.post_status = 'publish'

    AND $wpdb->posts.ID = messageto.post_id 

    AND (messageto.meta_key = 'message_from' AND messageto.meta_value = '".get_current_user_id()."')
	";

	$message_id_array = array(); 
	$message_id_array = $wpdb->get_col($querystr, 0);	

	$user_id_array = array();

	$user_id = '';
	foreach($message_id_array as $message_id) {

		$user_id = get_post_meta($message_id, 'message_to', true );

		array_push($user_id_array, $user_id);

	}

	return count(array_unique($user_id_array));
}

