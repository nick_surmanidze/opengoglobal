<?php
$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

global $wpdb;

$querystr = "
   SELECT DISTINCT
      $wpdb->posts.ID      
    FROM
        $wpdb->posts
    WHERE       
        $wpdb->posts.post_type = 'profile' 
        AND $wpdb->posts.post_status = 'publish'
		";
		 
$pageposts = $wpdb->get_col($querystr, 0);

	$profileidarray1 = $pageposts;
	$profileidarray2 = $pageposts;
	$profileidarray3 = $pageposts;
	$profileidarray4 = $pageposts;
	$profileidarray5 = $pageposts;
	$profileidarray6 = $pageposts;
	$profileidarray7 = $pageposts;
	$profileidarray8 = $pageposts;
	$profileidarray9 = $pageposts;
	$profileidarray10 = $pageposts;
	$profileidarray11 = $pageposts;
	$profileidarray12 = $pageposts;
	$profileidarray13 = $pageposts;
	$profileidarray14 = $pageposts;
	$profileidarray15 = $pageposts;
	$profileidarray16 = $pageposts;
	$profileidarray17 = $pageposts;



if(isset($_POST['idealindustry'])) {

$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $_POST['idealindustry'];

$qstring = $qstring . "(idealindustry.meta_key = 'ideal_job_industry' AND idealindustry.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealindustry.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealindustry";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealindustry.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray1 = array();
$profileidarray1 = $pageposts;

}





if(isset($_POST['idealseniority'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealseniority'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealseniority.meta_key = 'ideal_job_seniority' AND idealseniority.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealseniority";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealseniority.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray2 = array();
$profileidarray2 = $pageposts;
}



if(isset($_POST['idealjobtitle'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealjobtitle'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(idealjobtitle.meta_key = 'ideal_job_job_function' AND idealjobtitle.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealjobtitle";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealjobtitle.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray3 = array();
$profileidarray3 = $pageposts;
}



if(isset($_POST['idealjoblocation'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $_POST['idealjoblocation'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(ideallocation.meta_key = 'ideal_job_location' AND ideallocation.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS ideallocation";
$querywhere = $querywhere . "AND $wpdb->posts.ID = ideallocation.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray4 = array();
$profileidarray4 = $pageposts;

}



if(isset($_POST['idealjobbasicsalary'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['idealjobbasicsalary'];

foreach ($qarray as $param) {

$param = str_replace("< ", "", $param);

$param = str_replace("> ", "", $param);

$param = $wpdb->esc_like( sanitize_text_field($param));

	if ($param == "30.000") {

		$qstring = $qstring . "(idealpay.meta_key = 'ideal_job_basic_salary' AND idealpay.meta_value LIKE '%".$param."%' AND idealpay.meta_value NOT LIKE '%30.000-%') OR ";	
	} 
	else 
	{
		$qstring = $qstring . "(idealpay.meta_key = 'ideal_job_basic_salary' AND idealpay.meta_value LIKE '%".$param."%') OR ";
	}



}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealpay";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealpay.post_id "; 

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray5 = array();
$profileidarray5 = $pageposts;
}





if(isset($_POST['idealjobcompanytype'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealjobcompanytype'];
$qstring = $qstring . "(idealcompanytype.meta_key = 'ideal_job_company_type' AND idealcompanytype.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(idealcompanytype.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealcompanytype";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealcompanytype.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray6 = array();
$profileidarray6 = $pageposts;
}





if(isset($_POST['idealjobcontract'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['idealjobcontract'];
$qstring = $qstring . "(idealcontract.meta_key = 'ideal_job_contract_type' AND idealcontract.meta_value LIKE '%any%') OR ";

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(idealcontract.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS idealcontract";
$querywhere = $querywhere . "AND $wpdb->posts.ID = idealcontract.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray7 = array();
$profileidarray7 = $pageposts;
}



if(isset($_POST['currentjobindustry'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['currentjobindustry'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentindustry.meta_key = 'search_industry_experience' AND currentindustry.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentindustry";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentindustry.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray8 = array();
$profileidarray8 = $pageposts;
}





if(isset($_POST['currentemployer'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['currentemployer'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(currentemployer.meta_key = 'search_company_experience' AND currentemployer.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);

$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentemployer";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentemployer.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray9 = array();
$profileidarray9 = $pageposts;
}



if(isset($_POST['currentjobtitle'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['currentjobtitle'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentjobtitle.meta_key = 'search_role_experience' AND currentjobtitle.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentjobtitle";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentjobtitle.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray10 = array();
$profileidarray10 = $pageposts;
}





if(isset($_POST['currentjobseniority'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['currentjobseniority'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(currentseniority.meta_key = 'current_job_seniority' AND currentseniority.meta_value LIKE '%".$param."%') OR ";

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentseniority";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentseniority.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray11 = array();
$profileidarray11 = $pageposts;
}



if(isset($_POST['currentsalary'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['currentsalary'];

foreach ($qarray as $param) {

$param = str_replace("< ", "", $param);
$param = str_replace("> ", "", $param);
$param = $wpdb->esc_like( sanitize_text_field($param));

	if ($param == "30.000") {
		$qstring = $qstring . "(currentpay.meta_key = 'current_job_basic_salary' AND currentpay.meta_value LIKE '%".$param."%' AND currentpay.meta_value NOT LIKE '%30.000-%') OR ";	
	} 
	else 
	{
		$qstring = $qstring . "(currentpay.meta_key = 'current_job_basic_salary' AND currentpay.meta_value LIKE '%".$param."%') OR ";
	}

}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentpay";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentpay.post_id ";


// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray12 = array();
$profileidarray12 = $pageposts;
}





if(isset($_POST['currentcontract'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['currentcontract'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(currentcontract.meta_key = 'current_job_type_of_contract' AND currentcontract.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS currentcontract";
$querywhere = $querywhere . "AND $wpdb->posts.ID = currentcontract.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray13 = array();
$profileidarray13 = $pageposts;
}



if(isset($_POST['institution'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['institution'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));	
$qstring = $qstring . "(institution.meta_key = 'search_institution' AND institution.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS institution";
$querywhere = $querywhere . "AND $wpdb->posts.ID = institution.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray14 = array();
$profileidarray14 = $pageposts;
}



if(isset($_POST['qualification'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();

$qarray = $_POST['qualification'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(qualification.meta_key = 'search_qualification_type' AND qualification.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS qualification";
$querywhere = $querywhere . "AND $wpdb->posts.ID = qualification.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray15 = array();
$profileidarray15 = $pageposts;
}



//course name

if(isset($_POST['coursename'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';

$qarray = array();

$qarray = $_POST['coursename'];

foreach ($qarray as $param) {
$param = $wpdb->esc_like( sanitize_text_field($param));
$qstring = $qstring . "(coursename.meta_key = 'search_course_name' AND coursename.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3); 
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS coursename";
$querywhere = $querywhere . "AND $wpdb->posts.ID = coursename.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray16 = array();
$profileidarray16 = $pageposts;
}

//grade

if(isset($_POST['gradeattained'])) {
	$queryparameters = "";
$queryfrom = "";
$querywhere = "";
$pageposts = array();

$qstring = '';
$qarray = array();
$qarray = $_POST['gradeattained'];

foreach ($qarray as $param) {

$param = $wpdb->esc_like( sanitize_text_field($param));

$qstring = $qstring . "(gradeattained.meta_key = 'search_grade' AND gradeattained.meta_value LIKE '%".$param."%') OR ";
}

$qstring = substr($qstring, 0, -3);
$queryparameters = $queryparameters . " AND (" . $qstring . ")";

$queryfrom = $queryfrom . ", $wpdb->postmeta AS gradeattained";
$querywhere = $querywhere . "AND $wpdb->posts.ID = gradeattained.post_id ";

// compose a query and run it

$querystr = "

   SELECT DISTINCT
      $wpdb->posts.ID      

    FROM

        $wpdb->posts,

        $wpdb->postmeta AS refsort".$queryfrom."

    WHERE       

        $wpdb->posts.post_type = 'profile' 

        AND $wpdb->posts.post_status = 'publish'

        AND $wpdb->posts.ID = refsort.post_id

        ".$querywhere."

        AND refsort.meta_key = 'refferal_sort' 

        ".$queryparameters." ORDER BY refsort.meta_value DESC";


$pageposts = $wpdb->get_col($querystr, 0);

$profileidarray17 = array();
$profileidarray17 = $pageposts;
}






$profileidarray = array();

$profileidarray = array_intersect(
	$profileidarray1,
	$profileidarray2,
	$profileidarray3,
	$profileidarray4,
	$profileidarray5,
	$profileidarray6,
	$profileidarray7,
	$profileidarray8,
	$profileidarray9,
	$profileidarray10,
	$profileidarray11,
	$profileidarray12,
	$profileidarray13,
	$profileidarray14,
	$profileidarray15,
	$profileidarray16,
	$profileidarray17
	);



$number_of_results = count($profileidarray); 




//the piece of code below runs search field concatenation for each profile. Shuld be done once.
// $foreachid = array();
// $foreachid = array_unique($profileidarray);
// $ii=0;
// foreach ($foreachid as $profile_id) {
//     $ii++;
//     if (($ii < 1500) && ($ii > 799)) {
//        ////merge_search_fields($profile_id);
// ////calculate_profile_progress($profile_id);
// $profile_to_job = generate_talent_fields_array($profile_id);
// ////delete_post_meta($profile_id, "profile_to_job_array");
// update_post_meta($profile_id, "profile_to_job_array_serialized", serialize($profile_to_job)); 
//     }


// }


print_r(number_format($number_of_results));

die();




