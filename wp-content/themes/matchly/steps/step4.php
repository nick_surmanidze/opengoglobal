<!-- ////////////////////  Beginning of Step 4 ////////////////////////////////////////////-->

					<div class="q-step" id="step4">
						<div class="q-step-title">What Is Your Current (Or Most Recent) Job?</div>
						<div class="q-step-indicator">
							<ul class="stepline">
								<li class="cur-step">1</li>
								<li class="cur-step">2</li>
								<li class="cur-step">3</li>
							</ul>


						</div>
						<div class="q-step-questions-wrapper">

							<div class="q-step-form">
								<form class="form-horizontal" role="form" method="post" id="step4form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">
									<input type="hidden" value="4" name="step"/>

									
									<div class="form-group">													
										<label for="organisationname" class="col-sm-3 control-label">Organisation Name</label>
										<div class="col-sm-9">
											<input type="text" class="form-control validate-it" id="organisationname" name="organisationname" placeholder="Enter Organisation Name" maxlength="100"/>
										</div>
										<div class="val-message"></div>
									</div>	
									
									<div class="form-group">													
										<label for="industry" class="col-sm-3 control-label">Industry</label>
										<div class="col-sm-9">
											<select id="industry" name="industry" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_industry', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>	
																							    
											</select>
										</div>
										<div class="val-message"></div>
									</div>	

									<div class="form-group">													
										<label for="companytype" class="col-sm-3 control-label">Company Type</label>
										<div class="col-sm-9">
											<select id="companytype" name="companytype" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_company_type', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>	
																								    
											</select>
										</div>
										<div class="val-message"></div>
									</div>  


									<div class="form-group">													
										<label for="jobfunction" class="col-sm-3 control-label">Job Function</label>
										<div class="col-sm-9">
											<select id="jobfunction" name="jobfunction" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_job_function', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div> 


									<div class="form-group">													
										<label for="currentseniority" class="col-sm-3 control-label">Seniority</label>
										<div class="col-sm-9">
											<select id="currentseniority" name="currentseniority" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_seniority', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div> 

									<div class="form-group">													
										<label for="currentjobyearstarted" class="col-sm-3 control-label">Year Started</label>
										<div class="col-sm-9">
											<select id="currentjobyearstarted" name="currentjobyearstarted" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_year_started', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div> 

									<div class="form-group">													
										<label for="currentjobtypeofcontract" class="col-sm-3 control-label">Type of Contract</label>
										<div class="col-sm-9">
											<select id="currentjobtypeofcontract" name="currentjobtypeofcontract" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_type_of_contract', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div> 

									<div class="form-group">													
										<label for="currentnoticeperiod" class="col-sm-3 control-label">Notice Period</label>
										<div class="col-sm-9">
											<select id="currentnoticeperiod" name="currentnoticeperiod" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_current_notice_period', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div> 


									<div class="form-group">													
										<label for="currentjobbasicsalary" class="col-sm-3 control-label">Basic Salary</label>
										<div class="col-sm-9">
											<select id="currentjobbasicsalary" name="currentjobbasicsalary" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_basic_salary', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div> 

									<div class="form-group">													
										<label for="currentjobcurrency" class="col-sm-3 control-label">Basic Salary Currency</label>
										<div class="col-sm-9">
											<select id="currentjobcurrency" name="currentjobcurrency" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('current_job_basic_salary_currency', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div> 

									<button type="submit" id="submitform4" style="display:none">Submit</button>
									
								</form>

							</div>

							<div class="q-step-instructions">
								<div class="q-instructions-inner">
									
									<?php the_field('current_job', 'option'); ?>
								</div>
							</div>

						</div>

						<div class="q-step-footer">
							<div class="btn btn-default save-and-continue-later-btn"><a href="<?php echo wp_logout_url( home_url().'/thank-you-for-registration/' ); ?>">Save and Continue Later</a></div>
							<button class="btn btn-default q-next-step4">Create Profile</button>
							


							<script>
							$(document).ready(function() {
								


							$( ".q-next-step4" ).click(function() {

								function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step4form').submit();
									
								} 


							});

							});

							</script>
						</div>
					</div>
<!-- ////////////////////  END of Step 4 ////////////////////////////////////////////-->