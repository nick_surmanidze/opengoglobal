				<!-- ////////////////////  Beginning of Step 1 ////////////////////////////////////////////-->
				<div class="q-step" id="step1">
					<div class="q-step-title">Let’s Get Started</div>
					
					<div class="q-step-questions-wrapper">

						<div class="q-step-form">
							
											<ul class="role-picker">
												<li><a href="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php?role=talent"><i class="fa fa-graduation-cap"></i> Talent</a></li>
												<li><a href="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php?role=recruiter"><i class="fa fa-briefcase"></i> Recruiter</a></li>
												
											</ul>

						</div>

						<div class="q-step-instructions">
							<div class="q-instructions-inner">
								
								<?php the_field('role_picker', 'option'); ?>
							</div>
						</div>

					</div>

					<div class="q-step-footer">

					</div>
				</div>
				<!-- ////////////////////  END of Step 1 ////////////////////////////////////////////-->