<!-- ////////////////////  Beginning of Step 52 ////////////////////////////////////////////-->



					<div class="q-step" id="step52">

						<div class="q-step-title">Add A Previous Job</div>

						<div class="q-step-indicator">

							<ul class="stepline">


							</ul>





						</div>

						<div class="q-step-questions-wrapper">



							<div class="q-step-form">

								<form class="form-horizontal" role="form" method="post" id="step52form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">

									<input type="hidden" value="52" name="step"/>



									<div class="form-group">													

										<label for="previousorganisationname2" class="col-sm-3 control-label">Organisation Name</label>

										<div class="col-sm-9">

											<input type="text" class="form-control validate-it" id="previousorganisationname2" name="previousorganisationname2" placeholder="Enter Organisation Name" maxlength="100" />

										</div>

										<div class="val-message"></div>

									</div>										



									<div class="form-group">													

										<label for="previousindustry2" class="col-sm-3 control-label">Industry</label>

										<div class="col-sm-9">

											<select id="previousindustry2" name="previousindustry2" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_industry', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
											<div class="val-message"></div>
									</div> 







									<div class="form-group">													

										<label for="previouscompanytype2" class="col-sm-3 control-label">Company Type</label>

										<div class="col-sm-9">

											<select id="previouscompanytype2" name="previouscompanytype2" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('current_job_company_type', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>	

																								    

											</select>

										</div>

										<div class="val-message"></div>

									</div>  





									<div class="form-group">													

										<label for="previousjobfunction2" class="col-sm-3 control-label">Job Function</label>

										<div class="col-sm-9">

											<select id="previousjobfunction2" name="previousjobfunction2" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_job_function', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
											<div class="val-message"></div>
									</div> 





									<div class="form-group">													

										<label for="previousseniority2" class="col-sm-3 control-label">Seniority</label>

										<div class="col-sm-9">

											<select id="previousseniority2" name="previousseniority2" class="form-control validate-it" >

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_seniority', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
											<div class="val-message"></div>
									</div> 



									<div class="form-group">													

										<label for="previousyearstarted2" class="col-sm-3 control-label">Year Started</label>

										<div class="col-sm-9">

											<select id="previousyearstarted2" name="previousyearstarted2" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_year_started', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
											<div class="val-message"></div>
									</div> 





									<div class="form-group">													

										<label for="previousyearended2" class="col-sm-3 control-label">Year Ended</label>

										<div class="col-sm-9">

											<select id="previousyearended2" name="previousyearended2" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_year_ended', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
											<div class="val-message"></div>
									</div> 



									<div class="form-group">													

										<label for="previoustypeofcontract2" class="col-sm-3 control-label">Type of Contract</label>

										<div class="col-sm-9">

											<select id="previoustypeofcontract2" name="previoustypeofcontract2" class="form-control validate-it" >

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_type_of_contract', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {

													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
											<div class="val-message"></div>
									</div> 



									<button type="submit" id="submitform52" style="display:none">Submit</button>

									

								</form>



							</div>



							<div class="q-step-instructions">

								<div class="q-instructions-inner">

									

									<?php the_field('previous_experience', 'option'); ?>

								</div>

							</div>



						</div>



						<div class="q-step-footer">

							<div class="btn btn-default save-and-continue-later-btn"><a href="/wp-content/themes/matchly/additional-information-controller.php?step=back"><i class="fa fa-arrow-circle-left"></i> Back</a></div>

							<button class="btn btn-default q-next-step52"><i class="fa fa-plus-circle"></i> Add Job To Profile</button>





							<script>

							$(document).ready(function() {

							$( ".q-next-step52" ).click(function() {

							function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step52form').submit();
									
								} 
						 	                                   

							});

							});



							</script>

						</div>

					</div>

<!-- ////////////////////  END of Step 52 ////////////////////////////////////////////-->