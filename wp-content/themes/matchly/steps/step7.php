<!-- ////////////////////  Beginning of Step 7 ////////////////////////////////////////////-->

					<div class="q-step" id="step7">
						<div class="q-step-title">Add Personal Statement</div>
						<div class="q-step-indicator">
							<ul class="stepline">

							</ul>


						</div>
						<div class="q-step-questions-wrapper">

							<div class="q-step-form">
								<form class="form-horizontal" role="form" method="post" id="step7form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">
									<input type="hidden" value="7" name="step"/>

									
									<div class="form-group">													
										<label for="pointofdifference" class="col-sm-3 control-label">Point of Difference</label>
										<div class="col-sm-9">
											<textarea onkeyup="countChar1(this)" class="form-control validate-it" id="pointofdifference" name="pointofdifference" placeholder="What is unique or special about you that sets you apart from others?" maxlength="140"></textarea>
										<div id="charNum1" class="charsleft"></div>
										</div>
										<div class="val-message"></div>
									</div>	


									<div class="form-group">													
										<label for="personality" class="col-sm-3 control-label">Personality</label>
										<div class="col-sm-9">
											<textarea onkeyup="countChar2(this)" class="form-control validate-it" id="personality" name="personality" placeholder="What words best describe your personality?" maxlength="140"></textarea>
										<div id="charNum2" class="charsleft"></div>
										</div>
										<div class="val-message"></div>
									</div>	

									<div class="form-group">													
										<label for="aspiration" class="col-sm-3 control-label">Career aspiration</label>
										<div class="col-sm-9">
											<textarea  onkeyup="countChar3(this)" class="form-control validate-it" id="aspiration" name="aspiration" placeholder="Ultimately, what are you trying to achieve with your career (i.e. where do you want to get to)?" maxlength="140"></textarea>
										<div id="charNum3" class="charsleft"></div>
										</div>
										<div class="val-message"></div>
									</div>	


									<button type="submit" id="submitform7" style="display:none">Submit</button>
									
								</form>

							</div>

							<div class="q-step-instructions">
								<div class="q-instructions-inner">
									
									<?php the_field('sales_pitch', 'option'); ?>
								</div>
							</div>

						</div>

						<div class="q-step-footer">
							<div class="btn btn-default save-and-continue-later-btn"><a href="/wp-content/themes/matchly/additional-information-controller.php?step=back"><i class="fa fa-arrow-circle-left"></i> Back</a></div>
							<button class="btn btn-default q-next-step7"><i class="fa fa-plus-circle"></i> Add Personal Statement</button>


							<script>





						

							 function countChar1(val) {
						        var len = val.value.length;
						        if (len >= 140) {
						          val.value = val.value.substring(0, 140);
						        } else {
						          $('#charNum1').text(140 - len);
						        }
						      };

						      	function countChar2(val) {
						        var len = val.value.length;
						        if (len >= 140) {
						          val.value = val.value.substring(0, 140);
						        } else {
						          $('#charNum2').text(140 - len);
						        }
						      };


						      function countChar3(val) {
						        var len = val.value.length;
						        if (len >= 140) {
						          val.value = val.value.substring(0, 140);
						        } else {
						          $('#charNum3').text(140 - len);
						        }
						      };

					$(document).ready(function() {


							$( ".q-next-step7" ).click(function() {

								function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step7form').submit();
									
								} 
                                       

							});

							});

							</script>
						</div>
					</div>
<!-- ////////////////////  END of Step 7 ////////////////////////////////////////////-->