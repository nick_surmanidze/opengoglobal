<!-- ////////////////////  Beginning of Step 6 ////////////////////////////////////////////-->



					<div class="q-step" id="step6">

						<div class="q-step-title">What Type Of Job Do You Want To Do Next?</div>

						<div class="q-step-indicator">

							<ul class="stepline">

								<li class="cur-step">1</li>

								<li class="cur-step">2</li>

								<li>3</li>


							</ul>





						</div>

						<div class="q-step-questions-wrapper">



							<div class="q-step-form">

								<form class="form-horizontal" role="form" method="post" id="step6form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">

									<input type="hidden" value="6" name="step"/>



									<div class="form-group">													

										<label for="idealjobfunction" class="col-sm-3 control-label">Job Function</label>

										<div class="col-sm-9">

<select class="selectpicker form-control manual-validation" multiple title='Select up to three values' name="idealjobfunction[]"  data-max-options="3" >
												

												<?php $variable = get_field('ideal_job_job_function', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>	

																								    

											</select>

										</div>

										<div class="val-message"></div>

									</div> 



									<div class="form-group">													

										<label for="idealseniority" class="col-sm-3 control-label">Seniority</label>

										<div class="col-sm-9">

											<select id="idealseniority" name="idealseniority" class="form-control validate-it" >

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('ideal_job_seniority', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>

										<div class="val-message"></div>

									</div> 	



									<div class="form-group">													

										<label for="idealjoblocation" class="col-sm-3 control-label">Location</label>

										<div class="col-sm-9">

									
<select id="idealjoblocation" class="selectpicker form-control manual-validation" multiple title='Select up to three values' name="idealjoblocation[]" data-max-options="3" >
												

												<?php $variable = get_field('ideal_job_location', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>	

																								    

											</select>

										</div>

										<div class="val-message"></div>

									</div> 								



									<div class="form-group">													

										<label for="idealjobindustry" class="col-sm-3 control-label">Industry</label>

										<div class="col-sm-9">

										

											<select id="idealjobindustry" class="selectpicker form-control manual-validation" multiple title='Select up to three values' name="idealjobindustry[]" data-max-options="3" >	

												<?php $variable = get_field('ideal_job_industry', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>	

																								    

											</select>

										</div>

										<div class="val-message"></div>

									</div>





									<div class="form-group">													

										<label for="idealjobcompanytype" class="col-sm-3 control-label">Company Type</label>

										<div class="col-sm-9">

											

												<select id="idealjobcompanytype"class="selectpicker form-control manual-validation" multiple title='Select up to three values' name="idealjobcompanytype[]"  data-max-options="3" >	
										
												<?php $variable = get_field('ideal_job_company_type', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>	

																								    

											</select>

										</div>

										<div class="val-message"></div>

									</div>



									<div class="form-group">													

										<label for="idealjobcontracttype" class="col-sm-3 control-label">Contract Type</label>

										<div class="col-sm-9">

											<select id="idealjobcontracttype" name="idealjobcontracttype" class="form-control validate-it" >

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('ideal_job_contract_type', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>

										<div class="val-message"></div>

									</div> 





									<div class="form-group">													

										<label for="idealjobbasicsalary" class="col-sm-3 control-label">Basic Salary</label>

										<div class="col-sm-9">

											<select id="idealjobbasicsalary" name="idealjobbasicsalary" class="form-control validate-it" >

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('ideal_job_basic_salary', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>

										<div class="val-message"></div>

									</div> 



									<div class="form-group">													

										<label for="idealjobbasicsalarycurrency" class="col-sm-3 control-label">Basic Salary Currency</label>

										<div class="col-sm-9">

											<select id="idealjobbasicsalarycurrency" name="idealjobbasicsalarycurrency" class="form-control validate-it" >

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('ideal_job_basic_salary_currency', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>

										<div class="val-message"></div>

									</div> 





									<button type="submit" id="submitform6" style="display:none">Submit</button>

									

								</form>



							</div>



							<div class="q-step-instructions">

								<div class="q-instructions-inner">

									

									<?php the_field('ideal_job', 'option'); ?>

								</div>

							</div>



						</div>



						<div class="q-step-footer">

							<div class="btn btn-default save-and-continue-later-btn"><a href="<?php echo wp_logout_url( home_url().'/thank-you-for-registration/' ); ?>">Save and Continue Later</a></div>

							<button class="btn btn-default q-next-step6">Next Step</button>





							<script>

							$(document).ready(function() {

								if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
								    $('.selectpicker').selectpicker('mobile');
								}


							$( ".q-next-step6" ).click(function() {




								function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});



								 $('.selectpicker').selectpicker('render');	

								 $(".manual-validation").each(function(){ 

								 	$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             					hasvalues = $(this).closest('.form-group').find('ul.selectpicker > li.selected').html();


             						  if (!hasvalues) {

             						  	thisisvalid = false;

             						   $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');

             						  }
        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step6form').submit();
									
								} 


                                    

							});



							});



							</script>

						</div>

					</div>

<!-- ////////////////////  END of Step 6 ////////////////////////////////////////////-->