<!-- ////////////////////  Beginning of Step 51 ////////////////////////////////////////////-->



					<div class="q-step" id="step51">

						<div class="q-step-title">Add A Previous Job</div>

						<div class="q-step-indicator">

							<ul class="stepline">


							</ul>





						</div>

						<div class="q-step-questions-wrapper">



							<div class="q-step-form">

								<form class="form-horizontal first-submit" role="form" method="post" id="step51form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">

									<input type="hidden" value="51" name="step"/>



									<div class="form-group">													

										<label for="previousorganisationname1" class="col-sm-3 control-label">Organisation Name</label>

										<div class="col-sm-9">

											<input type="text" class="form-control validate-it" id="previousorganisationname1" name="previousorganisationname1" placeholder="Enter Organisation Name" maxlength="100"/>

										</div>

										<div class="val-message"></div>

									</div>										



									<div class="form-group">													

										<label for="previousindustry1" class="col-sm-3 control-label">Industry</label>

										<div class="col-sm-9">

											<select id="previousindustry1" name="previousindustry1" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_industry', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
														$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
										<div class="val-message"></div>
									</div> 







									<div class="form-group">													

										<label for="previouscompanytype1" class="col-sm-3 control-label">Company Type</label>

										<div class="col-sm-9">

											<select id="previouscompanytype1" name="previouscompanytype1" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('current_job_company_type', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
												$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>	

																								    

											</select>

										</div>

									<div class="val-message"></div>

									</div>  





									<div class="form-group">													

										<label for="previousjobfunction1" class="col-sm-3 control-label">Job Function</label>

										<div class="col-sm-9">

											<select id="previousjobfunction1" name="previousjobfunction1" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_job_function', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
												$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
										<div class="val-message"></div>
									</div> 





									<div class="form-group">													

										<label for="previousseniority1" class="col-sm-3 control-label">Seniority</label>

										<div class="col-sm-9">

											<select id="previousseniority1" name="previousseniority1" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_seniority', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
												$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
										<div class="val-message"></div>
									</div> 



									<div class="form-group">													

										<label for="previousyearstarted1" class="col-sm-3 control-label">Year Started</label>

										<div class="col-sm-9">

											<select id="previousyearstarted1" name="previousyearstarted1" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_year_started', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
												$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
										<div class="val-message"></div>
									</div> 





									<div class="form-group">													

										<label for="previousyearended1" class="col-sm-3 control-label">Year Ended</label>

										<div class="col-sm-9">

											<select id="previousyearended1" name="previousyearended1" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_year_ended', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
												$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
										<div class="val-message"></div>
									</div> 



									<div class="form-group">													

										<label for="previoustypeofcontract1" class="col-sm-3 control-label">Type of Contract</label>

										<div class="col-sm-9">

											<select id="previoustypeofcontract1" name="previoustypeofcontract1" class="form-control validate-it">

												<option selected disabled hidden value=''>None Selected</option>

												<?php $variable = get_field('previous_experience_type_of_contract', 'option'); 

												$temp_array = explode("\n", $variable);

												foreach ($temp_array as $item) {
												$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";

												}

												?>													    

											</select>

										</div>
										<div class="val-message"></div>

									</div> 



									<button type="submit" id="submitform51" style="display:none">Submit</button>

									

								</form>





							</div>



							<div class="q-step-instructions">

								<div class="q-instructions-inner">

									

									<?php the_field('previous_experience', 'option'); ?>

								</div>

							</div>



						</div>

<!-- Modal -->
<div class="modal fade" id="addnext-modal" tabindex="-1" role="dialog" aria-labelledby="addnext-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    		<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Add A Previous Job</h4>
		      </div>

		      <div class="modal-body">
				<h1>The job has been added to your profile.</h1>
				</h2>Would you like to add another one?</h2>
		      </div>

		      <div class="modal-footer">
		      	<button id="save-and-back" class="btn btn-default">NO</button>
				<button id="add-another" class="btn btn-default">YES</button>		
		      </div>
  		
    </div>
  </div>
</div>

						<div class="q-step-footer">

							<div class="btn btn-default save-and-continue-later-btn"><a href="/wp-content/themes/matchly/additional-information-controller.php?step=back"><i class="fa fa-arrow-circle-left"></i> Back</a></div>

							<button class="btn btn-default q-next-step51"><i class="fa fa-plus-circle"></i> Add Job To Profile</button>

							<script>

							$(document).ready(function() {

								

							//activating skip button - adding hidden variable to the form
								


							$( ".q-next-step51" ).click(function() {

							    
								function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step51form').submit();
									
								} 
				  
							});

							$( "#step51form" ).submit(function( event ) {

								if ($('#step51form').hasClass('final-submit')) {

								$("#step51form.final-submit").submit();	

								} else {
									 event.preventDefault();
								$('#addnext-modal').modal('show');

								$( "#addnext-modal" ).on( "click", "#save-and-back", function() {
									 	$("#step51form").addClass('final-submit');
										$("#step51form").prepend("<input type='hidden' value='1' name='skipto'/>"); 	
										$("#submitform51").click();	
								 });

								$( "#addnext-modal" ).on( "click", "#add-another", function() {
										$("#step51form").addClass('final-submit');
										$("#submitform51").click();	 
								 });

								//  swal({   title: "The job has been added to your profile.",   
								// 	text: "Would you like to add another one?",   
								// 	type: "success",   
								// 	showCancelButton: true,   
								// 	confirmButtonColor: "#1676be",   
								// 	confirmButtonText: "Yes",   
								// 	cancelButtonText: "No",   
								// 	closeOnConfirm: false,   
								// 	closeOnCancel: false,
								// 	allowOutsideClick: true }, 
								// 	function(isConfirm){   
								// 	if (isConfirm) {
										
								// 		$("#step51form").addClass('final-submit');
								// 		$("#submitform51").click();	     
								// 	 } else {     
									 	
								// 	 	$("#step51form").addClass('final-submit');
								// 		$("#step51form").prepend("<input type='hidden' value='1' name='skipto'/>"); 	
								// 		$("#submitform51").click();	  
								// } });
								
								}
							});



							});



							</script>

						</div>

					</div>

<!-- ////////////////////  END of Step 51 ////////////////////////////////////////////-->