<!-- ////////////////////  Beginning of Step 3////////////////////////////////////////////-->
					<div class="q-step" id="step3">
						<div class="q-step-title">Add Specialist Skills</div>
						<div class="q-step-indicator">
							<ul class="stepline">

							</ul>


						</div>
						<div class="q-step-questions-wrapper">

							<div class="q-step-form">
								<form class="form-horizontal" role="form" method="post" id="step3form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">
									<input type="hidden" value="3" name="step"/>

									
									<div class="form-group">													
										<label for="specialskill1" class="col-sm-3 control-label">Specialist Skill</label>
										<div class="col-sm-9">
											<input type="text" class="form-control validate-it" id="specialskill1" name="specialskill1" placeholder="Enter Specialist Skill" maxlength="100"  />
										</div>
										<div class="val-message"></div>
									</div>	
									
									<div class="form-group">													
										<label for="specialskill2" class="col-sm-3 control-label">Specialist Skill</label>
										<div class="col-sm-9">
											<input type="text" class="form-control validate-it" id="specialskill2" name="specialskill2" placeholder="Enter Specialist Skill" maxlength="100"  />
										</div>
										<div class="val-message"></div>
									</div>	

									<div class="form-group">													
										<label for="specialskill3" class="col-sm-3 control-label">Specialist Skill</label>
										<div class="col-sm-9">
											<input type="text" class="form-control validate-it" id="specialskill3" name="specialskill3" placeholder="Enter Specialist Skill" maxlength="100"  />
										</div>
										<div class="val-message"></div>
									</div>	

									<button type="submit" id="submitform3" style="display:none">Submit</button>
									
								</form>

							</div>

							<div class="q-step-instructions">
								<div class="q-instructions-inner">
									
									<?php the_field('special_skills', 'option'); ?>
								</div>
							</div>

						</div>

						<div class="q-step-footer">
							<div class="btn btn-default save-and-continue-later-btn"><a href="/wp-content/themes/matchly/additional-information-controller.php?step=back"><i class="fa fa-arrow-circle-left"></i> Back</a></div>
							<button class="btn btn-default q-next-step3"><i class="fa fa-plus-circle"></i> Add Skills To Profile</button>


							<script>
							$(document).ready(function() {
								


							$( ".q-next-step3" ).click(function() {


									function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step3form').submit();
									
								} 
								 	
                                        

							});


							});






							</script>
						</div>
					</div>
					<!-- ////////////////////  END of Step 3 ////////////////////////////////////////////-->