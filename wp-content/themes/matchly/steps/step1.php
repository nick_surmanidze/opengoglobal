				<!-- ////////////////////  Beginning of Step 1 ////////////////////////////////////////////-->
				<div class="q-step" id="step1">
					<div class="q-step-title">Please Confirm Your Contact Details</div>
					<div class="q-step-indicator">
						<ul class="stepline">
							<li class="cur-step">1</li>
							<li>2</li>
							<li>3</li>
						</ul>


					</div>
					<div class="q-step-questions-wrapper">

						<div class="q-step-form">
							<form class="form-horizontal" role="form" method="post" id="step1form" action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">
								<input type="hidden" value="1" name="step"/>

								<div class="form-group">
									<label for="firstname" class="col-sm-3 control-label">First Name</label>
									<div class="col-sm-9">
										<input type="text" class="form-control validate-it" id="firstname" name="firstname" placeholder="Enter Name" maxlength="100"  value="<?php $current_user = wp_get_current_user(); echo $current_user->user_firstname; ?>" />
									</div>
									<div class="val-message"></div>
								</div>

								<div class="form-group">
									<label for="surname" class="col-sm-3 control-label">Surname</label>
									<div class="col-sm-9">
										<input type="text" class="form-control validate-it" id="surname" name="surname" placeholder="Enter Surname" maxlength="100"  value="<?php $current_user = wp_get_current_user(); echo $current_user->user_lastname; ?>" />
									</div>
									<div class="val-message"></div>
								</div>


								<div class="form-group">
									<label for="emailaddress" class="col-sm-3 control-label">Email</label>
									<div class="col-sm-9">
										<input type="email" value="<?php $current_user = wp_get_current_user(); echo $current_user->user_email; ?>" class="form-control validate-email" id="emailaddress" name="emailaddress" placeholder="Enter Email Address" maxlength="100"/>
									</div>
									<div class="val-message"></div>
								</div>


								<button type="submit" id="submitform1" style="display:none">Submit</button>

							</form>

						</div>

						<div class="q-step-instructions">
							<div class="q-instructions-inner">
								
								<?php the_field('about_you', 'option'); ?>
							</div>
						</div>

					</div>

					<div class="q-step-footer">
						<div class="btn btn-default save-and-continue-later-btn"><a href="<?php echo wp_logout_url( home_url().'/thank-you-for-registration/' ); ?>">Save and Continue Later</a></div>
						<button class="btn btn-default q-next-step1">Next Step</button>


						<script>

					$(document).ready(function() {

						function isValidEmailAddress(emailAddress) {
						    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
						    return pattern.test(emailAddress);
						};

						$( ".q-next-step1" ).click(function() {



							function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field Is Required').css('display','inline-block');
             						 	 }

        							});
									var emailAddress = $('.validate-email').val();
								 if (isValidEmailAddress(emailAddress)) {
								 	$('.validate-email').closest('.form-group').find('.val-message').html('').css('display','none');
								 	
								 } else {
								 	thisisvalid = false;
 									$('.validate-email').closest('.form-group').find('.val-message').html('Please Enter a Valid Email Address').css('display','inline-block');
								 	
								 }
								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step1form').submit();
								} 

						});

						});

						</script>
					</div>
				</div>
				<!-- ////////////////////  END of Step 1 ////////////////////////////////////////////-->