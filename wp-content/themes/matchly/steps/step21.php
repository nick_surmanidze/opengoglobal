					<!-- ////////////////////  Beginning of Step 2 - 1////////////////////////////////////////////-->
					<div class="q-step" id="step21">
						<div class="q-step-title">Add A Qualification To Your Profile</div>
						<div class="q-step-indicator">
							<ul class="stepline">
							</ul>


						</div>
						<div class="q-step-questions-wrapper">

							<div class="q-step-form">
								<form class="form-horizontal" role="form" method="post" id="step21form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">
									<input type="hidden" value="21" name="step"/>

									<div class="form-group">													
										<label for="institution1" class="col-sm-3 control-label">University</label>
										<div class="col-sm-9">
											<select id="institution1" name="institution1" class="form-control multiselect-one-value chosen-select validate-it" data-placeholder="Type three letters or select from list">
												<option selected disabled hidden value=''></option>
												<?php $variable = get_field('qualifications_and_skills_institution', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>	

									<div class="form-group">													
										<label for="coursename1" class="col-sm-3 control-label">Course Topic</label>
										<div class="col-sm-9">											
											<select id="coursename1" name="coursename1" class="form-control validate-it" >
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_course_topics', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>	
	
									<div class="form-group">													
										<label for="qualificationtype1" class="col-sm-3 control-label">Qualification Type</label>
										<div class="col-sm-9">
											<select id="qualificationtype1" name="qualificationtype1" class="form-control validate-it" >
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_qualification_type', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>               


									<div class="form-group">													
										<label for="gradeattained1" class="col-sm-3 control-label">Grade Attained</label>
										<div class="col-sm-9">
											<select id="gradeattained1" name="gradeattained1" class="form-control validate-it" >
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_grade_attained', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>   

									<div class="form-group">													
										<label for="yearattained1" class="col-sm-3 control-label">Year Attained</label>
										<div class="col-sm-9">
											<select id="yearattained1" name="yearattained1" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_year_attained', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>  


									
									<button type="submit" id="submitform21" style="display:none">Submit</button>
									
								</form>

							</div>

							<div class="q-step-instructions">
								<div class="q-instructions-inner">
									
									<?php the_field('qualifications_and_skills', 'option'); ?> 
								</div>
							</div>

						</div>
<!-- Modal -->
<div class="modal fade" id="addnext-modal" tabindex="-1" role="dialog" aria-labelledby="addnext-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    		<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Add A Qualification</h4>
		      </div>

		      <div class="modal-body">
				<h1>The qualification has been added to your profile.</h1>
				</h2>Would you like to add another one?</h2>
		      </div>

		      <div class="modal-footer">
		      	<button id="save-and-back" class="btn btn-default">NO</button>
				<button id="add-another" class="btn btn-default">YES</button>		
		      </div>
  		
    </div>
  </div>
</div>
						<div class="q-step-footer">
							<div class="btn btn-default save-and-continue-later-btn"><a href="/wp-content/themes/matchly/additional-information-controller.php?step=back"><i class="fa fa-arrow-circle-left"></i> Back</a></div>
							<button class="btn btn-default q-next-step21"><i class="fa fa-plus-circle"></i> Add Qualification To Profile</button>
							


							<script>
							$(document).ready(function() {


							$( ".q-next-step21" ).click(function() {

								function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step21form').submit();
									
								} 
                                        

							});



							$( "#step21form" ).submit(function( event ) {

								if ($('#step21form').hasClass('final-submit')) {

								$("#step21form.final-submit").submit();	

								} else {
									 event.preventDefault();


								$('#addnext-modal').modal('show');

								$( "#addnext-modal" ).on( "click", "#save-and-back", function() {
									 	$("#step21form").addClass('final-submit');
										$("#step21form").prepend("<input type='hidden' value='1' name='skipto'/>"); 	
										$("#submitform21").click();	 
								 });

								$( "#addnext-modal" ).on( "click", "#add-another", function() {
										$("#step21form").addClass('final-submit');
										$("#submitform21").click();	 
								 });


								//  swal({   title: "The qualification has been added to your profile.",   
								// 	text: "Would you like to add another one?",   
								// 	type: "success",   
								// 	showCancelButton: true,   
								// 	confirmButtonColor: "#1676be",   
								// 	confirmButtonText: "Yes",   
								// 	cancelButtonText: "No",   
								// 	closeOnConfirm: false,   
								// 	closeOnCancel: false,
								// 	allowOutsideClick: true }, 
								// 	function(isConfirm){   
								// 	if (isConfirm) {
										
								// 		$("#step21form").addClass('final-submit');
								// 		$("#submitform21").click();	     
								// 	 } else {     
									 	
								// 	 	$("#step21form").addClass('final-submit');
								// 		$("#step21form").prepend("<input type='hidden' value='1' name='skipto'/>"); 	
								// 		$("#submitform21").click();	  
								// } });
								
								}
							});


							});






							</script>
						</div>
					</div>
					<!-- ////////////////////  END of Step 21 ////////////////////////////////////////////-->