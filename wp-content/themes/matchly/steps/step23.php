<!-- ////////////////////  Beginning of Step 23////////////////////////////////////////////-->
					<div class="q-step" id="step23">
						<div class="q-step-title">Add A Qualification To Your Profile</div>
						<div class="q-step-indicator">
							<ul class="stepline">

							</ul>


						</div>
						<div class="q-step-questions-wrapper">

							<div class="q-step-form">
								<form class="form-horizontal" role="form" method="post" id="step23form"  action="<?php echo get_stylesheet_directory_uri(); ?>/reg-controller.php">
									<input type="hidden" value="23" name="step"/>

									<div class="form-group">													
										<label for="institution3" class="col-sm-3 control-label">University</label>
										<div class="col-sm-9">
											<select id="institution3" name="institution3" class="form-control multiselect-one-value chosen-select validate-it" data-placeholder="Type three letters or select from list">
												<option selected disabled hidden value=''></option>
												<?php $variable = get_field('qualifications_and_skills_institution', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>	

									<div class="form-group">													
										<label for="coursename3" class="col-sm-3 control-label">Course Topic</label>
										<div class="col-sm-9">
											<select id="coursename3" name="coursename3" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_course_topics', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>	
	
									<div class="form-group">													
										<label for="qualificationtype3" class="col-sm-3 control-label">Qualification Type</label>
										<div class="col-sm-9">
											<select id="qualificationtype3" name="qualificationtype3" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_qualification_type', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>               


									<div class="form-group">													
										<label for="gradeattained3" class="col-sm-3 control-label">Grade Attained</label>
										<div class="col-sm-9">
											<select id="gradeattained3" name="gradeattained3" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_grade_attained', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>   

									<div class="form-group">													
										<label for="yearattained3" class="col-sm-3 control-label">Year Attained</label>
										<div class="col-sm-9">
											<select id="yearattained3" name="yearattained3" class="form-control validate-it">
												<option selected disabled hidden value=''>None Selected</option>
												<?php $variable = get_field('qualifications_and_skills_year_attained', 'option'); 
												$temp_array = explode("\n", $variable);
												foreach ($temp_array as $item) {
													$item = preg_replace( "/\r|\n/", "", $item);
													echo "<option value='".$item."'>".$item."</option>";
												}
												?>													    
											</select>
										</div>
										<div class="val-message"></div>
									</div>  


									
									<button type="submit" id="submitform23" style="display:none">Submit</button>
									
								</form>

							</div>

							<div class="q-step-instructions">
								<div class="q-instructions-inner">
									
									<?php the_field('qualifications_and_skills', 'option'); ?>
								</div>
							</div>

						</div>

						<div class="q-step-footer">
							<div class="btn btn-default save-and-continue-later-btn"><a href="/wp-content/themes/matchly/additional-information-controller.php?step=back"><i class="fa fa-arrow-circle-left"></i> Back</a></div>
							<button class="btn btn-default q-next-step23"><i class="fa fa-plus-circle"></i> Add Qualification To Profile</button>


							<script>
							$(document).ready(function() {
								


							$( ".q-next-step23" ).click(function() {

								function validateStep() {
								var thisisvalid = true;

								 $(".validate-it").each(function(){ 

								 		$(this).closest('.form-group').find('.val-message').html('').css('display','none');

	             						var hasvalues = $(this).val(); 

             						  	if (!hasvalues) {
             						  		thisisvalid = false;
             						  		 $(this).closest('.form-group').find('.val-message').html('This Field is Required').css('display','inline-block');
             						 	 }

        							});


								 return thisisvalid;
								}



								if (validateStep()) {
									$('#step23form').submit();
									
								} 
                                        

							});


							});






							</script>
						</div>
					</div>
					<!-- ////////////////////  END of Step 23 ////////////////////////////////////////////-->