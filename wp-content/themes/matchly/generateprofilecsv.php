<?php 
// Include WP files so our controller can actually work with WP
 // Runs forever and ever...
set_time_limit(-1);
 set_time_limit(0);
ini_set('memory_limit', '1024M');


$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

global $wpdb;

function generatecsv() {
  global $wpdb;

	  
      header('Content-Encoding: UTF-8');
      header('Content-type: text/csv; charset=UTF-8');
      
      header("Content-Disposition: attachment; filename=MatchlyProfiles.csv");
      header("Pragma: no-cache");
      header("Expires: 0");

      //here we put the data

      // 1. Get Id's of all profiles
      // 2. Get data for every ID
      // 3. Put that data into the file


      $querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        
    WHERE       
        $wpdb->posts.post_type = 'profile'
        AND $wpdb->posts.post_status = 'publish' 
        ";

$pageposts = $wpdb->get_col($querystr, 1);

$profileidarray = array();
$profileidarray = $pageposts; // now we have profile ID array


      //open stream  
      $file = fopen('php://output', 'w');  

      // Add Headers                            
      fputcsv($file, array(
        'USER ID',
        'USER NAME',
        'Display Name',
        'Profile ID', 
        'Profile Title',
        'Registration Step',
        'User Role',
        'Appeared on search',
        'Times Viewed',
        'Name',
        'Surname',
        'Year Of Birth',
        'Home Town',
        'Home Country',
        'Email',
        'Phone Number',
        'Institution (1)',
        'Course Name (1)',
        'Qualification Type (1)',
        'Grade Attained (1)',
        'Year Attained (1)',
        'Institution (2)',
        'Course Name (2)',
        'Qualification Type (2)',
        'Grade Attained (2)',
        'Year Attained (2)',
        'Institution (3)',
        'Course Name (3)',
        'Qualification Type (3)',
        'Grade Attained (3)',
        'Year Attained (3)',
        'Special Skills (1)',
        'Special Skills (2)',
        'Special Skills (3)',
        'Current Organisation Name',
        'Current Industry',
        'Current Company Type',
        'Current Job Function',
        'Current Seniority',
        'Current Job Year Started',
        'Current Job Type of Contract',
        'Current Job Notice Period',
        'Current job Basic Salary',
        'Current Job Basic Salary Currency',
        'Previous Organisation Name (1)',
        'Previous Organisation Industry (1)',
        'Previous Company Type (1)',
        'Previous Job Function (1)',
        'Previous Seniority (1)',
        'Previous Job Year Started (1)',
        'Previous Job Year Ended (1)',
        'Previous Type of Contract (1)',
        'Previous Organisation Name (2)',
        'Previous Organisation Industry (2)',
        'Previous Company Type (2)',
        'Previous Job Function (2)',
        'Previous Seniority (2)',
        'Previous Job Year Started (2)',
        'Previous Job Year Ended (2)',
        'Previous Type of Contract (2)',
        'Ideal Job Function',
        'Ideal Job Seniority',
        'Ideal Job Location',
        'Ideal Job Industry',
        'Ideal Job Company Type',
        'Ideal Job Contract Type',
        'Ideal Job Basic Salary',
        'Ideal Job Basic Salary Current',
        'Sales Pitch - Point of Difference',
        'Sales Pitch - Personality',
        'Sales Pitch - Career Aspiration',
        'Appeared On Search',
        'Was Opened',
        'Status'

        )); 

$data = array();

      foreach ($profileidarray as $profileID) {

        $profileRow = array(
            get_post_field( 'post_author', $profileID ),
            get_userdata(get_post_field( 'post_author', $profileID ))->user_login,
            get_userdata(get_post_field( 'post_author', $profileID ))->display_name,
            $profileID,
            get_the_title( $profileID),
            preg_replace( "/\r|\n/", "", strip_tags( get_field( "user_step", 'user_'.get_post_field( 'post_author', $profileID )))),
            preg_replace( "/\r|\n/", "",  strip_tags( get_field( "user_role", 'user_'.get_post_field( 'post_author', $profileID )))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("appeared_on_search", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("was_opened", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("about_you_first_name", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("about_you_surname", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("about_you_year_of_birth", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("about_you_home_town", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("about_you_home_country", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("about_you_email_address", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("about_you_phone_number", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_institution1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_course_name1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_qualification_type1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_grade_attained1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_year_attained1", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_institution2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_course_name2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_qualification_type2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_grade_attained2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_year_attained2", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_institution3", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_course_name3", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_qualification_type3", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_grade_attained3", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_year_attained3", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_special_skills1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_special_skills1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("qualifications_and_skills_special_skills1", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_organisation_name", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_industry", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_company_type", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_job_function", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_seniority", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_year_started", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_type_of_contract", $profileID))),
            preg_replace(  "/\r|\n/", "", strip_tags(get_field("current_job_current_notice_period", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_basic_salary", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("current_job_basic_salary_currency", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_organisation_name1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_industry1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_company_type1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_job_function1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_seniority1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_year_started1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_year_ended1", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_type_of_contract1", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_organisation_name2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_industry2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_company_type2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_job_function2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_seniority2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_year_started2", $profileID))),
            preg_replace(  "/\r|\n/", "", strip_tags(get_field("previous_experience_year_ended2", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("previous_experience_type_of_contract2", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_job_function", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_seniority", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_location", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_industry", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_company_type", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_contract_type", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_basic_salary", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("ideal_job_basic_salary_currency", $profileID))),

            preg_replace( "/\r|\n/", "",  strip_tags(get_field("sales_pitch_point_of_difference", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("sales_pitch_personality", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("sales_pitch_career_aspiration", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("appeared_on_search", $profileID))),
            preg_replace( "/\r|\n/", "",  strip_tags(get_field("was_opened", $profileID))),
            get_post_status( $profileID )
          );
        array_push($data, $profileRow);
      }


     foreach ($data as $row) {

            fputcsv($file, $row);   

     }

    
      exit(); 
}

generatecsv();

?>