<?php 
// Include WP files so our controller can actually work with WP
 // Runs forever and ever...
set_time_limit(-1);
 set_time_limit(0);
ini_set('memory_limit', '1024M');

$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');

global $wpdb;

function generatecsv() {
  global $wpdb;

	   header("Content-type: text/csv");
      header("Content-Disposition: attachment; filename=MatchlyUsers.csv");
      header("Pragma: no-cache");
      header("Expires: 0");

      //here we put the data

      // 1. Get ID of all users
      // 2. Check if the user has associated profile


function get_profile_status($user_id) {
global  $wp_query;
  $query = new WP_Query( 'author='.$user_id.'&post_type=profile' );

        // The Loop
        if ( $query->have_posts() ) {

          while ( $query->have_posts() ) {

            $query->the_post();

            $profileID = get_the_ID();

            $post_status = get_post_status( $profileID );
          }

        }

        return $post_status;
}


function get_last_login_date($user_id) {

  $user_log = get_user_meta($user_id, 'user_log', true);
  if($user_log) {

    $dates_array = array();
    $dates_array = explode("#", trim($user_log, "#"));
    $last_time_logged_in = end($dates_array);

    return $last_time_logged_in;


  } else {

    return "N/A";

  }
}

function times_logged_in($user_id) {

  $user_log = get_user_meta($user_id, 'user_log', true);

  if($user_log) {

    $dates_array = array();
    $dates_array = explode("#", trim($user_log, "#"));
    $times = count($dates_array);

    return $times;


  } else {

    return "N/A";

  }
}

$allusers = get_users();
// Array of WP_User objects.



      //open stream  
      $file = fopen('php://output', 'w');  

      // Add Headers                            
      fputcsv($file, array(
        'User ID',
        'Login',
        'Display Name',
        'Email',
        'Role', 
        'Registration Step', 
        'Registration Date',
        'Received Mail From',
        'Send Mail To',
        'Profile Status',
        'Active Number of Days',
        'Last Login Date'

        )); 

$data = array();

      foreach ($allusers as $user) {

        $profileRow = array(

            $user->ID,
            $user->user_login,
            $user->display_name,
            $user->user_email,
            preg_replace( "/\r|\n/", "", strip_tags( get_field( "user_role", 'user_'.$user->ID ))),
            preg_replace( "/\r|\n/", "", strip_tags( get_field( "user_step", 'user_'.$user->ID ))),
            $user->user_registered,
            preg_replace( "/\r|\n/", "", strip_tags( get_field( "received_email_from", 'user_'.$user->ID ))),
            preg_replace( "/\r|\n/", "", strip_tags( get_field( "sent_email_to", 'user_'.$user->ID ))),
            get_profile_status($user->ID),
            times_logged_in($user->ID),
            get_last_login_date($user->ID)



          );
        array_push($data, $profileRow);
      }


     foreach ($data as $row) {

            fputcsv($file, $row);   

     }

    
      exit(); 
}

generatecsv();

?>