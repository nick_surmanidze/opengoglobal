<?php
$f = fopen('lock-smart-notifications', 'w') or die ('Cannot create lock file');
if (flock($f, LOCK_EX | LOCK_NB)) {  // if lock
 
	// Runs forever and ever...
	set_time_limit(-1);
	ignore_user_abort(1);
	ini_set('memory_limit', '4000M');
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');


	// Include WP files so our controller can actually work with WP

	$location = $_SERVER['DOCUMENT_ROOT'];
	 
	include ($location . '/wp-config.php');
	include ($location . '/wp-load.php');
	include ($location . '/wp-includes/pluggable.php');

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///    Function For Sending Notifications to all users who have profile and new matched jobs
	///
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	## Using offset not to start over if the script starts over.

	$offset_option = get_option("smarter_job_notification_offset");

	$last_time_ran = get_option("smarter_job_notification_last_time_ran");

		

	if($last_time_ran != date('m/d/Y')) {




		if($offset_option > 0) {

			send_smart_notifications($offset_option);	

		} else {

			send_smart_notifications(0);	

		}


	}




} // endif lock







function set_html_content_type() {
	return 'text/html';
}





function update_log($file, $message) {

	$message = $message . "\r\n";
	file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
}






function send_smart_notifications($offset) {

global $wpdb;




	## Here The Script Needs to Decide to Start Over Or Just Continue...

	if($offset == 0) {
		update_log('smart-notifications-log.txt', "\r\n");
		update_log('smart-notifications-log.txt', "############################################");
		update_log('smart-notifications-log.txt', "##### Started At ##### ".date('m/d/Y h:i:s a', time()));
		update_log('smart-notifications-log.txt', "##### Sending smart notifications #####");	
	} else {
		update_log('smart-notifications-log.txt', "##### Resuming sending. Offset:" . $offset);	
	}



	## Get array of published profiles


	$querystr = "

	   SELECT DISTINCT
	      $wpdb->posts.ID      
	    FROM
	        $wpdb->posts
	    WHERE       
	        $wpdb->posts.post_type = 'profile' 
	        AND $wpdb->posts.post_status = 'publish'";


	$pageposts = $wpdb->get_col($querystr, 0);

	$profileidarray = array();

	## So, now we have an array of published profiles
	$profileidarray = $pageposts;

	#this is just for testing
	//$profileidarray = array(3392, 2294);

	$total_number_of_recipients = 0;

	foreach ($profileidarray as $profile_id) {

			// This is kind of a red alert button for stoping script execution (Place it inside foreach cycle)
			if (file_exists('stop.txt')) {
			    die();
			}



		# get user id
		$user_id = get_post_field( 'post_author', $profile_id );
		# Get user OBJECT
		$user_info = get_userdata($user_id);

		## Just making sure user is subscribed... 
		if(get_user_meta($user_id, "smarter_notifications", true) !== 'disabled') {

			# if we are here, than user is subscribed |or| did not unsubscribe yet ;)

			# Now we need to find matched jobs
			$user_new_jobs_array = array();

			$user_new_jobs_array_1 = array();
			$user_new_jobs_array_2 = array();

			$user_new_jobs_array_1 = aggregate_jobs(0, 20, $user_id);
			$user_new_jobs_array_2 = aggregate_jobs(1, 40, $user_id);

			$user_new_jobs_array = array_merge($user_new_jobs_array_1['current_raw'], $user_new_jobs_array_2['current_raw']);

			if(count($user_new_jobs_array) > 0) {

				# Now we found some smart matches for this user.
				# Let's make sure that we did not send them before

				$user_old_jobs_array = array();

				#checking if had that array in the meta field and if yes than let's unserialize it
				if(strlen(get_user_meta($user_id, "old_array_of_smart_jobs", true)) > 0) {
					$user_old_jobs_array = unserialize(get_user_meta($user_id, "old_array_of_smart_jobs", true));
				}

				#####This Variable Will Be Used For Output
				$new_jobs_html_output = '';


				$used_jobs_to_push_into_meta = array();

				$counter = 0;

				$payment = sanitize_text_field(get_field("ideal_job_basic_salary_currency", $profile_id)) . " " . sanitize_text_field(get_field("ideal_job_basic_salary", $profile_id));

				$location = 'London';

				foreach($user_new_jobs_array as $job) :

					if($counter < 5) {

						if(!in_array($job[0], $user_old_jobs_array)) {

								#Now we know that this job was not used in notifications before.

								array_push($used_jobs_to_push_into_meta, (string)$job[0]);

								$counter++;

								$new_jobs_html_output .= $counter . "<span>.     </span><strong><a href='".$job[4]."'>".$job[0]."</a></strong><br>";
								$new_jobs_html_output .= "".$payment.". ".$location.". ".$job[1]."<br><br>";

						}

					}

				endforeach;

				// check if we have new job matches
				if (count($used_jobs_to_push_into_meta) > 0):


					$total_number_of_recipients ++; 

				if ($total_number_of_recipients > $offset) { 


				// Now we need to send email

					$unsubscribe_link = site_url() . '/unsubscribe/?uid=' . $user_id;
					
					add_filter( 'wp_mail_content_type', 'set_html_content_type' );


					$mail_message = '';
					$mail_message .= "Great news!  Our clever Smarter Job Search robots have found some new jobs for you.<br><br>";
					$mail_message .= "We have included a sample of your most recent jobs matches below.  ";
					$mail_message .= "(To see all of your job matches <a href='http://opengoglobal.com' title='http://opengoglobal.com'>click here</a>).<br><br>";

					$mail_message .= $new_jobs_html_output;

					$mail_message .= "We have based these matches on your OpenGo profile.  ";
					$mail_message .= "If these jobs aren’t quite what you are looking for <a href='http://opengoglobal.com' title='http://opengoglobal.com'>click here to edit your profile</a>.<br><br>";
					
					$mail_message .= "Good luck,<br>";
					$mail_message .= "<strong>Ian Hallett</strong><br>";
					$mail_message .= "Founder, OpenGo<br><br>";

					$mail_message .= "_________________<br>";

					$mail_message .= "To unsubscribe please follow <a href='".$unsubscribe_link."'>this link</a> or change your subscription to notifications from <a href='http://opengoglobal.com' title='http://opengoglobal.com'>OpenGo</a> settings page.";

					$headers = 'From: "OpenGo" <ian@opengoglobal.com><br>';

					$to = $user_info ->user_email;

					$sent = wp_mail($to, 'Today’s Jobs from OpenGo', $mail_message, $headers);

					// Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
					remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
					update_option( "smarter_job_notification_offset", $total_number_of_recipients );



					if($sent) {
					#update log
					update_log('smart-notifications-log.txt', "# (".$total_number_of_recipients.") Sent to: (".$user_info ->display_name. " / ".$user_info ->user_email." )" );
					
					} else {
					update_log('smart-notifications-log.txt', "# (".$total_number_of_recipients.") NOT Sent to: (".$user_info ->display_name. " / ".$user_info ->user_email." )" );
					}
					


					// If $sent =  true 

					// work with old array. Make it shorter if it is over 100 items, serialize and write back into the meta

					$merged_old_array = array();

					$merged_old_array = array_merge($used_jobs_to_push_into_meta, $user_old_jobs_array);

					if(count($merged_old_array) > 40) {
						$merged_old_array = array_slice($merged_old_array, 0, 40);
					}

					update_user_meta($user_id, "old_array_of_smart_jobs", serialize($merged_old_array));

					// Just to unload the script little bit
					sleep (2);

					### Uncomment this line only in case when need to delete previous meta
					//update_user_meta($user_id, "old_array_of_smart_jobs", "");
					}

				endif; // end of the mailing and checking if there are new job matches


				
				// foreach ($user_old_jobs_array as $itm) {
				// 	echo $itm . "<br>";
				// }

				// echo $new_jobs_html_output;

				//echo $mail_message;

			}



		}


	} // end of main foreach looping through profiles
	


	update_log('smart-notifications-log.txt', "##### Finished At ##### ".date('m/d/Y h:i:s a', time()));
	update_log('smart-notifications-log.txt', "Sent to: " . $total_number_of_recipients . " recipients.");
	update_log('smart-notifications-log.txt', "############################################");
	update_log('smart-notifications-log.txt', "\r\n"); 


	$total_number_of_recipients = 0;

	update_option( "smarter_job_notification_offset", $total_number_of_recipients );

	update_option("smarter_job_notification_last_time_ran", date('m/d/Y'));

} // end of function



die();
