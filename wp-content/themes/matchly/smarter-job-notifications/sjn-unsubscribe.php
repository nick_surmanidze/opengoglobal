<?php
// Include WP files so our controller can actually work with WP

	$location = $_SERVER['DOCUMENT_ROOT'];
	 
	include ($location . '/wp-config.php');
	include ($location . '/wp-load.php');
	include ($location . '/wp-includes/pluggable.php');



if(isset($_GET['uid'])) {
	
	 $user_id =  sanitize_text_field($_GET['uid']);

	 update_user_meta($user_id, "smarter_notifications", "disabled");

}

wp_redirect( home_url() ); 				
exit;



