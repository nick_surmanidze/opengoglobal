<?php
/*
Template Name: Home
*/
/**
 * The template for displaying Home Page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Matchly
 */

 if(isset($_GET['ref']))
{
 session_start();
$_SESSION['ref'] = $_GET['ref'];
wp_redirect( home_url() . "/referral"); 
exit;
} 


// this redirects from home page to profile if logged in.
 if (is_front_page()) {

 	if (is_user_logged_in()) 	{
		wp_redirect( home_url() . "/steps"); 				
		exit;
	}
 }

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,300,700' rel='stylesheet' type='text/css'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css' rel='stylesheet' type='text/css'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-theme.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/font-awesome-4.2.0/css/font-awesome.min.css">
<link rel='shortcut icon' href='<?php echo get_home_url(); ?>/favicon.ico' type='image/x-icon'/ >
<?php wp_head(); ?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/responsive.css" type="text/css"/>


<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '945342705495492']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=945342705495492&amp;ev=PixelInitialized" /></noscript>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-61069280-1', 'auto');
ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">

		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">

		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="home-wrapper" style="background: url(<?php  the_field( 'home_background_image', 4); ?>) no-repeat center center; background-size: cover;">
					<div class="row">
					  <div class="col-md-6 home-we-connect">
						<div class="logo-wrapper">
							<a href="/"><img src="<?php  the_field('h-logo', 4); ?>" alt="Matchly"></a>
						</div>
						<div class="hero-text-wrapper">
							<h1><?php  the_field('home_heading', 4); ?></h1>
							<p><?php  the_field('home_sub_heading', 4); ?></p><br>
							<a href="/more" class="find-out-more btn-default btn">Find Out More</a>
						</div>
					  </div>
					  <div class="col-md-6 home-social-connect">
					  	<?php echo do_shortcode('[userpro_social_connect width="400px" register_redirect="/steps" login_redirect="/my-profile"]'); ?>
					  	<p><?php  the_field('sub_button_text', 4); ?></p>
						<div class="home-menu">
							<?php wp_nav_menu( array( 'theme_location' => 'home-footer' ) ); ?>
						</div>
					  </div>
					</div>
				</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

