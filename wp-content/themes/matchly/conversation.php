<?php
/*
Template Name: Conversation
/**
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		<?php } else { ?>
			<main id="main" class="site-main" role="main">
		<?php } ?>
<?php

function is_current_view($value) {

	$current = '';

	if(isset($_GET['show']) && $_GET['show'] == $value) {
		$current = 'current';
	} elseif (!isset($_GET['show']) && $value == "inbox"){

		$current = 'current';

	}
	print_r($current);
}

// now we need to define the other user
$me = get_current_user_id();
$other = null;
if(isset($_GET['u'])) {
$other = $_GET['u'];
}

// Now we need to get array of matches

		$querystr = "
SELECT DISTINCT
  $wpdb->posts.ID      
FROM
    $wpdb->posts,
    $wpdb->postmeta AS messagefrom,
    $wpdb->postmeta AS messageto

WHERE       
    $wpdb->posts.post_type = 'message' 
    AND $wpdb->posts.post_status = 'publish'

    AND $wpdb->posts.ID = messagefrom.post_id 
    AND $wpdb->posts.ID = messageto.post_id 


    AND 

    (
    	((messagefrom.meta_key = 'message_from' AND messagefrom.meta_value = '".$me."')
    AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".$other."')) 
OR 
	((messagefrom.meta_key = 'message_from' AND messagefrom.meta_value = '".$other."')
    AND (messageto.meta_key = 'message_to' AND messageto.meta_value = '".$me."'))
)
	";

		$message_id_array = array(); 
		$message_id_array = $wpdb->get_col($querystr, 0);
// Cool! Now we have an array with conversation
		#BUT, we need to get last 10 messages only and set an offset..

		if(count($message_id_array) > 10) {

			$message_id_array = array_slice($message_id_array, -10);
			$offset_item_id = $message_id_array[1];
		}

?>


<?php while ( have_posts() ) : the_post(); ?>
	<div class="container container-fluid">
		<div class="row content-wrapper">							
			<div class="inner-wrapper">
				<div class="inner-title"><i class="fa fa-comments-o"></i>  Conversation
				<a href="<?php echo site_url(); ?>/dashboard" class="btn btn-default" style="float:right;">Back To Dashboard</a>
				</div>
					<div class="inner-content-wrap inbox-wrapper">
						<div class="col-sm-3 inbox-menu">
							<a href="<?php echo site_url().'/inbox/?show=inbox'; ?>" class="<?php is_current_view('inbox'); ?>"><i class="fa fa-envelope-o"></i> Inbox</a><br>
							<a href="<?php echo site_url().'/inbox/?show=sent'; ?>" class="<?php is_current_view('sent'); ?>"><i class="fa fa-paper-plane-o"></i> Sent</a><br>
							<a href="<?php echo site_url().'/inbox/?show=unread'; ?>" class="<?php is_current_view('unread'); ?>"><i class="fa fa-bell-o"></i> Unread</a><br>
							<a href="<?php echo site_url().'/inbox/?show=starred'; ?>" class="<?php is_current_view('starred'); ?>"><i class="fa fa-star-o"></i> Starred</a><br>
							<a href="<?php echo site_url().'/inbox/?show=all'; ?>" class="<?php is_current_view('all'); ?>"><i class="fa fa-archive"></i> All</a><br>
						</div>

						<div class="col-sm-9 inbox-content">

							<div class="conversation-messages-wrapper" data-new-offset="<?php echo $offset_item_id; ?>">

								<?php 		if(count($message_id_array) > 10) { ?>

									<div class="load-more-messages">+ Load 10 More</div>

								<?php } ?>
								
								<div class="loading-messages"><img src="<?php echo get_template_directory_uri(); ?>/images/spinner-white.GIF" alt="loading.." ></div>
								

								<?php 
								$subject = "No Subject";
								$i = 0;
								foreach($message_id_array as $message_id) { 
								$i ++;
								if ($i == 1) {
								$subject = get_the_title($message_id);
								}	
								?>

								<div class="message-item <?php echo conversation_in_or_out($message_id); ?>">
									<div class="triangle"></div>
									<div class="message-subject"><?php echo get_from_or_to($message_id) ?> (<?php echo get_the_title($message_id); ?>)</div>
									<div class="message-date"><?php echo message_time_by_id($message_id); ?></div>
									<div class="message-body"><?php echo apply_filters('the_content', get_post_field('post_content', $message_id)); ?></div>			
								</div>

<?php 
mark_read($message_id);
}
?>





							</div>

							<div class="message-composer">
								<span>Reply:</span>
								<textarea id="mew-message"></textarea>
								<img class="sending-message" src="<?php echo get_template_directory_uri(); ?>/images/spinner-white.GIF" alt="loading.." >
								<button class="btn btn-default reply-btn"><i class="fa fa-paper-plane-o"></i>  Send</button>
							</div>

						</div>
					

					</div>


				</div>
			</div>	
		</div>
	</div>	

	<script>

	$( document ).ready(function() {

		function scroll_down() {
			var $cont = $('.conversation-messages-wrapper');
			$cont[0].scrollTop = $cont[0].scrollHeight;
		}

		scroll_down();
    
	$( document ).on("click", ".reply-btn", function(){

		var message = $('#mew-message').val();

		var to = "<?php echo $other; ?>";

		var subject = "<?php echo $subject; ?>";



		if (message.length > 0) {

			$('.sending-message').css("display", "inline-block");

		$('#mew-message').val('');

		$.ajax({
	        data: ({
	        		action : 'send_message_in_conversation',
	        	 'message': message,
	        	 'to': to,
	        	 'subject': subject
	     			}),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) { 
	      	if(msg != "Failed") {
	      		console.log(msg);
	      		$( ".conversation-messages-wrapper" ).append(msg);
	      		scroll_down();
	      		$('.sending-message').css("display", "none");
	      	}
	      });

	  }

		scroll_down();
		
	});

	$( document ).on("click", ".load-more-messages", function(){

		// Now we need to load more messages when appropriate button is clicked

		var button = $(this);

		var from = "<?php echo $other; ?>";

		var offset = $('.conversation-messages-wrapper').attr( 'data-new-offset' );
			$('.loading-messages').css("display", "inline-block");
		$.ajax({
	        data: ({
	        		action : 'load_more_messages',
	        	 'from': from,
	        	 'offset': offset
	     			}),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {

	      console.log(msg); 

		  var data = JSON.parse(msg);

			$('.loading-messages').after(data.messages);

			$('.loading-messages').css("display", "none");

			$('.conversation-messages-wrapper').attr( 'data-new-offset', data.newoffset);

			if(data.hasmore == 0) {
				button.hide();
			}

	      });

	});

	});
	</script>


<?php endwhile; // end of the loop. ?>
</main><!-- #main -->
</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
