<?php
/**
 * The template for displaying all single posts.
 *
 * @package Matchly
 */

get_header(); ?>

	<div id="primary" class="content-area">
				<?php $backgroundimageurl = get_field("background_image", "options");

		 if(isset($backgroundimageurl)) { ?>
		 	<main id="main" class="site-main" role="main" style="background: url(<?php echo $backgroundimageurl; ?>) no-repeat center center; background-size: cover; background-attachment: fixed; ">
		 <?php } else { ?>

		<main id="main" class="site-main" role="main">
			
		<?php } ?>
		<?php while ( have_posts() ) : the_post(); 
		//update the counter
		$profid = get_the_ID();
		$curnum = strip_tags(get_field( "was_opened", $profid ));
		$curnum = $curnum + 1;		
		update_field( "was_opened", $curnum, $profid);
		?>
<div class="container container-fluid">

						<div class="row content-wrapper">

							<div class="public-profile-wrapper">

								<h1>Talent Number <?php global $post; $author_id = $post->post_author; echo $author_id; ?></h1>


<div class="separator"></div> 

						<div class="profile-section-title">Ideal Job</div>
						<div class="profile-content">

									<div class="profile-inner-section  row">

										<div class="col-md-6">


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Job Function:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_job_function", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Seniority:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_seniority", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Location:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_location", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Industry:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_industry", $profid)); ?></div>
										</div>

										</div>

										<div class="col-md-6">



										<div class="profile-item-wrapper">
											<div class="profile-item-title">Company Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_company_type", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Contract Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_contract_type", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Basic Salary:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_basic_salary", $profid)); ?></div>
										</div>


										<div class="profile-item-wrapper">
											<div class="profile-item-title">Currency:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("ideal_job_basic_salary_currency", $profid)); ?></div>
										</div>

										</div>

									</div>


						</div>
						<div class="separator"></div> 

								<div class="profile-section-title">Current Job</div>

								<div class="profile-content">

									<div class="profile-inner-section  row">

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Organisation Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_organisation_name", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Industry:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_industry", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Company Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_company_type", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Job Function:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_job_function", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Seniority:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_seniority", $profid)); ?></div>
										</div>

										</div>

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Started:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_year_started", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type of Contract:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_type_of_contract", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Notice Period:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_current_notice_period", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Basic Salary:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_basic_salary", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Basic Salary Currency:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("current_job_basic_salary_currency", $profid)); ?></div>
										</div>

										</div>

									</div>
								</div>
	
<div class="separator"></div> 
						<?php

			$val1 = get_field("previous_experience_organisation_name1", $profid);
			$val2 = get_field("previous_experience_industry1", $profid);

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>

								<div class="profile-section-title">Previous Experience</div>
								<div class="profile-content">
									<div class="profile-inner-section  row">

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Organisation Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_organisation_name1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Industry:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_industry1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Company Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_company_type1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Job Function:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_job_function1", $profid)); ?></div>
										</div>

										</div>

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Seniority:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_seniority1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Started:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_started1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Ended:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_ended1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type of Contract:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_type_of_contract1", $profid)); ?></div>
										</div>

										</div>

									</div>

<div class="separator"></div> 
						<?php

			$val1 = get_field("previous_experience_organisation_name2", $profid);
			$val2 = get_field("previous_experience_industry2", $profid);

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>


									<div class="profile-inner-section  row">

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Organisation Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_organisation_name2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Industry:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_industry2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Company Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_company_type2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Job Function:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_job_function2", $profid)); ?></div>
										</div>

										</div>

										<div class="col-md-6">

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Seniority:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_seniority2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Started:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_started2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Ended:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_year_ended2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type of Contract:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("previous_experience_type_of_contract2", $profid)); ?></div>
										</div>

										</div>

									</div>

									<div class="separator"></div> 

					<?php } ?>



								</div>
				<?php } ?>




<?php if (strip_tags(get_field("qualifications_and_skills_special_skills1", $profid))) { ?> 
								<div class="profile-section-title">Specialist Skills</div>
								<div class="profile-content">
									<div class="profile-inner-section  row">

										<div class="profile-item-wrapper">
											
											<div class="specialist-skill profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_special_skills1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											
											<div class="specialist-skill profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_special_skills2", $profid)); ?></div>
										</div>	
																			
										<div class="profile-item-wrapper">
											
											<div class="specialist-skill profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_special_skills3", $profid)); ?></div>
										</div>

									</div>
								</div>

<div class="separator"></div> 
<?php } ?> 

<?php if (strip_tags(get_field("qualifications_and_skills_institution1", $profid))) { ?> 
<div class="profile-section-title">Qualifications</div>
								<div class="profile-content">

									<div class="profile-inner-section  row">
									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">University:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_institution1", $profid)); ?></div>
										</div>
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Course Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_course_name1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_qualification_type1", $profid)); ?></div>
										</div>
									</div>

									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Grade Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_grade_attained1", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_year_attained1", $profid)); ?></div>
										</div>
									
									</div>
									</div>

			<div class="separator"></div> 
								
			<?php

			$val1 = get_field("qualifications_and_skills_institution2", $profid);
			$val2 = get_field("qualifications_and_skills_course_name2", $profid);

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>

									<div class="profile-inner-section  row">
									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">University:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_institution2", $profid)); ?></div>
										</div>
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Course Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_course_name2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_qualification_type2", $profid)); ?></div>
										</div>
									</div>

									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Grade Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_grade_attained2", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_year_attained2", $profid)); ?></div>
										</div>
									
									</div>
									</div>

			<div class="separator"></div> 
									
									<?php } ?>

						<?php

			$val1 = strip_tags(get_field("qualifications_and_skills_institution3", $profid));
			$val2 = strip_tags(get_field("qualifications_and_skills_course_name3", $profid));

			 if ((strlen ( $val1 ) > 1) &&  ((strlen ($val2 ) > 1))) { ?>

									<div class="profile-inner-section  row">
									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">University:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_institution3", $profid)); ?></div>
										</div>
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Course Name:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_course_name3", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Type:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_qualification_type3", $profid)); ?></div>
										</div>
									</div>

									<div class="col-md-6">
										
										<div class="profile-item-wrapper">
											<div class="profile-item-title">Grade Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_grade_attained3", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title">Year Attained:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("qualifications_and_skills_year_attained3", $profid)); ?></div>
										</div>
									
									</div>
									</div>
				<div class="separator"></div> 
				<?php } ?>
			</div>
			<?php  } ?>

				



<?php if (strip_tags(get_field("sales_pitch_point_of_difference", $profid))) {?>
							<div class="profile-section-title">Personal</div>
								<div class="profile-content">
									<div class="profile-inner-section  row sales-pitch">

										<div class="profile-item-wrapper">
											<div class="profile-item-title-full-width">The unique or special thing about me that sets me apart from others:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("sales_pitch_point_of_difference", $profid)); ?></div>
										</div>

										<div class="profile-item-wrapper">
											<div class="profile-item-title-full-width">Words that best describe my personality:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("sales_pitch_personality", $profid)); ?></div>
										</div>	
																			
										<div class="profile-item-wrapper">
											<div class="profile-item-title-full-width">What I am trying to achieve with my career:</div>
											<div class="profile-item-value"><?php echo strip_tags(get_field("sales_pitch_career_aspiration", $profid)); ?></div>
										</div>

									</div>
								</div>
								<?php } ?> 
							<div class="separator"></div> 
							<div class="report-abuse">
							<button  type="button" class="btn btn-warning report-abuse-trigger" style="float:left"><i class="fa fa-exclamation-triangle"></i> Report Abuse</button>
								<?php $check_pool = is_this_talent_in_my_pool($profid, 1, '');

					            if($check_pool != 1) { ?>

					                <div class="add-to-talent-pool btn btn-default add-new" data-profile-id="<?php echo $profid; ?>" ><i class="fa fa-plus-circle"></i> Add To Talent Pool</div>

					            <?php } else { ?>

					                 <div class="add-to-talent-pool btn btn-default in-my-pool" data-profile-id="<?php echo $profid; ?>"><i class="fa fa-users"></i> In My Talent Pool</div>

					            <?php } ?>
							<button type="button" class="btn btn-default" data-toggle="modal" data-target="#mail-modal">
							<i class="fa fa-envelope-o"></i> Send Message
							</button>
							</div>





<!-- Modal -->
<div class="modal fade mail-function" id="mail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="form-horizontal">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Send Message</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="form-group">
					<label for="emailaddress" class="col-sm-12 control-label">Your Email</label>
						<div class="col-sm-12">
						 <input type="email" id="sender-mail" value="<?php $current_user = wp_get_current_user(); echo $current_user->user_email; ?>" class="form-control"  name="emailaddress" placeholder="e.g. j.doe@example.com" maxlength="100" required />
							<span class="val-message invalid-email">Please enter valid email.</span>
						</div>
				</div>

				<?php show_subject_dropdown($profid); ?>

				<div class="form-group no-bottom-margin">
					<label  class="col-sm-12 control-label">Message</label>
				</div>
		        <textarea name="message" id="modal-message" placeholder="Please enter your message.." required maxLength="1000"></textarea>
		      <span class="val-message no-message-text">Please enter your message text</span>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
		        <button type="button" class="btn btn-default" id="send-message-button"><i class="fa fa-paper-plane-o"></i> Send</button>
		      </div>
  		</div>
    </div>
  </div>
</div>


							</div>

						</div>
						</div>



<!-- Modal -->
<div class="modal fade mail-function" id="talent-pool-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top: 120px;">
    <div class="modal-content">
        <div class="form-horizontal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Add To Talent Pool</h4>
                <h5 style="  margin-bottom: 0; margin-top: 3px;">Tag this talent profile with up to three descriptors</h5>
              </div>

              <div class="modal-body">

                <div id="misc-info"></div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 1</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-one" class="form-control" />
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 2</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-two" class="form-control"  />
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12 control-label">Tag 3</label>
                        <div class="col-sm-12">
                         <input type="text" id="tag-three" class="form-control"  />
                        </div>
                </div>

                </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-default" id="create-talent-pool"><i class="fa fa-plus-circle"></i> Add</button>
              </div>
        </div>
    </div>
  </div>
</div>
<script>

$(document).ready(function() {

	    $(document).on('click', '.add-to-talent-pool.add-new', function(){

        $("#talent-pool-modal").attr('data-profile-id', $(this).data("profile-id"));
        $("#talent-pool-modal").modal('show');

    });

    // clean up on hide

    $('#talent-pool-modal').on('hide.bs.modal', function(e) {

       $("#talent-pool-modal").attr('data-profile-id', "");
       $("#tag-one").val('');
       $("#tag-two").val('');
       $("#tag-three").val('');

    });

    $(document).on('click', '#create-talent-pool', function(){

        var tagOne = $("#tag-one").val();
        var tagTwo = $("#tag-two").val();
        var tagThree = $("#tag-three").val();
        var profileID = $("#talent-pool-modal").attr('data-profile-id');

        $("#talent-pool-modal").modal('hide');

                 $.ajax({
            data: ({
                action : 'create_talent_pool',
                'profile': profileID,
                'tag-one':tagOne,
                'tag-two':tagTwo,
                'tag-three':tagThree
            }),
            type: 'POST',
            async: true,     
            url: ajaxurl
          }).done(function( msg ) {
                console.log(msg);

                $(".add-to-talent-pool.add-new[data-profile-id='" + profileID + "']").html('<i class="fa fa-users"></i> In My Talent Pool').addClass('in-my-pool').removeClass('add-new');
                swal("Done!", "Profile was added to your talent pool!", "success");
             });

    });

	$('.report-abuse-trigger').click(function(){

	swal({   title: "Are you sure?",
	text: "Report will be sent to the administrator.",   
	type: "warning",   
	showCancelButton: true,   
	confirmButtonColor: "#DD6B55",   
	confirmButtonText: "Send",   
	cancelButtonText: "No, cancel!",   
	closeOnConfirm: false,   
	closeOnCancel: false }, 
		function(isConfirm){   if (isConfirm) {     
			
			swal("Done!", "Report was sent to the admin!", "success");
		$('.report-abuse').css('display', 'none');

		 $.ajax({
	    data: ({action : 'reportabuse', 'url': window.location.href, 'sender' :<?php $user_ID = get_current_user_id(); echo $user_ID; ?> }),
	    type: 'POST',
	    async: true,     
	    url: ajaxurl
	  }).done(function( msg ) {
	        console.log(msg);
	      });
		} else {     
			swal("Cancelled", "Sending abuse report was cancelled.", "error");   
		} });
	});

	$('.no-message-text').hide();
	$('.invalid-email').hide();

    $(document).on('click', '#send-message-button', function(){

    function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
	};

    	if( $("#modal-message").val().length > 1 && isValidEmailAddress($('#sender-mail').val())) {

	var messageText = $("#modal-message").val();
	var recipientID = <?php echo get_post_field( 'post_author', $profid ); ?>;
	var subject = $('#subject').find(":selected").data('job-id');
	var profileID = <?php echo $profid; ?>;
	var senderEmail = $('#sender-mail').val();

	console.log(messageText + ' ' + recipientID + ' ' + subject + ' ' +  profileID + ' ' +  senderEmail );

	$('.no-message-text').hide();
	$('.invalid-email').hide();

	$('#mail-modal').modal('hide');

		

		 $.ajax({
	        data: ({
	        	action : 'send_single_message',
	        	'recipient': recipientID,
	        	'message' : messageText,
	        	'profileid' : profileID,
	        	'subject' : subject,
	        	'sender-mail' : senderEmail,
	        	'sender-id' : <?php echo get_current_user_id(); ?>
	        }),
	        type: 'POST',
	        async: true,     
	        url: ajaxurl
	      }).done(function( msg ) {
		        console.log(msg);
		        swal("Sent!", "Your Message Was Sent!", "success");
	 		 });


} else {

	if($("#modal-message").val().length < 2) {

		$('.no-message-text').show();
	}

	if(!isValidEmailAddress($('#sender-mail').val())) {

		$('.invalid-email').show();

	}
}
});



});

</script>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
