<?php 
// Include WP files so our controller can actually work with WP
 // Runs forever and ever...
set_time_limit(-1);
 set_time_limit(0);
ini_set('memory_limit', '1024M');


$location = $_SERVER['DOCUMENT_ROOT'];
 
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');


function replace_fields() {

  global $wpdb;

      $querystr = "
   SELECT DISTINCT
        $wpdb->posts.post_title,
        $wpdb->posts.ID      
    FROM
        $wpdb->posts
        
    WHERE       
        $wpdb->posts.post_type = 'profile'
        AND $wpdb->posts.post_status = 'publish' 
        ";

        $pageposts = $wpdb->get_col($querystr, 1);
        $profileidarray = array();
        $profileidarray = $pageposts; // now we have profile ID array


      foreach ($profileidarray as $profileID) {


        # ideal job function   |  get_field("ideal_job_job_function", $profileID);
        # ideal location       |  get_field("ideal_job_location", $profileID);
        # ideal seniority      |  get_field("ideal_job_seniority", $profileID);
        # ideal job industry   |  get_field("ideal_job_industry", $profileID);
 

        $function = get_field("previous_experience_job_function2", $profileID);

        //$location = get_field("ideal_job_location", $profileID);

        $seniority = get_field("previous_experience_seniority2", $profileID);

        $industry = get_field("previous_experience_industry2", $profileID);


        // now check the variables and assign new values


//Industry

if (strpos($industry, "Any") !== false) { $industry = str_replace("Any","Any", $industry); }
if (strpos($industry, "Aerospace & Defense") !== false) { $industry = str_replace("Aerospace & Defense","Aerospace and Defense", $industry); }
if (strpos($industry, "Aerospace &#038; Defense") !== false) { $industry = str_replace("Aerospace &#038; Defense","Aerospace and Defense", $industry); }

if (strpos($industry, "Alternative Energy") !== false) { $industry = str_replace("Alternative Energy","Alternative Energy", $industry); }
if (strpos($industry, "Automobiles & Parts") !== false) { $industry = str_replace("Automobiles & Parts","Automotive", $industry); }
if (strpos($industry, "Automobiles &#038; Parts") !== false) { $industry = str_replace("Automobiles &#038; Parts","Automotive", $industry); }

if (strpos($industry, "Banks") !== false) { $industry = str_replace("Banks","Financial Services", $industry); }
if (strpos($industry, "Beverages") !== false) { $industry = str_replace("Beverages","Beverages", $industry); }
if (strpos($industry, "Charity & Non-profit") !== false) { $industry = str_replace("Charity & Non-profit","Charity", $industry); }
if (strpos($industry, "Charity &#038; Non-profit") !== false) { $industry = str_replace("Charity &#038; Non-profit","Charity", $industry); }
if (strpos($industry, "Charity &#038; Non-Profit") !== false) { $industry = str_replace("Charity &#038; Non-Profit","Charity", $industry); }

if (strpos($industry, "Chemicals") !== false) { $industry = str_replace("Chemicals","Chemicals", $industry); }
if (strpos($industry, "Construction & Materials") !== false) { $industry = str_replace("Construction & Materials","Construction", $industry); }
if (strpos($industry, "Construction &#038; Materials") !== false) { $industry = str_replace("Construction &#038; Materials","Construction", $industry); }

if (strpos($industry, "Education") !== false) { $industry = str_replace("Education","Schools or Universities", $industry); }
if (strpos($industry, "Electricity") !== false) { $industry = str_replace("Electricity","Electricity", $industry); }
if (strpos($industry, "Electronic & Electrical Equipment") !== false) { $industry = str_replace("Electronic & Electrical Equipment","Electronic", $industry); }
if (strpos($industry, "Electronic &#038; Electrical Equipment") !== false) { $industry = str_replace("Electronic &#038; Electrical Equipment","Electronic", $industry); }

if (strpos($industry, "Environmental Services") !== false) { $industry = str_replace("Environmental Services","Environment", $industry); }
if (strpos($industry, "Equity Investments Instruments") !== false) { $industry = str_replace("Equity Investments Instruments","Equities and Investments", $industry); }
if (strpos($industry, "Financial Services") !== false) { $industry = str_replace("Financial Services","Financial Services", $industry); }
if (strpos($industry, "Fixed Line Telecommunications") !== false) { $industry = str_replace("Fixed Line Telecommunications","Telecommunications", $industry); }
if (strpos($industry, "Food & Drug Retailers") !== false) { $industry = str_replace("Food & Drug Retailers","Retail", $industry); }
if (strpos($industry, "Food &#038; Drug Retailers") !== false) { $industry = str_replace("Food &#038; Drug Retailers","Retail", $industry); }

if (strpos($industry, "Food Producers") !== false) { $industry = str_replace("Food Producers","Food Production", $industry); }
if (strpos($industry, "Forestry & Paper") !== false) { $industry = str_replace("Forestry & Paper","", $industry); }
if (strpos($industry, "Forestry &#038; Paper") !== false) { $industry = str_replace("Forestry &#038; Paper","", $industry); }

if (strpos($industry, "Gas, Water & Multi Utilities") !== false) { $industry = str_replace("Gas, Water & Multi Utilities","Utilities", $industry); }
if (strpos($industry, "Gas, Water &#038; Multi Utilities") !== false) { $industry = str_replace("Gas, Water &#038; Multi Utilities","Utilities", $industry); }

if (strpos($industry, "General Industries") !== false) { $industry = str_replace("General Industries","", $industry); }
if (strpos($industry, "General Retailers") !== false) { $industry = str_replace("General Retailers","Retail", $industry); }
if (strpos($industry, "Government") !== false) { $industry = str_replace("Government","Government", $industry); }
if (strpos($industry, "Health Care Equipment & Services") !== false) { $industry = str_replace("Health Care Equipment & Services","Healthcare", $industry); }
if (strpos($industry, "Health Care Equipment &#038; Services") !== false) { $industry = str_replace("Health Care Equipment &#038; Services","Healthcare", $industry); }

if (strpos($industry, "Hospitality") !== false) { $industry = str_replace("Hospitality","Hospitality", $industry); }
if (strpos($industry, "Household Goods & Home Construction") !== false) { $industry = str_replace("Household Goods & Home Construction","Household", $industry); }
if (strpos($industry, "Household Goods &#038; Home Construction") !== false) { $industry = str_replace("Household Goods &#038; Home Construction","Household", $industry); }

if (strpos($industry, "Industrial Engineering") !== false) { $industry = str_replace("Industrial Engineering","Industrial Engineering", $industry); }
if (strpos($industry, "Industrial Metals & Mining") !== false) { $industry = str_replace("Industrial Metals & Mining","Metals and Mining", $industry); }
if (strpos($industry, "Industrial Metals &#038; Mining") !== false) { $industry = str_replace("Industrial Metals &#038; Mining","Metals and Mining", $industry); }

if (strpos($industry, "Industrial Transportation") !== false) { $industry = str_replace("Industrial Transportation","Transport", $industry); }
if (strpos($industry, "Leisure Goods") !== false) { $industry = str_replace("Leisure Goods","Leisure", $industry); }
if (strpos($industry, "Life Insurance") !== false) { $industry = str_replace("Life Insurance","Insurance", $industry); }
if (strpos($industry, "Media") !== false) { $industry = str_replace("Media","Media", $industry); }
if (strpos($industry, "Mining") !== false) { $industry = str_replace("Mining","Metals and Mining", $industry); }
if (strpos($industry, "Mobile Telecommunication") !== false) { $industry = str_replace("Mobile Telecommunication","Telecommunications", $industry); }
if (strpos($industry, "Nonequity Investment Instruments") !== false) { $industry = str_replace("Nonequity Investment Instruments","Equities and Investments", $industry); }
if (strpos($industry, "Non Life Insurance") !== false) { $industry = str_replace("Non Life Insurance","Insurance", $industry); }
if (strpos($industry, "Oil & Gas Producers") !== false) { $industry = str_replace("Oil & Gas Producers","Oil and Gas", $industry); }
if (strpos($industry, "Oil &#038; Gas Producers") !== false) { $industry = str_replace("Oil &#038; Gas Producers","Oil and Gas", $industry); }

if (strpos($industry, "Oil Equipment, Services, & Distribution") !== false) { $industry = str_replace("Oil Equipment, Services, & Distribution","Oil and Gas", $industry); }
if (strpos($industry, "Oil Equipment, Services, &#038; Distribution") !== false) { $industry = str_replace("Oil Equipment, Services, &#038; Distribution","Oil and Gas", $industry); }
if (strpos($industry, "Oil Equipment. Services &#038; Distribution") !== false) { $industry = str_replace("Oil Equipment. Services &#038; Distribution","Oil and Gas", $industry); }
if (strpos($industry, "Personal Goods") !== false) { $industry = str_replace("Personal Goods","Household", $industry); }
if (strpos($industry, "Pharmaceuticals & Biotechnology") !== false) { $industry = str_replace("Pharmaceuticals & Biotechnology","Pharmaceuticals, Biotechnology", $industry); }
if (strpos($industry, "Pharmaceuticals &#038; Biotechnology") !== false) { $industry = str_replace("Pharmaceuticals &#038; Biotechnology","Pharmaceuticals, Biotechnology", $industry); }

if (strpos($industry, "Professional Services") !== false) { $industry = str_replace("Professional Services","Consulting, Accounting", $industry); }
if (strpos($industry, "Retail") !== false) { $industry = str_replace("Retail","Retail", $industry); }
if (strpos($industry, "Real Estate Investment Trust") !== false) { $industry = str_replace("Real Estate Investment Trust","Equities and Investments", $industry); }
if (strpos($industry, "Real Estate Investment &#038; Services") !== false) { $industry = str_replace("Real Estate Investment &#038; Services","Equities and Investments", $industry); }

if (strpos($industry, "Software & Computer Services") !== false) { $industry = str_replace("Software & Computer Services","Technology", $industry); }
if (strpos($industry, "Software &#038; Computer Services") !== false) { $industry = str_replace("Software &#038; Computer Services","Technology", $industry); }

if (strpos($industry, "Sports") !== false) { $industry = str_replace("Sports","Sports", $industry); }
if (strpos($industry, "Support Services") !== false) { $industry = str_replace("Support Services","Support Services", $industry); }
if (strpos($industry, "Technology & Internet") !== false) { $industry = str_replace("Technology & Internet","Technology", $industry); }
if (strpos($industry, "Technology &#038; Internet") !== false) { $industry = str_replace("Technology &#038; Internet","Technology", $industry); }

if (strpos($industry, "Technology Hardware & Equipment") !== false) { $industry = str_replace("Technology Hardware & Equipment","Technology", $industry); }
if (strpos($industry, "Technology Hardware &#038; Equipment") !== false) { $industry = str_replace("Technology Hardware &#038; Equipment","Technology", $industry); }

if (strpos($industry, "Tobacco") !== false) { $industry = str_replace("Tobacco","Tobacco", $industry); }
if (strpos($industry, "Travel & Leisure") !== false) { $industry = str_replace("Travel & Leisure","Travel", $industry); }
if (strpos($industry, "Travel &#038; Leisure") !== false) { $industry = str_replace("Travel &#038; Leisure","Travel", $industry); }

if (strpos($industry, "Other") !== false) { $industry = str_replace("Other","Other", $industry); }


if (strpos($industry, "Telecommunicationss") !== false) { $industry = str_replace("Telecommunicationss","Telecommunications", $industry); }
if (strpos($industry, ", ,") !== false) { $industry = str_replace(", ,",", ", $industry); }

update_post_meta($profileID, "previous_experience_industry2", $industry);

//Seniority

if (strpos($seniority, "CEO / Managing Director") !== false) { $seniority = str_replace("CEO / Managing Director","Chief Executive", $seniority); }
if (strpos($seniority, "Senior Executive") !== false) { $seniority = str_replace("Senior Executive","Managing Director", $seniority); }
if (strpos($seniority, "Entry Level") !== false) { $seniority = str_replace("Entry Level","Analyst", $seniority); }
update_post_meta($profileID, "previous_experience_seniority2", $seniority);


// //Location
// if (strpos($location, "UK &#8211; London") !== false) { $location = str_replace("UK &#8211; London","London", $location); }
// if (strpos($location, "UK - London") !== false) { $location = str_replace("UK - London","London", $location); }
// update_post_meta($profileID, "ideal_job_location", $location);


// functions
if (strpos($function, "Content Management & Content Creation") !== false) { $function = str_replace("Content Management & Content Creation","Content Management, Content Creation", $function); }
if (strpos($function, "Content Management &#038; Content Creation") !== false) { $function = str_replace("Content Management &#038; Content Creation","Content Management, Content Creation", $function); }

if (strpos($function, "Strategy Consulting") !== false) { $function = str_replace("Strategy Consulting","Strategy Consultant", $function); }
if (strpos($function, "Operational Consulting") !== false) { $function = str_replace("Operational Consulting","Management Consultant", $function); }
if (strpos($function, "Programme and Change Consulting") !== false) { $function = str_replace("Programme and Change Consulting","Change Consultant", $function); }
if (strpos($function, "Human Resource Consulting") !== false) { $function = str_replace("Human Resource Consulting","HR Consultant", $function); }
if (strpos($function, "Technology Consulting") !== false) { $function = str_replace("Technology Consulting","Technology Consultant, Risk Consultant", $function); }
if (strpos($function, "Corporate Affairs") !== false) { $function = str_replace("Corporate Affairs","Corporate Affairs and PR", $function); }

if (strpos($function, " and PR and PR") !== false) { $function = str_replace(" and PR and PR"," and PR", $function); }
if (strpos($function, " and PR and PR") !== false) { $function = str_replace(" and PR and PR"," and PR", $function); }

if (strpos($function, "Customer Service") !== false) { $function = str_replace("Customer Service","Customer Service", $function); }
if (strpos($function, "Finance &#038; Accounting") !== false) { $function = str_replace("Finance &#038; Accounting","Finance, Accounting", $function); }
if (strpos($function, "Finance & Accounting") !== false) { $function = str_replace("Finance & Accounting","Finance, Accounting", $function); }

if (strpos($function, "General Management") !== false) { $function = str_replace("General Management","General Manager", $function); }
if (strpos($function, "Human Resources") !== false) { $function = str_replace("Human Resources","Human Resources", $function); }
if (strpos($function, "Technology &#8211; Infrastructure") !== false) { $function = str_replace("Technology &#8211; Infrastructure","Technology Infrastructure", $function); }
if (strpos($function, "Technology &#8211; Software") !== false) { $function = str_replace("Technology &#8211; Software","Software", $function); }
if (strpos($function, "Technology &#8211; User Experience") !== false) { $function = str_replace("Technology &#8211; User Experience","User Experience", $function); }
if (strpos($function, "Legal") !== false) { $function = str_replace("Legal","Law", $function); }
if (strpos($function, "Operations") !== false) { $function = str_replace("Operations","Operations", $function); }
if (strpos($function, "Product &#038; Commercial Management") !== false) { $function = str_replace("Product &#038; Commercial Management","Product Management, Commercial Management", $function); }
if (strpos($function, "Project &#038; Programme Management") !== false) { $function = str_replace("Project &#038; Programme Management","Project Management, Programme Management", $function); }
if (strpos($function, "Project &#038; Program Management") !== false) { $function = str_replace("Project &#038; Program Management","Project Management, Programme Management", $function); }
if (strpos($function, "Quality Assurance") !== false) { $function = str_replace("Quality Assurance","Quality Assurance", $function); }
if (strpos($function, "Research &#038; Development") !== false) { $function = str_replace("Research &#038; Development","Research and Development", $function); }
if (strpos($function, "Sales. Marketing &#038; Business Development") !== false) { $function = str_replace("Sales. Marketing &#038; Business Development","Sales, Marketing", $function); }
if (strpos($function, "Sales, Marketing &#038; Business Development") !== false) { $function = str_replace("Sales, Marketing &#038; Business Development","Sales, Marketing", $function); }
if (strpos($function, "Strategy &#038; Planning") !== false) { $function = str_replace("Strategy &#038; Planning","Strategy and Planning", $function); }
if (strpos($function, "Supply Chain &#038; Procurement") !== false) { $function = str_replace("Supply Chain &#038; Procurement","Supply Chain, Logistics, Procurement", $function); }
if (strpos($function, "Supply Chain, Logistics &#038; Procurement") !== false) { $function = str_replace("Supply Chain, Logistics &#038; Procurement","Supply Chain, Logistics, Procurement", $function); }
if (strpos($function, "Teaching &#038; Education") !== false) { $function = str_replace("Teaching &#038; Education","Teaching and Education", $function); }
if (strpos($function, "Journalism, ") !== false) { $function = str_replace("Journalism","", $function); }
if (strpos($function, "Journalism,") !== false) { $function = str_replace("Journalism,","", $function); }
if (strpos($function, "Journalism") !== false) { $function = str_replace("Journalism, ","", $function); }

if (strpos($function, "Regulatory &#038; Compliance") !== false) { $function = str_replace("Regulatory &#038; Compliance","Regulatory and Compliance", $function); }

update_post_meta($profileID, "previous_experience_job_function2", $function);

  
      
}
echo "Done";
}

replace_fields();

?>