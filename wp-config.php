<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'opengoglobal' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_MEMORY_LIMIT', '128M');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+Ye37`#a:cvUjziMM37}/#>C5Vkb)<<BnQ?J-S|b/gu~l-6`; 4{G:(I&(q^EX7{');
define('SECURE_AUTH_KEY',  ',|)+c~CfM0-lK4zqyV0jhu[wyXI2%9tPhl?!(`?|e|ph,QIVz+3)xrO*&-ZtA.D^');
define('LOGGED_IN_KEY',    'b|2|hA37-IJwIOhl-,5z2!SkZWWXoK+nN]-M_|bz|@TgU|C21a,V-XILV@F+GUN#');
define('NONCE_KEY',        'd|4+vAZ.4gcDX4-`JK9:>x>(pE#t9,aLNx</_ytZ9##4K![p36DblO%/k,dud73)');
define('AUTH_SALT',        '/PyW+YDK=JMv?:H^Ru)4|C4bn Z%vg[-+euk[!`{[`O/`{Hl^)b;5PUq>!exEqO[');
define('SECURE_AUTH_SALT', '~VL( +U>3LJ/JoaI1eK#o*J}Y_s-ynYnK(SRY$^}WEbO)1XjH7TeB#%5 CAB!yf|');
define('LOGGED_IN_SALT',   'Y(RwCUt7[GgS.=+xNZu Sw(^R|J@bCL#EF7WX|Hc&6]!F+@yjL|]*.*z%KQ:gYU1');
define('NONCE_SALT',       'YahqD`K_9OaSW+aF/3tU8~6D--P->*#yp$FwF(E F,`O8T RhBoV5nC=Vk-^qq6B');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'og_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


